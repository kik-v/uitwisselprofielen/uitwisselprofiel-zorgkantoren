---
title: 3.1.0 Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
description: "Aantal arbeidsovereenkomsten voor bepaalde tijd op de peildatum ten op zichte van het aantal arbeidsovereenkomsten voor bepaalde en onbepaalde tijd op de peildatum." 
weight: 3
---
## Indicator

**Definitie:** Aantal arbeidsovereenkomsten voor bepaalde tijd op de peildatum ten op zichte van het aantal arbeidsovereenkomsten voor bepaalde en onbepaalde tijd op de peildatum.

**Teller:** Aantal arbeidsovereenkomsten voor bepaalde tijd op de peildatum.

**Noemer:** Aantal arbeidsovereenkomsten voor bepaalde en onbepaalde tijd op de peildatum.

## Toelichting
Deze indicator betreft het aandeel arbeidsovereenkomsten voor bepaalde tijd, ten op zichte van alle arbeidsovereenkomsten voor bepaalde en onbepaalde tijd op de peildatum. 

NB: De indicator wordt een keer per kwartaal opgevraagd. In de uitvraag worden 3 peildatums gevraagd. In deze functionele beschrijving wordt de indicator voor een peildatum beschreven. In de uitvraag dient de query 3 keer uitgevoerd te worden.

## Uitgangspunten

* Alle arbeidsovereenkomsten voor bepaalde en onbepaalde tijd met zorg- en niet-zorggerelateerde functies op de peildatum worden geïncludeerd.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle arbeidsovereenkomsten op één van de peildata in het kwartaal.
2. Bepaal (o.b.v. de functie in de arbeidsovereenkomst) voor elke arbeidsovereenkomst uit stap 1 of de functie zorg of niet-zorg gerelateerd was.
3. Bepaal voor elke arbeidsovereenkomst uit stap 2 de vestiging.
4. Bereken o.b.v. stap 3 per vestiging het aantal arbeidsovereenkomsten voor bepaalde tijd.
5. Bereken o.b.v. stap 3 per vestiging het aantal arbeidsovereenkomsten voor zowel bepaalde als onbepaalde tijd.
6. Bereken de indicator door per vestiging en voor de totale organisatie de resultaten van stappen 3 en 4 te delen en te vermenigvuldigen met 100%.

**Peildatum: dd-mm-jj**
| Organisatieniveau  | Aantal arbeidsovereenkomsten bepaalde tijd | Aantal arbeidsovereenkomsten voor bepaalde en onbepaalde tijd | Percentage bepaalde tijd |
|----------------|--------|-----------|-----------|
| Organisatie | Stap 4 | Stap 5 |  Stap 6 | 
| Vestiging 1      | Stap 4 | Stap 5 |  Stap 6 | 
| Vestiging 2      | Stap 4 | Stap 5 |  Stap 6 | 
| Vestiging N      | Stap 4 | Stap 5 |  Stap 6 | 
