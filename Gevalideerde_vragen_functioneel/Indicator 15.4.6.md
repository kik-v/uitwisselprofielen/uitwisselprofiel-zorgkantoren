---
title: 15.4.6  Gedeclareerd bedrag per Wlz VV leveringsvorm
weight: 
---

## Toelichting

Deze indicator betreft de procentuele verhouding van het bedrag dat door de zorgaanbieder per leveringsvorm in de langdurige zorg in de sector VV voor zorg is gedeclareerd. De indicator wordt berekend op organisatieniveau voor een bepaalde periode.

## Uitgangspunten

* De declaratie wordt geïncludeerd wanneer de datum waarop of periode waarin de zorg geleverd is, in de verslagperiode ligt.
* De indeling van een declaratie in een bepaalde categorie vindt plaats op basis van de prestatiecode. Een beschrijving van de manier waarop dit plaatsvindt staat nader beschreven in de Algemene uitgangspunten in de paragraaf 'Het gebruik van declaraties'.


## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle declaraties waarvan de datum waarop of periode waarin de zorg geleverd is in de verslagperiode ligt.
2. Bepaal op basis van stap 1 per declaratie het gedeclareerde bedrag en de leveringsvorm waaronder de declaratie valt.
3. Bereken het gedeclareerde bedrag in Euro per leveringsvorm en voor alle leveringsvormen tezamen.
4. Bereken het percentage, door het bedrag per leveringsvorm te delen door het bedrag van de leveringsvormen tezamen en te vermenigvuldigen met 100%.


Periode: dd-mm-jjjj tot en met dd-mm-jjjj

| Organisatie-onderdeel |  % gedeclareerd bedrag Verblijf | % gedeclareerd bedrag VPT | % gedeclareerd bedrag MPT | % gedeclareerd bedrag PGB | 
|---|---|---|---|---|
| Totaal organisatie | Stap 4 | Stap 4 | Stap 4 | Stap 4 |

