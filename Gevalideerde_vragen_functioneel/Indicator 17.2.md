---
title: 17.2. Geplande toe- en afname van het aantal cliënten per (clustering van) leveringsvorm(en)
description: ""
weight: 17
---

## Definitie

**Definitie:** Geplande toe- en afname van het aantal cliënten per vestiging en per (clustering van) leveringsvorm(en) op een peildatum.

**Teller:** Aantal cliënten.

**Noemer:** Niet van toepassing.

## Toelichting
Door middel van deze uitvraag wordt de geplande toe- en afname met betrekking tot het aantal cliënten per vestiging en per (clustering van) leveringsvorm(en) op een peildatum gevraagd. Het betreft mutaties die vanaf 1-1-2021 gepland zijn. 

De zorgaanbieder vult handmatig de cijfers in.

De indicator wordt per vestiging ingevuld. De peildatum betreft 1 juli 2024. 

## Uitgangspunten
- De geplande capaciteit van een vestiging en van de organisatie wordt uitgedrukt in het aantal personen dat in een vestiging (en bij de organisatie als geheel) van een zorgaanbieder in zorg kan komen.
- Er is sprake van een vpt-cluster (in een instelling) wanneer er ten minste drie personen met een Wlz-indicatie met zorgprofiel VV met leveringsvorm vpt op één vestiging kunnen wonen.

### Realisatiefases intramuraal en VPT geclusterd   
1. Conceptfase: Het strategisch vastgoedplan is vastgesteld door de Raad van Toezicht. Er is een uitgewerkte marktanalyse gedaan en het plan is verkennend besproken met externe stakeholders. Denk hierbij aan: gemeenten, financiers, corporaties, beleggers.
2. Initiatiefase: In deze fase wordt de projectorganisatie opgezet met de stakeholders. De contractvormen worden bepaald (aankoopgrond, huurovereenkomst), de bouwlocatie wordt gezocht en er vindt een toets ruimtelijke ordening plaats. De kosten die mogelijk een rol kunnen gaan spelen worden opgenomen in de stichtingskostenopzet en/of in budgetposten. De overall planning met mijlpalen is opgeleverd.
3. Definitiefase: Het functioneel en het technisch programma van eisen wordt in deze fase opgesteld en het structuurontwerp van het gebouw wordt gemaakt (voorloper van de ontwerpen in de ontwerpfase). Dit is ook de fase waarin de investerings-, vastgoedexploitatie- en de totale exploitatie kosten worden berekend. Voordat fase 4 start is de (haalbare) businesscase goedgekeurd door de Raad van Toezicht. 
4. Ontwerpfase: In deze fase wordt het voorlopig ontwerp, het definitief ontwerp en het technisch ontwerp opgesteld en opgeleverd. De gesprekken over ruimtelijke ordening vinden plaats met de gemeente. De bouwkosten worden geindiceerd en de definitieve prijs wordt opgenomen in de directiebegroting. In deze fase vindt ook de aanbesteding en contractvorming plaats. 
5. Voorbereiding- en realisatiefase: De bouw gaat van start, is gaande of is voltooid. In deze fase vindt ook het gereed maken van de inrichting en de faciliteiten binnen het pand plaats. 
6. Gerealiseerd: "De woningen zijn opgeleverd en het pand kan in gebruik worden genomen."
7. Geannuleerd: Het bouwplan is geannuleerd, de betreffende woningen zullen niet meer worden opgeleverd en de beoogde extra verpleegzorgplekken komen te vervallen.

### Realisatiefases MPT en VPT niet-geclusterd  
1. Voorbereiding: Het plan om uitbreiding/krimp van verpleegzorgplekken te realiseren is door het bestuur geaccordeerd. In deze fase worden de voorbereidingen getroffen voor de uitbreiding/krimp
2. Gerealiseerd: De uitbreiding of krimp van het aantal verpleegzorgplekken is gerealiseerd
3. Geannuleerd: Het plan voor uitbreiding/krimp van verpleegzorgplekken is geannuleerd. 

## Berekening

Een berekening is niet van toepassing.

Intramuraal en VPT geclusterd

| Naam vestiging | Gemeente | Toename / afname | (Cluster van) leveringsvorm(en) | Gepland aantal cliënten | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- | --- |
| ... | ... | ... | ... | ... | ... | ... |

MPT en VPT niet-geclusterd

| Naam vestiging | Gemeente | Toename / afname | (Cluster van) leveringsvorm(en) | Gepland aantal cliënten | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- | --- |
| ... | ... | ... | ... | ... | ... | ... |

## Voorbeeld

Intramuraal en VPT geclusterd

Peildatum: 15-7-2024

| Naam vestiging | Gemeente | Toename / afname | (Cluster van) leveringsvorm(en) | Gepland aantal cliënten | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- | --- |
| De Eik | Stad | Afname | VPT-geclusterd | 10 | Gerealiseerd | 1-6-2026 |
| De Beuk | Dorp | Toename | Verblijf | 25 | Voorbereiding | 1-1-2025 |

MPT en VPT niet-geclusterd

| Naam vestiging | Gemeente | Toename / afname | (Cluster van) leveringsvorm(en) | Gepland aantal cliënten | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- | --- |
| De Eik | Stad | Toename | VPT niet-geclusterd | 15 | Gerealiseerd | 1-6-2026 |
| De Beuk | Dorp | Afname | MPT | 28 | Voorbereiding | 1-1-2025 |

