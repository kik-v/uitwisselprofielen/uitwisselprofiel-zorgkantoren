---
title: 20.5 - Solvabiliteit - Reserves-Ratio
weight: n.t.b.
---

## Indicator

**Definitie:** De Reserves-Ratio wordt gedefinieerd als (Reserves) / (totale opbrengsten). De Reserves zijn gedefinieerd in Indicator 20.4.

**Teller:** Reserves o.b.v. bestemmingsfondsen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Opbrengsten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de Reserves-Ratio van de organisatie.

## Uitgangspunten

* Peildatum Reserves-Ratio o.b.v. bestemmingsfondsen is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.
* Meetperiode Opbrengsten is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
* Bij Peildatum 31-dec van het voorgaande jaar hoort de meetperiode 1-jan t/m 31-dec van het voorgaande jaar. Idem voor de andere combinatie in het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Reserves-Ratio = Reserves [zie Indicator 20.4]  / *"P Som der bedrijfsopbrengsten"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental    | Waarde |
|-----------|--------|
| Reserves-Ratio | Stap 1 |
