---
title: 19.6 - Liquiditeit, EBITDA (Earnings Before Interest, Tax, Deprecation and Amortisation)
weight: n.t.b.
---

## Indicator

**Definitie:** De EBITDA wordt gedefinieerd als opbrengsten - kosten zonder renten, belastingen, en afschrijvingen.

**Teller:** EBITDA over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

**Noemer:** n.v.t.

## Toelichting

Deze indicator berekent de EBITDA van de organisatie. De Earnings Before Interest, Tax, Deprication and Amortisation (EBITDA) geeft de winstgevendheid van operaties weer, zonder evt. vertekening door afschrijvingen of rentekosten. Dit bedrag is wat een bedrijf overhoud om investeringen te doen uit de lopende cashflow en een buffer op te bouwen via rendement. Dit zou minimaal genoeg moeten zijn om deze kosten te dekken.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. EBITDA = *"P Som der bedrijfsopbrengsten"* - *"Q Som der bedrijfslasten"* + *"Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental | Waarde |
|--------|--------|
| EBITDA | Stap 1 |
