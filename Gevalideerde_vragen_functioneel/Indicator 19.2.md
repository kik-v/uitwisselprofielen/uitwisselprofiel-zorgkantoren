---
title: 19.2 - Liquiditeit, Werkkapitaal op korte termijn
weight: n.t.b.
---

## Indicator

**Definitie:** Het werkkapitaal op korte termijn wordt gedefinieerd als vlottende activa - kortlopende schulden.

**Teller:** Werkkapitaal op korte termijn op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** n.v.t.

## Toelichting

Deze indicator berekent werkkapitaal op korte termijn van de organisatie.

Formule om het werkkapitaal te berekenen op korte termijn: Werkkapitaal = vlottende activa - kortlopende schulden. Vlottende activa zijn voorraden, debiteuren, kortlopende vorderingen en andere bezittingen die op korte termijn verkocht kunnen worden voor geld. Liquide middelen zijn het aanwezige geld op bankrekeningen of in kas. Kortlopende schulden zijn crediteuren, kortlopende schulden, belastingschulden, pensioenschulden en andere schulden die op korte termijn afgelost moeten worden.

## Uitgangspunten

* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Werkkapitaal op korte termijn = *"B Vlottende activa"* - *"G Kortlopende schulden (ten hoogste 1 jaar)"*.

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental                        | Waarde |
|-------------------------------|--------|
| Werkkapitaal op korte termijn | Stap 1 |
