---
title: 19.8 - Liquiditeit, Cashflow
weight: n.t.b.
---
## !!! Nog niet publiceren, de berekening van het kasstroomoverzicht is nog een open issue !!!

## Indicator

**Definitie:** Cashflow wordt dedefinieerd als (resultaat na belastingen + afschrijvingen) / (rentelasten + aflossingen op lang vreemd vermogen)

**Teller:** Resultaat na belastingen + afschrijvingen, beiden over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

**Noemer:** Rentelasten + aflossingen op lang vreemd vermogen, beiden over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de cashflow van de organisatie. De cashflow geeft de mogelijkheid van een bedrijf aan om op korte termijn betalingen te voldoen. Het verschil in cashflows tussen twee perioden geeft de verbetering of verslechtering van deze situatie aan.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. "Uitgaven ter aflossing van leningen" = (*F Langlopende schulden (nog voor meer dan een jaar)* op peildatum eerste dag voor het begin van de meetperiode) - (*F Langlopende schulden (nog voor meer dan een jaar)* op peildatum laatste dag van de meetperiode)
2. Cashflow = *"S Resultaat na belastingen"* + *"Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"* / *"R.IV Rentelasten en soortgelijke kosten"* + "Uitgaven ter aflossing van leningen"

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

De post "Uitgaven ter aflossing van leningen" wordt in jaarrekeningen van zorgorganisaties vaak benoemd als "aflossing langlopende schulden".

| Kental   | Waarde |
|----------|--------|
| Cashflow | Stap 2 |
