---
title: 21.2 - Rentabiliteit, REV (Resultaat Eigen Vermogen)
weight: n.t.b.
---

## Indicator

**Definitie:** Het Resultaat Eigen Vermogen (REV) wordt gedefinieerd als (resultaat na belastingen) / (Eigen vermogen)

**Teller:** Resultaat na belastingen over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

**Noemer:** Eigen vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent het Resultaat Eigen Vermogen (REV) van de organisatie. Deze ratio laat de verhouding zien tussen het bedrijfsresultaat (verschil tussen opbrengsten en kosten) en het vermogen dat mede ontstaan is door dit resultaat.

## Uitgangspunten

* De peildatum eigen vermogen is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.
* De meetperiode resultaat na belastingen is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
* Bij Peildatum 31-dec van het voorgaande jaar hoort de meetperiode 1-jan t/m 31-dec van het voorgaande jaar. Idem voor de andere combinatie in het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. REV = *"S Resultaat na belastingen"* / *"D Eigen vermogen"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental | Waarde |
|--------|--------|
| REV    | Stap 1 |
