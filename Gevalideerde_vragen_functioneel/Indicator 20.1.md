---
title: 20.1 - Solvabiliteit, Solvabiliteitsratio
weight: n.t.b.
---

## Indicator

**Definitie:** Solvabiliteitsratio wordt gedefinieerd als (Eigen vermogen) / (Totaal vermogen)

**Teller:** Eigen vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Totaal vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de Solvabiliteitsratio van de organisatie. De solvabiliteit geeft aan in hoeverre een organisatie kan voldoen aan de langlopende betalingsverplichtingen, oftewel de mate waarin de organisatie over een buffer beschikt om eventuele verliezen op te vangen.

## Uitgangspunten

* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Solvabiliteitsratio = *"D Eigen vermogen"* / (*"H Totaal passiva"*)
2. Solvabiliteitsratio% = Solvabiliteit * 100

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental               | Waarde |
|----------------------|--------|
| Solvabiliteitsratio  | Stap 1 |
| Solvabiliteitsratio% | Stap 2 |
