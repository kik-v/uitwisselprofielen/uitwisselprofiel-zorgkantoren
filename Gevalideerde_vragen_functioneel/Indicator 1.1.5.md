---
title: 1.1.5 Gemiddeld aantal personeelsleden Q1 2024
description: "Het gemiddeld aantal personeelsleden per kwartaal: per vestiging per zorg en niet-zorg gerelateerde functie per financieringsstroom naar rato van de duur van de overeenkomst."  
weight: 1
---
## Indicator

**Definitie:** Het gemiddeld aantal personeelsleden per kwartaal: per vestiging per zorg en niet-zorg gerelateerde functie per financieringsstroom naar rato van de duur van de overeenkomst.

**Teller:** Het gemiddeld aantal personeelsleden over de periode.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal personeelsleden in een kwartaal. Een persoon telt naar rato mee voor de periode dat hij/zij personeelslid is in het kwartaal.

Bijvoorbeeld, tijdens een kwartaal telt een persoon die gedurende de helft van het kwartaal over een werkovereenkomst beschikte (en dus personeelslid was), als 0,5 mee in de berekening van de indicator. 

Deze indicator wordt berekend op organisatieniveau, per vestiging en per zorg/niet-zorg gerelateerde functie. Ook worden per categorie totalen berekend.


## Uitgangspunten

* Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd.
* Per vestiging en voor de gehele organisatie.


**Voorbeeld**

Onderstaande tabel beschrijft een voorbeeld van de manier waarop een medewerker meetelt in elk van de categorieen. Deze medewerker beschikte de helft van een kwartaal over een werkovereenkomst. De eerste helft (een kwart van het kwartaal) was de medewerker specialist ouderen geneeskunde (een functie als zorgverlener), de tweede helft manager (een functie als niet-zorgverlener).  

| Periode:       |  Zorg  | Niet-zorg | Totaal |
|----------------|--------|-----------|--------|
| Totaal organisatie |  0,25  | 0,25      | 0,5    |
| Vestiging 1      |  0,25  | 0,25      | 0,5    |
| Vestiging 2      |  -     | -         | -      |
| Vestiging N      |  -     | -         | -      |


## Berekening

Deze indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer alle personen die in het kwartaal personeelslid waren.  
(De begindatum van de werkovereenkomst ligt voor of op de einddatum van het kwartaal en de einddatum ligt niet voor de start van het kwartaal).
2. Bepaal o.b.v. de functie in de werkovereenkomst voor elke persoon uit stap 1 of deze persoon zorg of niet-zorg gerelateerd was.
3. Bepaal o.b.v. de werkovereenkomst voor elke persoon uit stap 2 de vestiging.
4. Bereken per persoon het aantal dagen dat de werkovereenkomst(en) duurde(n) in de meetperiode en deel deze door het totaal aantal dagen dat de meetperiode duurde.
5. Tel de uitkomsten van de berekeningen uit stap 4 bij elkaar op en groepeer per vestiging, per vestiging voor zorg- en niet-zorg en voor de totale organisatie.


| Periode:       |  Zorg  | Niet-zorg | Totaal |
|----------------|--------|-----------|--------|
| Totaal organisatie | Stap 5 | Stap 5    | Stap 5 |
| Vestiging 1      | Stap 5 | Stap 5    | Stap 5 |
| Vestiging 2      | Stap 5 | Stap 5    | Stap 5 |
| Vestiging N      | Stap 5 | Stap 5    | Stap 5 |

