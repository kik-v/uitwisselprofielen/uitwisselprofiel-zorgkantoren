---
title: 18.1 - Balans o.b.v. Grootboek (Prismant)
weight: n.t.b.
---

## Indicator

**Definitie:** Balans berekend op basis van het grootboek o.b.v. Prismant

**Teller:** Balans op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** n.v.t.

## Toelichting

Deze indicator berekent de posten in het jaarrekening model C "Balans voor zorgaanbieders" zoals beschreven in Bijlage 1 van de [Regeling openbare jaarverantwoording WMG](https://wetten.overheid.nl/BWBR0045649/2022-01-01#Bijlage1) op basis van het Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25. Onderstaande tabel beschrijft deze mapping tussen grootboekrubrieken en jaarrekening.

De posten in de kolom "Jaarrekeningpost" zijn op twee manieren in verband gebracht met de rubrieken uit het grootboek:

1. Onder de jaarrekeningpost staan één of meer regels genoemd met grootboekrubrieken die opgeteld te jaarrekening post vormen.
2. Achter de jaarrekeningpost staat de berekening op basis van andere jaarrekeningposten.

Hiermee is de structuur uit bijlage 1 van de Regeling openbare jaarverantwoording WMG gehandhaafd. Vooralsnog is het hoogste detailniveau niet meegenomen. (Feitelijk wordt vooralsnog hier het  model A voor kleinere zorgorganisaties gebruikt.)

Waar bij de grootboekrubrieken een "x" genoteerd is, wordt bedoeld dat alle rubrieken met op die positie een willekeurig cijfer meegenomen worden bij de optelling.

| Jaarrekeningpost                                    | Rubriek        | Omschrijving                                                   |
| --------------------------------------------------- | -------------- | -------------------------------------------------------------- |
| A Vaste activa                                      |                | = A.I + A.II + A.III                                           |
| A.I Immateriële vaste activa                        |                |                                                                |
|                                                     | 00xxxx         | Immateriële vaste activa                                       |
| A.II Materiële vaste activa                         |                |                                                                |
|                                                     | 01xxxx         | Materiële vaste activa                                         |
| A.III Financiële vaste activa                       |                |                                                                |
|                                                     | 03xxxx         | Financiële vaste activa                                         |
| B Vlottende activa                                  |                | = B.I + B.II + B.III + B.IV                                     |
| B.I Voorraden                                       |                |                                                                |
|                                                     | 3xxxxx         | Voorraden                                                      |
| B.II Vorderingen                                    |                |                                                                |
|                                                     | 12xxxx         | Vorderingen                                                    |
| B.III Effecten                                      |                |                                                                |
|                                                     | 131xxx         | Effecten                                                       |
| B.IV Liquide middelen                               |                |                                                                |
|                                                     | 132xxx         | Liquide middelen                                               |
| C Totaal activa                                     |                | = A + B                                                       |
| D Eigen vermogen                                    |                | + D.I + D.II + D.III + D.IV + D.V + D.VI + D.VII + D.VIII             |
| D.I Gestort en opgevraagd kapitaal                  |                |                                                                |
|                                                     | 0511xx         | Stichtingskapitaal c.q. Ledenkapitaal                          |
| D.II Agio                                           |                |                                                                |
|                                                     | - | Bestaat niet in Prismant, waarde altijd 0                                                               |
| D.III Herwaarderingsreserve                         |                |                                                                |
|                                                     | - | Bestaat niet in Prismant, waarde altijd 0                                                               |
| D.IV Wettelijke en statutaire reserve               |                |                                                                |
|                                                     | 05413x         | Algemene reserves                                              |
| D.V Bestemmingsreserve                              |                |                                                                |
|                                                     | 05311x         | Reserve aanvaardbare kosten                                    |
|                                                     | 05342x         | Bestemmingsreserve vervanging inventaris                       |
|                                                     | 05423x         | Bestemmingsreserves                                            |
| D.VI Bestemmingsfonds                               |                |                                                                |
|                                                     | 05323x         | Bestemmingsreserve huisvesting (voorheen Vernieuwingsfonds)    |
| D.VII Overige reserves                              |                |                                                                |
|                                                     | 0539xx         | Overige reserves                                               |
|                                                     | 05493x         | Overige reserves, waaronder fondsen en fundaties               |
|                                                     | 05915x         | Egalisatierekeningen afschrijving instandhoudingsinvesteringen |
| D.VIII Onverdeelde winst                            |                |                                                                |
|                                                     | 091xxx         | Saldo resultatenrekening                                       |
| E Voorzieningen                                     |                |                                                                |
|                                                     | 06xxxx         | Voorzieningen                                                  |
| F Langlopende schulden (nog voor meer dan een jaar) |                |                                                                |
|                                                     | 07xxxx         | Langlopende schulden                                           |
| G Kortlopende schulden (ten hoogste 1 jaar)         |                |                                                                |
|                                                     | 14xxx          | Kortlopende schulden en overlopende passiva                    |
|                                                     | 15xxx          | Kortlopende schulden en overlopende passiva                    |
| H Totaal passiva                                    |                | = D + E + F + G                                                 |

## Uitgangspunten

* Zie algemene uitgangspunten voor de werkwijze met de balans
* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Op te nemen in de algemene uitgangspunten

Begrippen, uitgangspunten en werkwijze i.z. grootboekgegevens
1. Een balans heeft een datum en bevat voor een verzameling balansrubrieken voor iedere rubriek een saldo. Als een balans voor een rubriek geen saldo bevat, dan wordt aangenomen dat het saldo van die rubriek nul is.
1. De startbalans, ook wel openingsbalans genoemd, is de balans bij aanvang van een administratie, nog voordat enige boeking in het grootboek heeft plaatsgevonden.
3. Een beginbalans op een peildatum bevat de startbalans vermeerderd met het totaal van alle (balans)boekingen **tot** de peildatum, dus **tot en met** de dag voor de peildatum.
4. De balans (of eindbalans) op een peildatum bevat de startbalans vermeerderd met alle boekingen **tot en met** de peildatum.
5. Alle indicatoren gaan uit van een maximale resolutie voor zowel de balans als de winst- en verliesrekening van 1 maand. M.a.w. een (eind)balans betreft de laatste dag van een maand, en een periode in de winst- en verliesrekening betreft een geheel aantal maanden. Als gevolg kunnen in het ETL proces van grootboek naar datastation per rubriek alle boekingen per maand geconsolideerd worden. Een dergelijke geconsolideerde boeking dient dan als datum de laatste dag van de betreffende maand te krijgen. Uiteraard mogen de oorspronkelijke boekingen in de betreffende maand dan niet opgenomen worden in het datastation. Dat zou tot dubbeltellingen leiden.
6. In het ETL proces van grootboek naar datastation kunnen daarnaast beginbalansen geïmporteerd worden om de efficientie van berekeningen verder te vergroten. Een dergelijke beginbalans dient steeds als datum de eerste dag van de betreffende maand te hebben, en bevat dus de cumulatieve saldi tot en met de laatste dag van de voorgaande maand. Indicatoren bepalen de balans op een peildatum (altijd de laatste dag van de maand) door het meest recente beginbalans saldo (op de eerste dag van de volgende maand of eerder) te vermeerderen met alle boekingen vanaf die meest recente begindatum tot en met de balansdatum. 
7. Om alle indicatoren te kunnen berekenen dient de vroegste beginbalansdatum niet later dan 1 januari van twee jaar geleden te zijn. In 2025 is dat dus 1 januari 2023. In combinatie met het punt 5 en punt 6 hiervoor is het mogelijk om het datastation te voeden met alleen de beginbalansen op de eerste dag van iedere maand vanaf 1 januari van twee jaar geleden, zonder individuele (geconsolideerde) boekingen.

## Berekening

Deze indicator wordt als volgt berekend:

1. Bepaal per rubriek het beginbalanssaldo op de meest recente datum op of voor de dag na de peildatum. Voor een rubriek die geen beginbalans saldo heeft wordt als saldo 0 aangenomen.
2. Bereken de jaarrekeningposten waarbij grootboekrubrieken genoemd zijn door voor de betreffende rubrieken het het meest recente beginbalanssaldo uit stap 1. te vermeerderen met het totaal van de bedragen uit alle boekingen (op die rubriek) vanaf de datum van de beginbalans tot en met de peildatum. Jaarrekeningposten waar geen rubriek is genoemd krijgen de waarde 0.
3. Bereken de jaarrekeningposten waarbij een berekening is beschreven door die berekening uit te voeren op basis van de in punt 2. berekende posten.

| Jaarrekeningpost                             | Bedrag  |
|----------------------------------------------|---------|
| A Vaste activa                               | Stap 2  |
| A.I Immateriële vaste activa                 | Stap 1  |
| A.II Materiële vaste activa                  | Stap 1  |
| A.III Financiële vaste activa                | Stap 1  |
| B Vlottende activa                           | Stap 2  |
| etc. volgens structuur in bovenstaande tabel | |
