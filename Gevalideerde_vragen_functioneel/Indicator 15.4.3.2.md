---
title: 15.4.3.2 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm - peildatum 1-4-2023
description: ""
weight: 15
---
## Indicator

**Definitie:** Het aantal cliënten met een Wlz-indicatie met zorgprofiel VV per vestiging en op organisatieniveau op een peildatum.

**Teller:** Aantal cliënten met een Wlz-indicatie met zorgprofiel VV.

**Noemer:** Niet van toepassing.


## Toelichting

Deze indicator geeft het aantal cliënten bij een zorgaanbieder op een peildatum per leveringsvorm aan. Het betreft cliënten met een Wlz-indicatie met zorgprofiel VV. Deze indicator wordt op organisatieniveau en per vestiging berekend.


## Uitgangspunten

Geen.


## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op een peildatum beschikken over een Wlz-indicatie met zorgprofiel VV. 
2. Bepaal per cliënt uit stap 1 de vestiging(en) en de langdurige zorgsector(en) waaronder de Wlz-indicatie(s) valt of vallen. 
3. Bereken op basis van stap 2 per vestiging en voor de totale organisatie het aantal cliënten op de peildatum.

Peildatum: dd-mm-jjjj 
| Organisatieonderdeel | Verblijf | VPT | MPT |  PGB  | 
|---|---|---|---|---|
| Totaal organisatie | Stap 3 | Stap 3  | Stap 3 |Stap 3| 
| Vestiging 1        | Stap 3 | Niet van toepassing  | Stap 3 | Stap 3 | 
| Vestiging 2        | Stap 3 | Niet van toepassing  | Stap 3 | Stap 3 | 
| Vestiging N        | Stap 3 | Niet van toepassing  | Stap 3 | Stap 3 | 

