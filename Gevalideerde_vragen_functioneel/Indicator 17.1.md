---
title: 17.1. Geplande toe- en afname van het aantal wooneenheden
description: ""
weight: 17
---

## Definitie

**Definitie:** Geplande toe- en afname van het aantal wooneenheden per vestiging op een peildatum.

**Teller:** Aantal wooneenheden.

**Noemer:** Niet van toepassing.

## Toelichting
Door middel van deze uitvraag wordt de geplande toe- en afname van het aantal wooneenheden per vestiging op een peildatum gevraagd. Het betreft geplande mutaties vanaf 1-1-2021. 

De zorgaanbieder vult handmatig de cijfers in.

De indicator wordt per vestiging ingevuld. De peildatum betreft 1 juli 2024.

## Uitgangspunten

- De capaciteit van een vestiging en van de organisatie wordt uitgedrukt in het aantal wooneenheden.
- Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een locatie met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die locatie wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’–eenheid’.
- Het betreft wooneenheden die geschikt zijn voor bewoning door een of meerdere cliënten en/of een partner (of personen partners zijn, is onbekend).

### Realisatiefases intramuraal en VPT geclusterd   
1. Conceptfase: Het strategisch vastgoedplan is vastgesteld door de Raad van Toezicht. Er is een uitgewerkte marktanalyse gedaan en het plan is verkennend besproken met externe stakeholders. Denk hierbij aan: gemeenten, financiers, corporaties, beleggers.
2. Initiatiefase: In deze fase wordt de projectorganisatie opgezet met de stakeholders. De contractvormen worden bepaald (aankoopgrond, huurovereenkomst), de bouwlocatie wordt gezocht en er vindt een toets ruimtelijke ordening plaats. De kosten die mogelijk een rol kunnen gaan spelen worden opgenomen in de stichtingskostenopzet en/of in budgetposten. De overall planning met mijlpalen is opgeleverd.
3. Definitiefase: Het functioneel en het technisch programma van eisen wordt in deze fase opgesteld en het structuurontwerp van het gebouw wordt gemaakt (voorloper van de ontwerpen in de ontwerpfase). Dit is ook de fase waarin de investerings-, vastgoedexploitatie- en de totale exploitatie kosten worden berekend. Voordat fase 4 start is de (haalbare) businesscase goedgekeurd door de Raad van Toezicht. 
4. Ontwerpfase: In deze fase wordt het voorlopig ontwerp, het definitief ontwerp en het technisch ontwerp opgesteld en opgeleverd. De gesprekken over ruimtelijke ordening vinden plaats met de gemeente. De bouwkosten worden geindiceerd en de definitieve prijs wordt opgenomen in de directiebegroting. In deze fase vindt ook de aanbesteding en contractvorming plaats. 
5. Voorbereiding- en realisatiefase: De bouw gaat van start, is gaande of is voltooid. In deze fase vindt ook het gereed maken van de inrichting en de faciliteiten binnen het pand plaats. 
6. Gerealiseerd: "De woningen zijn opgeleverd en het pand kan in gebruik worden genomen."
7. Geannuleerd: Het bouwplan is geannuleerd, de betreffende woningen zullen niet meer worden opgeleverd en de beoogde extra verpleegzorgplekken komen te vervallen.

### Realisatiefases MPT en VPT niet-geclusterd  
1. Voorbereiding: Het plan om uitbreiding/krimp van verpleegzorgplekken te realiseren is door het bestuur geaccordeerd. In deze fase worden de voorbereidingen getroffen voor de uitbreiding/krimp
2. Gerealiseerd: De uitbreiding of krimp van het aantal verpleegzorgplekken is gerealiseerd
3. Geannuleerd: Het plan voor uitbreiding/krimp van verpleegzorgplekken is geannuleerd. 


## Berekening

Een berekening is niet van toepassing.

Intramuraal en VPT-geclusterd:

| Naam vestiging | Gemeente | Geplande toename van het aantal wooneenheden | Geplande afname van het aantal wooneenheden | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- |
| ... | ... | ... | ... | ... | ... |

MPT en VPT niet-geclusterd:

| Naam vestiging | Gemeente | Geplande toename van het aantal wooneenheden | Geplande afname van het aantal wooneenheden | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- |
| ... | ... | ... | ... | ... | ... |

## Voorbeeld

| Organisatieonderdeel | Gemeente | Geplande toename van het aantal wooneenheden | Geplande afname van het aantal wooneenheden | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- |
| Organisatie | - | 20 | 10 | - | - |
| De Beuk | Dorp | 20 | 0 | Voorbereiding | 1-1-2025 |
| De Eik | Stad | 0 | 10 | Gerealiseerd | 1-6-2026 |

