---
title: 2.1 Aantal ingezette uren personeel
description: "Het aantal ingezette uren van zorg gerelateerde personeelsleden per kwartaal per vestiging." 
weight: 2
---
## Indicator

**Definitie:** Het aantal ingezette uren van zorg gerelateerde personeelsleden per kwartaal per vestiging.

**Teller:** Het aantal ingezette uren van zorg gerelateerde personeelsleden per kwartaal.

**Noemer:** Niet van toepassing.

## Toelichting
Deze indicator betreft per vestiging per kwartaal het aantal geregistreerde uren van zorg gerelateerde personeelsleden die binnen de meetperiode over een arbeidsovereenkomst beschikten. Op basis van deze uren kan het aantal ingezette fte’s van deze personen berekend worden. Iedere afnemer van gegevens kan op basis van het totaal aantal uren zijn/haar eigen definitie van fte toepassen, zoals 36, 38 of 40 uur per week of 1692 uren per jaar. Voor de berekening van uren worden meeruren, overuren en oproepuren meegerekend.

Deze indicator wordt op organisatieniveau en per vestiging berekend, voor zorg gerelateerd personeel. Ook worden totalen berekend. De verslagperiode betreft een kwartaal.


## Uitgangspunten

* Per vestiging en voor de gehele organisatie.
* Personen met een of meerdere opeenvolgende arbeidsovereenkomsten gedurende (een deel van de) meetperiode worden geïncludeerd indien het een zorggerelateerde functie betreft (o.b.v. de functie in de arbeidsovereenkomst).
* Om deze indicator te kunnen berekenen dienen de uren beschikbaar te zijn vanuit de urenregistratie van de zorgaanbieder.
* De geregistreerde ingezette uren, zijn uren exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.
* Toebedeling van ingezette uren naar een vestiging is op basis van de vestiging in de urenregistratie. Indien deze niet beschikbaar zijn, worden de uren toebedeeld aan de vestiging gerelateerd aan de arbeidsovereenkomst. 

Onderstaande tabel beschrijft een voorbeeld van de manier waarop de ingezette uren van medewerker A worden meegeteld in elk van de indelingen. Deze medewerker heeft in de verslagperiode (1 januari tot 1 april) ingezette uren op 2 vestigingen (69 uren op vestiging 1 en 69 uren op vestiging 2).

| Periode: | ingezette uren|
| --- | --- |
| Totaal organisatie | 138 |
| vestiging 1 | 69  |
| vestiging 2 | 69  |
| vestiging N | - |

## Berekening

Deze indicator wordt als volgt berekend:
1.	Selecteer alle personeelsleden die zorgverlener zijn.
2.	Tel per persoon het aantal uren (gewerkte tijd in uren) die de persoon heeft geregistreerd in de verslagperiode per vestiging.
3.	Bereken het aantal uren per vestiging en in totaal door alle geselecteerde uren van alle geselecteerde personen per vestiging bij elkaar op te tellen.

Periode: dd-mm-jjjj
| Organisatieonderdeel | Ingezette uren|
| --- | --- |
| Totaal organisatie | stap 3 |
| Vestiging 1 | stap 3 |
| Vestiging 2 | stap 3 |
| Vestiging N | stap 3 |

