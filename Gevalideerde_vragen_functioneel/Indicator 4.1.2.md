---
title: 4.1.2 Gemiddelde contractomvang personeel Q2 2023
description: "Aantal contractuele uren aan personeelsleden met een arbeidsovereenkomst gedeeld door het aantal personeelsleden met een arbeidsovereenkomst op een peildatum." 
weight: 4
---
## Indicator

**Definitie:** Aantal contractuele uren aan personeelsleden met een arbeidsovereenkomst gedeeld door het aantal personeelsleden met een arbeidsovereenkomst op een peildatum..

**Teller:** Aantal contractuele uren aan personeelsleden met een arbeidsovereenkomst op een peildatum.

**Noemer:** Aantal personeelsleden met een arbeidsovereenkomst op een peildatum.

## Toelichting
Deze indicator betreft het aantal contractuele uren aan personen met een arbeidsovereenkomst op een peildatum ten op zichte van het aantal personen met een arbeidsovereenkomst op die peildatum.

Deze indicator wordt berekend op organisatieniveau, per vestiging en per zorg/niet-zorg gerelateerde functies. Ook worden per categorie totalen berekend.


## Uitgangspunten

* Alle personeelsleden met een arbeidsovereenkomst (met zorg- en niet-zorggerelateerde functies) worden geïncludeerd. NB: voor een 'persoon met een arbeidsovereenkomst' kan ook een 'personeelslid in loondienst' gelezen worden.
* Toebedeling van contractuele uren naar een vestiging is op basis van de geregistreerde vestiging in de arbeidsovereenkomst.
* De peildatum betreft de eerste dag van een kwartaal.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle arbeidsovereenkomsten op de peildatum.  
2. Bepaal o.b.v. de functie in de arbeidsovereenkomst(en) uit stap 1 of de functie zorg- of niet-zorg gerelateerd was.
3. Bepaal per arbeidsovereenkomst de vestiging en het aantal contractuele uren per week (op de betreffende peildatum).
4. Bereken op basis van stap 3 per vestiging en in totaal, het totaal aantal contractuele uren (per week) voor zorg- en niet-zorggerelateerde functies.
5. Bereken op basis van stap 2 per vestiging en in totaal, het aantal personen met zorg- en niet-zorggerelateerde functies.
6. Bereken op basis van stap 4 en 5 per vestiging en in totaal en voor zorg- en niet zorggerelateerde functies, het gemiddeld aantal contractuele uren per personeelslid. 

Peildatum: dd-mm-jjjj

| Organisatie-onderdeel |  Gemiddeld aantal contractuele uren per personeelslid met een zorggerelateerde functie  | Gemiddeld aantal contractuele uren per personeelslid met een niet-zorggerelateerde functie | 
|--------------------|--------|------------|
| Totaal organisatie | Stap 6 | Stap 6     | 
| Vestiging 1        | Stap 6 | Stap 6     | 
| Vestiging 2        | Stap 6 | Stap 6     | 
| Vestiging N        | Stap 6 | Stap 6     | 

