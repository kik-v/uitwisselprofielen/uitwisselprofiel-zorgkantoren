---
title: 12.1.1 Verzuimfrequentie (excl. zwangerschapsverlof) Q1 2023
description: "De verhouding tussen het aantal verzuimgevallen (exclusief zwangerschapsverlof) en het aantal personeelsleden."
weight: 12
---
## Indicator

**Definitie:** De verhouding tussen het aantal verzuimgevallen (exclusief zwangerschapsverlof) en het aantal personeelsleden.

**Teller:** Aantal ziekmeldingen.

**Noemer:** Aantal arbeidsovereenkomsten.

## Omschrijving
Het aantal nieuwe ziekmeldingen ten op zichte van het aantal personeelsleden in een kwartaal.

Deze indicator wordt berekend op organisatieniveau, per vestiging en voor zowel zorg/niet-zorg gerelateerde functies. Ook worden per categorie aantallen en totalen berekend.


## Uitgangspunten

* Alle personen met een of meerdere arbeidsovereenkomsten (met zorg- en niet-zorggerelateerde functies) worden geïncludeerd.
* Per vestiging en voor de gehele organisatie.
* De zorgaanbieder geeft zelf aan welke meldingen nieuwe ziekmeldingen betreffen.
* Zwangerschaps- en bevallingsverlof worden uitgesloten.


## Berekening

1. Selecteer alle personen met een of meerdere arbeidsovereenkomsten in het kwartaal.
2. Bepaal o.b.v. elke arbeidsovereenkomst uit stap 1 de vestiging en het aantal nieuwe ziekmeldingen exclusief zwangerschap in het kwartaal.
3. Bereken o.b.v. stap 2 per vestiging en voor de totale organisatie het aantal arbeidsovereenkomsten.
4. Bereken o.b.v. stap 2 per vestiging en voor de totale organisatie het aantal nieuwe ziekmeldingen (exclusief zwangerschap).
5. Bereken de indicator door per vestiging en voor de totale organisatie de resultaten uit stap 4 te delen door de resultaten uit stap 3 en te vermenigvuldigen met 100%.

_**Vestiging: Naam vestiging**_

_**Kwartaal: mm-jj t/m mm-jj**_

| Organisatieonderdeel       |  Aantal arbeidsovereenkomsten  | Aantal nieuwe ziekmeldingen | Verzuimfrequentie excl. zwangerschap | 
|----------------|--------|-----------|--------|
| Totaal organisatie | Stap 3 | Stap 4    | Stap 5 |  
| Vestiging 1      | Stap 3 | Stap 4    | Stap 5 | 
| Vestiging 2      | Stap 3 | Stap 4    | Stap 5 | 
| Vestiging N      | Stap 3 | Stap 4    | Stap 5 | 
