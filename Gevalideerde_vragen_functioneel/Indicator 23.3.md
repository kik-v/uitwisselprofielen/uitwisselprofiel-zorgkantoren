---
title: 23.3 - Kapitaal, CAPEX (Capital Expenditures)
weight: n.t.b.
---

## Indicator

**Definitie:** De CAPEX (Capital Expenditures) worden gedefinieerd als (toename van de vaste activa) / afschrijvingen.

**Teller:** Toename van de vaste activa over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

**Noemer:** Afschrijvingen over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de CAPEX (Capital Expenditures) van de organisatie. De Capital Expenditures (CAPEX) tonen de totale investeringen ten opzichte van wat afgeschreven wordt.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. (*"A Vaste activa"*, op peildatum einde meetperiode - *"A Vaste activa"*, op peildatum begin meetperiode) / *"Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental | Waarde |
|--------|--------|
| CAPEX  | Stap 1 |
