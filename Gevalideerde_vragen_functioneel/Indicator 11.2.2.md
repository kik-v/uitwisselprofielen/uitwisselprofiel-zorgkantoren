---
title: 11.2.2 Kortdurend ziekteverzuimpercentage (incl. zwangerschapsverlof) Q2 2023
description: "Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden, in procenten van het totaal aantal beschikbare kalenderdagen van personeelsleden in de verslagperiode.(inclusief zwangerschaps- en bevallingsverlof)"
weight: 11
---
## Indicator

**Definitie:** Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden, in procenten van het totaal aantal beschikbare kalenderdagen van personeelsleden in de verslagperiode. Het ziekteverzuimpercentage is inclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor. Er is sprake van kortdurend verzuim wanneer het aantal verzuimdagen 28 dagen of korter is.

**Teller:** Aantal ziektedagen.

**Noemer:** Aantal kalenderdagen.

## Toelichting
Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden, in procenten van het totaal aantal beschikbare kalenderdagen (dus geen werkdagen) van personeel in loondienst per kwartaal. Het ziekteverzuimpercentage betreft het verzuim van 28 dagen of korter en inclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor.


## Uitgangspunten

* Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. 
* Zwangerschaps- of bevallingsverlof worden geincludeerd.
* Ziekteperiodes van 28 dagen en korter worden geincludeerd.
* In de berekening wordt de parttime geincludeerd.
* 1 ziektedag = 1 kalenderdag waarop een personeelslid ziek is.
* Wanneer het ziektepercentage bij de verzuimmelding ontbreekt, wordt het ziektepercentage verondersteld 100% te zijn.
* Voor de berekening van deze indicator is het noodzakelijk dat in de administratie van de zorgaanbieder wijzigingen in parttime factor, vestiging en functie gedurende de verslagperiode beschikbaar zijn.
* Indien een ziekteperiode langer doorloopt of nog geen eindatum heeft dan wordt de einddatum van het kwartaal gebruikt in de berekening als einddatum van de ziekteperiode (dus indien de ziekteperiode van 20 maart tot en met 15 april is en de berekening gaat over het 1e kwartaal dan wordt een einddatum van de ziekteperiode van 31 maart gehanteerd in de berekening). Dit is alleen van toepassing op een eindatum van een ziekteperiode in de berekening. De startdatum van een ziekteperiode wordt niet aangepast! 


## Berekening

De indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer alle personeelsleden (o.b.v. de werkovereenkomst) in de betreffende verslagperiode. 
2. Bepaal o.b.v. stap 1 per persooneelslid per werkovereenkomst voor elke dag binnen de verslagperiode de vestiging, functie en parttimefactor.
3. Selecteer van alle personeelsleden uit stap 1 de ziekteperiodes met een duur van 28 dagen of langer en inclusief zwangerschaps- en bevallingsverlof.
4. Bepaal o.b.v. stap 3 per personeelslid per werkovereenkomst voor elke dag binnen de ziekteperiode het ziektepercentage.
5. Bereken o.b.v. stap 2 en stap 4 per personeelslid per werkovereenkomst voor elke dag binnen de verslagperiode het ziekteverzuim door de parttimefactor te vermenigvuldigen met het ziektepercentage.
6. Pas per personeelslid per werkovereenkomst voor elke dag binnen de verslagperiode de parttimefactor toe op de kalenderdag.
7. Bereken per zorg- en niet-zorggerelateerde functie, per vestiging en voor de totale organisatie het verzuimpercentage door het ziekteverzuim van de betreffende personeelsleden bij elkaar op te tellen en te delen door de optelling van het aantal kalenderdagen waarop de parttime is toegepast. Vermenigvuldig elk resultaat met 100%.

| Ziekteverzuimpercentage periode:       |  Zorg  | Niet-zorg | Totaal |
|----------------|--------|-----------|--------|
| Totaal organisatie | Stap 7 | Stap 7    | Stap 7 |
| Vestiging 1      | Stap 7 | Stap 7    | Stap 7 |
| Vestiging 2      | Stap 7 | Stap 7   | Stap 7 |
| Vestiging N      | Stap 7 | Stap 7    | Stap 7 |
