---
title: 15.4.2.2 Aantal cliënten met een Wlz-indicatie per sector - peildatum 1-4-2023
description: "Het aantal cliënten met een Wlz-indicatie per sector per vestiging en op organisatieniveau op een peildatum."
weight: 15
---

## Indicator

**Definitie:** Het aantal cliënten met een Wlz-indicatie per sector per vestiging en op organisatieniveau op een peildatum.

**Teller:** Aantal cliënten met een Wlz-indicatie.

**Noemer:** Niet van toepassing.

## Toelichting

Geen.


## Uitgangspunten

* De peildatum betreft het moment waarop de gegevens zijn geregisteerd in het bronsysteem en verwerkt in het datastation. Er wordt dus niet gewerkt met een nader te bepalen of zelf gekozen peildatum.

## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op de peildatum beschikken over een Wlz-indicatie. 
2. Bepaal per cliënt uit stap 1 de vestiging(en) en de langdurige zorgsector(en) waaronder de Wlz-indicatie(s) valt of vallen. 
3. Bereken op basis van stap 2 per vestiging en voor de totale organisatie het aantal cliënten op de peildatum.

Peildatum: dd-mm-jjjj 
|Organisatieonderdeel  |  VV  | LG | LVG |  VG  | ZGAUD | ZGVIS | GGZ-B | GGZ-W|
|----------------|--------|-----------|--------|--------|--------|-----------|--------|--------|
| Totaal organisatie | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 | Stap 3 |
| Vestiging 1      | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 | Stap 3 |
| Vestiging 2      | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 | Stap 3 |
| Vestiging N      | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 | Stap 3 |

