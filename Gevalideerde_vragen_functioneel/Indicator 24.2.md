---
title: 24.2 - Overig - ICR (Interest Coverage Ratio)
weight: n.t.b.
---

## Indicator

**Definitie:** De ICR wordt gedefinieerd als (EBIT) / (rentelasten). De EBIT wordt gedefinieerd als opbrengsten - kosten zonder renten en belastingen.

**Teller:** EBIT over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

**Noemer:** Rentelasten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de Interest Coverage Ratio (ICT)van de organisatie.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. EBIT = *"P Som der bedrijfsopbrengsten"* - *"Q Som der bedrijfslasten"*
2. ICR = EBIT / *"R.IV Rentelasten en soortgelijke kosten"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental | Waarde |
|--------|--------|
| ICR    | Stap 2 |
