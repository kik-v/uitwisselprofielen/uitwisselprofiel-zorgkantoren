---
title: 24.7 - Overig - Arbeidsintensiteit PNIL
weight: n.t.b.
---

## Indicator

**Definitie:** De Arbeidsintensiteit PNIL wordt gedefinieerd als (kosten personeel niet in loondienst) / (opbrengst)

**Teller:** Kosten personeel niet in Loondienst (PNIL) over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

**Noemer:** Opbrengst over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de Arbeidsintensiteit PNIL van de organisatie.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Arbeidsintensiteit PNIL = *"Q.II Kosten uitbesteed werk en andere externe kosten"* / *"P Som der bedrijfsopbrengsten"*
2. Arbeidsintensiteit PNIL% = Arbeidsintensiteit PNIL * 100

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental                   | Waarde |
|--------------------------|--------|
| Arbeidsintensiteit PNIL  | Stap 1 |
| Arbeidsintensiteit PNIL% | Stap 2 |
