---
title: 19.7 - Liquiditeit, EBITDA-ratio
weight: n.t.b.
---

## Indicator

**Definitie:** De EBITDA-ratio wordt gedefinieerd als wordt gedefinieerd als (EBITDA) / (Opbrengsten). De EBITDA is gedefinieerd in Indicator 19.6.

**Teller:** EBITDA over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

**Noemer:** Opbrengsten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de EBITDA-ratio van de organisatie. De EBITDA t.o.v. opbrengsten geeft een kengetal weer van de operationele resultaten, zonder rekening te houden met de afschrijvingen en rentelasten.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. EBITDA-ratio = EBITDA [zie Indicator 19.6] / *"P Som der bedrijfsopbrengsten"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental       | Waarde |
|--------------|--------|
| EBITDA-ratio | Stap 1 |
