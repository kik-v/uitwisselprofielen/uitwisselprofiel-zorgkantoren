---
title: 15.3. Aantal personen dat per vestiging kan wonen
description: "Het aantal personen dat bij een zorgaanbieder per vestiging en op organisatieniveau kan wonen."
weight: 15
---

## Indicator

**Definitie:** Het aantal personen dat bij een zorgaanbieder per vestiging en op organisatieniveau kan wonen. 

**Teller:** Aantal bewoners.

**Noemer:** Niet van toepassing.


## Toelichting
Deze indicator geeft de capaciteit van een zorgaanbieder op een bepaald moment aan. De capaciteit betreft per vestiging het aantal personen dat bij een zorgaanbieder kan wonen, oftewel bewoner. Een bewoner betreft bijvoorbeeld een cliënt of de partner van een cliënt. 

Deze indicator wordt op organisatieniveau en per vestiging berekend.


## Uitgangspunten

* De capaciteit van een vestiging en van de organisatie wordt uitgedrukt in het aantal personen dat in een vestiging (en bij de organisatie als geheel) van een zorgaanbieder kan wonen, oftewel bewoner.
* De capaciteit wordt bepaald op basis van het aantal personen dat in elke wooneenheid kan wonen.
* Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een lokatie met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die lokatie wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’–eenheid’.
* De peildatum betreft het moment waarop de gegevens zijn geregisteerd in het bronsysteem en verwerkt in het datastation. Er wordt dus niet gewerkt met een nader te bepalen peildatum.


## Berekening

Deze indicator wordt als volgt berekend:

1. Bepaal per vestiging het aantal personen dat in de betreffende vestiging kan wonen (bewoners). 
2. Bereken het totaal aantal bewoners voor de organisatie.

| Organisatieonderdeel | Capaciteit aantal bewoners |  
|----------------|--------|
| Totaal organisatie | Stap 2 | 
| Vestiging 1 | Stap 1 | 
| Vestiging 2 | Stap 1 | 
| Vestiging N | Stap 1 | 
