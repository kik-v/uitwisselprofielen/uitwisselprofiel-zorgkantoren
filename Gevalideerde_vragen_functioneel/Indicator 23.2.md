---
title: 23.2 - Kapitaal, Net-debt / EBITDA
weight: n.t.b.
---

## Indicator

**Definitie:** De Net-debt / EBITDA wordt gedefinieerd als (vreemd vermogen) / EBITDA. Het vreemd vermogen is kortlopende schulden + langlopende schulden. De EBITDA wordt gedefinieerd als opbrengsten - kosten zonder renten, belastingen, en afschrijvingen.

**Teller:** Vreemd vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** EBITDA over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de net-debt/EBITDA van de organisatie. De net-debt/EBITDA geeft de verhouding weer tussen het vreemd vermogen en de opbrengst voor aftrek van rente, belasting, afboekingen en afschrijvingen. Het geeft daarmee een inschatting van de termijn (hoeveel jaar) die een  zorgaanbieder nodig heeft om haar (rentedragende) schuld terug te
betalen.

## Uitgangspunten

* De peildatum vreemd vermogen is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.
* De meetperiode EBITDA is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
* Bij Peildatum 31-dec van het voorgaande jaar hoort de meetperiode 1-jan t/m 31-dec van het voorgaande jaar. Idem voor de andere combinatie in het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Net-debt / EBITDA = (*"F Langlopende schulden (nog voor meer dan een jaar)"* + *"G Kortlopende schulden (ten hoogste 1 jaar)"*) / EBITDA [zie Indicator 19.6]

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental            | Waarde |
|-------------------|--------|
| Net-debt / EBITDA | Stap 1 |
