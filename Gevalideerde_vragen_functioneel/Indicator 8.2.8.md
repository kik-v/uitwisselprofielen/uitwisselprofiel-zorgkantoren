---
title: 8.2.8 Percentage kosten personeel niet in loondienst (PNIL) Q4 2024
description: "Percentage kosten van personeel niet in loondienst (PNIL)."
weight: 8
---
## Indicator

**Definitie:** Percentage kosten van personeel niet in loondienst (PNIL).

**Teller:** Kosten van personeel niet in loondienst (PNIL).

**Noemer:** Kosten personeel in loondienst en personeel niet in loondienst.

## Toelichting

Deze indicator betreft de kosten van personeelsleden niet in loondienst (PNIL) ten op zichte van personeel in loondienst (PIL) en personeel niet in loondienst (PIL).

Deze indicator betreft de kosten van zowel zorg- als niet zorg-gerelateerd personeel en wordt per kwartaal, per vestiging en voor de totale organisatie berekend.

### Kosten personeel niet in loondienst (PNIL)

Kosten van personeel niet in loondienst (PNIL) betreft alle vergoedingen aan personeel die niet in loondienst en als uitzendkracht binnen de instelling werkzaam zijn. Hierbij kan gedacht worden aan uitzendkrachten, vergoedingen voor freelance bezigheidstherapeuten, consulenten, medisch adviseurs, docenten en dergelijke, maar ook voor medisch specialisten die niet in loondienst van de instelling werken. Denk bijvoorbeeld ook aan kappers en assistenten voor de eredienst en dergelijke.

Bij vergoedingen is geen sprake van een dienstbetrekking. De instelling is niet inhoudingsplichtig.
Het personeel voert de door haar opgedragen werkzaamheden echter wel uit onder de (directe) leiding van de instelling, en maakt daarbij veelal gebruik van faciliteiten, materialen en gereedschappen van de instelling.

Voorbeelden hiervan zijn:

- uitzendkrachten en freelance medewerkers;
- oproep- en invalkrachten voor zover de instelling daarvoor niet inhoudingsplichtig is;
- medische en andere specialisten niet in loondienst.

Bij uitbestede werkzaamheden worden werkzaamheden niet uitgevoerd onder leiding van de instelling. Hiervoor koopt de instelling diensten in en geeft aan derden opdracht tot uit voering en leiding van dergelijke werkzaamheden (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende rubrieken conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS m.b.t. PNIL|
|----------------|
| WBedOvpUik           W.B.A       4012010      Uitzendkrachten    Uitzendkrachten overige personeelskosten |
| WBedOvpUikUik        W.B.A010    4012010.01    Uitzendkrachten    Uitzendkrachten overige personeelskosten |
| WBedOvpUikFor        W.B.A020    4012010.02    Uitzendkrachten  formatief    Uitzendkrachten  formatief |
| WBedOvpUikPrj        W.B.A030    4012010.03    Uitzendkrachten projectmatig    Uitzendkrachten projectmatig |
| WBedOvpUikBfo        W.B.A040    4012010.04    Uitzendkrachten boven formatief    Uitzendkrachten boven formatief |
| WBedOvpUikPro        W.B.A050    4012010.05    Uitzendkrachten programma's    Uitzendkrachten programma's |
| WBedOvpUit           W.B.C       4012020    Uitzendbedrijven    Uitzendbedrijven overige personeelskosten |
| WBedOvpMaf           W.B.D       4012030    Management fee    Management fee overige personeelskosten |
| WBedOvpZzp           W.B.E       4012040    Ingehuurde ZZP-ers    Ingehuurde ZZP-ers overige personeelskosten |
| WBedOvpPay           W.B.F       4012050    Ingehuurde payrollers    Ingehuurde payrollers overige personeelskosten |
| WBedOvpOip           W.B.G       4012060    Overig ingeleend personeel    Overig ingeleend personeel overige personeelskosten |

| Specificatie rubrieken Prismant m.b.t. PNIL|
|----------------|
| 418000 Vergoedingen voor niet in loondienst verrichte arbeid |
| 418100 Uitzendkrachten |
| 418200 Overig personeel niet in loondienst |

### Kosten personeel in loondienst (PIL)

Loonkosten van personeel in loondienst betreft de salariskosten van in dienstbetrekking verrichte arbeid op basis van een arbeidsovereenkomst. De instelling treedt op als werkgever en is uit dien hoofde inhoudingsplichtig voor de loonbelasting en premieheffing. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 29).

Op grond van hiervan worden tot de salarissen gerekend:

- Brutosalaris (volgens inschalingtabellen) inclusief doorbetaalde salarissen tijdens ziekte;
- Vakantiebijslag;
- Vergoedingen voor overwerk, onregelmatige dienst, bereikbaarheids-, aanwezigheids- en slaapdienst;
- Eindejaarsuitkeringen e.d. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 30).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende rubrieken conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WPerLes S.A 4001000 Lonen en salarissen |
| WPerSol S.B 4002000 Sociale lasten |

| Specificatie rubrieken Prismant|
|----------------|
| 411 Algemene en administratieve functies |
| 412 Hotelfuncties |
| 413 Patiënt- c.q. bewonergebonden functies |
| 414 Leerling personeel |
| 415 Terrein- en gebouwgebonden functies |
| 422100 Berekende sociale kosten vakantiebijslag |
| 422300 Aandeel werknemer premies sociale verzekeringen |
| 422400 Aandeel werkgever premies sociale verzekeringen |
| 422410 Korting / vrijlating basispremie WAO / werkgeversdeel Awf (Algemeen werkloosheidsfonds) |
| 422500 Ziektekostenverzekering |
| 422600 Pensioenkosten |
| 422900 Overgeboekte sociale kosten |

## Uitgangspunten

- De loonkosten van personeel in loondienst worden geïncludeerd.
- De kosten van stagiaires worden niet geïncludeerd.
- De sociale kosten en overige kosten van personeel in loondienst worden niet geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Bereken de kosten die vallen onder personeel niet in loondienst (PNIL) voor de betreffende meetperiode per vestiging en voor de totale organisatie.
2. Bereken de kosten die vallen onder personeel in loondienst (PIL) en PNIL voor de betreffende meetperiode per vestiging en voor de totale organisatie.
3. Deel per vestiging en voor de totale organisatie de resultaten uit stap 1 door de resultaten uit stap 2 en vermenigvuldig met 100%.

| Periode:       | Kosten PNIL (Euro) | Kosten PIL (Euro) | Percentage PNIL t.o.v. PIL+PNIL |
|----------------|----|----|----|
| Totaal organisatie | Stap 1 | Stap 2 | Stap 3 |
| Vestiging 1 | Stap 1 | Stap 2 | Stap 3 |
| Vestiging 2 | Stap 1 | Stap 2 | Stap 3 |
| Vestiging N | Stap 1 | Stap 2 | Stap 3 |
| Vestiging Overig (vestiging onbekend) | Stap 1 | Stap 2 | Stap 3 |
