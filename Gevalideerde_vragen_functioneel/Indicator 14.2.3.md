---
title: 14.2.3 Aantal cliënten per leveringsvorm - peildatum 1-7-2023
description: "Aantal cliënten per leveringsvorm op peildatum: per vestiging en in totaal met betrekking tot cliënten met een Wlz-indicatie met zorgprofiel VV."
weight: 14
---
## Indicator

**Definitie:** Aantal cliënten per leveringsvorm op peildatum: per vestiging en in totaal met betrekking tot cliënten met een Wlz-indicatie met zorgprofiel VV.

**Teller:** Aantal cliënten per leveringsvorm.

**Noemer:** Niet van toepassing.

## Toelichting 
Deze informatievraag betreft per vestiging en voor de organisatie als geheel het aantal cliënten per leveringsvorm op een peildatum. Het gaat om cliënten met een Wlz-indicatie met zorgprofiel VV.

## Uitgangspunten

* Personen met een Wlz-indicatie met zorgprofiel VV worden geïncludeerd.
* Het landelijk beleid en de afspraken over het vraagstuk overheveling zorg thuis is nog in ontwikkeling. De gehanteerde definitie voor geclusterd VPT in dit uitwisselprofiel betreft een werkdefinitie die wordt aangepast als nadere invulling van het beleid dit noodzakelijk maakt. In deze indicator wordt daarvoor gewerkt met een verhouding VPT/Verblijf.


## Berekening
Deze indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op de peildatum zorg ontvangen en beschikken over een Wlz-indicatie met zorgprofiel VV.
2. Bepaal voor alle cliënten uit stap 1 de leveringsvorm en de vestiging.
3. Bereken op basis van stap 2 per vestiging en voor de organisatie als geheel het totaal aantal cliënten per leveringsvorm.
4. Bereken op basis van stap 2 en 3 de verhouding VPT t.o.v. Verblijf.

Datum: dd-mm-jjj

| Organisatieonderdeel | Verblijf | VPT | MPT | PGB + DTV | Totaal | Verhouding VPT/Verblijf |  
| --- | --- | --- | --- | --- | --- | --- |  
| Organisatie | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 4 |
| Vestiging 1 | Stap 3 | Niet van toepassing | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing |
| Vestiging 2 | Stap 3 | Niet van toepassing | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing |
| Vestiging N | Stap 3 | Niet van toepassing | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing |
