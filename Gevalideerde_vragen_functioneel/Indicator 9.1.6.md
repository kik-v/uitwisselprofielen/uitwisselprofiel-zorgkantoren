---
title: 9.1.6 Aantal vrijwilligers Q2 2024
description: "Aantal vrijwilligers per kwartaal per vestiging per financieringsstroom."
weight: 9
---
## Indicator

**Definitie:** Aantal vrijwilligers per kwartaal per vestiging per financieringsstroom.

**Teller:** Aantal vrijwilligers.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal vrijwilligers per kwartaal. 

Deze indicator wordt berekend op organisatieniveau, per vestiging.


## Uitgangspunten

Op deze indicator zijn de volgende Uitgangspunten van toepassing:

* Alle vrijwilligers worden geïncludeerd.
* Per vestiging en voor de gehele organisatie.
* Per kwartaal.


**Voorbeeld**

Onderstaande tabel beschrijft een voorbeeld van de manier waarop een vrijwilliger meetelt in elk van de categorieen. Deze vrijwilliger beschikte in het kwartaal over een werkovereenkomst. 

| Periode:     |  Aantal vrijwilligers  | 
|----------------|--------|
| Totaal organisatie |  1     | 
| Vestiging 1      |  1     | 
| Vestiging 2      |  -     | 
| Vestiging N      |  -     | 


## Berekening

Deze indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer o.b.v. de werkovereenkomst alle personen die gedurende het kwartaal vrijwilliger waren.
2. Bepaal o.b.v. de werkovereenkomst voor elke persoon uit stap 1 de vestiging.
3. Bereken o.b.v stap 3 per vestiging en voor de totale organisatie het aantal personen.

Periode: dd-mm-jjjj t/m dd-mm-jjjj
| Organisatieonderdeel | Aantal vrijwilligers | 
|---|---|
| Totaal organisatie |  Stap 3     | 
| Vestiging 1      |  Stap 3     | 
| Vestiging 2      |  Stap 3     | 
| Vestiging N      |  Stap 3     | 
