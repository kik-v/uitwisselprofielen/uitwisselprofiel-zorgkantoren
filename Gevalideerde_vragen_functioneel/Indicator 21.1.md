---
title: 21.1 - Rentabiliteit, Winstratio
weight: n.t.b.
---

## Indicator

**Definitie:** De winstratio wordt gedefinieerd als (resultaat na belastingen) / (totaal opbrengsten)

**Teller:** Resultaat na belastingen over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

**Noemer:** Totaal opbrengsten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de winstratio van de organisatie. Winstgevendheid van de totale onderneming. Dit geeft een indicatie van de mogelijke toevoeging aan reserves. Herhaaldelijke verliezen kunnen leiden tot een verdamping van het eigen vermogen.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Winstratio = *"S Resultaat na belastingen"* / *"P Som der bedrijfsopbrengsten"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental     | Waarde |
|------------|--------|
| Winstratio | Stap 1 |
