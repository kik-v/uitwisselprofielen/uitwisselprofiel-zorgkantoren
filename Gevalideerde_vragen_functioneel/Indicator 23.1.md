---
title: 23.1 - Kapitaal, LTV (Loan to Value)
weight: n.t.b.
---

## Indicator

**Definitie:** Loan to Value (LTV) wordt gedefinieerd als (langlopende schulden) / (materiële vaste activa)

**Teller:** Langlopende schulden op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Materiële vaste activa op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de Loan to Value (LTV) van de organisatie. De loan to value (LTV) geeft de verhouding weer tussen de leensom en de (markt)waarde van de activa, en geeft daarmee aan in hoeverre een actief is gefinancierd met langlopende schulden. Hoe hoger de LTV, hoe minder zekerheid er is dat de leensom kan worden terugbetaald uit de opbrengst van de verkoop van het actief. Zorgaanbieders kunnen bijvoorbeeld beschikken over een pand, aangekocht middels een leensom. Een (te) hoge LTV geeft aan dat er in verhouding sprake is van een (te) hoge leensom oftewel activa die in hoge mate zijn gefinancierd met vreemd vermogen.

## Uitgangspunten

* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. LTV = *"F Langlopende schulden (nog voor meer dan een jaar)"* / *"A.III Financiële vaste activa"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental | Waarde |
|--------|--------|
| LTV    | Stap 1 |
