---
title: 24.1 - Overig, Dekking vaste activa met lang vermogen
weight: n.t.b.
---

## Indicator

**Definitie:** De dekking vaste activa met lang vermogen wordt gedefinieerd als (vaste activa) / (langlopende schulden)

**Teller:** Vaste activa op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Langlopende schulden op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de dekking vaste activa met lang vermogen van de organisatie.

## Uitgangspunten

* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Dekking vaste activa met lang vermogen = *"A Vaste activa"* / *"F Langlopende schulden (nog voor meer dan een jaar)"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental                                  | Waarde |
|-----------------------------------------|--------|
| Dekking  vaste activa met lang vermogen | Stap 1 |
