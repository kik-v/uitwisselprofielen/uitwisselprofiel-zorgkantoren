---
title: 13.3.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau - peildatum 1-7-2023
description: "Het percentage doorstroom betreft het aantal personen dat op twee opeenvolgende peilmomenten werkzaam is als personeelslid in loondienst in die organisatie en een oplopend kwalificatieniveau heeft op peildatum ten opzichte van peildatum minus 1 kwartaal, gedeeld door het aantal personeelsleden in loondienst van peildatum en peildatum minus 1 kwartaal."
weight: 13
---
## Indicator

**Definitie:** Het percentage doorstroom betreft het aantal personen dat op twee opeenvolgende peilmomenten werkzaam is als personeelslid in loondienst in die organisatie en een oplopend kwalificatieniveau heeft op peildatum ten opzichte van peildatum minus 1 kwartaal, gedeeld door het aantal personeelsleden in loondienst van peildatum en peildatum minus 1 kwartaal.

**Teller:** Aantal personeelsleden met een arbeidsovereenkomst op peildatum en op peildatum minus 1 kwartaal, dat op peildatum een opgelopen kwalificatieniveau heeft ten opzichte van peildatum minus 1 kwartaal.

**Noemer:** Aantal personeelsleden met een arbeidsovereenkomst op peildatum en peildatum minus 1 kwartaal.

## Toelichting

De indicator betreft het aantal personeelsleden in loondienst bij wie het kwalificatieniveau een kwartaal later is opgelopen ten opzichte van de peildatum minus 1 kwartaal, gedeeld door het aantal personeelsleden in loondienst op de peildatum.

De teller van deze indicator betreft het aantal personeelsleden in loondienst die zorgverlener zijn en bij wie op de peildatum het kwalificatieniveau is opgelopen ten opzichte van de peildatum minus een kwartaal. De noemer betreft het aantal personeelsleden in loondienst die zorgverlener zijn en beschikken over een arbeidsovereenkomst op de peildatum. De indicator wordt op organisatieniveau berekend.


## Uitgangspunten

* Personen met een of meerdere arbeidsovereenkomsten, beschikken over een kwalificatieniveau 1 t/m 6 (uit de lijst van het kwaliteitskader) en zorgverlener zijn op de peildatum en op de peildatum minus een kwartaal worden geïncludeerd in de berekening van de noemer.
* Een persoon telt in de berekening van de teller als ‘1’ mee wanneer de persoon op de peildatum beschikte over een arbeidsovereenkomst met een opgelopen kwalificatieniveau ten op zichte van de peildatum minus een kwartaal.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die zorgverlener zijn en beschikken over een of meerdere arbeidsovereenkomsten op de peildatum.
2. Selecteer o.b.v. stap 1 van de personen de arbeidsovereenkomsten die op de peildatum (o.b.v. de functie in de arbeidsovereenkomst) beschikken over een kwalificatieniveau 1 t/m 6 (uit de lijst van het kwaliteitskader).
3. Selecteer o.b.v. stap 2 welke arbeidsovereenkomsten op de peildatum minus 1 kwartaal (o.b.v. de functie in de arbeidsovereenkomst) beschikken over een aflopend kwalificatieniveau 1 t/m 6 (uit de lijst van het kwaliteitskader) t.o.v. het kwalificatieniveau van de functie in de arbeidsovereenkomst op de peildatum zelf.
4. Bereken het totaal aantal personen behoorende bij de arbeidsovereenkomsten uit stap 3.
5. Bereken de indicator door uitkomst van stap 4 te delen door de uitkomst van stap 1 en dit te vermenigvuldigen met 100%. 


|  | **% personeelsleden in loondienst met een oplopend kwalificatieniveau** |
| --- | --- |
| Aantal: | Stap 5 |

