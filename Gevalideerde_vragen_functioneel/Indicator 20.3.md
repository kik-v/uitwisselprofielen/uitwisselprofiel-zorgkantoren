---
title: 20.3 - Solvabiliteit, Debt ratio
weight: n.t.b.
---

## Indicator

**Definitie:** Debt ratio wordt gedefinieerd als (Vreemd vermogen) / (Totaal vermogen)

**Teller:** Vreemd vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Totaal vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

Deze indicator berekent de Debt ratio van de organisatie.

## Uitgangspunten

* De peildatum  is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Debt ratio = (*"F Langlopende schulden (nog voor meer dan een jaar)"* + *"G Kortlopende schulden (ten hoogste 1 jaar)"*) / (*"H Totaal passiva"*)

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental     | Waarde |
|------------|--------|
| Dept ratio | Stap 1 |
