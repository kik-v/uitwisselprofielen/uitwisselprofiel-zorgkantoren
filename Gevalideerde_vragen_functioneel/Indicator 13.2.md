---
title: 13.2 Percentage uitstroom personeel in loondienst
description: "Het percentage uitstroom betreft het aantal personen dat op de peildatum minus 1 jaar werkzaam was als zorggerelateerd personeelslid in loondienst bij het concern, maar dat niet is op de peildatum, gedeeld door het aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar."
weight: 13
---
## Indicator

**Definitie:** Het percentage uitstroom betreft het aantal personen dat op de peildatum minus 1 jaar werkzaam was als zorggerelateerd personeelslid in loondienst bij het concern, maar dat niet is op de peildatum, gedeeld door het aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar.

**Teller:** Aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar dat niet werkzaam is als zorggerelateerd personeelslid in loondienst op de peildatum.

**Noemer:** Aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar.

## Toelichting

De indicator betreft het aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar, en die op de peildatum niet meer in loondienst zijn of niet meer over een zorggerelateerde functie beschikken, ten op zichten van het (totaal) aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar. 

De teller van deze indicator betreft het aantal zorggerelateerde personeelsleden in loondienst die op de peildatum minus 1 jaar zorggerelateerde personeelslid in loondienst waren en een jaar later geen personeelslid in loondienst (meer) zijn of niet meer over een zorggerelateerde functie beschikken. 

De noemer betreft het het aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar.

De indicator wordt berekend op concernniveau.


## Uitgangspunten

* Personen die tegelijkertijd meerdere arbeidsovereenkomsten hebben tellen (o.b.v. de functie in de arbeidsovereenkomst) maximaal 1 keer mee per zorggerelateerde en/of niet-zorggerelateerde functie.
* Per peildatum (elk kwartaal) wordt gekeken welke personeelsleden in loondienst op de peildatum zijn uitgetsroomd ten op zichte van de personen een jaar eerder dan de peildatum.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die op de peildatum minus 1 jaar personeelslid in loondienst waren en beschikken over een zorggerelateerde functie.  
2. Bereken o.b.v. stap 1 het aantal personeelsleden in loondienst voor de totale organisatie (noemer).
3. Selecteer o.b.v. stap 1 alle personen die op de peildatum geen personeelslid in loondienst (meer) waren of niet meer beschikten over een zorggerelateerde functie (teller).
4. Bereken o.b.v. stap 3 het aantal personeelsleden in loondienst voor het totale concern, deel door de resultaten uit stap 3 en vermenigvuldig met 100%.

Peildatum: dd-mm-jjjj
| Organisatieonderdeel | Totaal PIL met zorgfunctie 1 jaar voor peildatum | UItgestroomde PIL met zorgfunctie | % zorggerelateerde uitstroom t.o.v. totaal zorggerelateerd |
|---|---|---|---|
| Totaal organisatie | Stap 2 | Stap 3 | Stap 4 |

