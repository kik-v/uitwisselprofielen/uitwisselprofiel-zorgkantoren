---
title: 10.1.6 Aantal leerlingen op 1-4-2024
description: "Het aantal BBL-leerlingen op 1-4-2024 per vestiging per financieringsstroom."
weight: 10
---
## Indicator

**Definitie:** Het aantal BBL-leerlingen op 1-4-2024 per vestiging per financieringsstroom.

**Teller:** Aantal BBL-leerlingen.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal BBL-leerlingen op 1-4-2024. 

Deze indicator wordt berekend op organisatieniveau en per vestiging.


## Uitgangspunten

* Alle BBL-leerlingen worden geïncludeerd.
* Per vestiging en voor de gehele organisatie.
* Op 1-4-2024.

**Voorbeeld**

Onderstaande tabel beschrijft een voorbeeld van de manier waarop een BBL-leerling meetelt in elk van de categorieën. Deze BBL-leerling beschikte op de peildatum over 1 arbeidsovereenkomst. 

| Peildatum:     |  Aantal BBL-leerlingen  | 
|----------------|--------|
| Totaal organisatie |  1     | 
| Vestiging 1      |  1     | 
| Vestiging 2      |  -     | 
| Vestiging N      |  -     | 


## Berekening

Deze indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer o.b.v. de werkovereenkomst alle personen die op de peildatum BBL-leerlingen waren.  
2. Bepaal o.b.v. de arbeidsovereenkomst voor elke persoon uit stap 1 de vestiging.
3. Bereken o.b.v stap 2 per vestiging en in totaal het aantal BBL-leerlingen.


| Peildatum:     |  Aantal BBL-leerlingen  | 
|----------------|--------|
| Totaal organisatie |  Stap 3     | 
| Vestiging 1      |  Stap 3     | 
| Vestiging 2      |  Stap 3     | 
| Vestiging N      |  Stap 3     | 
