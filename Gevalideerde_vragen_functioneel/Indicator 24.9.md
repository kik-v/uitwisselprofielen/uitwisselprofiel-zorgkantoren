---
title: 24.9 - Overig - Uitbesteed werk vs vast personeel
weight: n.t.b.
---

## Indicator

**Definitie:** Het uitbesteed werk vs vast personeel wordt gedefinieerd als (kosten uitbesteed werk) / (personeelskosten).

**Teller:** Kosten uitbesteed werk over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

**Noemer:** Personeelskosten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent het uitbesteed werk vs vast personeel van de organisatie.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Uitbesteed werk vs vast personeel = *"Q.II Kosten uitbesteed werk en andere externe kosten" / (*"Q.III Lonen en salarissen"* + *"Q.IV Sociale lasten"* + *"Q.V Pensioenlasten"*)

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental                             | Waarde |
|------------------------------------|--------|
| Uitbesteed werk vs vast personeel  | Stap 1 |
