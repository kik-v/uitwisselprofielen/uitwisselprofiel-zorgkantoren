---
title: 19.1 - Liquiditeit, DSCR (Debt Service Coverage Ratio)
weight: n.t.b.
---
## !!! Nog niet publiceren, de berekening van het kasstroomoverzicht is nog een open issue !!!

## Indicator

**Definitie:** De DSCR wordt gedefinieerd als EBITDA / (rente + aflossingen). De EBITDA is gedefinieerd in Indicator 19.6.

**Teller:** EBITDA over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

**Noemer:** rente + aflossingen in de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de DSCR van de organisatie. De debt service coverage ratio (DSCR) zegt iets over de kredietwaardigheid van een bedrijf - het geeft aan of er voldoende operationele kasstromen worden gegenereerd om aan de rente- en aflossingsverplichtingen te kunnen voldoen.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. "Uitgaven ter aflossing van leningen" = (*F Langlopende schulden (nog voor meer dan een jaar)* op peildatum eerste dag voor het begin van de meetperiode) - (*F Langlopende schulden (nog voor meer dan een jaar)* op peildatum laatste dag van de meetperiode)
2. DSCR = EBITDA [zie Indicator 19.6] / (*"R.IV Rentelasten en soortgelijke kosten"* + "Uitgaven ter aflossing van leningen")

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

De post "Uitgaven ter aflossing van leningen" wordt in jaarrekeningen van zorgorganisaties vaak benoemd als "aflossing langlopende schulden".

| Kental | Waarde |
|--------|--------|
| DSCR   | Stap 2 |
