---
title: 13.1 Percentage instroom personeel in loondienst
description: "Het percentage instroom betreft het aantal personen dat op de peildatum minus 1 jaar niet werkzaam was als zorggerelateerd personeelslid in loondienst bij het concern, maar dat wel is op de peildatum, gedeeld door het aantal zorggerelateerde personeelsleden in loondienst op de peildatum"
weight: 13
---
## Indicator

**Definitie:** Het percentage instroom betreft het aantal personen dat op de peildatum minus 1 jaar niet werkzaam was als zorggerelateerd personeelslid in loondienst bij het concern, maar dat wel is op de peildatum, gedeeld door het aantal zorggerelateerde personeelsleden in loondienst op de peildatum.

**Teller:** Aantal zorggerelateerde personeelsleden in loondienst op de peildatum dat niet werkzaam was als zorggerelateerde personeelslid op de peildatum minus 1 jaar.

**Noemer:** Aantal zorggerelateerde personeelsleden in loondienst op de peildatum.

## Toelichting

De indicator betreft het aantal nieuwe zorggerelateerde personeelsleden in loondienst op de peildatum ten op zichte van het (totaal) aantal zorggerelateerde personeelsleden op de peildatum. 

De teller van deze indicator betreft alle zorggerelateerde personeelsleden die op de peildatum (in een kwartaal) zorggerelateerd personeelslid in loondienst zijn en dat op de peildatum minus 1 jaar nog niet waren. 

De noemer betreft het aantal zorggerelateerde personeelsleden in loondienst op de peildatum.

De indicator wordt berekend op organisatieniveau voor zorggerelateerde functies.


## Uitgangspunten

* Personen die tegelijkertijd meerdere arbeidsovereenkomsten hebben tellen (o.b.v. de functie in de arbeidsovereenkomst) maximaal 1 keer mee per zorggerelateerde en/of niet-zorggerelateerde functie.
* OP een peildatum (elk kwartaal) wordt een jaar teruggekeken.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die op de peildatum personeelslid zijn en beschikken over een zorggerelateerde functie.  
2. Bereken o.b.v. stap 1 het aantal personen voor de totale organisatie (noemer).
3. Selecteer alle personen die op de peildatum minus 1 jaar geen personeelslid waren of niet over een zorggerelateerde functie beschikten (teller).
4. Bereken o.b.v. stap 3 het aantal personen voor de totale organisatie, deel door de resultaten uit stap 2 en vermenigvuldig met 100%.

Peildatum: dd-mm-jjjj
| Organisatieonderdeel | Aantal PIL met zorgfunctie | Aantal ingestroomde PIL met zorgfunctie | % zorggerelateerde instroom t.o.v. totaal zorggerelateerd  | 
|----|---|---|---|
| Totaal organisatie | Stap 2 | Stap 3 | Stap 4 |
