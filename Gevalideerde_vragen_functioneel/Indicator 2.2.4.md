---
title: 2.2.4 Aantal verloonde uren Q4 2023
description: "Het aantal verloonde uren van personeel in loondienst per kwartaal, per vestiging en ingedeeld in zorg en niet-zorg gerelateerd personeel." 
weight: 2
---
## Indicator

**Definitie:** Het aantal verloonde uren van personeel in loondienst per kwartaal, per vestiging en ingedeeld in zorg en niet-zorg gerelateerd personeel.

**Teller:** Het aantal verloonde uren van personeel in loondienst.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal verloonde uren van personeel in loondienst. Verloonde uren betreft het aantal uren waarover het loon van de werknemer is berekend. Zie tevens: https://download.belastingdienst.nl/belastingdienst/docs/memo_verloonde_uren_lh0361t5fd.pdf

Deze indicator betreft zowel zorg- als niet zorg-gerelateerd personeel in loondienst en wordt per kwartaal, per vestiging en voor de totale organisatie berekend.

Op basis van deze uren kan het aantal fte’s van deze personen berekend worden. Iedere afnemer van gegevens kan op basis van het aantal uren zijn/haar eigen definitie van fte toepassen, zoals 36, 38 of 40 uur per week.


## Uitgangspunten

* Alle personeelsleden in loondienst (zorg- en niet-zorg) worden geïncludeerd.
* De verloonde uren worden ingedeeld bij de vestiging die geregistreerd staat in de arbeidsovereenkomst.
* De verloonde uren worden ingedeeld in de periode waarin de salarisstrook is aangemaakt. 


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle arbeidsovereenkomsten gedurende het kwartaal.
2. Bepaal (o.b.v. de functie in de arbeidsovereenkomst) voor elke arbeidsovereenkomst uit stap 1 of de functie zorg of niet-zorg gerelateerd was.
3. Bepaal voor elke arbeidsovereenkomst uit stap 2 de vestiging.
4. Bereken per arbeidsovereenkomst het aantal verloonde uren in het betreffende kwartaal.
5. Tel de uitkomsten van de berekeningen uit stap 4 bij elkaar op en groepeer per vestiging, per vestiging voor zorg- en niet-zorg en voor de totale organisatie.

**Kwartaal: mm-yy t/m mm-yy**
| Aantal verloonde uren:  | Zorggerelateerd | Niet-Zorggerelateerd | Totaal |
|----------------|--------|-----------|--------|
| Organisatie    | Stap 5 | Stap 5    | Stap 5 |
| Vestiging 1    | Stap 5 | Stap 5    | Stap 5 |
| Vestiging 2    | Stap 5 | Stap 5    | Stap 5 |
| Vestiging N    | Stap 5 | Stap 5    | Stap 5 |
