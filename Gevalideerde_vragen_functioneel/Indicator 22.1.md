---
title: 22.1 - Groei, Omzetgroei
weight: n.t.b.
---

## Indicator

**Definitie:** De omzetgroei wordt gedefinieerd als (totale opbrengst verslagjaar) / (totale opbrengst vergelijkingsjaar)

**Teller:** Totale opbrengst over meetperiode verslagjaar 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

**Noemer:** Totale opbrengst over meetperiode vergelijkingsjaar 1-jan t/m 31-dec van het jaar voor het voorgaande jaar of 1-jan t/m 30-6 van het voorgaande jaar.

## Toelichting

Deze indicator berekent de omzetgroei van de organisatie. De omzetgroei geeft aan in hoeverre de totale omzet van een instelling is gegroeid of gekrompen ten opzichte van het jaar/periode ervoor.

## Uitgangspunten

* De meetperiode verslagjaar is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.
* De meetperiode vergelijkingsjaar 1-jan t/m 31-dec van het jaar voor het voorgaande jaar of 1-jan t/m 30-6 van het voorgaande jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Als de beginbalansdatum later is dan de eerste dag van de meetperiode vergelijkingsjaar geef de melding "Onvoldoende grootboekhistorie beschikbaar om deze indicator te berekenen.", anders ga door met stap 2.
2. Omzetgroei = *"P Som der bedrijfsopbrengsten"*, in meetperiode verslagjaar / *"P Som der bedrijfsopbrengsten"*, in meetperiode vergelijkingsjaar
3. Omzetgroei% = Omzetgroei * 100

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental      | Waarde |
|-------------|--------|
| Omzetgroei  | Stap 1 |
| Omzetgroei% | Stap 2 |
