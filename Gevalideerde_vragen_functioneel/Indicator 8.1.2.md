---
title: 8.1.2 Percentage ingezette uren personeel niet in loondienst (PNIL). Q2 2023
description: "Aantal ingezette uren PNIL ten op zichte van het aantal ingezette uren door personeelsleden en PNIL gedurende een kwartaal."
weight: 8
---
## Indicator

**Definitie:** Aantal ingezette uren PNIL ten op zichte van het aantal ingezette uren door personeelsleden en PNIL gedurende een kwartaal.

**Teller:** Aantal uren aan PNIL ingezet in het kwartaal.

**Noemer:** Aantal uren aan personeelsleden met een arbeidsovereenkomst en uren PNIL ingezet in het kwartaal.


## Omschrijving
Deze indicator betreft het aandeel aan uren van personeel niet in loondienst (PNIL) dat gedurende het kwartaal is ingezet ten op zichte van het aantal uren aan personeel in loondienst dat is ingezet in het kwartaal.

Deze indicator betreft zowel zorg- als niet zorg-gerelateerd personeel en wordt per kwartaal, per vestiging en voor de totale organisatie berekend.

Op basis van de ingezette uren kan het aantal (ingezette) fte’s berekend worden. Iedere afnemer van gegevens kan op basis van het totaal aantal uren zijn/haar eigen definitie van fte toepassen, zoals 36, 38 of 40 uur per week of 1692 uren per jaar. 


## Uitgangspunten

* Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd.
* De ingezette uren zijn exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle werkovereenkomsten van personeel in loondienst en personeel niet in loondienst gedurende het kwartaal.
2. Bepaal o.b.v. stap 1 voor elke werkovereenkomst de vestiging.
2. Bepaal o.b.v. stap 2 voor elke werkovereenkomst of dit een arbeidsovereenkomst (PIL) betreft of een werkovereenkomst voor personeel niet in loondienst (PNIL).
4. Bereken per werkovereenkomst het aantal ingezette uren in het betreffende kwartaal.
5. Bereken o.b.v. stap 4 het aantal ingezette uren aan PIL en PNIL per vestiging en voor de totale organisatie.
6. Bereken per vestiging en voor de totale organisatie het aantal ingezette uren aan PNIL en deel door het aantal uren aan PNIL en PIL en vermenigvuldig met 100%.


| Kwartaal:       |  Ingezette uren PNIL  | Ingezette uren PIL | Percentage ingezette uren PNIL |
|----------------|--------|-----------|--------|
| Organisatie    | Stap 5 | Stap 5    | Stap 6 |
| Vestiging 1    | Stap 5 | Stap 5    | Stap 6 |
| Vestiging 2    | Stap 5 | Stap 5    | Stap 6 |
| Vestiging N    | Stap 5 | Stap 5    | Stap 6 |

