---
title: 24.4 - Overig - Verhouding vorderingen en totale opbrengsten
weight: n.t.b.
---

## Indicator

**Definitie:** De Verhouding vorderingen en totale opbrengsten wordt gedefinieerd als (vorderingen) / (opbrengensten)

**Teller:** Vorderingen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Opbrengsten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de Verhouding vorderingen en totale opbrengsten van de organisatie.

## Uitgangspunten

* De peildatum vorderingen is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.
* De meetperiode opbrengsten is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
* Bij Peildatum 31-dec van het voorgaande jaar hoort de meetperiode 1-jan t/m 31-dec van het voorgaande jaar. Idem voor de andere combinatie in het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Verhouding vorderingen en totale opbrengsten = *"B.II Vorderingen"* / *"P Som der bedrijfsopbrengsten"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental                                       | Waarde |
|----------------------------------------------|--------|
| Verhouding vorderingen en totale opbrengsten | Stap 1 |
