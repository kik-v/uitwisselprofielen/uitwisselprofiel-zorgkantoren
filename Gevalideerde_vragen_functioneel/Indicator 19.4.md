---
title: 19.4 - Liquiditeit, Quick ratio
weight: n.t.b.
---

## Indicator

**Definitie:** Quick ratio wordt gedefinieerd als (Vlottende activa (incl. liquide middelen) - Voorraad) / (Kortlopende schulden)

**Teller:** (Vlottende activa (incl. liquide middelen) - Voorraad) op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Kortlopende schulden op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de Quick ratio van de organisatie. Grotere kortlopende schulden (passiva) dan kortlopende tegoeden (activa) betekent dat deze schulden op korte termijn niet afgelost zouden kunnen worden.

## Uitgangspunten

* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Quick ratio = (*"B Vlottende activa"* - *"B.I Voorraden"*) / *"G Kortlopende schulden (ten hoogste 1 jaar)"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental      | Waarde |
|-------------|--------|
| Quick ratio | Stap 1 |
