---
title: 20.4 - Solvabiliteit, Reserves
weight: n.t.b.
---

## Indicator

**Definitie:** De Reserves worden gedefinieerd als de som van Herwaarderingsreserve, Wettelijke en statutaire reserves, Bestemmingsreserve, Bestemmingsfonds Overige reserves en Onverdeelde winsten.

**Teller:** De som van Herwaarderingsreserve, Wettelijke en statutaire reserves, Bestemmingsreserve, Bestemmingsfonds Overige reserves en Onverdeelde winsten op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** n.v.t.

Deze indicator berekent de Reserves van de organisatie.

## Uitgangspunten

* De peildatum  is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Reserves = *"D.III Herwaarderingsreserve"* + *"D.IV Wettelijke en statutaire reserve"* + *"D.V Bestemmingsreserve"* + *"D.VI Bestemmingsfonds"* + *"D.VII Overige reserves"* + *"D.VIII Onverdeelde winst"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental   | Waarde |
|----------|--------|
| Reserves | Stap 1 |
