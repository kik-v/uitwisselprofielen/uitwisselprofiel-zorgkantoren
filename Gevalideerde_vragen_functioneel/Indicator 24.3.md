---
title: 24.3 - Overig - Kort vreemd vermogen t.o.v. lang vreemd vermogen
weight: n.t.b.
---

## Indicator

**Definitie:** Het kort vreemd vermogen t.o.v. lang vreemd vermogen wordt gedefinieerd als (kortlopende schulden) / (langlopende schulden)

**Teller:** Kortlopende schulden op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Langlopende schulden op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent het kort vreemd vermogen t.o.v. lang vreemd vermogen van de organisatie.

## Uitgangspunten

* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Kort vreemd vermogen t.o.v. lang vreemd vermogen = *"G Kortlopende schulden (ten hoogste 1 jaar)"* / *"F Langlopende schulden (nog voor meer dan een jaar)"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental                                           | Waarde |
|--------------------------------------------------|--------|
| Kort vreemd vermogen t.o.v. lang vreemd vermogen | Stap 1 |
