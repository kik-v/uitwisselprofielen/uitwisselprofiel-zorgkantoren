---
title: 14.1.5 Aantal cliënten per zorgprofiel - peildatum 1-1-2024
description: "Aantal cliënten per zorgprofiel op peildatum: per vestiging en in totaal met betrekking tot cliënten met een Wlz-indicatie met zorgprofiel VV."
weight: 14
---
## Indicator

**Definitie:** Aantal cliënten per zorgprofiel op peildatum: per vestiging en in totaal met betrekking tot cliënten met een Wlz-indicatie met zorgprofiel VV.

**Teller:** Aantal cliënten per zorgprofiel.

**Noemer:** Niet van toepassing.

## Toelichting 
Deze informatievraag betreft per vestiging het aantal cliënten per zorgprofiel op een peildatum. Het gaat om cliënten met een Wlz-indicatie met zorgprofiel VV1 t/m VV10.

## Uitgangspunten
* Personen met een Wlz-indicatie met zorgprofiel VV1 tot en met VV10 worden geïncludeerd.
* Per vestiging.
* Op een peildatum.

### Voorbeeld
Onderstaande tabel beschrijft een voorbeeld van de manier waarop cliënten A en B meetellen in elk van de indelingen. Deze cliënten beschikken op de peildatum respectievelijk over een Wlz-indicatie met zorgprofiel VV2 en VV8 en ontvangen zorg op de betreffende vestiging.

| Vestiging 1 |  |
| --- | --- |
| Zorgprofiel | Aantal |
| VV1 |  |
| VV2 | 1 |
| VV… |  |
| VV8 | 1 |
| VVN |  |
| Totaal | 2 |


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op de peildatum zorg ontvangen op de betreffende vestiging en beschikken over een Wlz-indicatie met zorgprofiel VV1 tot en met VV10.
2. Bepaal voor alle cliënten uit stap 1 het zorgprofiel (VV1 tot en met VV10).
3. Bereken op basis van stap 2 het aantal cliënten per zorgprofiel.
4. Bereken op basis van stap 1 het totaal aantal cliënten.

| Vestiging: |  |
| --- | --- |
| Zorgprofiel | Aantal |
| VV1 | Stap 3 |
| VV2 | Stap 3 |
| VV… | Stap 3 |
| VV8 | Stap 3 |
| VVN | Stap 3 |
| Totaal | Stap 4 |
