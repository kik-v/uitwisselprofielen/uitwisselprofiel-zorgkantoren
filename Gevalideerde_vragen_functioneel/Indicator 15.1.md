---
title: 15.1. Aantal wooneenheden
description: "Het aantal wooneenheden waarover een zorgaanbieder per vestiging en op organisatieniveau beschikt."
weight: 15
---
## Indicator

**Definitie:** Het aantal wooneenheden waarover een zorgaanbieder per vestiging en op organisatieniveau beschikt.

**Teller:** Aantal wooneenheden.

**Noemer:** Niet van toepassing.


## Toelichting

Deze indicator geeft de capaciteit van een zorgaanbieder op een bepaald moment aan. De capaciteit betreft het aantal wooneenheden dat geschikt is voor personen, zoals bijvoorbeeld cliënten en eventueel een partner. 

Deze indicator wordt op organisatieniveau en per vestiging berekend. 


## Uitgangspunten

* Het betreft wooneenheden waarin een of meerdere personen kunnen wonen, zoals cliënten en eventueel een partner. 
* Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een lokatie met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die lokatie wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’–eenheid’.
* De peildatum betreft het moment waarop de gegevens zijn geregisteerd in het bronsysteem en verwerkt in het datastation. Er wordt dus niet gewerkt met een nader te bepalen peildatum.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle wooneenheden.  
2. Bepaal per wooneenheid de vestiging.
3. Bereken het aantal wooneenheden per vestiging en voor de totale organisatie.

| Organisatieonderdeel | Aantal wooneenheden | 
|----|---|
| Totaal organisatie | Stap 3 | 
| Vestiging 1 | Stap 3 | 
| Vestiging 2 | Stap 3 | 
| Vestiging N | Stap 3 | 
