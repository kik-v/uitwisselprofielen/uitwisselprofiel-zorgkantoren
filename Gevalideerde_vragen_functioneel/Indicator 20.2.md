---
title: 20.2 - Solvabiliteit, Weerstandsvermogen
weight: n.t.b.
---

## Indicator

**Definitie:** Weerstandsvermogen wordt gedefinieerd als (Eigen vermogen) / (Opbrengsten)

**Teller:** Eigen vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Opbrengsten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de Weerstandsvermogen van de organisatie. In de zorg wordt weerstandsvermogen veelal berekend door het eigen vermogen te delen door de totale bedrijfsopbrengsten. Deze maat geeft aan hoeveel vermogen een instelling heeft om eventuele calamiteiten op te vangen.

## Uitgangspunten

* Peildatum Eigen vermogen is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.
* Meetperiode Opbrengsten is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
* Bij Peildatum 31-dec van het voorgaande jaar hoort de meetperiode 1-jan t/m 31-dec van het voorgaande jaar. Idem voor de andere combinatie in het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Weerstandsvermogen = *"D Eigen vermogen"* / *"P Som der bedrijfsopbrengsten"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental             | Waarde |
|--------------------|--------|
| Weerstandsvermogen | Stap 1 |
