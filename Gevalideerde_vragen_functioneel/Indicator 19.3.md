---
title: 19.3 - Liquiditeit, Werkkapitaalratio
weight: n.t.b.
---

## Indicator

**Definitie:** Werkkapitaalratio wordt gedefinieerd als (het werkkapitaal op korte termijn) / (Opbrengsten)

**Teller:** Werkkapitaal op kortetermijn op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Opbrengsten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent werkkapitaalratio van de organisatie.

## Uitgangspunten

* De peildatum Werkkapitaal is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.
* De meetperiode Opbrengsten is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
* Bij Peildatum 31-dec van het voorgaande jaar hoort de meetperiode 1-jan t/m 31-dec van het voorgaande jaar. Idem voor de andere combinatie in het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Werkkapitaalratio = Werkkapitaal op korte termijn [zie Indicator 19.2] / *"P Som der bedrijfsopbrengsten"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental            | Waarde |
|-------------------|--------|
| Werkkapitaalratio | Stap 1 |
