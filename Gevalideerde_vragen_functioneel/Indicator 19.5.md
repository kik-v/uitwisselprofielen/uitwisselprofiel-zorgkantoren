---
title: 19.5 - Liquiditeit, Current ratio
weight: n.t.b.
---

## Indicator

**Definitie:** Current ratio wordt gedefinieerd als (Vlottende activa (incl. liquide middelen)) / (Kortlopende schulden)

**Teller:** Vlottende activa (incl. liquide middelen) op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

**Noemer:** Kortlopende schulden op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Toelichting

Deze indicator berekent de Current ratio van de organisatie. Deze ratio geeft aan in hoeverre een zorgaanbieder kan voldoen aan de betalingsverplichtingen op korte termijn (< 1 jaar), oftewel de mate waarin de verschaffers van het kort vreemd vermogen uit de vlottende activa kunnen worden betaald.

## Uitgangspunten

* De peildatum is 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Current ratio = *"B Vlottende activa"* / *"G Kortlopende schulden (ten hoogste 1 jaar)"*

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental        | Waarde |
|---------------|--------|
| Current ratio | Stap 1 |
