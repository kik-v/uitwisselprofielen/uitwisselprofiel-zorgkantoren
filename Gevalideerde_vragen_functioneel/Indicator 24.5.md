---
title: 24.5 - Overig - Arbeidsintensiteit PIL & PNIL
weight: n.t.b.
---

## Indicator

**Definitie:** De arbeidsintensiteit PIL & PNIL wordt gedefinieerd als (personeelskosten) / (opbrengst).

**Teller:** Personeelskosten (zowel PIL als PNIL) over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

**Noemer:** Opbrengst over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.

## Toelichting

Deze indicator berekent de arbeidsintensiteit PIL & PNIL van de organisatie.

## Uitgangspunten

* De meetperiode is 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.

## Berekening

Deze indicator wordt als volgt berekend:

1. Arbeidsintensiteit PIL & PNIL = (*"Q.III Lonen en salarissen"* + *"Q.IV Sociale lasten"* + *"Q.V Pensioenlasten"* + *"Q.II Kosten uitbesteed werk en andere externe kosten"*) / *"P Som der bedrijfsopbrengsten"*
2. Arbeidsintensiteit PIL & PNIL% = Arbeidsintensiteit PIL & PNIL * 100

De schuingedrukte begrippen zijn gedefinieerd in indicator 18.1 en 18.2.

| Kental                         | Waarde |
|--------------------------------|--------|
| Arbeidsintensiteit PIL & PNIL  | Stap 1 |
| Arbeidsintensiteit PIL & PNIL% | Stap 2 |
