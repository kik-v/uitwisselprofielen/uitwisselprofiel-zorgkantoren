---
title: 7.1.7 Aantal ingezette uren aan personeel in loondienst per cliënt Q3 2024
description: "Het aantal ingezette uren aan personeelsleden in loondienst met een zorggerelateerde functie per vestiging per kwartaal ten op zichte van het aantal cliënten per vestiging per kwartaal. Ingezette uren zijn inclusief meeruren, overuren en oproepuren."
weight: 7
---
## Indicator

**Definitie:** Het aantal ingezette uren aan personeelsleden in loondienst met een zorggerelateerde functie per vestiging per kwartaal ten op zichte van het aantal cliënten per vestiging per kwartaal. Ingezette uren zijn inclusief meeruren, overuren en oproepuren.

**Teller:** Aantal ingezette uren aan personeelsleden in loondienst met een zorggerelateerde functie met een arbeidsovereenkomst.

**Noemer:** Aantal cliënten met een Wlz-indicatie met zorgprofiel VV4 tot en met VV10.

## Toelichting
Deze indicator betreft het aantal geregistreerde uren aan personeelsleden in loondienst met een zorg gerelateerde functie ten opzichte van het aantal cliënten met een Wlz-indicatie met zorgprofiel VV4 tot en met VV10.

Deze teller betreft het aantal geregistreerde uren van personeelsleden in loondienst met een zorggerelateerde functie, en binnen de verslagperiode over een arbeidsovereenkomst beschikte. Op basis van deze uren kan het aantal ingezette fte’s van deze personeelsleden berekend worden. Iedere afnemer van gegevens kan op basis van het totaal aantal uren zijn/haar eigen definitie van fte toepassen, zoals 36, 38 of 40 uur per week of 1692 uren per jaar. Voor de berekening van uren worden meeruren, overuren en oproepuren meegerekend.

De noemer betreft het totaal aantal geregistreerde cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10 in het kwartaal.

Deze indicator wordt op concernniveau en per vestiging berekend, voor personeelsleden met zorg gerelateerde functie. De verslagperiode betreft een kwartaal.

## Uitgangspunten

* Per vestiging en voor de gehele organisatie.
* Personen die tegelijkertijd over een of meerdere arbeidsovereenkomsten beschikten, tellen mee in de berekening wanneer de uren geregistreerd zijn als ‘zorgverlener’ (o.b.v. de functie in de arbeidsovereenkomst).
* Om deze indicator te kunnen berekenen dienen de uren beschikbaar te zijn vanuit de urenregistratie.
* De geregistreerde ingezette uren, zijn uren exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.
* Toebedeling van ingezette uren naar een vestiging is op basis van de vestiging in de urenregistratie. Indien deze niet beschikbaar zijn, worden de uren toebedeeld aan de vestiging gerelateerd aan de arbeidsovereenkomst. 


## Berekening

Deze indicator wordt als volgt berekend:
1.  Selecteer alle personeelsleden in loondienst die (o.b.v. hun functie in de arbeidsovereenkomst) ‘zorgverlener’ zijn.
2.  Tel per persoon per vestiging het aantal ingezette uren die de persoon heeft geregistreerd in de verslagperiode.
3.  Bereken het aantal uren per vestiging en in totaal door alle geselecteerde uren van alle geselecteerde personen per vestiging bij elkaar op te tellen.
4. Selecteer alle cliënten met een Wlz-indicatie met zorgprofiel VV4 – VV10 die in het kwartaal zorg ontvangen op de betreffende vestiging. 
5. Bereken op basis van het resultaat uit stap 4 het aantal cliënten per vestiging en voor de totale organisatie.
6. Bereken de indicator door het resultaat uit stap 3 te delen door het resultaat uit stap 5.

Kwartaal: dd-mm-jjjj t/m dd-mm-jjjj

| Organisatieonderdeel | Ingezette uren aan personeel in loondienst met een zorggerelateerde functie | Aantal cliënten VV4  - VV10 | Ingezette uren per cliënt | 
| --- | --- | --- | --- |
| Totaal organisatie | Stap 3 | Stap 5 | Stap 6 |
| Vestiging 1 | Stap 3 | Stap 5 | Stap 6 |
| Vestiging 2 | Stap 3 | Stap 5 | Stap 6 |
| Vestiging N | Stap 3 | Stap 5 | Stap 6 |
