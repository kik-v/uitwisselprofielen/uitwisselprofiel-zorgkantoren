---
title: 14.3.4 Aantal cliënten per zorgprofiel per leveringsvorm - peildatum 1-10-2023
description: "Aantal cliënten per zorgprofiel op peildatum: per vestiging met betrekking tot financieringsstroom Wlz, zorgprofiel VV per leveringsvorm."
weight: 14
---
## Indicator

**Definitie:** Aantal cliënten per zorgprofiel op peildatum: per vestiging met betrekking tot financieringsstroom Wlz, zorgprofiel VV per leveringsvorm.

**Teller:** Aantal cliënten per zorgprofiel per leveringsvorm.

**Noemer:** Niet van toepassing.

## Toelichting 

Deze informatievraag betreft per vestiging het aantal cliënten per zorgprofiel, per leveringsvorm op een peildatum. Het gaat om cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10.


## Uitgangspunten

* Personen die zorg ontvangen op de betreffende vestiging en beschikken over een Wlz-indicatie met zorgprofiel VV4 tot en met VV10 worden geïncludeerd.
* Per vestiging.

**Voorbeeld**

Onderstaande tabel beschrijft een voorbeeld van de manier waarop cliënten A en B meetellen in elk van de indelingen. Deze cliënten beschikken op de peildatum over respectievelijk een Wlz-indicatie met zorgprofiel VV5 met behandeling en VV8 zonder behandeling en verblijven op de betreffende vestiging.

|     Vestiging 1      |     Leveringsvorm                |                                      |               |              |
|--------------------|----------------------------------|--------------------------------------|------------|--------------|
|     Zorgprofiel    |     Verblijf  met behandeling    |     Verblijf   zonder behandeling    |         MPT    |       PGB    |
|     VV4            |                                  |                                      |                 |              |
|     VV5            |     1                            |                                      |                |              |
|     VV…            |                                  |                                      |                  |              |
|     VV8            |                                  |     1                                |                 |              |
|     VVN            |                                  |                                      |                  |              |
|     Totalen        |     1                            |     1                                |          0      |     0        |


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op de peildatum zorg ontvangen op de betreffende vestiging en beschikken over een Wlz-indicatie met zorgprofiel VV4 tot en met VV10.
2. Bepaal voor alle cliënten uit stap 1 de leveringsvorm (Verblijf, MPT of PGB).
3. Bepaal voor alle cliënten met leveringsvorm Verblijf uit stap 2 of dit met of zonder behandeling is.
4. Bepaal voor alle cliënten uit stap 3 het zorgprofiel (VV4 tot en met VV10).
5. Bereken op basis van stap 2 het aantal cliënten per leveringsvorm MPT en PGB.
6. Bereken op basis van stap 3 het aantal cliënten met en zonder behandeling voor de leveringsvorm Verblijf.
7. Bereken op basis van stap 5 het aantal cliënten per zorgprofiel, per leveringsvorm MPT en PGB.
8. Bereken op basis van stap 6 het aantal cliënten per zorgprofiel met en zonder behandeling voor de leveringsvorm Verblijf.
9. Bereken het aantal clienten per zorgprofiel.
10. Bereken op basis van stap 1 het totaal aantal cliënten.

|     Vestiging 1      |                                        |                                      |                      |               |                |
|--------------------|----------------------------------------|--------------------------------------|---------------|---------------|----------------|
|     Zorgprofiel    |     Verblijf met        behandeling    |     Verblijf zonder   behandeling    |         MPT       |     PGB       |     Totaal     |
|     VV4            |     Stap 8                             |     Stap 8                           |         Stap 7    |     Stap 7    |     Stap 9     |
|     VV5            |     Stap 8                             |     Stap 8                           |       Stap 7    |     Stap 7    |     Stap 9     |
|     VV…            |     Stap 8                             |     Stap 8                           |        Stap 7    |     Stap 7    |     Stap 9     |
|     VV8            |     Stap 8                             |     Stap 8                           |       Stap 7    |     Stap 7    |     Stap 9     |
|     VVN            |     Stap 8                             |     Stap 8                           |          Stap 7    |     Stap 7    |     Stap 9     |
|     Totalen        |     Stap 6                             |     Stap 6                           |         Stap 5    |     Stap 5    |     Stap 10    |
