def indicator_zk_2_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = 0
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = 0
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        contract_periode = werkovereenkomstafspraak.get_periode()
                        meetperiode = trim_to_period(contract_periode, periode)
                        if meetperiode:
                            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                            for gewerkte_periode in gewerkte_perioden:
                                if date_in_period(gewerkte_periode.get_date(), meetperiode):
                                        werklocatie = gewerkte_periode.get_locatie().split('/')[-1]
                                        if werklocatie:
                                            indicator[werklocatie] += gewerkte_periode.get_uren()
                                        else:
                                            indicator[locatie] += gewerkte_periode.get_uren()
                                        indicator["Totaal"] += gewerkte_periode.get_uren()
    output_string = f"\nZK 2.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ': ' + str(indicator[vestiging])
    return (output_string)