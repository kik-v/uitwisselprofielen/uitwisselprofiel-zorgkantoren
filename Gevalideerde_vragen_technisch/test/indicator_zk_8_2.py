def indicator_zk_8_2(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 8.2 - Percentage kosten personeel niet in loondienst (PNIL): {start_periode}, eind_periode: {eind_periode}'
    print(output_string)
    totaal_pil, totaal_pnil = 0, 0
    rubrieken_pil = {
        "WPerLes",
        "WPerSol",
        "411000",
        "412000",
        "413000",
        "414000",
        "415000",
        "422100",
        "422300",
        "422400",
        "422410",
        "422500",
        "422600",
        "422900",
    }
    rubrieken_pil = expand_rubrieken(rubrieken_pil)  # expand list with children rubrieken for RGS
    
    rubrieken_pnil = [
        "WBedOvpUik",
        "WBedOvpUit",
        "WBedOvpMaf",
        "WBedOvpZzp",
        "WBedOvpPay",
        "WBedOvpOip",
        "418000"
    ]
    rubrieken_pnil = expand_rubrieken(rubrieken_pnil)  # expand list with children rubrieken for RGS
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken_pil:
                totaal_pil += post.price
            if rubriek_id in rubrieken_pnil:
                totaal_pnil += post.price
    totaal = totaal_pnil + totaal_pil

    output_string += f'\n    Totaal organisatie: Kosten PIL: {round(totaal_pil, 2)}, Kosten PNIL: {round(totaal_pnil, 2)}, Totaal:{round(totaal, 2)}'
    return output_string

# file.write(indicator_zk_8_2(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')