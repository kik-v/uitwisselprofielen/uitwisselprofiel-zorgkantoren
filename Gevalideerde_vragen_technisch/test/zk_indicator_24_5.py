def indicator_zk_24_5(start_periode, eind_periode, test_instances):
    output_string = f'\n24.5 - Arbeidsintensiteit PIL & PNIL, start_periode: {start_periode}, eind_periode: {eind_periode}'
    # Arbeidsintensiteit PIL & PNIL = ("Q.III Lonen en salarissen" + "Q.IV Sociale lasten" + "Q.V Pensioenlasten" + "Q.II Kosten uitbesteed werk en andere externe kosten") / "P Som der bedrijfsopbrengsten"
    # Arbeidsintensiteit PIL & PNIL% = Arbeidsintensiteit PIL & PNIL * 100

    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
    }

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price

    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
    totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
    totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
    totalen['P.IV Overige bedrijfsopbrengsten'] )

    if totalen['P Som der bedrijfsopbrengsten'] != 0 :
        waarde_1 = (totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
                  totalen['Q.III Lonen en salarissen'] +
                  totalen['Q.IV Sociale lasten'] + 
                  totalen['Q.V Pensioenlasten'])/totalen['P Som der bedrijfsopbrengsten']
        waarde_2 = 100*((totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
                  totalen['Q.III Lonen en salarissen'] +
                  totalen['Q.IV Sociale lasten'] + 
                  totalen['Q.V Pensioenlasten'])/totalen['P Som der bedrijfsopbrengsten'])
    else:
        waarde_1 = 'ongedefineerd'
        waarde_2 = 'ongedefineerd'

    output_string += '\n    Kental: Arbeidsintensiteit PIL & PNIL'
    output_string += f'\n    Waarde: {round(waarde_1, 4)}'
    output_string += '\n    Kental: Arbeidsintensiteit PIL & PNIL%'
    output_string += f'\n    Waarde: {round(waarde_2, 4)}'

    return output_string

    # file.write(indicator_zk_24_5(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')