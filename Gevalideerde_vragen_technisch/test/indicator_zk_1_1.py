def indicator_zk_1_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["zorg"] = 0
    indicator["Totaal"]["niet-zorg"] = 0
    indicator["Totaal"]["totaal"] = 0

    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    dagen_periode = days_in_period(periode)
    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    zorg_functie = werkovereenkomstafspraak.get_functie() in ZorgFuncties
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    meetperiode = trim_to_period(contract_periode, periode)
                    if meetperiode:
                        if locatie not in indicator:
                            indicator[locatie] = {}
                            indicator[locatie]["zorg"] = 0
                            indicator[locatie]["niet-zorg"] = 0
                            indicator[locatie]["totaal"] = 0
                        indicator[locatie]["totaal"] += days_in_period(meetperiode)/dagen_periode
                        indicator["Totaal"]["totaal"] += days_in_period(meetperiode)/dagen_periode
                        if zorg_functie:
                            indicator[locatie]["zorg"] += days_in_period(meetperiode)/dagen_periode
                            indicator["Totaal"]["zorg"] += days_in_period(meetperiode)/dagen_periode
                        else:
                            indicator[locatie]["niet-zorg"] += days_in_period(meetperiode)/dagen_periode
                            indicator["Totaal"]["niet-zorg"] += days_in_period(meetperiode)/dagen_periode
    output_string = f"ZK 1.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ':'
        for zorg in indicator[vestiging]:
            output_string += ' ' + zorg + ': ' + str(indicator[vestiging][zorg])
    return (output_string)
