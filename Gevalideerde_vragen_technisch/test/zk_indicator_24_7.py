def indicator_zk_24_7(peildatum, test_instances):
    start_periode = date(peildatum.year, 1, 1)
    eind_periode = peildatum
    output_string = f'\n24.7 - Arbeidsintensiteit PNIL, peildatum {peildatum}'
    # output_string = f'\n24.7 - Arbeidsintensiteit PNIL, start_periode: {start_periode}, eind_periode: {eind_periode}'
    # Arbeidsintensiteit PNIL = "Q.II Kosten uitbesteed werk en andere externe kosten" / "P Som der bedrijfsopbrengsten"
    # Arbeidsintensiteit PNIL% = Arbeidsintensiteit PNIL * 100
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
    }
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81' and start_periode <= post.datum :
                totalen['P.I Netto omzet'] += post.price
                # print(f'P.I : {post.subject}, price {post.price}')
            elif (rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']) and start_periode <= post.datum :
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
                # print(f'P.IV : {post.subject}, price {post.price}')
            elif rubriek_id[:3] in ['417', '418']:
                # print(f'Q.II : {post.subject}, price {post.price}')
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price

    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
    totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
    totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
    totalen['P.IV Overige bedrijfsopbrengsten'] )
    p_total = totalen['P Som der bedrijfsopbrengsten'] 
    q2_total = totalen['Q.II Kosten uitbesteed werk en andere externe kosten']
    print(f'P: {p_total}, Q2: {q2_total}')

    if totalen['P Som der bedrijfsopbrengsten'] != 0 :
        waarde = totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] / totalen['P Som der bedrijfsopbrengsten']
        waarde = round(waarde, 4)
        waarde_percent = 100 * waarde    
    else:
        waarde = 'ongedefineerd'
        waarde_percent = 'ongedefinieerd'

    output_string += '\n    Kental: Arbeidsintensiteit PNIL'
    output_string += f'\n    Waarde: {waarde}'
    output_string += '\n    Kental: Arbeidsintensiteit PNIL%'
    output_string += f'\n    Waarde: {waarde_percent}'

    return output_string

# file.write(indicator_zk_24_7(date(2024,12,31), test_grootboek_instances) + '\n')
