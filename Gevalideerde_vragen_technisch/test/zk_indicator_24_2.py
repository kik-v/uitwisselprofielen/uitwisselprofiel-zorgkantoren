def indicator_zk_24_2(peildatum, test_instances):
    output_string = f'\nZK 24.2, ICR (Interest Coverage Ratio), peildatum: {peildatum}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
        'R.IV Rentelasten en soortgelijke kosten': 0,
    }

    period_start = date(peildatum.year,1,1)
    
    # https://gitlab.com/kik-v/uitwisselprofielen/uitwisselprofiel-zorgkantoren/-/blob/dev/Gevalideerde_vragen_functioneel/Indicator%2024.2.md
    # Teller: EBIT over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
    # Noemer: Rentelasten over meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-6 van het lopende jaar.
    # EBIT = "P Som der bedrijfsopbrengsten" - "Q Som der bedrijfslasten"
    # ICR = EBIT / "R.IV Rentelasten en soortgelijke kosten"

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if period_start <= post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
            elif rubriek_id[:3] in ['485', '901']:
                totalen['R.IV Rentelasten en soortgelijke kosten'] += post.price

    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
    
    ebit = totalen['P Som der bedrijfsopbrengsten'] - totalen['Q Som der bedrijfslasten']
    
    if totalen['R.IV Rentelasten en soortgelijke kosten'] != 0:
        waarde = ebit / totalen['R.IV Rentelasten en soortgelijke kosten']
        waarde = round(waarde, 8)
    else:
        waarde = 'ongedefineerd'

    output_string += '\n    Kental: ICR'
    output_string += f'\n    Waarde: {waarde}'
    if 1: 
        output_string += '\n    P: ' + str(totalen['P Som der bedrijfsopbrengsten'])
        output_string += '\n    Q: ' + str(totalen['Q Som der bedrijfslasten'])
        output_string += '\n    R4: ' + str(totalen['R.IV Rentelasten en soortgelijke kosten'])

    return output_string

#file.write(indicator_zk_24_2(date(2024,6,30), test_grootboek_instances) + '\n')
