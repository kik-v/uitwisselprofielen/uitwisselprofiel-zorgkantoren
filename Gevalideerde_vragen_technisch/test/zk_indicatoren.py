from variables import *
from functions import *
from rdf_client_objecten import *
from rdf_fin_objecten import *

def indicator_zk_19_6(start_periode, eind_periode, test_instances):
    output_string = f'\n19.6 - Liquiditeit, EBITDA (Earnings Before Interest, Tax, Deprecation and Amortisation), start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
    
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
        
    for jaarrekeningpost in ['P Som der bedrijfsopbrengsten', 'Q Som der bedrijfslasten', 'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    # EBITDA = "P Som der bedrijfsopbrengsten" - "Q Som der bedrijfslasten" + "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"
    ebitda = totalen['P Som der bedrijfsopbrengsten'] - totalen['Q Som der bedrijfslasten'] + totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']
    output_string += f'\n    EBITDA: {round(ebitda,2)}'
    return (output_string)

def indicator_zk_19_7(start_periode, eind_periode, test_instances):
    output_string = f'\n19.7 - Liquiditeit, EBITDA-ratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
    
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
        
    for jaarrekeningpost in ['P Som der bedrijfsopbrengsten', 'Q Som der bedrijfslasten', 'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    # EBITDA = "P Som der bedrijfsopbrengsten" - "Q Som der bedrijfslasten" + "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"
    ebitda = totalen['P Som der bedrijfsopbrengsten'] - totalen['Q Som der bedrijfslasten'] + totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']
    # EBITDA-ratio = EBITDA [zie Indicator 19.6] / "P Som der bedrijfsopbrengsten"
    ebitda_ratio = ebitda / totalen['P Som der bedrijfsopbrengsten']
    output_string += f'\n    EBITDA: {round(ebitda_ratio,2)}'
    return (output_string)

def indicator_zk_20_1(start_periode, eind_periode, test_instances):
    output_string = f'\n20.1 - Solvabiliteitsratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'D Eigen vermogen': 0,
        'D.I Gestort en opgevraagd kapitaal': 0,
        'D.II Agio': 0,
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0,
        'E Voorzieningen': 0,
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
        'H Totaal passiva': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:4] == '0511':
                totalen['D.I Gestort en opgevraagd kapitaal'] += post.price
            elif rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
            elif rubriek_id[:2] == '06':
                totalen['E Voorzieningen'] += post.price
            elif rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price

    totalen['D Eigen vermogen'] =(totalen['D.I Gestort en opgevraagd kapitaal'] + 
        totalen['D.II Agio'] + 
        totalen['D.III Herwaarderingsreserve'] + 
        totalen['D.IV Wettelijke en statutaire reserve'] +
        totalen['D.V Bestemmingsreserve'] + 
        totalen['D.VI Bestemmingsfonds'] +
        totalen['D.VII Overige reserves'] + 
        totalen['D.VIII Onverdeelde winst'] )
    totalen['H Totaal passiva'] =(totalen['D Eigen vermogen'] + 
        totalen['E Voorzieningen'] + 
        totalen['F Langlopende schulden (nog voor meer dan een jaar)'] +
        totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] )
        
    for jaarrekeningpost in ['D Eigen vermogen', 'H Totaal passiva']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    
    if totalen['H Totaal passiva'] != 0:
        # Solvabiliteitsratio = "D Eigen vermogen" / "H Totaal passiva"
        solvabiliteitsratio = totalen['D Eigen vermogen'] / totalen['H Totaal passiva']
        output_string += f'\n    Solvabiliteitsratio: {round(solvabiliteitsratio,4)}'
    else:
        output_string += f'\n    Solvabiliteitsratio: [undefined]'
    return (output_string)

def indicator_zk_20_2(start_periode, eind_periode, test_instances):
    output_string = f'\n20.2 - Weerstandsvermogen, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'D Eigen vermogen': 0,
        'D.I Gestort en opgevraagd kapitaal': 0,
        'D.II Agio': 0,
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0,
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:4] == '0511':
                totalen['D.I Gestort en opgevraagd kapitaal'] += post.price
            elif rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
            elif rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price

    totalen['D Eigen vermogen'] =(totalen['D.I Gestort en opgevraagd kapitaal'] + 
        totalen['D.II Agio'] + 
        totalen['D.III Herwaarderingsreserve'] + 
        totalen['D.IV Wettelijke en statutaire reserve'] +
        totalen['D.V Bestemmingsreserve'] + 
        totalen['D.VI Bestemmingsfonds'] +
        totalen['D.VII Overige reserves'] + 
        totalen['D.VIII Onverdeelde winst'] )
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )

        
    for jaarrekeningpost in ['D Eigen vermogen', 'P Som der bedrijfsopbrengsten']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    
    if totalen['P Som der bedrijfsopbrengsten'] != 0:
        # Weerstandsvermogen = "D Eigen vermogen" / "P Som der bedrijfsopbrengsten"
        weerstandsvermogen = totalen['D Eigen vermogen'] / totalen['P Som der bedrijfsopbrengsten']
        output_string += f'\n    Weerstandsvermogen: {round(weerstandsvermogen,4)}'
    else:
        output_string += f'\n    Weerstandsvermogen: [undefined]'
    return (output_string)

def indicator_zk_21_1(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 21.1, Winstratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
        'R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten': 0,
        'R.II Andere rentebaten en soortgelijke opbrengsten': 0,
        'R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten': 0,
        'R.IV Rentelasten en soortgelijke kosten': 0,
        'R Resultaat voor belastingen': 0,
        'S.I Belastingen': 0,
        'S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen': 0,
        'S Resultaat na belastingen': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
            elif rubriek_id[:3] == '904':
                totalen['R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten'] += post.price
            elif rubriek_id[:3] == '900':
                totalen['R.II Andere rentebaten en soortgelijke opbrengsten'] += post.price
            elif rubriek_id[:3] == '903':
                totalen['R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten'] += post.price
            elif rubriek_id[:3] in ['485', '901']:
                totalen['R.IV Rentelasten en soortgelijke kosten'] += post.price
            elif rubriek_id[:3] == '902':
                totalen['S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen'] += post.price
    
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
    # R = P - Q + R.I + R.II - R.III - R.IV
    totalen['R Resultaat voor belastingen'] = (totalen['P Som der bedrijfsopbrengsten'] -
        totalen['Q Som der bedrijfslasten'] +
        totalen['R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten'] +
        totalen['R.II Andere rentebaten en soortgelijke opbrengsten'] -
        totalen['R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten'] -
        totalen['R.IV Rentelasten en soortgelijke kosten'] )
    totalen['S Resultaat na belastingen'] =(totalen['R Resultaat voor belastingen'] - 
        totalen['S.I Belastingen'] + 
        totalen['S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen'] )
        
    for jaarrekeningpost in ['P Som der bedrijfsopbrengsten', 'S Resultaat na belastingen']:
        output_string += f'\n    {jaarrekeningpost}: {totalen[jaarrekeningpost]}'

    if totalen['P Som der bedrijfsopbrengsten'] != 0: 
        winstratio = totalen['S Resultaat na belastingen'] / totalen['P Som der bedrijfsopbrengsten']
        output_string += f'\n    Winstratio: {round(winstratio,2)}'
    else:
        output_string += f'\n    Winstratio: undefined'
    return (output_string)

def indicator_zk_21_2(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 21.2 - Rentabiliteit, Resultaat Eigen Vermogen (REV), start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'D Eigen vermogen': 0,
        'D.I Gestort en opgevraagd kapitaal': 0,
        'D.II Agio': 0,
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0,
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
        'R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten': 0,
        'R.II Andere rentebaten en soortgelijke opbrengsten': 0,
        'R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten': 0,
        'R.IV Rentelasten en soortgelijke kosten': 0,
        'R Resultaat voor belastingen': 0,
        'S.I Belastingen': 0,
        'S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen': 0,
        'S Resultaat na belastingen': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:4] == '0511':
                totalen['D.I Gestort en opgevraagd kapitaal'] += post.price
            elif rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
            elif rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
            elif rubriek_id[:3] == '904':
                totalen['R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten'] += post.price
            elif rubriek_id[:3] == '900':
                totalen['R.II Andere rentebaten en soortgelijke opbrengsten'] += post.price
            elif rubriek_id[:3] == '903':
                totalen['R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten'] += post.price
            elif rubriek_id[:3] in ['485', '901']:
                totalen['R.IV Rentelasten en soortgelijke kosten'] += post.price
            elif rubriek_id[:3] == '902':
                totalen['S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen'] += post.price
    
    totalen['D Eigen vermogen'] =(totalen['D.I Gestort en opgevraagd kapitaal'] + 
        totalen['D.II Agio'] + 
        totalen['D.III Herwaarderingsreserve'] + 
        totalen['D.IV Wettelijke en statutaire reserve'] +
        totalen['D.V Bestemmingsreserve'] + 
        totalen['D.VI Bestemmingsfonds'] +
        totalen['D.VII Overige reserves'] + 
        totalen['D.VIII Onverdeelde winst'] )
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
    # R = P - Q + R.I + R.II - R.III - R.IV
    totalen['R Resultaat voor belastingen'] = (totalen['P Som der bedrijfsopbrengsten'] -
        totalen['Q Som der bedrijfslasten'] +
        totalen['R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten'] +
        totalen['R.II Andere rentebaten en soortgelijke opbrengsten'] -
        totalen['R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten'] -
        totalen['R.IV Rentelasten en soortgelijke kosten'] )
    totalen['S Resultaat na belastingen'] =(totalen['R Resultaat voor belastingen'] - 
        totalen['S.I Belastingen'] + 
        totalen['S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen'] )
        
    for jaarrekeningpost in ['D Eigen vermogen', 'P Som der bedrijfsopbrengsten', 'S Resultaat na belastingen']:
        output_string += f'\n    {jaarrekeningpost}: {totalen[jaarrekeningpost]}'

    if totalen['D Eigen vermogen'] != 0: 
        # REV = "S Resultaat na belastingen" / "D Eigen vermogen"
        winstratio = totalen['S Resultaat na belastingen'] / totalen['D Eigen vermogen']
        output_string += f'\n    REV: {round(winstratio,2)}'
    else:
        output_string += f'\n    REV: undefined'
    return (output_string)

def indicator_zk_22_1(start_periode, eind_periode, test_instances):
    output_string = f'\n22.1 - Groei, Omzetgroei, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
    }
    totalen_old = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
    }
    start_periode_old = start_periode - relativedelta(years=1)
    eind_periode_old = eind_periode - relativedelta(years=1)

    print(start_periode_old)
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
        
        if start_periode_old <= post.datum <= eind_periode_old :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen_old['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen_old['P.IV Overige bedrijfsopbrengsten'] += post.price
                
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
    totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
    totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
    totalen['P.IV Overige bedrijfsopbrengsten'] )

    totalen_old['P Som der bedrijfsopbrengsten'] = (totalen_old['P.I Netto omzet'] + 
    totalen_old['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
    totalen_old['P.III Geactiveerde productie voor het eigen bedrijf'] +
    totalen_old['P.IV Overige bedrijfsopbrengsten'] )
    if (totalen_old['P Som der bedrijfsopbrengsten'] != 0):
        waarde = totalen['P Som der bedrijfsopbrengsten']/totalen_old['P Som der bedrijfsopbrengsten']
        output_string += '\n    Kental: Omzetgroei'
        output_string += f'\n    Waarde: {round(waarde, 4)}'
        waarde_percent = 100*totalen['P Som der bedrijfsopbrengsten']/totalen_old['P Som der bedrijfsopbrengsten']
        output_string += '\n    Kental: Omzetgroei %'
        output_string += f'\n    Waarde: {round(waarde_percent, 4)}'
    else:
        output_string += '\n undefined'

    return (output_string)

def indicator_zk_23_1(peildatum, test_instances):
    #LTV = "F Langlopende schulden (nog voor meer dan een jaar)" / "A.III Financiële vaste activa"
    output_string = f'\n23.1 - Kapitaal, Loan to Value (LTV), peildatum: {peildatum}'
    
    totalen = {
        'A.III Financiële vaste activa': 0,
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '03':
                totalen['A.III Financiële vaste activa'] += post.price
            elif rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price
        
    waarde = totalen['F Langlopende schulden (nog voor meer dan een jaar)']/totalen['A.III Financiële vaste activa']
    output_string += '\n    Kental: LTV'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    return output_string


file.write(indicator_zk_19_6(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
file.write(indicator_zk_19_7(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
file.write(indicator_zk_20_1(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
file.write(indicator_zk_20_2(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
file.write(indicator_zk_21_1(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
file.write(indicator_zk_21_2(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
file.write(indicator_zk_22_1(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
file.write(indicator_zk_23_1(date(2025,1,1), test_grootboek_instances) + '\n')