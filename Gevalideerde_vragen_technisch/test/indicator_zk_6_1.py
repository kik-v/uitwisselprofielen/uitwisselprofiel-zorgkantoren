def indicator_zk_6_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                for gewerkte_periode in gewerkte_perioden:
                    dag = gewerkte_periode.get_date()
                    if date_in_period(date=dag, period=periode):
                        werkovereenkomstafspraak = get_afspraak_on_date(werk_overeenkomst=werkovereenkomst, date=dag)
                        functie = werkovereenkomstafspraak.get_functie()
                        if functie in ZorgFuncties:
                            functie_niveau = FunctieNiveaus[functie]
                            uren = gewerkte_periode.get_uren()
                            locatie = gewerkte_periode.get_locatie()
                            if not locatie:
                                locatie = werkovereenkomstafspraak.get_locatie()
                            if locatie:
                                if locatie not in indicator:
                                    indicator[locatie] = {}
                                if functie_niveau not in indicator[locatie]:
                                    indicator[locatie][functie_niveau] = 0
                                if functie_niveau not in indicator["Totaal"]:
                                    indicator["Totaal"][functie_niveau] = 0
                                indicator[locatie][functie_niveau] += uren
                                indicator["Totaal"][functie_niveau] += uren
                                print(gewerkte_periode.get_subject(), dag, uren)
    output_string = f"\nZK 6.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in sorted(indicator.keys()):
        output_string += '\n    ' + str(vestiging) + ':'
        for kwalificatieniveau in sorted(indicator[vestiging].keys()):
            output_string += '\n         ' + str(kwalificatieniveau) + ': ' + str(indicator[vestiging][kwalificatieniveau])
    return (output_string)