def indicator_zk_1_2(datum, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["zorg"] = set()
    indicator["Totaal"]["niet-zorg"] = set()
    indicator["Totaal"]["totaal"] = set()

    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    zorg_functie = werkovereenkomstafspraak.get_functie() in ZorgFuncties
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if date_in_period(datum, contract_periode):
                        if locatie not in indicator:
                            indicator[locatie] = {}
                            indicator[locatie]["zorg"] = set()
                            indicator[locatie]["niet-zorg"] = set()
                            indicator[locatie]["totaal"] = set()
                        indicator[locatie]["totaal"].add(mens)
                        indicator["Totaal"]["totaal"].add(mens)
                        if zorg_functie:
                            indicator[locatie]["zorg"].add(mens)
                            indicator["Totaal"]["zorg"].add(mens)
                        else:
                            indicator[locatie]["niet-zorg"].add(mens)
                            indicator["Totaal"]["niet-zorg"].add(mens)
    output_string = f"\nZK 1.2 indicator datum: {datum}"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ':'
        for zorg in indicator[vestiging]:
            output_string += ' ' + zorg + ': ' + str(len(indicator[vestiging][zorg]))
    return (output_string)

# indicator_zk_1_2(date(2024,6,12), mensen)
