def indicator_zk_9_1(jaar, kwartaal, mensen):
    indicator = {}
    indicator["Totaal"] = []
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = []

    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() == onzpers.VrijwilligersOvereenkomst:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if period1_inperiod2(period1=periode, period2=werkovereenkomstafspraak.get_periode()):
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        if mens.get_subject() not in indicator[locatie]:
                            indicator[locatie].append(mens.get_subject())
                        if mens.get_subject() not in indicator["Totaal"]:    
                            indicator["Totaal"].append(mens.get_subject())
    output_string = f"\nZK 9.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in sorted(indicator.keys()):
        output_string += '\n    ' + str(vestiging) + ':' + str(len(indicator[vestiging]))
    output_string += '\n'
    return (output_string)