def indicator_zk_20_5(peildatum, test_instances):
    # Reserves-Ratio = Reserves [zie Indicator 20.4]  / "P Som der bedrijfsopbrengsten"
    output_string = f'\n20.5 - Reserves-Ratio, peildatum {peildatum}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0,
        'Reserve' :0
    }

    eind_periode = peildatum
    start_periode = eind_periode - relativedelta(years= 1)

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            # D.III leeg
            if rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
                
            
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price

    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    
#      Reserves = "D.III Herwaarderingsreserve" + "D.IV Wettelijke en statutaire reserve" + 
#      "D.V Bestemmingsreserve" + "D.VI Bestemmingsfonds" + "D.VII Overige reserves" + 
#      "D.VIII Onverdeelde winst"
    
    totalen['Reserve'] =(
        totalen['D.III Herwaarderingsreserve'] + 
        totalen['D.IV Wettelijke en statutaire reserve'] +
        totalen['D.V Bestemmingsreserve'] + 
        totalen['D.VI Bestemmingsfonds'] +
        totalen['D.VII Overige reserves'] + 
        totalen['D.VIII Onverdeelde winst'] )
         
    if totalen['P Som der bedrijfsopbrengsten'] != 0:
        waarde = (totalen['Reserve'] / totalen['P Som der bedrijfsopbrengsten'])
        output_string += f'\n    Reserves-Ratio: {round(waarde, 4)}'
    else :
        output_string += f'\n    Reserves-Ratio: ongedefineerd'

    return output_string

#file.write(indicator_zk_20_5(date(2025,1,1), test_grootboek_instances) + '\n')