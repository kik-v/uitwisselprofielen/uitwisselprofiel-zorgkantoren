def indicator_zk_7_1(jaar, kwartaal, mensen, clienten):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.InhuurOvereenkomst, onzpers.UitzendOvereenkomst]
    indicator = {}
    unique_clients = {}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = [0, 0, 0]
        unique_clients[vestiging.split('/')[-1]] = set()
    indicator["Totaal"] = [0, 0, 0]
    unique_clients["Totaal"] = set()
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        contract_locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        contract_periode = werkovereenkomstafspraak.get_periode()
                        meetperiode = trim_to_period(contract_periode, periode)
                        if meetperiode:
                            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                            for gewerkte_periode in gewerkte_perioden:
                                if date_in_period(gewerkte_periode.get_date(), meetperiode):
                                        werklocatie = gewerkte_periode.get_locatie().split('/')[-1]
                                        locatie = werklocatie if werklocatie else contract_locatie
                                        indicator[locatie][0] += gewerkte_periode.get_uren()
                                        indicator["Totaal"][0] += gewerkte_periode.get_uren()
    for zorg_proces in [instance for instance in clienten if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        indicatie = zorg_proces.indicatie
        zorg_profiel = indicatie.zorgprofiel
        if zorg_profiel in vv_zorgprofielen and period1_inperiod2(zorg_periode, periode):
            vestiging = zorg_proces.locatie.split('/')[-1]
            unique_clients[vestiging].add(zorg_proces.deelnemer.subject)
            unique_clients["Totaal"].add(zorg_proces.deelnemer.subject)
    for key in indicator:
        indicator[key][1] = len(unique_clients[key])
        indicator[key][2] = indicator[key][0] / indicator[key][1] if indicator[key][1] > 0 else "Ongedefineerd"

    output_string = f"\nZK 7.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ': ' + str(indicator[vestiging])
    return (output_string)
