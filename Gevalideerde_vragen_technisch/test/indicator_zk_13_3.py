def indicator_zk_13_3(peildatum, mensen):
    indicator = {}
    indicator = {}
    indicator["mensen_peildatum"] = set()
    indicator["mensen_eerder"] = set()

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                functie = werkovereenkomstafspraak.get_functie()
                if functie not in ZorgFuncties:
                    continue

                niveau = int(str(FunctieNiveaus[functie]).split("_")[1])
                if niveau > 6 or niveau < 1:
                    continue
                
                if date_in_period(peildatum, werkovereenkomstafspraak.get_periode()):
                    indicator["mensen_peildatum"].add((mens.get_subject(), niveau))
                if date_in_period(peildatum - relativedelta(months=3), werkovereenkomstafspraak.get_periode()):
                    indicator["mensen_eerder"].add((mens.get_subject(), niveau))
    
    output_string = f"\nZK 13.3 indicator peildatum: {peildatum}:"
    noemer = 0
    teller = 0
    for (mens_peildatum, niveau_peildatum) in indicator["mensen_peildatum"]:
        for (mens_eerder, niveau_eerder) in indicator["mensen_eerder"]:
            if mens_peildatum == mens_eerder:
                noemer += 1
                if niveau_eerder < niveau_peildatum:
                    teller += 1
    output_string += f'\n    teller: {teller}, noemer: {noemer}, indicator: {100*teller/noemer if noemer != 0 else "Ongedefinieerd"}'
    return (output_string)
