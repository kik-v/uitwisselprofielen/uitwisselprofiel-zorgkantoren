def indicator_zk_24_3(peildatum, test_instances):
    output_string = f'\n24.3 - Kort vreemd vermogen t.o.v. lang vreemd vermogen, peildatum: {peildatum}'
    # Current ratio = "B Vlottende activa" / "G Kortlopende schulden (ten hoogste 1 jaar)"
    totalen = {
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
    
    if totalen['F Langlopende schulden (nog voor meer dan een jaar)'] != 0:
        waarde = (totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] /totalen['F Langlopende schulden (nog voor meer dan een jaar)'] )
    else :
        waarde = 'ongedefineerd'

    output_string += '\n    Kental: Kort vreemd vermogen t.o.v. lang vreemd vermogen'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    return output_string

# file.write(indicator_zk_24_3(date(2025,1,1), test_grootboek_instances) + '\n')