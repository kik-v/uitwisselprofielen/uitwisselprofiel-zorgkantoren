def indicator_zk_24_9(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 24.9, Uitbesteed werk vs vast personeel, start_periode: {start_periode}, eind_periode: {eind_periode}'
    # Uitbesteed werk vs vast personeel = "Q.II Kosten uitbesteed werk en andere externe kosten" / ("Q.III Lonen en salarissen"* + "Q.IV Sociale lasten" + "Q.V Pensioenlasten")
    totalen = {
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
    }

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price

    if (totalen['Q.III Lonen en salarissen']+totalen['Q.IV Sociale lasten']+totalen['Q.V Pensioenlasten']) != 0:
        waarde = totalen['Q.II Kosten uitbesteed werk en andere externe kosten']/(
            totalen['Q.III Lonen en salarissen']+totalen['Q.IV Sociale lasten']+totalen['Q.V Pensioenlasten'])
    else:
        waarde = 'ongedefineerd'

    output_string += '\n    Kental: Uitbesteed werk vs vast personeel'
    output_string += f'\n    Waarde: {round(waarde, 4)}'

    return output_string
    
# file.write(indicator_zk_24_9(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')