def indicator_zk_5_1(jaar, kwartaal, mensen):
    indicator = {}
    unieke_mensen = {}
    unieke_mensen["Totaal"] = []
    for locatie in Vestigingen:
        indicator[locatie.split('/')[-1]] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        unieke_mensen[locatie.split('/')[-1]] = []
    indicator["Totaal"] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if period1_inperiod2(contract_periode, periode):
                        geboorte_datum = mens.get_geboortedatum()
                        # Gebruik eind van de periode om de leeftijd te berekenen
                        leeftijd = periode[1].year - geboorte_datum.year                                               
                        if (periode[1].month, periode[1].day) < (geboorte_datum.month, geboorte_datum.day):
                            leeftijd -= 1
                        categorie = int(((leeftijd-((leeftijd - 1) % 5))/5)-3)
                        if mens.get_subject() not in unieke_mensen["Totaal"]:
                            unieke_mensen["Totaal"].append(mens.get_subject())
                            indicator["Totaal"][categorie] +=1
                        if mens.get_subject() not in unieke_mensen[locatie]:
                            unieke_mensen[locatie].append(mens.get_subject())
                            indicator[locatie][categorie] +=1
    output_string = f"\nZK 5.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + vestiging + ': '
        for item in indicator[vestiging]:
            output_string += str(item) + ', '
    return (output_string)
