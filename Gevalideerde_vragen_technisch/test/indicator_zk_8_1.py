def indicator_zk_8_1(jaar, kwartaal, mensen):
    indicator = {}
    indicator["Totaal"] = {"uren_pil":0, "uren_pnil": 0}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {"uren_pil":0, "uren_pnil": 0}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
            for gewerkte_periode in gewerkte_perioden:
                dag = gewerkte_periode.get_date()
                if date_in_period(date=dag, period=periode):
                    werkovereenkomstafspraak = get_afspraak_on_date(werk_overeenkomst=werkovereenkomst, date=dag)
                    uren = gewerkte_periode.get_uren()
                    locatie = gewerkte_periode.get_locatie()
                    if not locatie:
                        locatie = werkovereenkomstafspraak.get_locatie()
                    if locatie:
                        if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:                           
                            indicator[locatie.split('/')[-1]]["uren_pil"] += uren
                            indicator["Totaal"]["uren_pil"] += uren
                        elif werkovereenkomst.get_rdftype() in PnilOvereenkomsten:
                            indicator[locatie.split('/')[-1]]["uren_pnil"] += uren
                            indicator["Totaal"]["uren_pnil"] += uren
    output_string = f"\nZK 8.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in sorted(indicator.keys()):
        output_string += '\n    ' + str(vestiging) + ':'
        for p_p in indicator[vestiging]:
            output_string += f"{p_p}: {indicator[vestiging][p_p]}  "
        output_string += 'percentage: ' + str(100*indicator[vestiging]["uren_pnil"]/(indicator[vestiging]["uren_pnil"] + indicator[vestiging]["uren_pil"]))
    output_string += '\n'
    return (output_string)
