def indicator_zk_12_2(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["overeenkomsten"] = set()
    indicator["Totaal"]["ziektes"] = set()

    maand = 3 * int(kwartaal[-1:]) - 2
    start_datum = date(jaar, maand, 1)
    eind_datum = start_datum + relativedelta(months=3) - timedelta(days=1)

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                continue
            periode = werkovereenkomst.get_periode()
            if periode[0] > eind_datum:
                continue
            if periode[1] != False and periode[1] < start_datum:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                afspraak_periode = werkovereenkomstafspraak.get_periode()
                if afspraak_periode[0] > eind_datum:
                    continue
                if afspraak_periode[1] != False and afspraak_periode[1] < start_datum:
                    continue
                locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                if locatie not in indicator.keys():
                    indicator[locatie] = {}
                    indicator[locatie]["overeenkomsten"] = set()
                    indicator[locatie]["ziektes"] = set()
                indicator[locatie]["overeenkomsten"].add(werkovereenkomst)
                indicator["Totaal"]["overeenkomsten"].add(werkovereenkomst)

                for verzuimperiode in werkovereenkomst.get_verzuim_periode_lijst():
                    if verzuimperiode.get_type() != onzpers.ZiektePeriode and verzuimperiode.get_type() != onzpers.ZwangerschapsVerlof:
                        continue
                    verzuim_periode = verzuimperiode.get_periode()
                    if verzuim_periode[0] > eind_datum:
                        continue
                    if verzuim_periode[0] > afspraak_periode[1]:
                        continue
                    if verzuim_periode[0] < afspraak_periode[0]:
                        continue
                    if verzuim_periode[0] < start_datum:
                        continue
                    if verzuim_periode[1] != None and verzuim_periode[1] < start_datum:
                        continue
                    if verzuim_periode[1] != None and verzuim_periode[1] < afspraak_periode[0]:
                        continue
                    
                    indicator[locatie]["ziektes"].add(verzuimperiode)
                    indicator["Totaal"]["ziektes"].add(verzuimperiode)
    
    output_string = f"\nZK 12.2 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n    ' + str(vestiging) + ':'
        for type in indicator[vestiging]:
            output_string += ' ' + type + ': ' + str(len(indicator[vestiging][type]))
        output_string += ' frequentie: ' + str(len(indicator[vestiging]["ziektes"]) / len(indicator[vestiging]["overeenkomsten"]))
    return (output_string)
