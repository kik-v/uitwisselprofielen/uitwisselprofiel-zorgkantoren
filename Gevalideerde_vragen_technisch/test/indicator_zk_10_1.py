def indicator_zk_10_1(peildatum, mensen):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Aantal BBL-leerlingen"] = set()

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() == onzpers.ArbeidsOvereenkomstBBL:
                for (
                    werkovereenkomstafspraak
                ) in werkovereenkomst.get_werkafspraken_lijst():
                    locatie = werkovereenkomstafspraak.get_werklocatie().split("/")[-1]
                    if locatie not in indicator:
                        indicator[locatie] = {}
                        indicator[locatie]["Aantal BBL-leerlingen"] = set()
                    if date_in_period(
                        peildatum, werkovereenkomstafspraak.get_periode()
                    ):
                        indicator[locatie]["Aantal BBL-leerlingen"].add(
                            mens.get_subject()
                        )
                        indicator["Totaal"]["Aantal BBL-leerlingen"].add(
                            mens.get_subject()
                        )
    output_string = f"\nZK 10.1 indicator peildatum: {peildatum}:"
    for vestiging in indicator:
        output_string += "\n    " + str(vestiging) + ":"
        output_string += " Aantal BBL-leerlingen: " + str(
            len(indicator[vestiging]["Aantal BBL-leerlingen"])
        )
    return output_string
