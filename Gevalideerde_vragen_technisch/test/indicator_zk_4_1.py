def indicator_zk_4_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
    indicator = {}
    uniek = {}
    for locatie_url in Vestigingen:
        locatie = locatie_url.split('/')[-1]
        indicator[locatie] = {"zorg": 0, "niet-zorg": 0}
        uniek[locatie] = {"zorg": set(), "niet-zorg": set()}
    indicator["Totaal"] = {"zorg": 0, "niet-zorg": 0}
    uniek["Totaal"] = {"zorg": set(), "niet-zorg": set()}
    maand = 3 * int(kwartaal[-1:]) - 2
    peildatum = date(jaar, maand, 1)
    for mens in mensen:
        # teller = {"zorg": 0, "niet-zorg": 0}
        noemer = {"totaal":0, "zorg": 0, "niet-zorg": 0}
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                werkovereenkomstafspraak = get_afspraak_on_date(werk_overeenkomst=werkovereenkomst, date=peildatum)
                if werkovereenkomstafspraak:
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                    omvang_waarde = contract_omvang.get_waarde()
                    omvang_eenheid = contract_omvang.get_eenheid()
                    if omvang_eenheid == onzpers.fte_36:
                        omvang_waarde = omvang_waarde * 36 # omvang in uren/week
                    
                    functie = werkovereenkomstafspraak.get_functie()
                    wel_niet_zorg = "zorg" if functie in ZorgFuncties else "niet-zorg"
                    indicator[locatie][wel_niet_zorg] += omvang_waarde
                    indicator["Totaal"][wel_niet_zorg] += omvang_waarde
                    uniek[locatie][wel_niet_zorg].add(mens)
                    uniek["Totaal"][wel_niet_zorg].add(mens)

    # Deel contractomvang door unieke personen om gemiddelde contractomvang per persoon te krijgen
    for vestiging in indicator:
        for zorg_niet_zorg in indicator[vestiging]:
            if len(uniek[vestiging][zorg_niet_zorg]) == 0:
                indicator[vestiging][zorg_niet_zorg] = "Ongedefinieerd"
            else:
                indicator[vestiging][zorg_niet_zorg] = indicator[vestiging][zorg_niet_zorg]/len(uniek[vestiging][zorg_niet_zorg])

    output_string = f"\nZK 4.1 indicator jaar: {jaar}, kwartaal {kwartaal}:\n"
    for vestiging in indicator:
        output_string += '    ' + str(vestiging) + ': zorg ' + str(indicator[vestiging]["zorg"]) + ', niet-zorg ' + str(indicator[vestiging]["niet-zorg"]) + '\n'
    return (output_string)
