def indicator_zk_23_2(peildatum, test_instances):
    output_string = f'\nZK 23_2,Kapitaal, Net-debt / EBITDA, peildatum: {peildatum}'
    totalen = {
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
    }

    period_start = date(peildatum.year,1,1)
    
    # https://gitlab.com/kik-v/uitwisselprofielen/uitwisselprofiel-zorgkantoren/-/blob/dev/Gevalideerde_vragen_functioneel/Indicator%2023.2.md
    # Teller: Vreemd vermogen op peildatum 31-dec van het voorgaande jaar of 30-jun van het lopende jaar.
    # Noemer: EBITDA over de meetperiode 1-jan t/m 31-dec van het voorgaande jaar of 1-jan t/m 30-jun van het lopende jaar.
    # Net-debt / EBITDA = ("F Langlopende schulden (nog voor meer dan een jaar)" + "G Kortlopende schulden (ten hoogste 1 jaar)") / EBITDA [zie Indicator 19.6]
    # EBITDA = "P Som der bedrijfsopbrengsten" - "Q Som der bedrijfslasten" + "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
            
            if post.datum >= period_start:
                if rubriek_id[:2] == '81':
                    totalen['P.I Netto omzet'] += post.price
                elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                    totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
                elif rubriek_id[:3] in ['417', '418']:
                    totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
                elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                    totalen['Q.III Lonen en salarissen'] += post.price
                elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                    totalen['Q.IV Sociale lasten'] += post.price
                elif rubriek_id[:4] == '4226':
                    totalen['Q.V Pensioenlasten'] += post.price
                elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                    totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
                elif (rubriek_id[:3] in 
                    ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                    rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                    totalen['Q.IX Overige bedrijfskosten'] += post.price

    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
    
    ebitda = (totalen['P Som der bedrijfsopbrengsten'] - totalen['Q Som der bedrijfslasten'] + 
         totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] )
    
    if ebitda != 0:
        waarde = (totalen['F Langlopende schulden (nog voor meer dan een jaar)'] + totalen['G Kortlopende schulden (ten hoogste 1 jaar)']) / ebitda
        waarde = round(waarde, 8)
    else:
        waarde = 'ongedefineerd'

    output_string += '\n    Kental: Net-debt / EBITDA'
    output_string += f'\n    Waarde: {waarde}'

    return output_string

#file.write(indicator_zk_23_2(date(2024,6,30), test_grootboek_instances) + '\n')
