def indicator_zk_20_4(start_periode, eind_periode, test_instances):
    output_string = f'\n20.4 - Solvabiliteit, Reserves, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            # D.III leeg
            if rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
    result = 0
    for jaarrekeningpost in totalen:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
        result += bedrag
    
    # Reserves = "D.III Herwaarderingsreserve" + "D.IV Wettelijke en statutaire reserve" + 
    # "D.V Bestemmingsreserve" + "D.VI Bestemmingsfonds" + "D.VII Overige reserves" + 
    # "D.VIII Onverdeelde winst"
    output_string += f'\n    Reserves: {round(result, 2)}'
        
    return output_string


#file.write(indicator_zk_20_4(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
