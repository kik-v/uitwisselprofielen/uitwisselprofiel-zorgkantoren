def indicator_zk_13_2(peildatum, mensen):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["mensen_peildatum"] = set()
    indicator["Totaal"]["mensen_eerder"] = set()

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                if not werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                    continue
                locatie = str(werkovereenkomstafspraak.get_werklocatie()).split("/")[-1]
                if locatie not in indicator:
                        indicator[locatie] = {}
                        indicator[locatie]["mensen_peildatum"] = set()
                        indicator[locatie]["mensen_eerder"] = set()
                if date_in_period(peildatum, werkovereenkomstafspraak.get_periode()):
                    indicator[locatie]["mensen_peildatum"].add(mens.get_subject())
                    indicator["Totaal"]["mensen_peildatum"].add(mens.get_subject())
                if date_in_period(peildatum - relativedelta(years=1), werkovereenkomstafspraak.get_periode()):
                    indicator[locatie]["mensen_eerder"].add(mens.get_subject())
                    indicator["Totaal"]["mensen_eerder"].add(mens.get_subject())
    
    output_string = f"\nZK 13.2 indicator peildatum: {peildatum}:"
    for key in indicator.keys():
        noemer = len(indicator[key]["mensen_eerder"])
        teller = len(indicator[key]["mensen_eerder"] - indicator[key]["mensen_peildatum"])
        output_string += f"\n   {key}: noemer: {noemer}, teller: {teller}, indicator: {(teller / noemer * 100) if noemer != 0 else 0}"
    return (output_string)
