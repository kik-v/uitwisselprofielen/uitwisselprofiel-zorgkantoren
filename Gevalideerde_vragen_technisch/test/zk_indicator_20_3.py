def indicator_zk_20_3(start_periode, eind_periode, test_instances):
    output_string = f'\n20.3 - Solvabiliteit, Debt ratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'D Eigen vermogen': 0,
        'D.I Gestort en opgevraagd kapitaal': 0,
        'D.II Agio': 0,
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0,
        'E Voorzieningen': 0,
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
        'H Totaal passiva': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:4] == '0511':
                totalen['D.I Gestort en opgevraagd kapitaal'] += post.price
            elif rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
            elif rubriek_id[:2] == '06':
                totalen['E Voorzieningen'] += post.price
            elif rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
    
    totalen['D Eigen vermogen'] =(totalen['D.I Gestort en opgevraagd kapitaal'] + 
        totalen['D.II Agio'] + 
        totalen['D.III Herwaarderingsreserve'] + 
        totalen['D.IV Wettelijke en statutaire reserve'] +
        totalen['D.V Bestemmingsreserve'] + 
        totalen['D.VI Bestemmingsfonds'] +
        totalen['D.VII Overige reserves'] + 
        totalen['D.VIII Onverdeelde winst'] )
    totalen['H Totaal passiva'] =(totalen['D Eigen vermogen'] + 
        totalen['E Voorzieningen'] + 
        totalen['F Langlopende schulden (nog voor meer dan een jaar)'] +
        totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] )
        
    for jaarrekeningpost in ['F Langlopende schulden (nog voor meer dan een jaar)', 'G Kortlopende schulden (ten hoogste 1 jaar)', 'H Totaal passiva']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    
    if totalen['H Totaal passiva'] == 0:
        output_string += f'\n    Debt ratio: undefined'
    else:
        # Debt ratio = ("F Langlopende schulden (nog voor meer dan een jaar)" + "G Kortlopende schulden (ten hoogste 1 jaar)") / ("H Totaal passiva")
        result = (totalen['F Langlopende schulden (nog voor meer dan een jaar)'] + totalen['G Kortlopende schulden (ten hoogste 1 jaar)']) / totalen['H Totaal passiva']
        output_string += f'\n    Debt ratio: {round(result, 2)}'

    return output_string


#file.write(indicator_zk_20_3(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
