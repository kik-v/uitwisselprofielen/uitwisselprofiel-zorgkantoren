def indicator_zk_23_3(peildatum, test_instances):
    output_string = f'\nZK 23_3, Kapitaal, CAPEX (Capital Expenditures), peildatum: {peildatum}'
    totalen = {
        'A Vaste activa': 0,
        'A.I Immateriële vaste activa': 0,
        'A.II Materiële vaste activa': 0,
        'A.III Financiële vaste activa': 0,
        'Q.IV Sociale lasten' : 0,
    }

    totalen_old = {
        'A Vaste activa': 0,
        'A.I Immateriële vaste activa': 0,
        'A.II Materiële vaste activa': 0,
        'A.III Financiële vaste activa': 0,
    }

    peildatum_old = peildatum - timedelta(days=360)

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '00':
                totalen['A.I Immateriële vaste activa'] += post.price
            elif rubriek_id[:2] == '01':
                totalen['A.II Materiële vaste activa'] += post.price
            elif rubriek_id[:2] == '03':
                totalen['A.III Financiële vaste activa'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if post.datum <= peildatum_old:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '00':
                totalen_old['A.I Immateriële vaste activa'] += post.price
            elif rubriek_id[:2] == '01':
                totalen_old['A.II Materiële vaste activa'] += post.price
            elif rubriek_id[:2] == '03':
                totalen_old['A.III Financiële vaste activa'] += post.price

    totalen['A Vaste activa'] = (totalen['A.I Immateriële vaste activa'] + 
            totalen['A.II Materiële vaste activa'] + 
            totalen['A.III Financiële vaste activa'] )
    
    totalen_old['A Vaste activa'] = (totalen_old['A.I Immateriële vaste activa'] + 
            totalen_old['A.II Materiële vaste activa'] + 
            totalen_old['A.III Financiële vaste activa'] )
    
    waarde = (totalen['A Vaste activa']-totalen_old['A Vaste activa'])/totalen['Q.IV Sociale lasten']

    output_string += '\n    Kental: CAPEX'
    output_string += f'\n    Waarde: {round(waarde, 4)}'

    return output_string

# print(indicator_zk_23_3(date(2025,1,1), test_grootboek_instances) + '\n')