def indicator_zk_24_1(peildatum, test_instances):
    output_string = f'\nZK 24.1, Overig, Dekking vaste activa met lang vermogen, peildatum: {peildatum}'
    totalen = {
        'A Vaste activa': 0,
        'A.I Immateriële vaste activa': 0,
        'A.II Materiële vaste activa': 0,
        'A.III Financiële vaste activa': 0,
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '00':
                totalen['A.I Immateriële vaste activa'] += post.price
            elif rubriek_id[:2] == '01':
                totalen['A.II Materiële vaste activa'] += post.price
            elif rubriek_id[:2] == '03':
                totalen['A.III Financiële vaste activa'] += post.price
            elif rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price

    
    totalen['A Vaste activa'] = (totalen['A.I Immateriële vaste activa'] + 
        totalen['A.II Materiële vaste activa'] + 
        totalen['A.III Financiële vaste activa'] )

   
    if(totalen['F Langlopende schulden (nog voor meer dan een jaar)'] != 0):
        waarde = totalen['A Vaste activa']/totalen['F Langlopende schulden (nog voor meer dan een jaar)']
    else :
        waarde = 'ongedefineerd'  

    output_string += '\n    Kental: Dekking  vaste activa met lang vermogen'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    return output_string

# file.write(indicator_zk_24_1(date(2025,1,1), test_grootboek_instances) + '\n')