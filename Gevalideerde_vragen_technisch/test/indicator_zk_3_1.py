def indicator_zk_3_1(peildatum, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["bepaald"] = 0
    indicator["Totaal"]["onbepaald"] = 0
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {}
        indicator[vestiging.split('/')[-1]]["bepaald"] = 0
        indicator[vestiging.split('/')[-1]]["onbepaald"] = 0

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            type_overeenkomst = werkovereenkomst.get_rdftype()
            if type_overeenkomst in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if date_in_period(peildatum, contract_periode):
                        if type_overeenkomst == onzpers.ArbeidsOvereenkomstBepaaldeTijd:
                            indicator["Totaal"]["bepaald"] += 1
                            indicator[locatie]["bepaald"] += 1
                        else:
                            indicator["Totaal"]["onbepaald"] += 1
                            indicator[locatie]["onbepaald"] += 1

    output_string = f"ZK 3.1 indicator peildatum {peildatum}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ':'
        for type_overeenkomst in indicator[vestiging]:
            output_string += ' ' + type_overeenkomst + ': ' + str(indicator[vestiging][type_overeenkomst])
    return (output_string)