def indicator_zk_2_2(jaar, kwartaal, financial_instances, test_instances):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["zorg"] = 0
    indicator["Totaal"]["niet-zorg"] = 0
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {}
        indicator[vestiging.split('/')[-1]]["zorg"] = 0
        indicator[vestiging.split('/')[-1]]["niet-zorg"] = 0
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    output_string = f"\nZK 2.2 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    verloonde_perioden = get_objects_from(instances=financial_instances, object_type=VerloondePeriode)
    for verloonde_periode in verloonde_perioden:
        uren_periode = (verloonde_periode.start_datum, verloonde_periode.eind_datum)
        if period1_inperiod2(uren_periode, periode):
            # Bepaal of de geldige werkafspraak een zorg of niet-zorg functie bevat
            overeenkomst = verloonde_periode.overeenkomst
            type_overeenkomst = overeenkomst.get_rdftype()
            if type_overeenkomst in geldige_overeenkomsten:
                zorg = is_care_on_date(werk_overeenkomst=overeenkomst, date=verloonde_periode.start_datum)
                werk_afspraak = get_afspraak_on_date(werk_overeenkomst=overeenkomst, date=verloonde_periode.start_datum)
                
                if werk_afspraak:
                    vestiging = werk_afspraak.get_werklocatie().split('/')[-1]
                else:
                    print("Uren gewerkt zonder geldige werkafspraak....")
                if zorg:
                    indicator["Totaal"]["zorg"] += verloonde_periode.aantal_uren
                    if vestiging:
                        indicator[vestiging]["zorg"] += verloonde_periode.aantal_uren
                else:
                    indicator["Totaal"]["niet-zorg"] += verloonde_periode.aantal_uren
                    if vestiging:
                        indicator[vestiging]["niet-zorg"] += verloonde_periode.aantal_uren
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ': ' + str(indicator[vestiging])
    return (output_string)

    # print(indicator_zk_2_2(jaar=2023, kwartaal="Q3", financial_instances=test_grootboek_instances, test_instances=test_instances))