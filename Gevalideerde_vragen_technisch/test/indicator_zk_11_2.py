def indicator_zk_11_2(jaar, kwartaal, mensen):
    indicator = {}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    indicator["Totaal"] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    einddatum = startdatum + relativedelta(months=3) - timedelta(days=1)
    periode = (startdatum, einddatum)
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36 # omvang in fte
                        functie = werkovereenkomstafspraak.get_functie()
                        wel_niet_zorg = "zorg" if functie in ZorgFuncties else "niet-zorg"
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                        indicator['Totaal'][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        indicator[locatie][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                            verzuim_periode = overlap_with_fixed_period(verzuim.get_periode(), afspraak_periode)
                            #Ziekte en/of Zwangerschap
                            if verzuim_periode and (verzuim.get_type() == onzpers.ZiektePeriode or verzuim.get_type() == onzpers.ZwangerschapsVerlof):
                                totaal_verzuim_dagen = days_in_period(overlap_with_fixed_period(verzuim.get_periode(), periode))
                                #kort/langdurend verzuim
                                if totaal_verzuim_dagen <= 28:
                                    hersteld = 0
                                    verzuim_corr = days_in_period(verzuim_periode)
                                    for verzuimtijd in verzuim.get_verzuimtijd_lijst():
                                        verzuimtijd_periode = overlap_with_fixed_period(verzuimtijd.get_periode(), verzuim_periode)
                                        if verzuimtijd_periode:
                                            verzuimtijd_dagen = days_in_period(verzuimtijd_periode)
                                            vp = verzuimtijd.get_ziekte_percentage() / 100
                                            hersteld += verzuimtijd_dagen * (1-vp)
                                    indicator['Totaal'][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)
                                    indicator[locatie][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)

    output_string = f"\nZK 11.2 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n' + vestiging 
        zorg_indicator = str(100 * indicator[vestiging]["zorg"]["teller"]/indicator[vestiging]["zorg"]["noemer"]) if indicator[vestiging]["zorg"]["noemer"] != 0 else "Ongedefinieerd"
        niet_zorg_indicator = str(100 * indicator[vestiging]["niet-zorg"]["teller"]/indicator[vestiging]["niet-zorg"]["noemer"]) if indicator[vestiging]["niet-zorg"]["noemer"] != 0 else "Ongedefinieerd"
        output_string += ', zorg: ' + zorg_indicator
        output_string += ', niet-zorg: ' + niet_zorg_indicator
    return output_string
