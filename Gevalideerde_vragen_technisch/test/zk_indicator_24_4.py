def indicator_zk_24_4(peildatum, test_instances):
    output_string = f'\nZK 24.4, Verhouding vorderingen en totale opbrengsten, peildatum: {peildatum}'
    totalen = {
        'B.II Vorderingen': 0,
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0
    }

    eind_periode = peildatum
    start_periode = peildatum - relativedelta(years= 1)

    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '12':
                totalen['B.II Vorderingen'] += post.price

    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    
    if totalen['P Som der bedrijfsopbrengsten'] != 0 :
        waarde = (totalen['B.II Vorderingen']/totalen['P Som der bedrijfsopbrengsten'])
    else:
        waarde = 'ongedefineerd'

    output_string += '\n    Kental: Verhouding vorderingen en totale opbrengsten'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    

    return output_string

# file.write(indicator_zk_24_4(date(2025,1,1), test_grootboek_instances) + '\n')