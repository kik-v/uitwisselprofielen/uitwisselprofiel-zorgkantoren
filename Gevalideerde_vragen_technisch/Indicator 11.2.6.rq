# Indicator: Zorgkantoren 11.2.6
# Parameters: -
# Ontologie: versie 2.3.0 of nieuwer

PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>

SELECT
    ?vestiging
    ?zorg
    (SUM(?ziek_totaal - ?hersteld_totaal) AS ?teller)
    (SUM(?noemer_deel) AS ?noemer)
    ((?teller / ?noemer) * 100 AS ?indicator)
{
    SELECT
        ?functie
        ?omvang
        ?vestiging
        ?zorg
        (MAX(?overeenkomst_totaal) AS ?noemer_deel)
        (SUM(?ziek_totaal_temp) AS ?ziek_totaal)
        (MAX(?hersteld_totaal_temp) AS ?hersteld_totaal)
    {
        SELECT
            ?functie
            ?omvang
            ?vestiging
            ?zorg
            ?verzuimperiode
            (MAX(?dagen_overeenkomst) AS ?overeenkomst_totaal)
            (MAX(?dagen_ziek_reken) AS ?ziek_totaal_temp)
            (SUM(?dagen_hersteld_reken) AS ?hersteld_totaal_temp)
        {
            BIND("2024-04-01"^^xsd:date AS ?start_periode)
            BIND("2024-06-30"^^xsd:date AS ?eind_periode)
            VALUES ?type_verzuim # Keuze in-/exclusief zwangerschapsverlog
                    { 
                        onz-pers:ZiektePeriode
                        onz-pers:ZwangerschapsVerlof
                    }
            ?functie 
                a onz-g:OccupationalPositionRole ;
                onz-g:startDatum ?start_functie .
                OPTIONAL {?functie onz-g:eindDatum ?eind_functie}
                FILTER (?start_functie <= ?eind_periode && (?eind_functie >= ?start_periode || !BOUND(?eind_functie)))
                BIND(IF(?start_functie < ?start_periode, ?start_periode, ?start_functie) AS ?start_functie_corr)
                BIND(IF(?eind_functie > ?eind_periode || !BOUND(?eind_functie), ?eind_periode, ?eind_functie) AS ?eind_functie_corr)
                OPTIONAL 
                {
                    ?functie a onz-pers:ZorgverlenerFunctie .
                    BIND(1 AS ?zorgfunctie)
                }
                BIND(IF(BOUND(?zorgfunctie),"Zorg","Niet-zorg") AS ?zorg)
            ?overeenkomst
                a onz-pers:ArbeidsOvereenkomst ;
                onz-g:isAbout ?functie ;
                onz-pers:heeftOpdrachtnemer ?persoon .
            ?locatie 
                a onz-g:StationaryArtifact ;
                ^onz-g:isAbout ?overeenkomst ;
                onz-g:partOf* ?v .
            ?v 
                a onz-org:Vestiging ;
                onz-g:identifiedBy ?vest_nr.
            ?vest_nr
                a onz-org:Vestigingsnummer .
                {
                    ?vest_nr onz-g:hasDataValue ?vestiging .
                } UNION {
                    BIND ("Totaal organisatie" AS ?vestiging)
                }
            ?omvang 
                a onz-pers:ContractOmvang ;
                onz-g:partOf ?overeenkomst ;
                onz-g:isAbout ?omvang_waarde ;
                onz-g:startDatum ?start_omvang .
                OPTIONAL {?omvang onz-g:eindDatum ?eind_omvang}
            FILTER (?start_omvang <= ?eind_functie_corr && (?eind_omvang >= ?start_functie_corr || !BOUND(?eind_omvang)))
            ?omvang_waarde
                onz-g:hasDataValue ?omvang_waarde_getal ;
                onz-g:hasUnitOfMeasure ?omvang_waarde_eenheid .
            ?omvang_waarde_eenheid
                onz-pers:hasDenominatorQualityValue onz-g:Week ;
                onz-pers:hasNumeratorQualityValue onz-g:Uur ;
                onz-g:hasDataValue ?omvang_waarde_factor .
            BIND (?omvang_waarde_getal * ?omvang_waarde_factor / 36 AS ?ptf)
            BIND (IF(?start_omvang < ?start_functie_corr, ?start_functie_corr, ?start_omvang) AS ?start_omvang_corr)
            BIND (IF(?eind_omvang > ?eind_functie_corr || !BOUND(?eind_omvang), ?eind_functie_corr, ?eind_omvang) AS ?eind_omvang_corr)
            ?start_omvang_corr ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?start_omvang_reken .
            ?eind_omvang_corr ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?eind_omvang_reken .
            BIND ((?eind_omvang_reken - ?start_omvang_reken + 1) * ?ptf AS ?dagen_overeenkomst)
            OPTIONAL {
                ?persoon
                    onz-g:isParticipantIn ?verzuimperiode .
                ?verzuimperiode a ?type_verzuim ;
                    onz-g:startDatum ?start_verzuim .
                ?start_verzuim ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?start_verzuimduur_reken .
                OPTIONAL 
                {
                    ?verzuimperiode onz-g:eindDatum ?eind_verzuim_voorl .
                }
                OPTIONAL {
                    ?verzuimperiode
                        onz-g:hasQuality ?verzuimtijd .
                    ?verzuimtijd
                        a onz-pers:VerzuimTijdKwaliteit ;
                        onz-g:hasQualityValue ?verzuimtijdwaarde .
                    ?verzuimtijdwaarde
                        a onz-g:DurationValue ;
                        onz-g:hasDataValue ?ziektepercentage ;
                        onz-g:hasUnitOfMeasure onz-g:percent ;
                        onz-g:startDatum ?start_ziektepercentage .
                    OPTIONAL {?verzuimtijdwaarde onz-g:eindDatum ?eind_ziektepercentage}
                }
            }
            BIND(IF(BOUND(?eind_verzuim_voorl), ?eind_verzuim_voorl, ?eind_periode) AS ?eind_verzuim)
            ?eind_verzuim ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?eind_verzuimduur_reken .
            BIND(?eind_verzuimduur_reken - ?start_verzuimduur_reken + 1 AS ?verzuimduur)
            FILTER(?verzuimduur <= 28 || !BOUND(?verzuimduur)) # Alleen kortdurend verzuim meenemen

            BIND (IF((?start_verzuim <= ?eind_omvang_corr && (?eind_verzuim >= ?start_omvang_corr || !BOUND(?eind_verzuim))), 1, 0) AS ?geen_ziekte_factor)
            BIND (IF(!BOUND(?verzuimperiode), 0, ?geen_ziekte_factor) AS ?ziekte_factor)
            BIND (IF(?start_verzuim < ?start_omvang_corr && ?ziekte_factor = 1, ?start_omvang_corr, ?start_verzuim) AS ?start_verzuim_corr)
            BIND (IF(BOUND(?verzuimperiode) && (?eind_verzuim > ?eind_omvang_corr || !BOUND(?eind_verzuim)) && ?ziekte_factor = 1, ?eind_omvang_corr, ?eind_verzuim) AS ?eind_verzuim_corr)
            BIND (IF((?start_ziektepercentage > ?eind_verzuim_corr || ?eind_ziektepercentage < ?start_verzuim_corr), 0, 1) AS ?geen_percentage_factor)
            BIND (IF(!BOUND(?geen_percentage_factor), 0, ?geen_percentage_factor * ?ziekte_factor) AS ?percentage_factor)
            BIND (IF(?start_ziektepercentage < ?start_verzuim_corr, ?start_verzuim_corr, ?start_ziektepercentage) AS ?start_ziektepercentage_corr)
            BIND (IF(?eind_ziektepercentage > ?eind_verzuim_corr || !BOUND(?eind_ziektepercentage), ?eind_verzuim_corr, ?eind_ziektepercentage) AS ?eind_ziektepercentage_corr)
            
            # Maak dummy verzuim en ziektepercentage datums als deze ontbreken, anders werkt de lookup naar de time:numerPosition niet
            BIND (IF(BOUND(?start_verzuim_corr), ?start_verzuim_corr, "2023-01-01"^^xsd:date) AS ?start_verzuim_corr_bound)
            BIND (IF(BOUND(?eind_verzuim_corr), ?eind_verzuim_corr, "2022-12-31"^^xsd:date) AS ?eind_verzuim_corr_bound)
            BIND (IF(BOUND(?start_ziektepercentage_corr), ?start_ziektepercentage_corr, "2023-01-01"^^xsd:date) AS ?start_zp_bound)
            BIND (IF(BOUND(?eind_ziektepercentage_corr), ?eind_ziektepercentage_corr, "2022-12-31"^^xsd:date) AS ?eind_zp_bound)
            ?start_verzuim_corr_bound ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?start_verzuim_reken .
            ?eind_verzuim_corr_bound ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?eind_verzuim_reken .
            ?start_zp_bound ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?start_zp_reken .
            ?eind_zp_bound ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?eind_zp_reken .

            BIND ((?eind_zp_reken - ?start_zp_reken + 1)*?ptf*?percentage_factor*(100 - ?ziektepercentage)/100 AS ?dagen_hersteld)
            BIND ((?eind_verzuim_reken - ?start_verzuim_reken + 1) * ?ptf * ?ziekte_factor AS ?dagen_ziek)
            BIND (IF(!BOUND(?dagen_ziek), 0, ?dagen_ziek) AS ?dagen_ziek_reken)
            BIND (IF(!BOUND(?dagen_hersteld), 0, ?dagen_hersteld) AS ?dagen_hersteld_reken)
        }
        GROUP BY ?vestiging ?zorg ?functie ?omvang ?verzuimperiode ?vestiging ?zorg
    }
    GROUP BY ?vestiging ?zorg ?functie ?omvang
}
GROUP BY ?vestiging ?zorg
ORDER BY ?vestiging DESC(?zorg)
