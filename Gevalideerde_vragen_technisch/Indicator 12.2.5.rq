# Indicator: Zorgkantoren 12.2.5
# Parameters: ($kwartaal)
# Ontologie: versie 2.0.0 of nieuwer

PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-zorg: <http://purl.org/ozo/onz-zorg#>

SELECT 
    ?vestiging
    (COUNT(?overeenkomst) AS ?aantal_arbeidsovereenkomsten)
    (SUM(?meldingen) AS ?aantal_meldingen)
    (SUM(?meldingen) / COUNT(?overeenkomst) AS ?verzuimfrequentie)
{
    {
        SELECT 
            DISTINCT ?overeenkomst 
            ?vestiging 
            (SUM(?melding) AS ?meldingen)
        {
            BIND('Q1' AS ?kwartaal)
            BIND(IF(?kwartaal = 'Q1', '2024-01-01'^^xsd:date, 
                IF(?kwartaal = 'Q2', '2024-04-01'^^xsd:date,
                IF(?kwartaal = 'Q3', '2024-07-01'^^xsd:date,
                IF(?kwartaal = 'Q4', '2024-10-01'^^xsd:date, 
                '')))) AS ?start_periode)
            BIND(?start_periode + "P3M"^^xsd:duration - "P1D"^^xsd:duration AS ?eind_periode)
            # Keuze in-/exclusief zwangerschapsverlog
            VALUES ?type_verzuim
            { 
                onz-pers:ZiektePeriode
                onz-pers:ZwangerschapsVerlof
            }

            # Bepaal filter voor definitie van overeenkomsten die geldig zijn voor personeelsleden
            VALUES ?personeels_overeenkomst 
            { 
                onz-pers:ArbeidsOvereenkomst
                onz-pers:UitzendOvereenkomst
                onz-pers:InhuurOvereenkomst
            }
            ?overeenkomst 
                a ?personeels_overeenkomst ; #Alleen personeel
                onz-pers:heeftOpdrachtnemer ?werknemer ;
                onz-g:isAbout ?locatie ;
                onz-g:startDatum ?start_overeenkomst .
            OPTIONAL {?overeenkomst onz-g:eindDatum ?eind_overeenkomst }
            
            FILTER (?start_overeenkomst <= ?eind_periode && (!BOUND(?eind_overeenkomst) || ?eind_overeenkomst >= ?start_periode))

            OPTIONAL {
                ?werknemer
                    ^onz-g:hasParticipant ?ziekteperiode .
                ?ziekteperiode
                    a ?type_verzuim ;
                    onz-g:startDatum ?start_ziekte .
                # Ziektemelding moet vallen binnen de meetperiode EN per overeenkomst binnen de geldigheid van die overeenkomst
                FILTER (?start_ziekte >= ?start_periode && ?start_ziekte <= ?eind_periode)
                FILTER (?start_ziekte >= ?start_overeenkomst && (?start_ziekte <= ?eind_overeenkomst || !BOUND(?eind_overeenkomst)))
            }
            BIND(IF(BOUND(?ziekteperiode), 1, 0) AS ?melding)
            #Bepaal de vestiging waar de werkzaamheden verricht worden volgens de werkovereenkomst
            {
                ?locatie onz-g:partOf* ?vestiging_uri .
                ?vestiging_uri 
                    a onz-org:Vestiging ;
                    onz-g:identifiedBy ?vest_nr.
                ?vest_nr
                    a onz-org:Vestigingsnummer ;
                    onz-g:hasDataValue ?vestiging .
            } UNION {
                #Includeer ook de organisatie als geheel en label deze als vestiging
                ?locatie onz-g:partOf*/onz-org:vestigingVan ?organisatie_uri .
                ?organisatie_uri 
                    a onz-g:Business ;
                    rdfs:label ?Organisatie .
                BIND(CONCAT('Totaal ',?Organisatie) AS ?vestiging)
            }
        }
        GROUP BY ?overeenkomst ?vestiging
    }
}
GROUP BY ?vestiging
