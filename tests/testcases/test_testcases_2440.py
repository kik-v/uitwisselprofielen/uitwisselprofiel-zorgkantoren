from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: "2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:

td_01 = [
    {
        "Description": "Testcase 01 (Geen matching rubrieken overige jaarrekeningsposten (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken jaarrekeningsposten (Sociale kosten) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000011" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken jaarrekeningsposten (000011) + Valt niet in meetperiode)",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-07-01",
                "accounting_item_type": "811000" ,
                "financial_entity_value": 10000  
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel matching rubrieken jaarrekeningsposten (P: Som der bedrijfsopbrengsten Activa: 811000 t/m 930000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            #B.II
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121001",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121002",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121003",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124111",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124113",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "125000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128330",
                "financial_entity_value": 10000
            },
            #P.I
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "811000",
                "financial_entity_value": 10000
            },
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel matching rubrieken jaarrekeningsposten (Bedrijfslasten: 417000 t/m 901000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            #B.II
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121000",
                "financial_entity_value": 10000
            },
            #P
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "891000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "919000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "920000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "930000",
                "financial_entity_value": 10000
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel matching rubrieken jaarrekeningsposten (Resultaat eigen vermogen: 811000 t/m 901000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121001",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121002",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121003",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124111",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124113",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "125000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128330",
                "financial_entity_value": 10000
            },
            #P
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "891000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "919000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "920000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "930000",
                "financial_entity_value": 10000
            }
        ]
    }
]



# Static Tests
def test_if_headers_are_correct_for_query_24_4(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_header_present('Kental')
    test.verify_header_present('waarde')

def test_if_number_of_rows_returned_is_correct_for_query_24_4(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten? 
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_24_4(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_value("waarde", "0.317423079522306214460181", where_conditions=[("Kental", "Verhouding vorderingen en totale opbrengsten")])


def test_if_dates_can_change_24_4(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

    test.set_reference_date_to("2024-01-01")

    # Assertions
    test.verify_value("waarde", "ongedefineerd", where_conditions=[("Kental","Verhouding vorderingen en totale opbrengsten")])
        # test.verify_value("waarde", "0", where_condition=("Kental","Totaal passiva"))

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_24_4_01(db_config):
    """ Testcase 01 (Geen matching rubrieken overige bedrijfsopbrengsten (ZZP kosten) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("waarde", "ongedefineerd", where_conditions=[("Kental","Verhouding vorderingen en totale opbrengsten")])
        # test.verify_value("waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_24_4_02(db_config):
    """ Testcase 02 (Geen matching rubrieken overige bedrijfsopbrengsten (Sociale kosten) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("waarde", "ongedefineerd", where_conditions=[("Kental","Verhouding vorderingen en totale opbrengsten")])
        # test.verify_value("waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_24_4_03(db_config):
    """ Testcase 03 (Wel matching rubrieken overige bedrijfsopbrengsten (812000) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("waarde", "ongedefineerd", where_conditions=[("Kental","Verhouding vorderingen en totale opbrengsten")])
        # test.verify_value("waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_24_4_04(db_config):
    """ Testcase 04 (Wel matching rubrieken jaarrekeningsposten (Verhouding vorderingen en totale opbrengsten: ) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        # test.verify_value("waarde", "160000", where_condition=("Kental","P Som der bedrijfsopbrengsten"))
        # test.verify_value("waarde", "10000", where_condition=("Kental","P.I Netto omzet"))
        # test.verify_value("waarde", "0", where_condition=("Kental","P.II Wijziging IN voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum"))
        # test.verify_value("waarde", "0", where_condition=("Kental","P.III Geactiveerde productie voor het eigen bedrijf"))
        # test.verify_value("waarde", "150000", where_condition=("Kental","P.IV Overige bedrijfsopbrengsten"))
        test.verify_value("waarde", "18.065758", where_conditions=[("Kental","Verhouding vorderingen en totale opbrengsten")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_24_4_04_a(db_config):
    """ Testcase 04a (Wel matching rubrieken jaarrekeningsposten (Verhouding vorderingen en totale opbrengsten: ) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        # test.verify_value("waarde", "1230000", where_condition=("Kental", "Q Som der bedrijfslasten"))
        # test.verify_value("waarde", "0", where_condition=("Kental", "Q.I Kosten van grond- en hulpstoffen"))
        # test.verify_value("waarde", "40000", where_condition=("Kental", "Q.II Kosten uitbesteed werk en andere externe kosten"))
        # test.verify_value("waarde", "520000", where_condition=("Kental", "Q.III Lonen en salarissen"))
        # test.verify_value("waarde", "80000", where_condition=("Kental", "Q.IV Sociale lasten"))
        # test.verify_value("waarde", "10000", where_condition=("Kental", "Q.V Pensioenlasten"))
        # test.verify_value("waarde", "460000", where_condition=("Kental", "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"))
        # test.verify_value("waarde", "0", where_condition=("Kental", "Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa"))
        # test.verify_value("waarde", "0", where_condition=("Kental", "Q.VIII Bijzondere waardevermindering van vlottende activa"))
        # test.verify_value("waarde", "120000", where_condition=("Kental", "Q.IX Bijzondere waardevermindering van vlottende activa"))
        test.verify_value("waarde", "0.071050533333333333333333", where_conditions=[("Kental","Verhouding vorderingen en totale opbrengsten")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_24_4_04_b(db_config):
    """ Testcase 04b (Wel matching rubrieken jaarrekeningsposten (Verhouding vorderingen en totale opbrengsten) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.4. Wat is het Verhouding vorderingen en totale opbrengsten?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.4.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        # test.verify_value("waarde", "160000", where_condition=("Kental","P Som der bedrijfsopbrengsten"))
        # test.verify_value("waarde", "10000", where_condition=("Kental","P.I Netto omzet"))
        # test.verify_value("waarde", "150000", where_condition=("Kental","P.IV Overige bedrijfsopbrengsten"))

        # test.verify_value("waarde", "10000", where_condition=("Kental","R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten"))
        # test.verify_value("waarde", "10000", where_condition=("Kental","R.II Andere rentebaten en soortgelijke opbrengsten"))
        # test.verify_value("waarde", "0", where_condition=("Kental","R.III waardeverandering van vorderingen die tot de vaste activa behoren en van effecten"))
        # test.verify_value("waarde", "0", where_condition = ("Kental", "R.IV Rentelasten en soortgelijke kosten"))
        # test.verify_value("waarde", "180000", where_condition=("Kental","R Resultaat voor belastingen"))
        test.verify_value("waarde", "1.204383866666666666666667", where_conditions=[("Kental","Verhouding vorderingen en totale opbrengsten")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

