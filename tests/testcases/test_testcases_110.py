from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK ((Vest. 1254)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], 
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen ZVL functie + Geen AOK ((Vest. 1287)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], 
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Geen ZVL functie + Wel WOK (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Wel WOK (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH. OK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
]

td_03_a = [
    {
        "Description": "Testcase 03a (Wel ZVL functie + Wel WOK (Vest. 1287))",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH. OK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03_b = [
    {
        "Description": "Testcase 03b (Wel ZVL functie + Wel WOK (Vest. 1254 - WOK 2/3 van meetperiode))",
        "Amount": 10, #Indicator score: 20
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-02-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-02-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-02-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-02-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
] 

td_03_c = [
    {
        "Description": "Testcase 03c (Wel ZVL functie + Wel WOK (Vest. 1287 WOK 1/3 van meetperiode))",
        "Amount": 10, #Indicator score: 12.222
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-03-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-03-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-03-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-03-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie (1/3 meetperiode) + Geen ZVL functie (2/3 meetperiode) + Wel WOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 12.222
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-02-01",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-02-01", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-02-01",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-02-01"
                                    }
                                ]
                            }
                        ]

                    },
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-02-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-02-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
]



#Static Tests
def test_if_headers_are_correct_for_query_1_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('zorg')
    test.verify_header_present('niet_zorg')
    test.verify_header_present('totaal')

def test_if_number_of_rows_returned_is_correct_for_query_1_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_1_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_value("zorg", "7.322222222222222222222222",where_conditions=[("vestiging","000001254")])

def test_if_dates_can_change_1_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')

    # Assertions
    # test.verify_value("zorg", "11.122222222222222222222221",where_conditions=[("vestiging","000001254")])
    # test.verify_value("niet_zorg", "1.011111111111111111111111",where_conditions=[("vestiging","000001254")])
    # test.verify_value("totaal", "12.133333333333333333333332",where_conditions=[("vestiging","000001254")])

    # test.verify_value("zorg", "2.022222222222222222222222",where_conditions=[("vestiging","000001287")])
    # test.verify_value("niet_zorg", "1.011111111111111111111111",where_conditions=[("vestiging","000001287")])
    # test.verify_value("totaal", "3.033333333333333333333333",where_conditions=[("vestiging","000001287")])

    # test.verify_value("zorg", "13.144444444444444444444443",where_conditions=[("vestiging","Totaal organisatie")])
    # test.verify_value("niet_zorg", "2.022222222222222222222222",where_conditions=[("vestiging","Totaal organisatie")])
    test.verify_value("totaal", "43.978021978021978021978020",where_conditions=[("vestiging","Totaal organisatie")])


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen WOK- client (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("totaal", "43.978021978021978021978020",where_conditions=[("vestiging","Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_1_0_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie + Geen WOK - client (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("totaal", "43.978021978021978021978020",where_conditions=[("vestiging","Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_1_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel WOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("niet_zorg", "12.362637362637362637362637",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_1_1_0_02_a(db_config):
    """ Testcase 02a (Geen ZVL functie + Wel WOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("niet_zorg", "2.813186813186813186813186",where_conditions=[("vestiging","000001287")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_1_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Wel WOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorg", "29.571428571428571428571429",where_conditions=[("vestiging","000001254")])
        # test.verify_value("niet_zorg", "1.011111111111111111111111",where_conditions=[("vestiging","000001254")])
        # test.verify_value("totaal", "22.244444444444444444444442",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_1_1_0_03_a(db_config):
    """ Testcase 03a (Wel ZVL functie + Wel WOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorg", "19.230769230769230769230768",where_conditions=[("vestiging","000001287")])
        # test.verify_value("niet_zorg", "1.011111111111111111111111",where_conditions=[("vestiging","000001287")])
        # test.verify_value("totaal", "13.144444444444444444444443",where_conditions=[("vestiging","000001287")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03b
def test_if_value_returned_is_correct_for_query_1_1_0_03_b(db_config):
    """ Testcase 03b (Wel ZVL functie + Wel WOK (Vest. 1254 - WOK 2/3 van meetperiode))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("zorg", "17.788888888888888888888891",where_conditions=[("vestiging","000001254")])
        test.verify_value("niet_zorg", "2.362637362637362637362637",where_conditions=[("vestiging","000001254")])
        # test.verify_value("totaal", "18.800000000000000000000002",where_conditions=[("vestiging","000001254")])
        

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03c
def test_if_value_returned_is_correct_for_query_1_1_0_03_c(db_config):
    """ Testcase 03c (Wel ZVL functie + Wel WOK (Vest. 1287 WOK 1/3 van meetperiode))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("zorg", "5.466666666666666666666662",where_conditions=[("vestiging","000001287")])
        test.verify_value("niet_zorg", "2.813186813186813186813186",where_conditions=[("vestiging","000001287")])
        # test.verify_value("totaal", "6.477777777777777777777773",where_conditions=[("vestiging","000001287")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_1_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie (1/3 meetperiode) + Geen ZVL functie (2/3 meetperiode) + Wel WOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1. Gemiddeld aantal personeelsleden (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("zorg", "14.677777777777777777777781",where_conditions=[("vestiging","000001254")])
        test.verify_value("niet_zorg", "12.362637362637362637362637", where_conditions=[("vestiging","000001254")])
        # test.verify_value("totaal", "22.355555555555555555555562",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
