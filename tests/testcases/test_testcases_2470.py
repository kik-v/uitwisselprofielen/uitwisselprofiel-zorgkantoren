from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: "2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:

td_01 = [
    {
        "Description": "Testcase 01 (Geen matching rubrieken overige jaarrekeningsposten (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken jaarrekeningsposten (Sociale kosten) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000011" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken jaarrekeningsposten (811000) + Valt niet in meetperiode)",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-07-01",
                "accounting_item_type": "811000" ,
                "financial_entity_value": 10000  
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel matching rubrieken jaarrekeningsposten (Arbeidsintensiteit PIL & PNIL: 811000 t/m 930000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "811000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "891000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "919000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "920000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "930000",
                "financial_entity_value": 10000
            },
        ]
    }
]




# Static Tests
def test_if_headers_are_correct_for_query_24_7(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL?
    """
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_header_present('kental')
    test.verify_header_present('waarde')

def test_if_number_of_rows_returned_is_correct_for_query_24_7(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_row_count(2)
   
def test_if_indicator_has_correct_value_for_query_24_7(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL?
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

    # test.set_start_period_to("2024-01-01")
    # test.set_end_period_to("2024-12-31")
    test.set_reference_date_new_param_to("2023-12-31")

    # Assertions
    test.verify_value("waarde", "ongedefinieerd", where_conditions=[("kental", "Arbeidsintensiteit PNIL%")])
    # test.verify_valu("waarde", "293.596736633168185299220100", where_condition=("kental", "Arbeidsintensiteit PIL & PNIL%"))


def test_if_dates_can_change_24_7(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL?
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")
    test.set_reference_date_new_param_to("2024-12-31")

    # Assertions
    test.verify_value("waarde", "16.859947173467952424862900", where_conditions=[("kental", "Arbeidsintensiteit PNIL%")])
    # test.verify_valu("waarde", "undefined", where_condition=("kental", "Arbeidsintensiteit PIL & PNIL%"))

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_24_7_01(db_config):
    """ Testcase 01 (Geen matching rubrieken overige bedrijfsopbrengsten (ZZP kosten) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("waarde", "16.859947173467952424862900", where_conditions=[("kental","Arbeidsintensiteit PNIL%")])
        # test.verify_value("waarde", "0", where_condition=("kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_24_7_02(db_config):
    """ Testcase 02 (Geen matching rubrieken overige bedrijfsopbrengsten (Sociale kosten) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("waarde", "16.859947173467952424862900", where_conditions=[("kental","Arbeidsintensiteit PNIL%")])
        # test.verify_value("waarde", "0", where_condition=("kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_24_7_03(db_config):
    """ Testcase 03 (Wel matching rubrieken overige bedrijfsopbrengsten (812000) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("waarde", "1.912734958323211869861300", where_conditions=[("kental","Arbeidsintensiteit PNIL%")])
        # test.verify_value("waarde", "0", where_condition=("kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_24_7_04(db_config):
    """ Testcase 04 (Wel matching rubrieken jaarrekeningsposten (Bedrijfsopbrengsten: 811000 t/m 930000) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.7. Wat is de Arbeidsintensiteit PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.7.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("waarde", "160000", where_condition=("kental","P Som der bedrijfsopbrengsten"))
        # test.verify_value("waarde", "10000", where_condition=("kental","P.I Netto omzet"))
        # test.verify_value("waarde", "0", where_condition=("kental","P.II Wijziging IN voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum"))
        # test.verify_value("waarde", "0", where_condition=("kental","P.III Geactiveerde productie voor het eigen bedrijf"))
        # test.verify_value("waarde", "150000", where_condition=("kental","P.IV Overige bedrijfsopbrengsten"))
        test.verify_value("waarde", "0.133773843521247502629900", where_conditions=[("kental","Arbeidsintensiteit PNIL%")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
