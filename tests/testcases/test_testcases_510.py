from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK ((Vest. 1254)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen ZVL functie + Geen AOK ((Vest. 1287)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel ZVL functie + Geen AOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel ZVL functie + Geen AOK (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen ZVL functie + Wel AOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03_a = [
    {
        "Description": "Testcase 03a (Geen ZVL functie + Wel AOK (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK (Vest. 1287))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK (Vest. 1254 - AOK 2/3 van meetperiode))",
        "Amount": 10, #Indicator score: 20
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-02-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-02-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-02-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-02-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 



#Static Tests
def test_if_headers_are_correct_for_query_5_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('16_20'),
    test.verify_header_present('21_25'),
    test.verify_header_present('26_30'),
    test.verify_header_present('31_35'),
    test.verify_header_present('36_40'),
    test.verify_header_present('41_45'),
    test.verify_header_present('46_50'),
    test.verify_header_present('51_55'),
    test.verify_header_present('56_60'),
    test.verify_header_present('61_65'),
    test.verify_header_present('66_70'),
    test.verify_header_present('71_75')

def test_if_number_of_rows_returned_is_correct_for_query_5_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_5_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
    # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])

# def test_if_indicator_has_correct_value_for_query_5_1_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0) Q1 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.1.rq')
#     # test.set_quarter_to('"Q1"')

#     # Assertions
#     test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
#     # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])

# def test_if_indicator_has_correct_value_for_query_5_1_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1.2. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0) Q2 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.2.rq')
#     # test.set_quarter_to('"Q2"')

#     # Assertions
#     test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
#     # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])

# def test_if_indicator_has_correct_value_for_query_5_1_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1.3. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0) Q3 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.3.rq')
#     # test.set_quarter_to('"Q3"')

#     # Assertions
#     test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
#     # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])

# def test_if_indicator_has_correct_value_for_query_5_1_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1.4. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0) Q4 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.4.rq')
#     # test.set_quarter_to('"Q4"')

#     # Assertions
#     test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
#     # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])

# def test_if_indicator_has_correct_value_for_query_5_1_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1.5. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0) Q1 2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')
#     # test.set_quarter_to('"Q1"')

#     # Assertions
#     test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
#     # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])

def test_if_dates_can_change_for_query_5_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')

    # Assertions
    test.verify_value("21_25", "3",where_conditions=[("vestiging","000001254")])
    # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_5_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK - client (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("21_25", "3",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_5_1_0_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie + Geen AOK - client (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("41_45", "6",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_5_1_0_02(db_config):
    """ Testcase 02 (Wel ZVL functie + Geen AOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query '31_35'
        # test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
        test.verify_value("31_35", "2",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_5_1_0_02_a(db_config):
    """ Testcase 02a (Wel ZVL functie + Geen AOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])
        test.verify_value("31_35", "2",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_5_1_0_03(db_config):
    """ Testcase 03 (Geen ZVL functie + Wel AOK (Vest. 1254)) 
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
        test.verify_value("31_35", "12",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_5_1_0_03_a(db_config):
    """ Testcase 03a (Geen ZVL functie + Wel AOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("41_45", "1",where_conditions=[("vestiging","000001287")])
        test.verify_value("31_35", "11",where_conditions=[("vestiging","000001287")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_5_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("31_35", "12",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_5_1_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("31_35", "11",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_5_1_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie (1/3 meetperiode) + Geen ZVL functie (2/3 meetperiode) + Wel AOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1. Leeftijdsopbouw personeel in loondienst (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 5.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("21_25", "1",where_conditions=[("vestiging","000001254")])
        test.verify_value("31_35", "12",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()