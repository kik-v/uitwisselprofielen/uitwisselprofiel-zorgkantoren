from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: "2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:

td_01 = [
    {
        "Description": "Testcase 01 (Geen matching rubrieken overige jaarrekeningsposten (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken jaarrekeningsposten (Sociale kosten) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000011" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken jaarrekeningsposten (000011) + Valt niet in meetperiode)",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-07-01",
                "accounting_item_type": "811000" ,
                "financial_entity_value": 10000  
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel matching rubrieken jaarrekeningsposten (Arbeidsintensiteit PIL & PNIL: 811000 t/m 422600) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "811000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "891000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "919000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "920000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "930000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "417000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413610",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413620",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414101",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414102",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414103",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414104",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414111",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414113",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414114",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414202",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414203",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414204",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414503",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414504",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414800",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "415000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "416000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419101",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419500onz-fin",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "420100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422410",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422600",
                "financial_entity_value": 10000
            }
        ]
    }
]




# Static Tests
def test_if_headers_are_correct_for_query_24_5(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL?
    """
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

    test.set_start_period_to("2024-01-01")
    test.set_end_period_to("2024-12-31")

    # Assertions
    test.verify_header_present('Kental')
    test.verify_header_present('waarde')

def test_if_number_of_rows_returned_is_correct_for_query_24_5(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

    test.set_start_period_to("2024-01-01")
    test.set_end_period_to("2024-12-31")

    # Assertions
    test.verify_row_count(2)
   
def test_if_indicator_has_correct_value_for_query_24_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL?
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

    test.set_start_period_to("2024-01-01")
    test.set_end_period_to("2024-12-31")

    # Assertions
    test.verify_value("waarde", "2.935967366331681852992201", where_conditions=[("Kental", "Arbeidsintensiteit PIL & PNIL")])
    # test.verify_valu("waarde", "293.596736633168185299220100", where_condition=("Kental", "Arbeidsintensiteit PIL & PNIL%"))


def test_if_dates_can_change_24_5(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL?
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_value("waarde", "ongedefineerd", where_conditions=[("Kental", "Arbeidsintensiteit PIL & PNIL")])
    # test.verify_valu("waarde", "undefined", where_condition=("Kental", "Arbeidsintensiteit PIL & PNIL%"))

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_24_5_01(db_config):
    """ Testcase 01 (Geen matching rubrieken overige bedrijfsopbrengsten (ZZP kosten) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("waarde", "2.935967366331681852992201", where_conditions=[("Kental","Arbeidsintensiteit PIL & PNIL")])
        # test.verify_value("waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_24_5_02(db_config):
    """ Testcase 02 (Geen matching rubrieken overige bedrijfsopbrengsten (Sociale kosten) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("waarde", "2.935967366331681852992201", where_conditions=[("Kental","Arbeidsintensiteit PIL & PNIL")])
        # test.verify_value("waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_24_5_03(db_config):
    """ Testcase 03 (Wel matching rubrieken overige bedrijfsopbrengsten (812000) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("waarde", "0.333080961660191885216398", where_conditions=[("Kental","Arbeidsintensiteit PIL & PNIL")])
        # test.verify_value("waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_24_5_04(db_config):
    """ Testcase 04 (Wel matching rubrieken jaarrekeningsposten (Bedrijfsopbrengsten: 811000 t/m 930000) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.5. Wat is de Arbeidsintensiteit PIL & PNIL?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.5.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("waarde", "160000", where_condition=("Kental","P Som der bedrijfsopbrengsten"))
        # test.verify_value("waarde", "10000", where_condition=("Kental","P.I Netto omzet"))
        # test.verify_value("waarde", "0", where_condition=("Kental","P.II Wijziging IN voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum"))
        # test.verify_value("waarde", "0", where_condition=("Kental","P.III Geactiveerde productie voor het eigen bedrijf"))
        # test.verify_value("waarde", "150000", where_condition=("Kental","P.IV Overige bedrijfsopbrengsten"))
        test.verify_value("waarde", "3.929553422917682242137663", where_conditions=[("Kental","Arbeidsintensiteit PIL & PNIL")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
