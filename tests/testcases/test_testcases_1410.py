from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": ""
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Wel Wlz-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Geen Wlz-indicatie (Zvw Indicatie))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_02_c = [
    {
        "Description": "Testcase 02c (Wel Zorgproces + Geen Wlz-indicatie (Wmo Indicatie))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel Zorgproces + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""] 
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_14_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')
    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('zorgprofiel')
    test.verify_header_present('indicator')

def test_if_number_of_rows_returned_is_correct_for_query_14_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')
    test.set_reference_date_to("2023-01-01")
    # Assertions
    test.verify_row_count(7)

def test_if_indicator_has_correct_value_for_query_14_1_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.5. Aantal clienten per zorgprofiel op 2024-01-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.5.rq')
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

def test_if_indicator_has_correct_value_for_query_14_1_6(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.6. Aantal clienten per zorgprofiel op 2024-04-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.6.rq')
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

def test_if_indicator_has_correct_value_for_query_14_1_7(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.7. Aantal clienten per zorgprofiel op 2024-07-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.7.rq')
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

def test_if_indicator_has_correct_value_for_query_14_1_8(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.8. Aantal clienten per zorgprofiel op 2024-10-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.8.rq')
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
    # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

def test_if_dates_can_change_14_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

    test.set_reference_date_to("2024-01-01")

    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_14_1_0_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')
        
        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_14_1_0_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()



# Testcase 02
# def test_if_value_returned_is_correct_for_query_14_1_0_02(db_config):
#     """ Testcase 02 (Wel Zorgproces + Geen Wlz-Indicatie)
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
#     """

#     # Load defined test data
#     dg = DataGenerator(db_config, td_02)

#     try:
#         # Start a test
#         test = QueryTest(db_config)

#         # Set the query to be tested
#         test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

#         # Change measuring period parameters of query
#         test.set_reference_date_to("2024-07-01")
#         # test.change_end_period("2023-12-31", "2024-12-31")

#         # Verify actual result of the query
#         test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
#         # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
#         # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

#     finally:
#         # Delete previously loaded data
#         dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_14_1_0_02_a(db_config):
    """ Testcase 02a (Geen Zorgproces + Wel Wlz-Indicatie (Niet op peildatum))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_14_1_0_02_b(db_config):
    """ Testcase 02b (Geen Zorgproces + Geen Wlz-Indicatie (Zvw Indicatie))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02c
def test_if_value_returned_is_correct_for_query_14_1_0_02_c(db_config):
    """ Testcase 02c (Geen Zorgproces + Geen Wlz-Indicatie (Wmo Indicatie))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_14_1_0_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_14_1_0_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces (Niet op Peildatum) + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("vestiging","000001287")]) #Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_14_1_0_04(db_config):
    """ Testcase 04 (Wel Zorgproces + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal clienten per zorgprofiel op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "10",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV"),("vestiging","000001287")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) # Vestiging 1287
        # test.verify_value("indicator", "1",where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV")]) # Vestiging 1254

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()