from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK ((Vest. 1254)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen ZVL functie + Geen AOK ((Vest. 1287)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Geen ZVL functie + Wel WOK (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Wel WOK (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH. OK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03_a = [
    {
        "Description": "Testcase 03a (Wel ZVL functie + Wel WOK (Vest. 1287))",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH. OK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
]

td_03_b = [
    {
        "Description": "Testcase 03b (Wel ZVL functie + Wel WOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 20
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-02-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-02-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-02-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-02-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
] 

td_04 = [
    {
        "Description": "Testcase 04 (Wel + Geen ZVL functie + Wel WOK (Vest. 1287))",
        "Amount": 10, #Indicator score: 12.222
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            },
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel + Geen ZVL functie + Wel WOK (Vest. 1254 & 1287))",
        "Amount": 10, #Indicator score: 12.222
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_1_2_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

    test.set_reference_date_new_param_to("2023-01-01")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('Zorg')
    test.verify_header_present('Niet_zorg')
    test.verify_header_present('Totaal')

def test_if_number_of_rows_returned_is_correct_for_query_1_2_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

    test.set_reference_date_new_param_to("2023-01-01")

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_1_2_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')


    test.set_reference_date_new_param_to("2023-01-01")

    # Assertions
    test.verify_value("Zorg", "9",where_conditions=[("vestiging","000001254")])
    # test.verify_value("Niet_zorg", "2",where_conditions=[("vestiging","000001254")])
    # test.verify_value("Totaal", "10",where_conditions=[("vestiging","000001254")])

    # test.verify_value("Zorg", "2",where_conditions=[("vestiging","000001287")])
    # test.verify_value("Niet_zorg", "1",where_conditions=[("vestiging","000001287")])
    # test.verify_value("Totaal", "3",where_conditions=[("vestiging","000001287")])

    # test.verify_value("Zorg", "10",where_conditions=[("vestiging","Totaal organisatie")])
    # test.verify_value("Niet_zorg", "3",where_conditions=[("vestiging","Totaal organisatie")])
    # test.verify_value("Totaal", "13",where_conditions=[("vestiging","Totaal organisatie")])

# def test_if_indicator_has_correct_value_for_query_1_2_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2.1. Aantal personeelsleden op 01-01-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.1.rq')

#     # Assertions
#     test.verify_value("Zorg", "9",where_conditions=[("vestiging","000001254")])

# def test_if_indicator_has_correct_value_for_query_1_2_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2.2. Aantal personeelsleden op 01-04-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.2.rq')

#     # Assertions
#     test.verify_value("Zorg", "9",where_conditions=[("vestiging","000001254")])

# def test_if_indicator_has_correct_value_for_query_1_2_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2.3. Aantal personeelsleden op 01-07-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

#     # Assertions
#     test.verify_value("Zorg", "9",where_conditions=[("vestiging","000001254")])

# def test_if_indicator_has_correct_value_for_query_1_2_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2.4. Aantal personeelsleden op 01-10-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.4.rq')

#     # Assertions
#     test.verify_value("Zorg", "9",where_conditions=[("vestiging","000001254")])

# def test_if_indicator_has_correct_value_for_query_1_2_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2.5. Aantal personeelsleden op 01-01-2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.5.rq')

#     # Assertions
#     test.verify_value("Zorg", "9",where_conditions=[("vestiging","000001254")])

def test_if_dates_can_change_1_2_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

    test.set_reference_date_new_param_to("2024-01-01")

    # Assertions
    # test.verify_value("Zorg", "9",where_conditions=[("vestiging","000001254")])
    # test.verify_value("Niet_zorg", "1",where_conditions=[("vestiging","000001254")])
    # test.verify_value("Totaal", "10",where_conditions=[("vestiging","000001254")])

    # test.verify_value("Zorg", "2",where_conditions=[("vestiging","000001287")])
    # test.verify_value("Niet_zorg", "1",where_conditions=[("vestiging","000001287")])
    # test.verify_value("Totaal", "3",where_conditions=[("vestiging","000001287")])

    # test.verify_value("Zorg", "11",where_conditions=[("vestiging","Totaal organisatie")])
    # test.verify_value("Niet_zorg", "2",where_conditions=[("vestiging","Totaal organisatie")])
    test.verify_value("Totaal", "42",where_conditions=[("vestiging","Totaal organisatie")])


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_2_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen WOK- client (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Totaal", "42",where_conditions=[("vestiging","Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_2_0_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie + Geen WOK - client (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Totaal", "42",where_conditions=[("vestiging","Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_1_2_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel WOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Niet_zorg", "12",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_1_2_0_02_a(db_config):
    """ Testcase 02a (Geen ZVL functie + Wel WOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Niet_zorg", "12",where_conditions=[("vestiging","000001287")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_1_2_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Wel WOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Zorg", "26",where_conditions=[("vestiging","000001254")])
        # test.verify_value("Niet_zorg", "1",where_conditions=[("vestiging","000001254")])
        # test.verify_value("Totaal", "20",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_1_2_0_03_a(db_config):
    """ Testcase 03a (Wel ZVL functie + Wel WOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Zorg", "32",where_conditions=[("vestiging","000001287")])
        # test.verify_value("Niet_zorg", "1",where_conditions=[("vestiging","000001287")])
        # test.verify_value("Totaal", "13",where_conditions=[("vestiging","000001287")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03b
def test_if_value_returned_is_correct_for_query_1_2_0_03_b(db_config):
    """ Testcase 03b (Wel ZVL functie + Wel WOK (Vest. 1254 - WOK niet op peildatum))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Zorg", "16",where_conditions=[("vestiging","000001254")])
        # test.verify_value("Niet_zorg", "1",where_conditions=[("vestiging","000001254")])
        # test.verify_value("Totaal", "10",where_conditions=[("vestiging","000001254")])
        

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_1_2_0_04(db_config):
    """ Testcase 04 (Wel ZVL + Geen ZVL functie + Wel WOK (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        # test.verify_value("Zorg", "12",where_conditions=[("vestiging","000001287")])
        # test.verify_value("Niet_zorg", "2",where_conditions=[("vestiging","000001287")])
        test.verify_value("Totaal", "34",where_conditions=[("vestiging","000001287")])
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_1_2_0_04_a(db_config):
    """ Testcase 04a (Wel + Geen ZVL functie + Wel WOK (Vest. 1254 & 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.2. Aantal personeelsleden op een peildatum (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
#         test.verify_value("Zorg", "19",where_conditions=[("vestiging","000001254")])
#         # test.verify_value("Niet_zorg", "1",where_conditions=[("vestiging","000001254")])
#         # test.verify_value("Totaal", "20",where_conditions=[("vestiging","000001254")])

        # test.verify_value("Zorg", "2",where_conditions=[("vestiging","000001287")])
        # test.verify_value("Niet_zorg", "11",where_conditions=[("vestiging","000001287")])
        test.verify_value("Totaal", "34",where_conditions=[("vestiging","000001287")])
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


