from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024"2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:
# PNIL kostensoorten: 
# WBedOvpUik, WBedOvpUit, WBedOvpMaf, WBedOvpZzp, WBedOvpPay, WBedOvpOip, PM418000

# PIL kostensoorten:
# WPerLes, WPerSol, PM411000, PM412000, PM413000, PM414000, PM415000, PM422100, PM422300, PM422400, PM422410, PM422500, PM422600, PM422900


td_01 = [
    {
        "Description": "Testcase 01 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode, Vest. 1287)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpUik" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WPerLes" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Wel matching rubrieken PNIL (0.5) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 50%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpUit" ,
                "financial_entity_value": 5000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WPerSol" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_b = [
    {
        "Description": "Testcase 01b (Wel matching rubrieken PNIL (1) + Wel matching rubrieken PIL(0.5) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 200%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpZzp" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411000" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_01_c = [
    {
        "Description": "Testcase 01c (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 10%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpMaf" ,
                "financial_entity_value": 1000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412000" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_d = [
    {
        "Description": "Testcase 01d (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 25%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpPay" ,
                "financial_entity_value": 1000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413000" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_e = [
    {
        "Description": "Testcase 01e (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 66%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpOip" ,
                "financial_entity_value": 8000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414000" ,
                "financial_entity_value": 12000  
            }
        ] 
    }
]

td_01_f = [
    {
        "Description": "Testcase 01f (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 75%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418000" ,
                "financial_entity_value": 7500  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "415000" ,
                "financial_entity_value": 10000
            }
        ] 
    }
]

td_01_g = [
    {
        "Description": "Testcase 01g (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 90%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418000" ,
                "financial_entity_value": 4500  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpOip" ,
                "financial_entity_value": 4500  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422100" ,
                "financial_entity_value": 5000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422300" ,
                "financial_entity_value": 5000
            }            
        ] 
    }
]

td_01_h = [
    {
        "Description": "Testcase 01h (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 100%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418000" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422400" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422410" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422500" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422600" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422900" ,
                "financial_entity_value": 2000
            }           
        ] 
    }
]

td_01_i = [
    {
        "Description": "Testcase 01i (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode, Vest. 1254)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_De_Beuk",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpUik" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_De_Beuk",
                "date": "2024-01-01",
                "accounting_item_type": "WPerLes" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
        #     {
        #         "costsite": "kp_Grotestraat",
        #         "date": "2024-01-01",
        #         "accounting_item_type": "WBedOvpZzp" ,
        #         "financial_entity_value": 10000  
        #     },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WPerSol" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken PNIL + Geen matching rubrieken PIL + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpZzp" ,
                "financial_entity_value": 10000  
            },
            # {
            #     "costsite": "kp_Grotestraat",
            #     "date": "2024-01-01",
            #     "accounting_item_type": "WPerSol" ,
            #     "financial_entity_value": 5000  
            # }
        ] 
    }
]

td_04 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt niet in meetperiode) ",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "WBedOvpZzp" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "WPerSol" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]


# Static Tests
def test_if_headers_are_correct_for_query_8_2_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('totaal_pil_kosten')
    test.verify_header_present('totaal_pnil_kosten')
    test.verify_header_present('percentage_kosten_pnil')


def test_if_number_of_rows_returned_is_correct_for_query_8_2_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')
    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_row_count(2)


def test_if_indicator_has_correct_value_for_query_8_2_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')
    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_value('percentage_kosten_pnil', "0E+2", where_conditions=[("vestiging","Totaal"),("totaal_pil_kosten","917.49")])

def test_if_indicator_has_correct_value_for_query_8_2_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.5. Percentage kosten Personeel Niet in Loondienst (PNIL) Q1 2024
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.5.rq')
    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_value('percentage_kosten_pnil', "48.176763504008217403925600", where_conditions=[("vestiging","Totaal")])#,("totaal_pil_kosten","917.49")])

def test_if_indicator_has_correct_value_for_query_8_2_6(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.6. Percentage kosten Personeel Niet in Loondienst (PNIL) Q2 2024
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.6.rq')
    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_value('percentage_kosten_pnil', None)

def test_if_indicator_has_correct_value_for_query_8_2_7(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.7. Percentage kosten Personeel Niet in Loondienst (PNIL) Q3 2024
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.7.rq')
    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_value('percentage_kosten_pnil', None)

def test_if_indicator_has_correct_value_for_query_8_2_8(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.8. Percentage kosten Personeel Niet in Loondienst (PNIL) Q4 2024
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.8.rq')
    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_value_in_list('percentage_kosten_pnil', (None,None))

def test_if_dates_can_set_for_query_8_2_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

    test.set_start_period_to("2024-01-01")
    test.set_end_period_to("2024-12-31")

    # Assertions
    test.verify_value('percentage_kosten_pnil', "48.176763504008217403925600", where_conditions=[("vestiging","Totaal")])#,("totaal_pil_kosten","917.49")])


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_8_2_0_01(db_config):
    """ Testcase 01 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "49.960069112468168993971500", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_8_2_0_01_a(db_config):
    """ Testcase 01a (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "33.763641883682044662583700", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01b
def test_if_value_returned_is_correct_for_query_8_2_0_01_b(db_config):
    """ Testcase 01b (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil',"66.130647476053270912484100", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01c
def test_if_value_returned_is_correct_for_query_8_2_0_01_c(db_config):
    """ Testcase 01c (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "10.619916613017488904010600", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01d
def test_if_value_returned_is_correct_for_query_8_2_0_01_d(db_config):
    """ Testcase 01d (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "10.619916613017488904010600", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01e
def test_if_value_returned_is_correct_for_query_8_2_0_01_e(db_config):
    """ Testcase 01e (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil',"40.179080127328914608542800",where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01f
def test_if_value_returned_is_correct_for_query_8_2_0_01_f(db_config):
    """ Testcase 01f (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "42.989876770617952142403800", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01g
def test_if_value_returned_is_correct_for_query_8_2_0_01_g(db_config):
    """ Testcase 01g (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "47.387034954542486231111600", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01h
def test_if_value_returned_is_correct_for_query_8_2_0_01_h(db_config):
    """ Testcase 01h (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_h)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "49.960069112468168993971500", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01i
def test_if_value_returned_is_correct_for_query_8_2_0_01_i(db_config):
    """ Testcase 01i (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_i)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "49.960069112468168993971500", where_conditions=[("vestiging", "Totaal")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_8_2_0_02(db_config):
    """ Testcase 02 (Geen matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "3.960292446717316803204200", where_conditions=[("vestiging", "Totaal")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_8_2_0_03(db_config):
    """ Testcase 03 (Wel matching rubrieken PNIL + Geen matching rubrieken PIL + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "97.778677486138269860822800", where_conditions=[("vestiging", "Onbekend")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_8_2_0_04(db_config):
    """ Testcase 04 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.2.0. Percentage kosten Personeel Niet in Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.2.0.rq')

        # set measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value('percentage_kosten_pnil', "48.176763504008217403925600", where_conditions=[("vestiging","Totaal")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()