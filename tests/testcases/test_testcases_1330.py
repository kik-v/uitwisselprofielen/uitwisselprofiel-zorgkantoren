from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2023-12-31

# Opmerkingen:
# -1 betekent peildatum - 1 kwartaal/3 maanden

# Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD -3M)",
        "Amount": 10,
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST OK niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_01_b = [
    {
        "Description": "Testcase 01a (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD -3M)",
        "Amount": 10,
        "Human": [
            {
                "UitzendOvereenkomst": [
                    {

                        "function": [
                            {
                                "label": "UTZ OK niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL Kwal. Niv 3 + 3",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 4))",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL Kwal. Niv 3 + 3",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (4 & 3))",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL Kwal. Niv 3 + 3",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (3 & NA))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (NA & 3))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_05_c = [
    {
        "Description": "Testcase 05c (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (3 (Niet op PD) & 4))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2022-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2022-01-01",
                                        "end_date" : "2022-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2022-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2022-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2022-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_05_d = [
    {
        "Description": "Testcase 05d (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (3 & 4 (Niet op PD)))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2022-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2022-01-01",
                                        "end_date" : "2022-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2022-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2022-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2022-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 07a (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 4))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_07_b = [
    {
        "Description": "Testcase 07b (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (4 & 3))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 4))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. 3",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. 4",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_08_b = [
    {
        "Description": "Testcase 08b (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (4 & 3))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. 4",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. 3",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_08_c = [
    {
        "Description": "Testcase 08c (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (Behandelaar & 3))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. Behandelaar",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Behandelaar",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. 3",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_08_d = [
    {
        "Description": "Testcase 08d (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & Leerling))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. 3",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Kwal. Niv. Leerling",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31", 
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2023-01-01",
                                        "end_date" : "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_for_query_13_3_0(db_config):
    """Test of de juiste header terugkomt in het resultaat
       Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')
    test.set_reference_date_new_param_to('2023-01-01')

    # Assertions
    # test.verify_header_present('teller_waarde')
    # test.verify_header_present('noemer_waarde')
    test.verify_header_present('perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau')

def test_if_number_of_rows_returned_is_correct_for_query_13_3_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

    test.set_reference_date_new_param_to('2023-01-01')

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_13_3_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 01-01-2024
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')
    test.set_reference_date_new_param_to('2023-01-01')

    # Assertions
    test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "12.5")

# def test_if_indicator_has_correct_value_for_query_13_3_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.1 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 01-01-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.1.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

# def test_if_indicator_has_correct_value_for_query_13_3_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.2 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 01-04-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.2.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

# def test_if_indicator_has_correct_value_for_query_13_3_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 01-07-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.3.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

# def test_if_indicator_has_correct_value_for_query_13_3_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.4 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 01-10-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.4.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

# def test_if_indicator_has_correct_value_for_query_13_3_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.5 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 01-01-2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.5.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

# def test_if_indicator_has_correct_value_for_query_13_3_6(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.6 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 2024-04-01
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.6.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

# def test_if_indicator_has_correct_value_for_query_13_3_7(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.7 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 2024-07-01
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.7.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

# def test_if_indicator_has_correct_value_for_query_13_3_8(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3.8 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau 2024-10-01
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.8.rq')
#     # test.set_reference_date_new_param_to('2024-01-01')
#     # Assertions
#     test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", {"4", "0.0"})

def test_if_dates_can_change_13_3_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

    test.set_reference_date_new_param_to('2024-01-01')

    # Assertions
    test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau","4")

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_13_3_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD -3M)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_13_3_0_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau","4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 01b
def test_if_value_returned_is_correct_for_query_13_3_0_01_b(db_config):
    """ Testcase 01b (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_13_3_0_02(db_config):
    """ Testcase 02 (Wel ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_13_3_0_03(db_config):

    """ Testcase 03 (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_13_3_0_04(db_config):
    """ Testcase 04 (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_13_3_0_04_a(db_config):
    """ Testcase 04a (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 4))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_13_3_0_04_b(db_config):
    """ Testcase 04b (Geen ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (4 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_13_3_0_05(db_config):
    """ Testcase 05 (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_13_3_0_05_a(db_config):
    """ Testcase 05a (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (3 & NA))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_13_3_0_05_b(db_config):
    """ Testcase 05b (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (NA & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05c
def test_if_value_returned_is_correct_for_query_13_3_0_05_c(db_config):
    """ Testcase 05c (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (3 (Niet op PD) & 4))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "2.857142857142857142857143")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05d
def test_if_value_returned_is_correct_for_query_13_3_0_05_d(db_config):
    """ Testcase 05d (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Geen Kwal. Niv. op PD & PD - 3M (3 & 4 (Niet op PD)))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "2.857142857142857142857143")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_13_3_0_06(db_config):
    """ Testcase 06 (Wel ZVL functie op PD & PD - 3M + Geen AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_13_3_0_07(db_config):
    """ Testcase 07 (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a
def test_if_value_returned_is_correct_for_query_13_3_0_07_a(db_config):
    """ Testcase 07a (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07b
def test_if_value_returned_is_correct_for_query_13_3_0_07_b(db_config):
    """ Testcase 07b (Geen ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_13_3_0_08(db_config):
    """ Testcase 08 (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08a
def test_if_value_returned_is_correct_for_query_13_3_0_08_a(db_config):
    """ Testcase 08a (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & 4))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "2.857142857142857142857143")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b
def test_if_value_returned_is_correct_for_query_13_3_0_08_b(db_config):
    """ Testcase 08b (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (4 & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "31.428571428571428571428571")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08c
def test_if_value_returned_is_correct_for_query_13_3_0_08_c(db_config):
    """ Testcase 08c (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (3 & Behandelaar))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08d
def test_if_value_returned_is_correct_for_query_13_3_0_08_d(db_config):
    """ Testcase 08d (Wel ZVL functie op PD & PD - 3M + Wel AOK op PD & PD - 3M + Wel Kwal. Niv. op PD & PD - 3M (Leerling & 3))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.3 Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.3.rq')

        # Change the peildatum of the query
        test.set_reference_date_new_param_to('2024-01-01')

        # Verify actual result of the query
        test.verify_value("perc_personeelsleden_in_loondienst_met_een_oplopend_kwalificatieniveau", "4")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()