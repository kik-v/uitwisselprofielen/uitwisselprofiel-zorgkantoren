from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-12-31

#Opmerkingen:

#Testcases:
 
td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen ODB Kwalificatieniveau + Geen PIL)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Geen PIL)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK + Geen ODB Kwalificatieniveau + Geen PIL)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Geen AOK + Geen PIL + ODB Kwalificatieniveau 1 - INH OK Inzet 8u)",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL ODB 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Wel PIL - AOK Inzet 8u)",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 2 - AOK Inzet 8u)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 2",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 2",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_c = [
    {
        "Description": "Testcase 05c (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 3 - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 3",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_d = [
    {
        "Description": "Testcase 05d (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 4 - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 4",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_e = [
    {
        "Description": "Testcase 05e (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 5 - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 5",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 5",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_f = [
    {
        "Description": "Testcase 05f (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 6 - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 6",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 6",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_g = [
    {
        "Description": "Testcase 05g (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Behandelaar - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Behandelaar",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Behandelaar",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_h = [
    {
        "Description": "Testcase 05h (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Leerling - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Leerling",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_i = [
    {
        "Description": "Testcase 05i (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Overig zorgpersoneel - AOK Inzet 8u, Vest. 1254)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Overig zorgpersoneel",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Overig zorgpersoneel",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_j = [
    {
        "Description": "Testcase 05j (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 + Niet AOK + ODB Kwalificatieniveau 1 (18u p/w + 18u p/w))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "",
                                        "end_date": "" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_k = [
    {
        "Description": "Testcase 05k (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 8u, Vest. 1287)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Geen ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 8u, Vest. 1254 )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + ODB Kwalificatieniveau 1 - AOK Inzet 8u, Vest. 1254 )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK (niet in meetperiode) Geen ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2023-01-01T09:00:00",
                                "end_datetime": "2023-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + Geen ODB Kwalificatieniveau - AOK Inzet 8u, Vest. 1254 )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK (niet in meetperiode) Geen ZVL ",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_for_query_6_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

    # test.set_start_period_to("2023-01-01")
    test.set_quarter_to("'Q1'")
    test.set_year_to("2023")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('Kwalificatieniveau')
    test.verify_header_present('ingezette_uren_aan_zorgverleners')
    test.verify_header_present('percentage')

def test_if_number_of_rows_returned_is_correct_for_query_6_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

    # test.set_start_period_to("2023-01-01")
    test.set_quarter_to("'Q1'")
    test.set_year_to("2023")

    # Assertions
    test.verify_row_count(27)


def test_if_indicator_has_correct_value_for_query_6_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

    # test.set_start_period_to("2023-01-01")
    test.set_quarter_to("'Q1'")
    test.set_year_to("2023")

    # Assertions
    test.verify_value("percentage", "0",where_conditions=[("vestiging", "000001287")])

def test_if_dates_can_change_for_query_6_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0): 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

    # Change measuring period parameters of query 

    # test.set_start_period_to("2024-01-01")
    test.set_quarter_to("'Q1'")
    test.set_year_to("2024")

    # Assertions
    test.verify_value("ingezette_uren_aan_zorgverleners", None)
    # test.verify_value("percentage", "100", where_conditions=[("vestiging", "000001287"))
    # test.verify_value("percentage", None)

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_6_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK + Geen ODB Kwalificatieniveau + Geen PIL (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("ingezette_uren_aan_zorgverleners", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_6_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Geen PIL (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')
  
        # Change measuring period parameters of query 
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("ingezette_uren_aan_zorgverleners", None)
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_6_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK + Geen ODB Kwalificatieniveau + Geen PIL (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("ingezette_uren_aan_zorgverleners", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_6_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Geen AOK + ODB Kwalificatieniveau 1 + Geen PIL (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_6_1_0_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_6_1_0_05_a(db_config):
    """ Testcase 05a (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Kwalificatieniveau 1")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_6_1_0_05_b(db_config):
    """ Testcase 05b (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 2 + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Kwalificatieniveau 2")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05c
def test_if_value_returned_is_correct_for_query_6_1_0_05_c(db_config):
    """ Testcase 05c (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 3 + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Kwalificatieniveau 3")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05d
def test_if_value_returned_is_correct_for_query_6_1_0_05_d(db_config):
    """ Testcase 05d (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 4 + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Kwalificatieniveau 4")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05e
def test_if_value_returned_is_correct_for_query_6_1_0_05_e(db_config):
    """ Testcase 05e (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 5 + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Kwalificatieniveau 5")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05f
def test_if_value_returned_is_correct_for_query_6_1_0_05_f(db_config):
    """ Testcase 05f (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 6 + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Kwalificatieniveau 6")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05g
def test_if_value_returned_is_correct_for_query_6_1_0_05_g(db_config):
    """ Testcase 05g (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Behandelaar + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"),("Kwalificatieniveau", "Behandelaar")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05h
def test_if_value_returned_is_correct_for_query_6_1_0_05_h(db_config):
    """ Testcase 05h (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Leerling + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_h)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Leerling")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05i
def test_if_value_returned_is_correct_for_query_6_1_0_05_i(db_config):
    """ Testcase 05i (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Overig Zorgpersoneel + Wel PIL - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_i)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"), ("Kwalificatieniveau","Overig zorgpersoneel")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05k
def test_if_value_returned_is_correct_for_query_6_1_0_05_k(db_config):
    """ Testcase 05k (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 + Niet AOK + ODB Kwalificatieniveau 1 (8u + 8u), Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_k)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", "80",where_conditions=[("vestiging","Totaal organisatie"),("Kwalificatieniveau","Kwalificatieniveau 1")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_6_1_0_06(db_config):
    """ Testcase 06 (Geen ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 8u, Vest. 1254 )
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()    


# Testcase 07
def test_if_value_returned_is_correct_for_query_6_1_0_07(db_config):
    """ Testcase 07 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + ODB Kwalificatieniveau 1 - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()    

# Testcase 08
def test_if_value_returned_is_correct_for_query_6_1_0_08(db_config):
    """ Testcase 08 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + Geen ODB Kwalificatieniveau - AOK Inzet 8u, Vest. 1254)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 6.1 Percentage ingezette uren personeel per kwalificatieniveau (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 6.1.rq')

        # Change measuring period parameters of query   
        # test.set_start_period_to("2024-01-01")
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value("ingezette_uren_aan_zorgverleners", "8",where_conditions=[("vestiging","Totaal organisatie")])
        test.verify_value("ingezette_uren_aan_zorgverleners", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()    
