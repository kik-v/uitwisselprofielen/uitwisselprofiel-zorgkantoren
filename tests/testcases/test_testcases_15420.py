from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Wel Wlz-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Zvw-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_c = [
    {
        "Description": "Testcase 02c (Wel Zorgproces + Wmo-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel Zorgproces + Wlz-indicatie (Geen Sector))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel Zorgproces + Wlz-indicatie (4VV))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel Zorgproces + Wlz-indicatie (1LG))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1LG"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel Zorgproces + Wlz-indicatie (2LVG))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["2LVG"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_d = [
    {
        "Description": "Testcase 04d (Wel Zorgproces + Wlz-indicatie (3VG))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["3VG"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_e = [
    {
        "Description": "Testcase 04e (Wel Zorgproces + Wlz-indicatie (3ZGAUD))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["3ZGAUD"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_f = [
    {
        "Description": "Testcase 04f (Wel Zorgproces + Wlz-indicatie (5ZGVIS))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["5ZGVIS"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_g = [
    {
        "Description": "Testcase 04g (Wel Zorgproces + Wlz-indicatie (3GGZ-B))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["3GGZ-B"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_h = [
    {
        "Description": "Testcase 04h (Wel Zorgproces + Wlz-indicatie (7GGZ-W))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["7GGZ_W"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_15_4_2_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_header_present("aantal_vv")
    test.verify_header_present('aantal_lg')
    test.verify_header_present('aantal_lvg')
    test.verify_header_present('aantal_vg')
    test.verify_header_present('aantal_zgaud')
    test.verify_header_present('aantal_zgvis')
    test.verify_header_present('aantal_ggzb')
    test.verify_header_present('aantal_ggzw')

def test_if_number_of_rows_returned_is_correct_for_query_15_4_2_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_row_count(3)

def test_if_indicator_has_correct_value_for_query_15_4_2_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

def test_if_indicator_has_correct_value_for_query_15_4_2_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.5 Aantal cliënten met een Wlz-indicatie per sector op 2024-01-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.5.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

def test_if_indicator_has_correct_value_for_query_15_4_2_6(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.6 Aantal cliënten met een Wlz-indicatie per sector op 2024-04-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.6.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

def test_if_indicator_has_correct_value_for_query_15_4_2_7(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.7 Aantal cliënten met een Wlz-indicatie per sector op 2024-07-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.7.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

def test_if_indicator_has_correct_value_for_query_15_4_2_8(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.8 Aantal cliënten met een Wlz-indicatie per sector op 2024-10-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.8.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

def test_if_dates_can_change_15_4_2_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
    test.set_reference_date_to("2024-07-01")

    # test.change_reference_date("2023-01-01","2024-07-01")

    # Assertions
    test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_15_4_2_0_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_15_4_2_0_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
    	
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_15_4_2_0_02(db_config):
    """ Testcase 02 (Wel Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_15_4_2_0_02_a(db_config):
    """ Testcase 02a (Geen Zorgproces + Wel Wlz-Indicatie (Niet op peildatum))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_15_4_2_0_02_b(db_config):
    """ Testcase 02b (Geen Zorgproces + Wel Zvw-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02c
def test_if_value_returned_is_correct_for_query_15_4_2_0_02_c(db_config):
    """ Testcase 02c (Geen Zorgproces + Wel Wmo-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_15_4_2_0_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_15_4_2_0_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_15_4_2_0_04(db_config):
    """ Testcase 04 (Geen Zorgproces + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "1",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_a(db_config):
    """ Testcase 04a (Wel Zorgproces + Wel Wlz-Indicatie (4VV))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vv", "12",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_b(db_config):
    """ Testcase 04b (Wel Zorgproces + Wel Wlz-Indicatie (1LG))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_lg", "12",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_c(db_config):
    """ Testcase 04c (Wel Zorgproces + Wel Wlz-Indicatie (2LVG))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_lvg", "14",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_d(db_config):
    """ Testcase 04d (Wel Zorgproces + Wel Wlz-Indicatie (3VG))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_vg", "13",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04e
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_e(db_config):
    """ Testcase 04e (Wel Zorgproces + Wel Wlz-Indicatie (3ZGAUD))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_zgaud", "10",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04f
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_f(db_config):
    """ Testcase 04f (Wel Zorgproces + Wel Wlz-Indicatie (5ZGVIS))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_zgvis", "13",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04g
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_g(db_config):
    """ Testcase 04g (Wel Zorgproces + Wel Wlz-Indicatie (3GGZ-B))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_ggzb", "12",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04h
def test_if_value_returned_is_correct_for_query_15_4_2_0_04_h(db_config):
    """ Testcase 04h (Wel Zorgproces + Wel Wlz-Indicatie (7GGZ-w))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.2.0 Aantal cliënten met een Wlz-indicatie per sector
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_h)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.2.0.rq')
        
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query

        test.verify_value("aantal_ggzw", "12",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()