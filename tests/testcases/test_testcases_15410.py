from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:


td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen financieringsstroom (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen financieringsstroom (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":["pgb"]
                    }
                ]
            }
        ]
    }
] 

td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen financieringsstroom (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Geen financieringsstroom (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Geen financieringsstroom + Wel Ciz indicatie (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
               "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm":[""]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel financieringsstroom (Wlz, Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ]
    }
]

td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (niet op Peildatum) + Zvw financieringsstroom (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel Zorgproces + Wel financieringsstroom (Wlz, Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        # "location": "Test locatie 1",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm":[""]
                    }
                ],
                "Occupancy": [
                    {   
                        "organisation_name": "Test zorg organisatie",
                        "kvk_number": "111111111",
                        "branch_details": [
                            {
                                "branch_name": "Test vestiging",
                                "branch_number": "000009988",
                                "branch_address": "De Dam 1",
                                "location_details": [
                                    {
                                        "location_name": "Test locatie 1",
                                        "housing_unit_details": [
                                                    {
                                                        "capacity": "5",
                                                    }
                                        ] 
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ] 
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel Zorgproces + Wel financieringsstroom (Zvw, Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        # "location": "Test locatie 1",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm":[""]
                    }
                ],
                "Occupancy": [
                    {   
                        "organisation_name": "Test zorg organisatie",
                        "kvk_number": "111111111",
                        "branch_details": [
                            {
                                "branch_name": "Test vestiging",
                                "branch_number": "000009988",
                                "branch_address": "De Dam 1",
                                "location_details": [
                                    {
                                        "location_name": "Test locatie 1",
                                        "housing_unit_details": [
                                                    {
                                                        "capacity": "5",
                                                    }
                                        ] 
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ] 
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel Zorgproces + Wel financieringsstroom (Wmo, Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        # "location": "Test locatie 1",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm":[""]
                    }
                ],
                "Occupancy": [
                    {   
                        "organisation_name": "Test zorg organisatie",
                        "kvk_number": "111111111",
                        "branch_details": [
                            {
                                "branch_name": "Test vestiging",
                                "branch_number": "000009988",
                                "branch_address": "De Dam 1",
                                "location_details": [
                                    {
                                        "location_name": "Test locatie 1",
                                        "housing_unit_details": [
                                                    {
                                                        "capacity": "5",
                                                    }
                                        ] 
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ] 
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_15_4_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')
    test.set_reference_date_to("2024-07-01")
    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('aantal_wlz')
    test.verify_header_present('aantal_wmo')
    test.verify_header_present('aantal_zvw')

def test_if_number_of_rows_returned_is_correct_for_query_15_4_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')
    test.set_reference_date_to("2024-07-01")
    # Assertions
    test.verify_row_count(3)

def test_if_indicator_has_correct_value_for_query_15_4_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

    test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
    # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
    # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])


def test_if_indicator_has_correct_value_for_query_15_4_1_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.5. Aantal cliënten per financieringsstroom op 2024-01-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.5.rq')

    # test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
    # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
    # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

def test_if_indicator_has_correct_value_for_query_15_4_1_6(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.6. Aantal cliënten per financieringsstroom op 2024-04-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.6.rq')

    # test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
    # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
    # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

def test_if_indicator_has_correct_value_for_query_15_4_1_7(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.7. Aantal cliënten per financieringsstroom op 2024-07-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.7.rq')

    # test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
    # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
    # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

def test_if_indicator_has_correct_value_for_query_15_4_1_8(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.8. Aantal cliënten per financieringsstroom op 2024-10-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.8.rq')

    # test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
    # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
    # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

def test_if_dates_can_change_15_4_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')
    
    test.set_reference_date_to("2024-10-01")

    # Assertions
    test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
    # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
    # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_15_4_1_0_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen financieringsstroom (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
        # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_15_4_1_0_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen financieringsstroom (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
        # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_15_4_1_0_02(db_config):
    """ Testcase 02 (Wel Zorgproces + Geen financieringsstroom (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
        # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_15_4_1_0_02_a(db_config):
    """ Testcase 02a (Wel Zorgproces + Wel financieringsstroom niet op peildatum (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
        # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_15_4_1_0_02_b(db_config):
    """ Testcase 02b (Wel Zorgproces + Geen financieringsstroom + Wel Ciz indicatie (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
        # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_15_4_1_0_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel financieringsstroom (Wlz, Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
        # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_15_4_1_0_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces (niet op Peildatum) + Zvw financieringsstroom (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "31",where_conditions=[("vestiging","Totaal Dummy Zorg B.V.")])
        # test.verify_value("aantal_wlz", "17",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_wlz", "14",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_15_4_1_0_04(db_config):
    """ Testcase 04 (Wel Zorgproces + Wel financieringsstroom (Wlz, Vest. 9988))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_wlz", "10",where_conditions=[("vestiging","000009988")])
        # test.verify_value("aantal_wmo", "0",where_conditions=[("vestiging","000009988")])
        # test.verify_value("aantal_zvw", "0",where_conditions=[("vestiging","000009988")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_15_4_1_0_04_a(db_config):
    """ Testcase 04a (Wel Zorgproces + Wel financieringsstroom (Zvw, Vest. 9988))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_wlz", "0",where_conditions=[("vestiging","000009988")])
        # test.verify_value("aantal_wmo", "0",where_conditions=[("vestiging","000009988")])
        test.verify_value("aantal_zvw", "10",where_conditions=[("vestiging","000009988")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_15_4_1_0_04_b(db_config):
    """ Testcase 04b (Wel Zorgproces + Wel financieringsstroom (Wmo, Vest. 9988))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.1.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_wlz", "0",where_conditions=[("vestiging","000009988")])
        test.verify_value("aantal_wmo", "10",where_conditions=[("vestiging","000009988")])
        # test.verify_value("aantal_zvw", "0",where_conditions=[("vestiging","000009988")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
        