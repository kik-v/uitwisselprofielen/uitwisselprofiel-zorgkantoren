from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2023-12-31

# Opmerkingen:


# Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen Wooneenheid - Alleen organisatie)",
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ],
        "Occupancy": [
            {   
                "organisation_name": "Test zorg organisatie",
                "kvk_number": "111111111",
            }
        ] 
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Wooneenheid - Organisatie + Vestiging)",
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ],

        "Occupancy": [
            {   
                "organisation_name": "Test zorg organisatie",
                "kvk_number": "111111111",
                "branch_details": [
                    {
                        "branch_name": "Iets_Buiten",
                        "branch_number": "000009988",
                        "branch_address": "Park 1",
                    }
                ]
            }
        ] 
    }
]

td_01_b = [
    {
        "Description": "Testcase 01b (Geen Wooneenheid - Organisatie + Vestiging + Locatie 1)",
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ],
        "Occupancy": [
            {   
                "organisation_name": "Test zorg organisatie",
                "kvk_number": "111111111",
                "branch_details": [
                    {
                        "branch_name": "Iets_Buiten",
                        "branch_number": "000009988",
                        "branch_address": "Park 1",
                        "location_details": [
                            {
                                "location_name": "Locatie_park_1",    
                            }
                        ]
                    }
                ]
            }
        ] 
    }
]

td_01_c = [
    {    
        "Description": "Testcase 01c (Geen Wooneenheid - Organisatie + Vestiging + Locatie 1 + Locatie 2)",
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ],
        "Occupancy": [
            {   
                "organisation_name": "Test zorg organisatie",
                "kvk_number": "111111111",
                "branch_details": [
                    {
                        "branch_name": "Iets_Buiten",
                        "branch_number": "000009988",
                        "branch_address": "Park 1",
                        "location_details": [
                            {
                                "location_name": "Locatie_park_1",    
                                "location_details_2": [
                                    {
                                        "location_2_name": "Eerste_verdieping",
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ] 
    }
]


td_02 = [
    {    
        "Description": "Testcase 02 (Wel Wooneenheid - 1x Locatie 1)",
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ],
        "Occupancy": [
            {   
                "organisation_name": "Test zorg organisatie",
                "kvk_number": "111111111",
                "branch_details": [
                    {
                        "branch_name": "Iets_Buiten",
                        "branch_number": "000009988",
                        "branch_address": "Park 1",
                        "location_details": [
                            {
                                "location_name": "Locatie_park_1",
                                "housing_unit_details": [
                                        {
                                            "capacity": "3"
                                        }
                                ]
                            }
                        ]
                    }
                ]
            }
        ] 
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Wooneenheid - 1x Locatie 1 & 1x Locatie 2)",
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm":[""]
                    }
                ]
            }
        ],
        "Occupancy": [
            {   
                "organisation_name": "Test zorg organisatie",
                "kvk_number": "111111111",
                "branch_details": [
                    {
                        "branch_name": "Iets_Buiten",
                        "branch_number": "000009988",
                        "branch_address": "Park 1",
                        "location_details": [
                            {
                                "location_name": "Locatie_park_1",
                                "housing_unit_details": [
                                            {
                                                "capacity": "3"
                                            }
                                        ]
                            },
                            {
                                "location_name": "Locatie_park_2",
                                "housing_unit_details": [
                                            {
                                                "capacity": "3"
                                            }
                                        ]
                            }
                        ]
                    }
                ]
            }
        ] 
    }
]


#Static Tests

def test_if_headers_are_correct_for_query_15_3(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')


    # Assertions
    test.verify_header_present("vestiging")
    test.verify_header_present("capaciteit")

def test_if_number_of_rows_returned_is_correct_for_query_15_3(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution

    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_15_3(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

    # Assertions
    test.verify_value("capaciteit", "64",where_conditions=[('vestiging', "000001254")])

# Tests using Generated Data

# Testcase 01

def test_if_value_returned_is_correct_for_query_15_3_01(db_config):
    """ Testcase 01 (Geen wooneenheid - Organisatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

        # Change the peildatum of the query
        # test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("capaciteit", "0",where_condition=("vestiging","Totaal Test zorg organisatie"))
        test.verify_value("capaciteit", "64",where_conditions=[('vestiging', "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a

def test_if_value_returned_is_correct_for_query_15_3_01_a(db_config):
    """ Testcase 01a (Geen Wooneenheid - Organisatie + Vestiging)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

        # Change the peildatum of the query
        # test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("capaciteit", "0",where_condition=("vestiging","Totaal Test zorg organisatie"))
        test.verify_value("capaciteit", "64",where_conditions=[('vestiging', "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01b

def test_if_value_returned_is_correct_for_query_15_3_01_b(db_config):
    """ Testcase 01b (Geen Wooneenheid - Organisatie + Vestiging + Locatie 1)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

        # Change the peildatum of the query
        # test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("capaciteit", "0",where_condition=("vestiging","Totaal Test zorg organisatie"))
        test.verify_value("capaciteit", "64",where_conditions=[('vestiging', "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01c

def test_if_value_returned_is_correct_for_query_15_3_01_c(db_config):
    """ Testcase 01c (Geen Wooneenheid - Organisatie + Vestiging + Locatie 1 + Locatie 2)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

        # Change the peildatum of the query
        # test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("capaciteit", "0",where_condition=("vestiging","Totaal Test zorg organisatie"))
        test.verify_value("capaciteit", "64",where_conditions=[('vestiging', "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_15_3_02(db_config):
    """ Testcase 02 (Wel wooneenheid - Locatie 1)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

        # Change the peildatum of the query
        # test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("capaciteit", "3",where_conditions=[("vestiging","000009988")])
        

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02_a

def test_if_value_returned_is_correct_for_query_15_3_02_a(db_config):
    """ Testcase 02a (Wel wooneenheid - Locatie 2)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3. Aantal personen dat per vestiging kan wonen
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.3.rq')

        # Change the peildatum of the query
        # test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("capaciteit", "6",where_conditions=[("vestiging","000009988")])
        

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

