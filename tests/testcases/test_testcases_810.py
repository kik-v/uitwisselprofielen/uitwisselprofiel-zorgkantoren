from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-12-31

#Opmerkingen:

#Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK/INH/UITZ )",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK )",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK )",
        "Amount": 10, # Indicator score =  0.212184629694221602139290 (10 * 36/36 = 10. 10/47 = 0.212...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK (8u inzet, Vest. 1287))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK + Wel INH OK (8u + 8u inzet))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel INH OK (8u inzet)",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel UTZ OK + Wel INH OK (8u + 8u inzet))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "UitzendOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",   
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",     
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 



#Static Tests

def test_if_headers_are_correct_for_query_8_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('uren_pil')
    test.verify_header_present('uren_pnil')
    test.verify_header_present('percentage_ingezette_uren_pnil')


def test_if_number_of_rows_returned_is_correct_for_query_8_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_8_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_value("percentage_ingezette_uren_pnil","0", where_conditions=[("vestiging", "Totaal organisatie")])

# def test_if_indicator_has_correct_value_for_query_8_1_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0) Q1 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.1.rq')
#     # test.set_quarter_to("Q1")

#     # Assertions
#     test.verify_value("percentage_ingezette_uren_pnil","0E+1", where_conditions=[("vestiging", "Totaal organisatie")])

# def test_if_indicator_has_correct_value_for_query_8_1_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1.2 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0) Q2 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.2.rq')
#     
# test.set_quarter_to("'Q1'")
#    test.set_year_to('2023')

#     # Assertions
#     test.verify_value("percentage_ingezette_uren_pnil",None)

# def test_if_indicator_has_correct_value_for_query_8_1_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1.3 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0) Q3 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.3.rq')
#     # test.set_quarter_to("'Q3'")

#     # Assertions
#     test.verify_value("percentage_ingezette_uren_pnil",None)

# def test_if_indicator_has_correct_value_for_query_8_1_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1.4 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0) Q4 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.4.rq')
#     # test.set_quarter_to("'Q4'")

#     # Assertions
#     test.verify_value("percentage_ingezette_uren_pnil",None)

# def test_if_indicator_has_correct_value_for_query_8_1_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1.5 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0) Q1 2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')
#           test.set_quarter_to("'Q1'")
        # test.set_year_to('2024')

#     # Assertions
#     test.verify_value("percentage_ingezette_uren_pnil",None)


def test_if_dates_can_change_for_query_8_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')

    # Assertions
    test.verify_value("percentage_ingezette_uren_pnil",None)

# Tests using generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_8_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("percentage_ingezette_uren_pnil",None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_8_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("percentage_ingezette_uren_pnil","0", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_8_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("percentage_ingezette_uren_pnil",None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_8_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("percentage_ingezette_uren_pnil","0", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_8_1_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK (8u inzet, Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("percentage_ingezette_uren_pnil","0", where_conditions=[("vestiging", "Totaal organisatie")])
        # test.verify_value("percentage_ingezette_uren_pnil","0", where_conditions=[("vestiging", "000001287")])
        test.verify_value("uren_pil","80", where_conditions=[("vestiging", "000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_8_1_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK + INH OK (8u + 8u))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("percentage_ingezette_uren_pnil","50", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c

def test_if_value_returned_is_correct_for_query_8_1_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK (8u + 8u))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("percentage_ingezette_uren_pnil","100", where_conditions=[("vestiging", "Totaal organisatie")])
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d

def test_if_value_returned_is_correct_for_query_8_1_0_04_d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel UTZ OK + Wel INH OK (8u + 8u))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 8.1 Percentage ingezette uren personeel niet in loondienst (PNIL) (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 8.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("percentage_ingezette_uren_pnil","100", where_conditions=[("vestiging", "Totaal organisatie")])
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()