from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "", 
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Wel Wlz-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie", 
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Wel Wlz-indicatie (<4VV))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_c = [
    {
        "Description": "Testcase 02c (Wel Zorgproces + Wel Wlz-indicatie (4VG-G))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VG-G"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_d = [
    {
        "Description": "Testcase 02d (Wel Zorgproces + Zvw-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie", 
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_e = [
    {
        "Description": "Testcase 02e (Wel Zorgproces + Wmo-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie", 
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel Zorgproces + Wel Wlz-indicatie (4VV) + Geen leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True, 
                        "ciz": ["4VV"], # "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [] #pgb, #vpt, mpt
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel Zorgproces + Wel Wlz-indicatie (5VV, zonder behandeling) + Geen leveringsvorm))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": False,
                        "ciz": ["5VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel Zorgproces + Wel Wlz-indicatie (8VV) + PGB leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["8VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04ec (Wel Zorgproces + Wel Wlz-indicatie (9BVV) + PGB leveringsvorm + Geen behandeling)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": False,
                        "ciz": ["9BVV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]


td_04_d = [
    {
        "Description": "Testcase 04d (Wel Zorgproces + Wel Wlz-indicatie (6VV) + VPT leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["6VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]

td_04_e = [
    {
        "Description": "Testcase 04e (Wel Zorgproces + Wel Wlz-indicatie (7VV) + MPT leveringsvorm))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["7VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["mpt"]
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_14_2_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
    test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('leveringsvorm')
    test.verify_header_present('indicator')

def test_if_number_of_rows_returned_is_correct_for_query_14_2_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
    test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_row_count(6)

def test_if_indicator_has_correct_value_for_query_14_2_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
    test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")]) 

def test_if_indicator_has_correct_value_for_query_14_2_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.5 Aantal cliënten per zorgprofiel per leveringsvorm op 2024-01-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.5.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

def test_if_indicator_has_correct_value_for_query_14_2_6(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.6 Aantal cliënten per zorgprofiel per leveringsvorm op 2024-04-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.6.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

def test_if_indicator_has_correct_value_for_query_14_2_7(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.7 Aantal cliënten per zorgprofiel per leveringsvorm 2024-07-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.7.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

def test_if_indicator_has_correct_value_for_query_14_2_8(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.8 Aantal cliënten per zorgprofiel per leveringsvorm op 2024-10-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.8.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
    # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
    # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 


def test_if_dates_can_change_for_query_14_2_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
    test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")]) 


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_14_2_0_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_14_2_0_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()



# Testcase 02
def test_if_value_returned_is_correct_for_query_14_2_0_02(db_config):
    """ Testcase 02 (Wel Zorgproces + Geen Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_14_2_0_02_a(db_config):
    """ Testcase 02a (Geen Zorgproces + Wel Wlz-Indicatie (Niet op peildatum))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_14_2_0_02_b(db_config):
    """ Testcase 02b (Geen Zorgproces + Wel Wlz-Indicatie (<4VV))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02c
def test_if_value_returned_is_correct_for_query_14_2_0_02_c(db_config):
    """ Testcase 02c (Geen Zorgproces + Wel Wlz-Indicatie (4VG-G))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02d
def test_if_value_returned_is_correct_for_query_14_2_0_02_d(db_config):
    """ Testcase 02d (Wel Zorgproces + Zvw-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02e
def test_if_value_returned_is_correct_for_query_14_2_0_02_e(db_config):
    """ Testcase 02e (Wel Zorgproces + Wmo-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_14_2_0_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_14_2_0_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-Indicatie)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
    
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_14_2_0_04(db_config):
    """ Testcase 04 (Wel Zorgproces + Wel Wlz-Indicatie (4VV))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
        test.verify_value("indicator", "10",where_conditions=[("leveringsvorm","Verblijf met behandeling"),("vestiging","000001287")]) 

        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_14_2_0_04_a(db_config):
    """ Testcase 04a (Wel Zorgproces + Wel Wlz-Indicatie (5VV + Geen behandeling) + Geen leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Assertions
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
        test.verify_value("indicator", "10",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001287")]) 
        
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_14_2_0_04_b(db_config):
    """ Testcase 04b (Wel Zorgproces + Wel Wlz-Indicatie (8VV) + PGB leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query        
        test.verify_value("indicator", "11",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
        
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_14_2_0_04_c(db_config):
    """ Testcase 04c (Wel Zorgproces + Wel Wlz-Indicatie (9BVV) + PGB leveringsvorm + Geen behandeling)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("indicator", "11",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
        
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d
def test_if_value_returned_is_correct_for_query_14_2_0_04_d(db_config):
    """ Testcase 04d (Wel Zorgproces + Wel Wlz-Indicatie (6VV) + VPT leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
        test.verify_value("indicator", "10",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001287")])
        
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04e
def test_if_value_returned_is_correct_for_query_14_2_0_04_e(db_config):
    """ Testcase 04e (Wel Zorgproces + Wel Wlz-Indicatie (7VV) + MPT leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.2.0 Aantal cliënten per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 14.2.0.rq')
    
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001287")])
        test.verify_value("indicator", "11",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001287")])
        
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Modulair pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "2",where_conditions=[("leveringsvorm","Persoonsgebonden budget"),("vestiging","000001254")])
        # test.verify_value("indicator", "1",where_conditions=[("leveringsvorm","Volledig pakket thuis"),("vestiging","000001254")])
        # test.verify_value("indicator", "3",where_conditions=[("leveringsvorm","Verblijf zonder behandeling"),("vestiging","000001254")]) 


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()