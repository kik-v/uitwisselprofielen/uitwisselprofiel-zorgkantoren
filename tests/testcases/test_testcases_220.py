from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-12-31

#Opmerkingen:

#Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK - StageOvereenkomst)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK )",
        "Amount": 10, # Indicator score = 36 (Niet zorg gerelateerd)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Geen ZVL functie + Wel AOK)",
        "Amount": 10, # Indicator score = 36 (Niet zorg gerelateerd)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK )",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 


td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK (36u Zorg inzet op Vest. 1254)",
        "Amount": 10, # 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK (18u Zorg inzet op Vest. 1254 & 1287)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ] 
                    },
                    { 
                      "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]   
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK (18u Zorg & Niet zorg inzet op Vest. 1254)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel AOK (18u (9+9) op Vest 1254 & 1287)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 9,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            },
                            {
                                "unit": 9,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-08", 
                                "end_date": "2024-01-13"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "paid_hours": [
                            {
                                "unit": 9,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-01", 
                                "end_date": "2024-01-05"
                            },
                            {
                                "unit": 9,
                                "unit_of_measure": "Uur",
                                "start_date": "2024-01-08", 
                                "end_date": "2024-01-13"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 




#Static Tests

def test_if_headers_are_correct_for_query_2_2_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2. Aantal verloonde uren (ONZ pers 3.0)
    """
    # Start a test
    test = QueryTest(db_config)

    # Set the query to be tested
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

    # Change the reference dates of the query
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('indicator_zorg')
    test.verify_header_present('indicator_niet_zorg')


def test_if_number_of_rows_returned_is_correct_for_query_2_2_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2. Aantal verloonde uren (ONZ pers 3.0)
    """

    # Start a test
    test = QueryTest(db_config)

    # Set the query to be tested
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

    # Change the reference dates of the query
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_2_2_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2. Aantal verloonde uren (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    # test.verify_value("indicator_zorg", None)
    test.verify_value("indicator_niet_zorg", "124.0", where_conditions=[("vestiging", "Totaal organisatie")])

def test_if_dates_can_change_2_2_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2. Aantal verloonde uren (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')

    # Assertions
    # test.verify_value("indicator_zorg","33.0", where_conditions=[("vestiging", "000001254")])
    # test.verify_value("indicator_zorg","12.0", where_conditions=[("vestiging", "000001287")])
    # test.verify_value("indicator_zorg","116.0", where_conditions=[("vestiging", "Totaal organisatie")])

    test.verify_value("indicator_zorg", None)

# Tests using generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_2_2_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query

        # test.verify_value("indicator_zorg","116.0", where_conditions=[("vestiging", "Totaal organisatie")])
        test.verify_value("indicator_zorg", None)
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_2_2_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query

        test.verify_value("indicator_niet_zorg", "360", where_conditions=[("vestiging", "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a

def test_if_value_returned_is_correct_for_query_2_2_0_02_a(db_config):
    """ Testcase 02a (Geen ZVL functie + Wel AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("indicator_zorg", "0", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("indicator_niet_zorg", "360", where_conditions=[("vestiging", "000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_2_2_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query

        test.verify_value("indicator_zorg", "360", where_conditions=[("vestiging", "000001254")])
        
        # test.verify_value("indicator_zorg", None)
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_2_2_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK Wel AOK (36u Zorg inzet op Vest. 1254))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query

        test.verify_value("indicator_zorg", "360", where_conditions=[("vestiging", "000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_2_2_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK (18u Zorg inzet op Vest. 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("indicator_zorg", "180", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("indicator_zorg", "180", where_conditions=[("vestiging", "000001287")])
        test.verify_value("indicator_zorg", "360", where_conditions=[("vestiging", "Totaal organisatie")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_2_2_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK (18u Zorg & Niet zorg inzet op Vest. 1254)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query

        # test.verify_value("indicator_zorg", "180", where_conditions=[("vestiging", "000001254")])
        test.verify_value("indicator_niet_zorg", "180", where_conditions=[("vestiging", "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c

def test_if_value_returned_is_correct_for_query_2_2_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK (18u (9+9) op Vest 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.2 Aantal verloonde uren (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.2.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("indicator_zorg", "180", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("indicator_zorg", "180", where_conditions=[("vestiging", "000001287")])
        test.verify_value("indicator_zorg", "360", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
