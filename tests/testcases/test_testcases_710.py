from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:

#Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen WLZ indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                 "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK + Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                 "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK + Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK)",
        "Amount": 10, #Indicator score: 3.478436552364288559660492 (10 extra zorgpersoneel voor de aanwezige clienten in dummyzorg testdata)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK + Clienten met Wlz 4vv)",
        "Amount": 10, #Indicator score: 1.304413707136608209872684 (10 extra clienten bij de noemer zowel als Teller verandering door toegevoegde zorgpersoneel)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK + Clienten met Wlz 1vv)",
        "Amount": 10, #Indicator score: 3.478436552364288559660492 (Geen verandering van Noemer t.o.v. aantal clienten)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"],
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel AOK + Clienten zonder Wlz)",
        "Amount": 10, #Indicator score: 3.478436552364288559660492 (Geen verandering van Noemer t.o.v. aantal clienten)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""],
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel AOK + Clienten met Wlz 4vv + 3 maanden zwangerschapsverlof + 3 weken verzuim)",
        "Amount": 10, #Indicator score: 1.304413707136608209872684 (10 extra clienten bij de noemer zowel als Teller verandering door toegevoegde zorgpersoneel)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],                        
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-15"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "start_date": "2024-02-01",
                                "end_date": "2024-05-01"
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location":"Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Clienten + Geen WLZ indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
                    {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [],
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Clienten + WLZ indicatie 1 t/m 3)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
                    {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Clienten + WLZ indicatie 4 t/m 10)",
        "Amount": 10, #Indicator score: 0 Geen zorgpersoneel = Teller: 0. 
        "Human": [
                    {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_for_query_7_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023') 

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('ingezette_uren')
    test.verify_header_present('aantal_clienten')
    test.verify_header_present('ingezetten_uren_per_client')

def test_if_number_of_rows_returned_is_correct_for_query_7_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)

def test_if_indicator_has_correct_value_for_query_7_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_value("ingezetten_uren_per_client","83.111111111111111111111111",where_conditions=[("vestiging","Totaal organisatie")])

def test_if_dates_can_change_7_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')
    
    # Assertions
    test.verify_value("ingezetten_uren_per_client",None)

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_7_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client",None)
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_7_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Assertions
        test.verify_value("ingezetten_uren_per_client",None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_7_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client",None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_7_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client","40", where_conditions=[("vestiging","000001287")])
        # Aantal clienten = 2, Aantal ingezette uren = 80

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_7_1_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK + clienten met Wlz 4VV)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client","6.666666666666666666666667", where_conditions=[("vestiging","000001287")])
        # Aantal clienten = 12, Aantal ingezette uren = 80

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
 
# Testcase 04b
def test_if_value_returned_is_correct_for_query_7_1_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK + Clienten met Wlz 1VV)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client","40", where_conditions=[("vestiging","000001287")])
        # Aantal clienten = 2, Aantal ingezette uren = 80
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_7_1_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK + Clienten geen Wlz indicatie)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client","40", where_conditions=[("vestiging","000001287")])
        # Aantal clienten = 2, Aantal ingezette uren = 80
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d
def test_if_value_returned_is_correct_for_query_7_1_0_04_d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel AOK + 3 weken verzuim + 3 maanden zwangerschapsverlof + 10 clienten met Wlz 4VV)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client","6.666666666666666666666667", where_conditions=[("vestiging","000001287")])
        # Aantal clienten = 12, Aantal ingezette uren = 80

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_7_1_0_05(db_config):
    """ Testcase 05 (Clienten met geen wlz indicatie)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client",None)
        # Geen ingezette uren toegevoegd

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_7_1_0_06(db_config):
    """ Testcase 06 (Clienten met Wlz indicatie 1 t/m 3)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client",None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_7_1_0_07(db_config):
    """ Testcase 07 (Clienten met Wlz indicatie 4 t/m 10)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 7.1. Aantal ingezette uren per client per kwartaal (ONZ Pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 7.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

    
        # Assertions
        test.verify_value("ingezetten_uren_per_client",None)
        # test.verify_value("aantal_clienten","13", where_conditions=[("vestiging","Totaal organisatie")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()