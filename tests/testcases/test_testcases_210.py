from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-12-31

#Opmerkingen:

#Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK - StageOvereenkomst)",
        "Amount": 10, # ingezette_uren score = 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK )",
        "Amount": 10, # ingezette_uren score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK (StageOvereenkomst))",
        "Amount": 10, # ingezette_uren score = 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_03_a = [
    {
        "Description": "Testcase 03a (Wel ZVL functie + Geen AOK (VrijwilligersOvereenkomst))",
        "Amount": 10, # ingezette_uren score = 0
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VW_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK (Vest. 1254))",
        "Amount": 10, # 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-02T09:00:00",
                                "end_datetime": "2024-01-02T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK - BPT (8u inzet op Vest. 1254 & 1287))",
        "Amount": 10, # ingezette_uren score = 0
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-02T09:00:00",
                                "end_datetime": "2024-01-02T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK - OBPT (8u inzet op Vest. 1254 & 1287))",
        "Amount": 10, # ingezette_uren score = 0
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-02T09:00:00",
                                "end_datetime": "2024-01-02T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel AOK + Wel INH OK (8u op Vest 1254 & 1287)",
        "Amount": 10, # ingezette_uren score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-02T09:00:00",
                                "end_datetime": "2024-01-02T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-03T09:00:00",
                                "end_datetime": "2024-01-03T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-04T09:00:00",
                                "end_datetime": "2024-01-04T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel AOK (24u op Vest. 1287))",
        "Amount": 10, # ingezette_uren score =  0.212184629694221602139290 (10 * 36/36 = 10. 10/47 = 0.212...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-02T09:00:00",
                                "end_datetime": "2024-01-02T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-03T09:00:00",
                                "end_datetime": "2024-01-03T17:00:00",
                                "location": "Locatie_Grotestraat_17",
                            },
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_e = [
    {
        "Description": "Testcase 04e (Wel ZVL functie + Wel AOK (InhuurOvereenkomst))",
        "Amount": 10, # ingezette_uren score = 80
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_f = [
    {
        "Description": "Testcase 04f (Wel ZVL functie + Wel AOK (UitzendOvereenkomst))",
        "Amount": 10, # ingezette_uren score = 0
        "Human": [
            {
                "UitzendOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "UITZ_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_g = [
    {
        "Description": "Testcase 04g (Wel ZVL functie + Wel AOK (OproepOvereenkomst))",
        "Amount": 10, # ingezette_uren score = 80
        "Human": [
            {
                "OproepOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "OP_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "location": "Vestiging_De_Beuk",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]




#Static Tests

def test_if_headers_are_correct_for_query_2_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('ingezette_uren')


def test_if_number_of_rows_returned_is_correct_for_query_2_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)


def test_if_ingezette_uren_has_correct_value_for_query_2_1_0(db_config):
    """ Test of de ingezette_uren de juiste waarde heeft
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_value("ingezette_uren", "748", where_conditions=[("vestiging", "Totaal organisatie")])

# def test_if_ingezette_uren_has_correct_value_for_query_2_1_1(db_config):
#     """ Test of de ingezette_uren de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1.1. Aantal ingezette uren personeel (ONZ pers 3.0) Q1 2023 
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.1.rq')
#     # test.set_quarter_to("'Q1'")    

#     # Assertions
#     test.verify_value("ingezette_uren", "4.0", where_condition=("vestiging","000001254"))

# def test_if_ingezette_uren_has_correct_value_for_query_2_1_2(db_config):
#     """ Test of de ingezette_uren de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1.2. Aantal ingezette uren personeel (ONZ pers 3.0) Q2 2023 
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.2.rq')
#     # test.set_quarter_to('Q2')    

#     # Assertions
#     test.verify_value("ingezette_uren", None)

# def test_if_ingezette_uren_has_correct_value_for_query_2_1_3(db_config):
#     """ Test of de ingezette_uren de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1.3. Aantal ingezette uren personeel (ONZ pers 3.0) Q3 2023 
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.3.rq')
#     # test.set_quarter_to('Q3')    

#     # Assertions
#     test.verify_value("ingezette_uren", None)    

# def test_if_ingezette_uren_has_correct_value_for_query_2_1_4(db_config):
#     """ Test of de ingezette_uren de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1.4. Aantal ingezette uren personeel (ONZ pers 3.0) Q4 2023 
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.4.rq')
#     # test.set_quarter_to('Q4')    

#     # Assertions
#     test.verify_value("ingezette_uren", None)    

# def test_if_ingezette_uren_has_correct_value_for_query_2_1_5(db_config):
#     """ Test of de ingezette_uren de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1.4. Aantal ingezette uren personeel (ONZ pers 3.0) Q1 2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')
#     # test.set_quarter_to("'Q1'")    

#     # Assertions
#     test.verify_value("ingezette_uren", None)    

def test_if_dates_can_change_2_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """
    
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')

    # Assertions
    test.verify_value("ingezette_uren", None)
    # test.verify_value("ingezette_uren","12.0", where_conditions=[("vestiging", "000001287")])
    # test.verify_value("ingezette_uren","956", where_conditions=[("vestiging", "Totaal organisatie")])

# Tests using generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_2_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query

        # test.verify_value("ingezette_uren", "1148", where_conditions=[("vestiging", "Totaal organisatie")])
        test.verify_value("ingezette_uren", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_2_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query

        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", None)
        # test.verify_value("ingezette_uren", "1148", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_2_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", None)
        # test.verify_value("ingezette_uren", "1148", where_conditions=[("vestiging", "Totaal organisatie")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a

def test_if_value_returned_is_correct_for_query_2_1_0_03_a(db_config):
    """ Testcase 03a (Wel ZVL functie + Geen AOK)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", None)
        # test.verify_value("ingezette_uren", "1148", where_conditions=[("vestiging", "Totaal organisatie")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_2_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK Wel AOK (8u inzet op Vest. 1254 & 1287))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "160", where_conditions=[("vestiging", "Totaal organisatie")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_2_1_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK - BPT (8u inzet op Vest. 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("ingezette_uren", "0", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_2_1_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK - OBPT (8u inzet op Vest. 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("ingezette_uren", "0", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c

def test_if_value_returned_is_correct_for_query_2_1_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK + Wel INH OK (8u op Vest 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", "160", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("ingezette_uren", "0", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d

def test_if_value_returned_is_correct_for_query_2_1_0_04_d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel AOK (24u inzet op Vest. 1287))
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("ingezette_uren", "0", where_conditions=[("vestiging", "000001254")])
        test.verify_value("ingezette_uren", "240", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "240", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04e

def test_if_value_returned_is_correct_for_query_2_1_0_04_e(db_config):
    """ Testcase 04e (Wel ZVL functie + Wel AOK - Inhuurovereekomst (8u inzet op Vest. 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("ingezette_uren", "0", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04f

def test_if_value_returned_is_correct_for_query_2_1_0_04_f(db_config):
    """ Testcase 04f (Wel ZVL functie + Wel AOK - Uitzendovereekomst (8u inzet op Vest. 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("ingezette_uren", "0", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04g

def test_if_value_returned_is_correct_for_query_2_1_0_04_g(db_config):
    """ Testcase 04g (Wel ZVL functie + Wel AOK - OproepOvereenkomst (8u inzet op Vest. 1254 & 1287)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 2.1. Aantal ingezette uren personeel (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 2.1.rq')

        # Change the reference dates of the query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("ingezette_uren", "0", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("ingezette_uren", "80", where_conditions=[("vestiging", "Totaal organisatie")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()