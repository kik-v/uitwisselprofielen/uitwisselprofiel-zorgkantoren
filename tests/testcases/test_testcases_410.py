from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:
# IND 4.1.0 testcases & testdata hetzelfde als voor IND 2.1.2 (IND 2.1.1. moet nog aangepast worden)

# Testcases: 

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK)",
        "Amount": 10, # 2.1.2. Indicator score = 0 (Teller)
                      # 2.1.1. Indicator score = 0 (Noemer)
                      # 4.1. Gndicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK )",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK )",
        "Amount": 10, # 2.1.2. Indicator score = 0 (Teller)
                      # 2.1.1. Indicator score = 0 (Noemer)
                      # 4.1. Gndicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK (36u p/w))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK (18u p/w))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK + Niet AOK (18u p/w + 18u p/w))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel AOK (6m 36u p/w, 6m 18u p/w))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (6m 36u p/w)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-06-30"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (6m 18u p/w)",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-07-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel AOK (Vest. 1287))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK (6m 18u p/w))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (6m 18u p/w)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                    "start_date": "2024-01-01",
                    "end_date": "2024-06-30",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-06-30"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie + Geen ZVL functie + Wel AOK (12m, 18u p/w + 18u p/w))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (12m 18u p/w)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                    "start_date": "2024-01-01",
                    "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL (12m 18u p/w)",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                    "start_date": "2024-01-01",
                    "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m))",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (6m 36u p/w)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-06-30"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL (6m 36u p/w)",
                                "caregiving_role": False,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-07-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


#Static Tests
def test_if_headers_are_correct_for_query_4_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('zorggerelateerd')
    test.verify_header_present('niet_zorggerelateerd')


def test_if_number_of_rows_returned_is_correct_for_query_4_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_4_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')
    
    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_value("zorggerelateerd", "18.711111111111111111111111", where_conditions=[("vestiging", "000001254")])
    # test.verify_value("nietzorg_gerelateerd", "0.187549880287310454908220", where_conditions=[("vestiging", "000001254")])
    # test.verify_value("zorggerelateerd", "2.35", where_condition=("vestiging", "000001287"))
    # test.verify_value("nietzorg_gerelateerd", "0.870370370370370370370370", where_condition=("vestiging", "000001287"))
    # test.verify_value("zorggerelateerd", "3.610636900853578463558766", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
    # test.verify_value("nietzorg_gerelateerd", "0.308601444517399868680236", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

# def test_if_indicator_has_correct_value_for_query_4_1_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1.1. gemiddelde contractomvang personeel Q1 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.1.rq')
#     # test.set_quarter_to("'Q1'")

#     # Assertions
#     test.verify_value("zorggerelateerd", "3.048993028659953524399690", where_conditions=[("vestiging", "000001254")])

# def test_if_indicator_has_correct_value_for_query_4_1_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1.2. gemiddelde contractomvang personeel Q2 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.2.rq')
#     # test.set_quarter_to("'Q2'")

#     # Assertions
#     test.verify_value("zorggerelateerd", "2.881920112123335669236160", where_conditions=[("vestiging", "000001254")])

# def test_if_indicator_has_correct_value_for_query_4_1_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1.3. gemiddelde contractomvang personeel Q3 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.3.rq')
#     test.set_quarter_to("'Q1'")
#        test.set_year_to('2024')

#     # Assertions
#     test.verify_value("zorggerelateerd", "2.957068837897853441894893", where_conditions=[("vestiging", "000001254")])

# def test_if_indicator_has_correct_value_for_query_4_1_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1.4. gemiddelde contractomvang personeel Q4 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.4.rq')
#     # test.set_quarter_to("'Q4'")

#     # Assertions
#     test.verify_value("zorggerelateerd", "3.371820349761526232114467", where_conditions=[("vestiging", "000001254")])

# def test_if_indicator_has_correct_value_for_query_4_1_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1.5. gemiddelde contractomvang personeel Q1 2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')
#     # test.set_quarter_to("'Q4'")

#     # Assertions
#     test.verify_value("zorggerelateerd", "3.191130298273155416012559", where_conditions=[("vestiging", "000001254")])

def test_if_dates_can_change_for_query_4_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')

    # Assertions
    test.verify_value("zorggerelateerd", "30.425", where_conditions=[("vestiging", "000001254")])
    # test.verify_value("nietzorg_gerelateerd", "0.182453416149068322981366", where_conditions=[("vestiging", "000001254")])
    # test.verify_value("zorggerelateerd", "2.298913043478260869565217", where_condition=("vestiging", "000001287"))
    # test.verify_value("nietzorg_gerelateerd", "0.851449275362318840579710", where_condition=("vestiging", "000001287"))
    # test.verify_value("zorggerelateerd", "3.005115089514066496163683", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
    # test.verify_value("nietzorg_gerelateerd", "0.300511508951406649616368", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    # Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_4_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "30.425", where_conditions=[("vestiging", "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_4_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "30.425", where_conditions=[("vestiging", "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_4_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "30.425", where_conditions=[("vestiging", "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_4_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK (12m))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "32.569230769230769230769231", where_conditions=[("vestiging", "000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_4_1_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK (12m))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "25.646153846153846153846154", where_conditions=[("vestiging", "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_4_1_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK + Geen AOK)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "25.646153846153846153846154", where_conditions=[("vestiging", "000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_4_1_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK (6m 36u p/w + 6m 18u p/w))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "32.569230769230769230769231", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("nietzorg_gerelateerd", "0.106431159420289855072464", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("zorggerelateerd", "2.298913043478260869565217", where_condition=("vestiging", "000001287"))
        # test.verify_value("nietzorg_gerelateerd", "0.851449275362318840579710", where_condition=("vestiging", "000001287"))
        # test.verify_value("zorggerelateerd", "2.743558776167471819645733", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        # test.verify_value("nietzorg_gerelateerd", "0.189210950080515297906602", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d
def test_if_value_returned_is_correct_for_query_4_1_0_04_d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel AOK (12m))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        # test.verify_value("zorggerelateerd", "3.156444099378881987577640", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("nietzorg_gerelateerd", "0.182453416149068322981366", where_conditions=[("vestiging", "000001254")])
        test.verify_value("zorggerelateerd", "28.9875", where_conditions=[("vestiging", "000001287")])
        # test.verify_value("nietzorg_gerelateerd", "0.196488294314381270903010", where_condition=("vestiging", "000001287"))
        # test.verify_value("zorggerelateerd", "3.595008051529790660225443", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        # test.verify_value("nietzorg_gerelateerd", "0.189210950080515297906602", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_4_1_0_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK (6m))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "25.646153846153846153846154", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("nietzorg_gerelateerd", "0.182453416149068322981366", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("zorggerelateerd", "2.298913043478260869565217", where_condition=("vestiging", "000001287"))
        # test.verify_value("nietzorg_gerelateerd", "0.851449275362318840579710", where_condition=("vestiging", "000001287"))
        # test.verify_value("zorggerelateerd", "3.005115089514066496163683", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        # test.verify_value("nietzorg_gerelateerd", "0.300511508951406649616368", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 06
def test_if_value_returned_is_correct_for_query_4_1_0_06(db_config):
    """ Testcase 06 (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "25.646153846153846153846154", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("nietzorg_gerelateerd", "0.751278772378516624040921", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("zorggerelateerd", "2.298913043478260869565217", where_condition=("vestiging", "000001287"))
        # test.verify_value("nietzorg_gerelateerd", "0.851449275362318840579710", where_condition=("vestiging", "000001287"))
        # test.verify_value("zorggerelateerd", "2.002056404230317273795535", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        # test.verify_value("nietzorg_gerelateerd", "0.759400705052878965922444", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a
def test_if_value_returned_is_correct_for_query_4_1_0_06_a(db_config):
    """ Testcase 06a (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1. Gemiddelde contractomvang personeel
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 4.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("zorggerelateerd", "32.569230769230769230769231", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("nietzorg_gerelateerd", "2.022192028985507246376812", where_conditions=[("vestiging", "000001254")])
        # test.verify_value("zorggerelateerd", "2.298913043478260869565217", where_condition=("vestiging", "000001287"))
        # test.verify_value("nietzorg_gerelateerd", "0.851449275362318840579710", where_condition=("vestiging", "000001287"))
        # test.verify_value("zorggerelateerd", "1.892109500805152979066023", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        # test.verify_value("nietzorg_gerelateerd", "1.892109500805152979066023", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()