from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:
# Verzuimperiode <28 dagen

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL Geen verlof",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL Geen verzuim/verlof (buiten Meetperiode)",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]                

td_03 = [ 
    {
        "Description": "Testcase 03 (Geen ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL (Vest. 1254))",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL Geen verzuim/verlof",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Geen verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Geen verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 28 dagen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 28 dagen verzuim & geen Zw. verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-28"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 28 dagen verzuim + Geen Zwangerschaps-bevallingsverlof(Vest. 1254))",
        "Amount": 10, # Teller: 14 * 0.5 (ptf) * 10 = 70
                      # Noemer: 
                      # Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (18u p/w) 28 dagen verzuim",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-28"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wel ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + 28 dagen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 28 dagen Zw. verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-28"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_c = [
    {
        "Description": "Testcase 05c (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + 28 dagen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (18u p/w) 28 dagen Zw. verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-28"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_d = [
    {
        "Description": "Testcase 05d (Wel ZVL functie + Wel AOK + Geen parttimefactor + 28 dagen verzuim (2x 14 dagen) + Geen Zwangerschaps-bevallingsverlof (Vest. 1287))",
        "Amount": 10, # Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 28 dagen verzuim & geen Zw. verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-03-01",
                                "end_date": "2024-03-14"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 29 dagen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Teller: 14 * 10 (aantal humans) = 140
                      # Noemer: 6628.666...
                      # Indicator: 2.112...
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 29 dagen verzuim & Geen Zw. verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-29"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + 29 dagen Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Teller: 14 * 0.5 (ptf) * 10 (aantal humans) = 70
                      # Noemer: 4798.666...
                      # Indicator: 1.458...
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (18u p/w) 6 weken verzuim, 2 weken verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-29"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06_b = [
    {
        "Description": "Testcase 06b (Wel ZVL functie + Wel AOK + Geen parttimefactor + 6 weken verzuim (3x2 weken) + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Teller: 14 * 10 (aantal humans) = 140
                      # Noemer: 6628.666...
                      # Indicator: 2.112...
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 6 weken verzuim, 6 weken verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-10-01",
                                "end_date": "2024-10-14"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-14"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-07-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-03-14"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06_c = [
    {
        "Description": "Testcase 06c (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 6 weken verzuim + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL (18u p/w) 6 weken verzuim, 2 weken verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-03-01",
                                "end_date": "2024-04-14"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06_d = [
    {
        "Description": "Testcase 06d (Wel ZVL functie + Wel AOK + Geen parttimefactor + 29 dagen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1287))",
        "Amount": 10, # Teller: 14 * 10 (aantal humans) = 140
                      # Noemer: 6628.666...
                      # Indicator: 2.112...
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 29 dagen verzuim & Geen Zw. verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-29"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 6 weken verzuim + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL (18u p/w) 6 weken verzuim, 6 weken verlof",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-03-01",
                                "end_date": "2024-04-14"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 07a (Wel ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 6 weken verzuim + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL (18u p/w) 6 weken verzuim, 6 weken verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 18,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-03-01",
                                "end_date": "2024-04-14"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


# Static Tests
def test_if_headers_are_correct_for_query_11_2(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to("2023")

    # Assertions
    test.verify_header_present("vestiging")
    test.verify_header_present("indicator_zorg")
    test.verify_header_present("indicator_niet_zorg")

def test_if_number_of_rows_returned_is_correct_for_query_11_2(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to("2023")

    # Assertions
    test.verify_row_count(3)

def test_if_indicator_has_correct_value_for_query_11_2(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 2023
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to("2023")

    # Assertions
    test.verify_value("indicator_zorg", "5.423337839346173402949165", where_conditions=[("vestiging", "Totaal organisatie")])
    # .in_row(1)

# def test_if_indicator_has_correct_value_for_query_11_2_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2.1. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) Q1 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.1.rq')

#     # Assertions
#     test.verify_value("indicator_zorg", "0", where_conditions=[("vestiging", "Totaal organisatie")])
#     # .in_row(1)

# def test_if_indicator_has_correct_value_for_query_11_2_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) Q2 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.2.rq')

#     # Assertions
#     test.verify_value("indicator_zorg", "0.600465360654507243113400", where_conditions=[("vestiging", "Totaal organisatie")])
#     # .in_row(1)

# def test_if_indicator_has_correct_value_for_query_11_2_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2.3. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) Q3 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.3.rq')

#     # Assertions
#     test.verify_value("indicator_zorg", "1.391196815705066275070500", where_conditions=[("vestiging", "Totaal organisatie")])
#     # .in_row(1)

# def test_if_indicator_has_correct_value_for_query_11_2_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2.4. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) Q4 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.4.rq')

#     # Assertions
#     test.verify_value("indicator_zorg", "0", where_conditions=[("vestiging", "Totaal organisatie")])
#     # .in_row(1)

# def test_if_indicator_has_correct_value_for_query_11_2_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2.5. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) Q1 2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.5.rq')

#     # Assertions
#     test.verify_value("indicator_zorg", "0", where_conditions=[("vestiging", "Totaal organisatie")])
#     # .in_row(1)

def test_if_dates_can_change_11_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to("2024")

    # Assertions
    test.verify_value("indicator_zorg","0.067777719204440193693660", where_conditions=[("vestiging","000001254")])

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_11_2_01(db_config):
    """ Testcase 01 (GGeen ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.067777719204440193693660", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_11_2_02(db_config):
    """ Testcase 02 (Wel ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.067777719204440193693660", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_11_2_03(db_config):
    """ Testcase 03 (Geen ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.067777719204440193693660", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_11_2_04(db_config):
    """ Testcase 04 Testcase 04 (Wel ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.041921495013671065318347", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_11_2_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.051802437016870326988494", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_11_2_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 28 dagen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg", "11.779940098841569354455556", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_11_2_05_a(db_config):
    """ Testcase 05a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 28 dagen verzuim + Geen Zwangerschaps-bevallingsverlof(Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg", "7.304143619378716105377669", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_11_2_05_b(db_config):
    """ Testcase 05b (Wel ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + 28 dagen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg", "0.041921495013671065318347", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05c
def test_if_value_returned_is_correct_for_query_11_2_05_c(db_config):
    """ Testcase 05c (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + 28 dagen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg", "0.051802437016870326988494", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05d
def test_if_value_returned_is_correct_for_query_11_2_05_d(db_config):
    """ Testcase 05d (Wel ZVL functie + Wel AOK + Geen parttimefactor + 28 dagen verzuim (14 + 14) + Geen Zwangerschaps-bevallingsverlof (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg", "13.566450685445226884453518", where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_11_2_06(db_config):
    """ Testcase 06 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 29 dagen verzuim + Geen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.041921495013671065318347", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a
def test_if_value_returned_is_correct_for_query_11_2_06_a(db_config):
    """ Testcase 06a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + 29 dagen Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.051802437016870326988494", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06b
def test_if_value_returned_is_correct_for_query_11_2_06_b(db_config):
    """ Testcase 06b (Wel ZVL functie + Wel AOK + Geen parttimefactor + 6 weken verzuim (3x2 weken) + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","5.910930796927620209886952", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06c
def test_if_value_returned_is_correct_for_query_11_2_06_c(db_config):
    """ Testcase 06c (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 6 weken verzuim + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.067777719204440193693660", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06d
def test_if_value_returned_is_correct_for_query_11_2_06_d(db_config):
    """ Testcase 06d (Wel ZVL functie + Wel AOK + Geen parttimefactor + 29 dagen verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL (Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.440903787657819075794825", where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_11_2_07(db_config):
    """ Testcase 07 (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 6 weken verzuim + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.067777719204440193693660", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a
def test_if_value_returned_is_correct_for_query_11_2_07_a(db_config):
    """ Testcase 07a (Wel ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 6 weken verzuim + 6 weken Zwangerschaps-bevallingsverlof (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 11.2. Kortdurend ziekteverzuimpercentage (Incl. Zwangerschapsverlof) 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 11.2.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to("2024")

        # Verify actual result of the query
        test.verify_value("indicator_zorg","0.067777719204440193693660", where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()