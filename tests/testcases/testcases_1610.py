from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2024-07-01

# Opmerkingen:
# 

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Korsakov syndroom)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Korsakov syndroom)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen Zorgproces (Niet op PD) + Wel Korsakov syndroom)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ],
                "Declaration": [
                    {
                        "approval": True,
                        "date": "2024-07-01",
                        "careperformancecode": "Vektis_EX001",
                        "careproductcode": "", 
                        "informationobjectvalue": "Syndroom van Korsakov Declaratie"
                    }
                ]
            }
        ]
    }
]


td_03 = [
    {
        "Description": "Testcase 03 (Wel Zorgproces + Geen Korsakov syndroom)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ],
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces + Geen Korsakov syndroom (Declaratie niet op PD))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],   
               "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ],
                "Declaration": [
                    {
                        "approval": True,
                        "date": "2023-07-01",
                        "careperformancecode": "Vektis_EX001",
                        "careproductcode": "", 
                        "informationobjectvalue": "Syndroom van Korsakov Declaratie"
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel Zorgproces + Wel Korsakov syndroom)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ],
                "Declaration": [
                    {
                        "approval": True,
                        "date": "2024-07-01",
                        "careperformancecode": "Vektis_EX001",
                        "careproductcode": "", 
                        "informationobjectvalue": "Syndroom van Korsakov Declaratie"
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_16_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')
    test.set_reference_date_to("2022-02-05")

    # Assertions
    test.verify_header_present('aantal_clienten')

def test_if_number_of_rows_returned_is_correct_for_query_16_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom?  
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')
    test.set_reference_date_to("2022-02-05")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_16_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom op een peildatum? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

    test.set_reference_date_to("2022-07-01")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value("aantal_clienten", None)

def test_if_indicator_has_correct_value_for_query_16_1_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.1. Wat is het aantal cliënten met Korsakov syndroom op 2023-01-01? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.1.rq')

    # peildatum hardcoded in SPARQL
    # test.set_reference_date_to("2022-07-01")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value("aantal_clienten", None)

def test_if_indicator_has_correct_value_for_query_16_1_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.2. Wat is het aantal cliënten met Korsakov syndroom op 2023-04-01? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.2.rq')

    # peildatum hardcoded in SPARQL
    # test.set_reference_date_to("2022-07-01")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value("aantal_clienten", None)

def test_if_indicator_has_correct_value_for_query_16_1_3(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.3. Wat is het aantal cliënten met Korsakov syndroom op 2023-07-01? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.3.rq')

    # peildatum hardcoded in SPARQL
    # test.set_reference_date_to("2022-07-01")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value("aantal_clienten", None)

def test_if_indicator_has_correct_value_for_query_16_1_4(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.4. Wat is het aantal cliënten met Korsakov syndroom op 2023-10-01? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.4.rq')

    # peildatum hardcoded in SPARQL
    # test.set_reference_date_to("2022-07-01")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value("aantal_clienten", None)

def test_if_indicator_has_correct_value_for_query_16_1_5(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.5. Wat is het aantal cliënten met Korsakov syndroom op 2024-01-01? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.5.rq')

    # peildatum hardcoded in SPARQL
    # test.set_reference_date_to("2022-07-01")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value("aantal_clienten", None)

def test_if_dates_can_change_16_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

    test.set_reference_date_to("2023-07-01")
    # test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value("aantal_clienten", None)

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_16_1_0_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Korsakov syndroom)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        # test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value("aantal_clienten", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_16_1_0_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Korsakov syndroom)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        # test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value("aantal_clienten", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_16_1_0_02(db_config):
    """ Testcase 02 (Geen Zorgproces + Wel Korsakov syndroom)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        # test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value("aantal_clienten", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_16_1_0_03(db_config):
    """ Testcase 03 (Wel Zorgproces + Geen Korsakov syndroom)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        # test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value("aantal_clienten", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_16_1_0_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces + Geen Korsakov syndroom (Declaratie niet op PD)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        # test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value("aantal_clienten", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_16_1_0_04(db_config):
    """ Testcase 04 (Wel Zorgproces + Wel Korsakov syndroom)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 16.1.0. Wat is het aantal cliënten met Korsakov syndroom? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 16.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        # test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value("aantal_clienten", "10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
