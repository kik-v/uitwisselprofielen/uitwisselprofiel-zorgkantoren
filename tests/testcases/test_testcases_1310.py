from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2024-06-30

#Opmerkingen:
# Voor indicator berekening. Als geen AOK op peildatum dan is indicator score 0
# ZVL-1 & AOK-1 betekent t.o.v. peildatum 

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]

                    },
                    {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ]

                    }
                ]
            }
        ]
    }
]


td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Wel AOK - 1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "ArbeidsOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen ZVL functie + Geen AOK + Wel ZVL functie - 1 + Geen AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Geen ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Geen ZVL functie + Geen AOK + Wel ZVL functie - 1 + Wel AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "ArbeidsOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Geen ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK -1)",
        "Amount": 10, #Indicator score: 0 
       "Human": [
             {
                  "ArbeidsOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "ArbeidsOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Geen ZVL functie + Wel AOK + Wel ZVL functie - 1 + Geen AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "ArbeidsOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "StageOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_09 = [
    {
        "Description": "Testcase 09 (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Wel AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "ArbeidsOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_10 = [
    {
        "Description": "Testcase 10 (Wel ZVL functie + Geen AOK + Wel ZVL functie - 1 + Geen AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_11 = [
    {
        "Description": "Testcase 11 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK -1)",
        "Amount": 10, #Indicator score: 71 (10 AOK, 8 AOK -1 (default data)) 
        "Human": [
             {
                  "ArbeidsOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "StageOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_12 = [
    {
        "Description": "Testcase 12 (Geen ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "ArbeidsOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_13 = [
    {
        "Description": "Testcase 13 (Wel ZVL functie + Geen AOK + Wel ZVL functie - 1 + Wel AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "StageOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "ArbeidsOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_14 = [
    {
        "Description": "Testcase 14 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "ArbeidsOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL functie",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_15 = [
    {
        "Description": "Testcase 15 (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Geen AOK -1)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                  "ArbeidsOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ],
                "StageOvereenkomst":[
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_16 = [
    {
        "Description": "Testcase 16 (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK -1)",
        "Amount": 10, #Indicator score:  
        "Human": [
             {
                  "ArbeidsOvereenkomst": [
                      {
                        "function": [
                            {
                                "label": "AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL functie",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_for_query_13_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')
    
    test.set_reference_date_new_param_to("2023-01-01")

    # Assertions
    test.verify_header_present('Aantal_PIL_met_zorgfunctie')
    test.verify_header_present('Aantal_ingestroomde_PIL_met_zorgfunctie')
    test.verify_header_present('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd')

def test_if_number_of_rows_returned_is_correct_for_query_13_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst
        
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')
    
    test.set_reference_date_new_param_to("2023-01-01")

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_13_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst     
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')
    
    test.set_reference_date_new_param_to("2023-01-01")

    # Assertions
    test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "100")

# def test_if_indicator_has_correct_value_for_query_13_1_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.1 Percentage instroom personeel in loondienst 01-01-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.1.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', {"27", "27.0"})

# def test_if_indicator_has_correct_value_for_query_13_1_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.2 Percentage instroom personeel in loondienst 01-04-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.2.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', {"9", "9.0"})

# def test_if_indicator_has_correct_value_for_query_13_1_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.3 Percentage instroom personeel in loondienst 01-07-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.3.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', {"9", "9.0"})

# def test_if_indicator_has_correct_value_for_query_13_1_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.4 Percentage instroom personeel in loondienst 01-10-2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.4.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', {"9", "9.0"})

# def test_if_indicator_has_correct_value_for_query_13_1_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.5 Percentage instroom personeel in loondienst 01-01-2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.5.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

# def test_if_indicator_has_correct_value_for_query_13_1_6(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.6 Percentage instroom personeel in loondienst 2024-04-01
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.6.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

# def test_if_indicator_has_correct_value_for_query_13_1_7(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.7 Percentage instroom personeel in loondienst 2024-07-01
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.7.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

# def test_if_indicator_has_correct_value_for_query_13_1_8(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1.8 Percentage instroom personeel in loondienst 2024-10-01
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.8.rq')

#     # Assertions
#     test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

def test_if_dates_can_change_13_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

    test.set_reference_date_new_param_to("2024-01-01")

    # Assertions
    test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd',"89.473684210526315789473700")

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_13_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_13_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_13_1_0_03(db_config):
    """ Testcase 03 (Geen ZVL functie + Geen AOK + Wel ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_13_1_0_04(db_config):
    """ Testcase 04 (Geen ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_13_1_0_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_13_1_0_06(db_config):
    """ Testcase 06 (Geen ZVL functie + Geen AOK + Wel ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_13_1_0_07(db_config):
    """ Testcase 07 (Geen ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_13_1_0_08(db_config):
    """ Testcase 08 (Geen ZVL functie + Wel AOK + Wel ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09
def test_if_value_returned_is_correct_for_query_13_1_0_09(db_config):
    """ Testcase 09 (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 10
def test_if_value_returned_is_correct_for_query_13_1_0_10(db_config):
    """ Testcase 10 (Wel ZVL functie + Geen AOK + Wel ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_10)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 11
def test_if_value_returned_is_correct_for_query_13_1_0_11(db_config):
    """ Testcase 11 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_11)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "91.666666666666666666666700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 12
def test_if_value_returned_is_correct_for_query_13_1_0_12(db_config):
    """ Testcase 12 (Geen ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_12)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 13
def test_if_value_returned_is_correct_for_query_13_1_0_13(db_config):
    """ Testcase 13 (Wel ZVL functie + Geen AOK + Wel ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_13)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "89.473684210526315789473700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 14
def test_if_value_returned_is_correct_for_query_13_1_0_14(db_config):
    """ Testcase 14 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_14)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "91.666666666666666666666700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 15
def test_if_value_returned_is_correct_for_query_13_1_0_15(db_config):
    """ Testcase 15 (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Geen AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_15)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "91.666666666666666666666700")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 16
def test_if_value_returned_is_correct_for_query_13_1_0_16(db_config):
    """ Testcase 16 (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK -1)
        Zorgkantoren inkoopondersteuning & beleidsontwikkeling 13.1 Percentage instroom personeel in loondienst op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_16)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 13.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value('Percentage_zorggerelateerde_instroom_tov_totaal_zorggerelateerd', "70.833333333333333333333300")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()