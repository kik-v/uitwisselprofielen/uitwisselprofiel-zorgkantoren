from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: "2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:

td_01 = [
    {
        "Description": "Testcase 01 (Geen matching rubrieken overige jaarrekeningsposten (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken jaarrekeningsposten (Sociale kosten) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000011" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken jaarrekeningsposten (000011) + Valt niet in meetperiode)",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-07-01",
                "accounting_item_type": "811000" ,
                "financial_entity_value": 10000  
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel matching rubrieken jaarrekeningsposten (B: Vlottende activa zonder B.I Voorraden: 311100 t/m 132400) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "312000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "319000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121001",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121002",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121003",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124111",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124113",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "125000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128330",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132400",
                "financial_entity_value": 10000
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel matching rubrieken jaarrekeningsposten (G: Kortlopende schulden: 141010 t/m 158330) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141010",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141020",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141120",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152220",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152410",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152420",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152610",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152620",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152710",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152720",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "154100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "154200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "155000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158330",
                "financial_entity_value": 10000
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel matching rubrieken jaarrekeningsposten (B-G: Vlottende activa - Kortlopende schulden: 311100 t/m 158330) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121001",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121002",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121003",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124111",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124113",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "125000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128330",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141010",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141020",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141120",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152220",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152410",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152420",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152610",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152620",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152710",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152720",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "154100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "154200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "155000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158310",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158320",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158330",
                "financial_entity_value": 10000
            }
        ]
    }
]


# Static Tests
def test_if_headers_are_correct_for_query_19_4(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """
    # Start a test
    test = QueryTest(db_config)

    # Set the query to be tested
    test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

    test.set_start_period_to("2024-01-01")
    test.set_end_period_to("2024-12-31")

    # Assertions
    test.verify_header_present('Kental')
    test.verify_header_present('Waarde')

def test_if_number_of_rows_returned_is_correct_for_query_19_4(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio 
    """
    # Start a test
    test = QueryTest(db_config)

    # Set the query to be tested
    test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")
        
    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_19_4(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """
    # Start a test
    test = QueryTest(db_config)

    # Set the query to be tested
    test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

    test.set_start_period_to("2023-01-01")
    test.set_end_period_to("2023-12-31")

    # Assertions
    test.verify_value("Waarde", "0", where_conditions=[("Kental","Quick ratio")])


def test_if_dates_can_change_19_4(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """
    # Start a test
    test = QueryTest(db_config)

    # Set the query to be tested
    test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

    test.set_start_period_to("2024-01-01")
    test.set_end_period_to("2024-12-31")

    # Assertions
    test.verify_value("Waarde", "0.380910168843025612467942", where_conditions=[("Kental","Quick ratio")])

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_19_4_01(db_config):
    """ Testcase 01 (Geen matching rubrieken overige bedrijfsopbrengsten (ZZP kosten) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("Waarde", "0.380910168843025612467942", where_conditions=[("Kental","Quick ratio")])
        # test.verify_value("Waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_19_4_02(db_config):
    """ Testcase 02 (Geen matching rubrieken overige bedrijfsopbrengsten (Sociale kosten) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("Waarde", "0.380910168843025612467942", where_conditions=[("Kental","Quick ratio")])
        # test.verify_value("Waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_19_4_03(db_config):
    """ Testcase 03 (Wel matching rubrieken overige bedrijfsopbrengsten (812000) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("Waarde", "0.380910168843025612467942", where_conditions=[("Kental","Quick ratio")])
        # test.verify_value("Waarde", "0", where_condition=("Kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_19_4_04(db_config):
    """ Testcase 04 (Wel matching rubrieken jaarrekeningsposten (B: Vlottende activa zonder B.I Voorraden: 121000 t/m 132400) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("Waarde", "95.087565773772317623393302", where_conditions=[("Kental","Quick ratio")])
        # test.verify_value("Waarde", "10000", where_condition=("Kental","P.I Netto omzet"))
        # test.verify_value("Waarde", "0", where_condition=("Kental","P.II Wijziging IN voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum"))
        # test.verify_value("Waarde", "0", where_condition=("Kental","P.III Geactiveerde productie voor het eigen bedrijf"))
        # test.verify_value("Waarde", "150000", where_condition=("Kental","P.IV Overige bedrijfsopbrengsten"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_19_4_04_a(db_config):
    """ Testcase 04a (Wel matching rubrieken jaarrekeningsposten (G: Kortlopende schulden: 141010 t/m 158330) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("Waarde", "0.003216161938215594032147", where_conditions=[("Kental","Quick ratio")])
        # test.verify_value("Waarde", "0", where_condition=("Kental", "Q.I Kosten van grond- en hulpstoffen"))
        # test.verify_value("Waarde", "40000", where_condition=("Kental", "Q.II Kosten uitbesteed werk en andere externe kosten"))
        # test.verify_value("Waarde", "520000", where_condition=("Kental", "Q.III Lonen en salarissen"))
        # test.verify_value("Waarde", "80000", where_condition=("Kental", "Q.IV Sociale lasten"))
        # test.verify_value("Waarde", "10000", where_condition=("Kental", "Q.V Pensioenlasten"))
        # test.verify_value("Waarde", "460000", where_condition=("Kental", "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"))
        # test.verify_value("Waarde", "0", where_condition=("Kental", "Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa"))
        # test.verify_value("Waarde", "0", where_condition=("Kental", "Q.VIII Bijzondere waardevermindering van vlottende activa"))
        # test.verify_value("Waarde", "120000", where_condition=("Kental", "Q.IX Bijzondere waardevermindering van vlottende activa"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_19_4_04_b(db_config):
    """ Testcase 04b (Wel matching rubrieken jaarrekeningsposten (B-G: Vlottende activa zonder B.I Voorraden - Kortlopende schulden: 121000 t/m 158330 + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 19.4. Wat is de Liquiditeit? Quick ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 19.4.rq')

        # Change measuring period parameters of query
        test.set_start_period_to("2024-01-01")
        test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value("Waarde", "0.802858613011212618434644", where_conditions=[("Kental","Quick ratio")])
        # test.verify_value("Waarde", "10000", where_condition=("Kental","P.I Netto omzet"))
        # test.verify_value("Waarde", "150000", where_condition=("Kental","P.IV Overige bedrijfsopbrengsten"))

        # test.verify_value("Waarde", "10000", where_condition=("Kental","R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten"))
        # test.verify_value("Waarde", "10000", where_condition=("Kental","R.II Andere rentebaten en soortgelijke opbrengsten"))
        # test.verify_value("Waarde", "0", where_condition=("Kental","R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten"))
        # test.verify_value("Waarde", "0", where_condition = ("Kental", "R.IV Rentelasten en soortgelijke kosten"))
        # test.verify_value("Waarde", "180000", where_condition=("Kental","R Resultaat voor belastingen"))
        # test.verify_value("Waarde", "20000", where_condition=("Kental","S Resultaat na belastingen"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

