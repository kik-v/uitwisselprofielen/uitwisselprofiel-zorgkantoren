from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: "2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:

td_01 = [
    {
        "Description": "Testcase 01 (Geen matching rubrieken overige jaarrekeningsposten (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken jaarrekeningsposten (Sociale kosten) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000011" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken jaarrekeningsposten (000011) + Valt niet in meetperiode)",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-07-01",
                "accounting_item_type": "811000" ,
                "financial_entity_value": 10000  
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel matching rubrieken jaarrekeningsposten (P: Som der bedrijfsopbrengsten Activa: 811000 t/m 930000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "811000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "891000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "919000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "920000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "930000",
                "financial_entity_value": 10000
            },
            #R.I
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "904000",
                "financial_entity_value": 10000
            },
            #R.II
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "900000",
                "financial_entity_value": 10000
            },
            #S.II
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "902000",
                "financial_entity_value": 10000
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel matching rubrieken jaarrekeningsposten (Bedrijfslasten: 417000 t/m 901000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            #QII
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "417000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418200",
                "financial_entity_value": 10000
            },
            #Q.III
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413610",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413620",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414101",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414102",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414103",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414104",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414111",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414113",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414114",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414202",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414203",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414204",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414503",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414504",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414800",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "415000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "416000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419101",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419500",
                "financial_entity_value": 10000
            },
            #Q.IV
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "420000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "420100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422410",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422900",
                "financial_entity_value": 10000
            },
            #Q.V
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422600",
                "financial_entity_value": 10000
            },
            #Q.VI
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480012",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480022",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480032",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480532",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480632",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480932",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481212",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481232",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481242",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481252",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482142",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482152",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482212",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482232",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482242",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482252",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482312",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482322",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482332",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482342",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482352",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482412",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482422",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482432",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482442",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482452",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483142",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483152",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484232",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484322",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484332",
                "financial_entity_value": 10000
            },
            #Q.IX
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423120",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423140",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423190",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423220",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423410",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423420",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423510",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423520",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423910",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423920",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "432000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "433000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "442000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "442100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "442200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "444000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "444100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "445000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "445100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "445200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "449000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453910",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453920",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453930",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453990",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454220",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454290",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "455000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "455100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "461000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "461100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "461200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462800",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "466000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467120",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "468000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "472000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486115",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486125",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486135",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486215",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486225",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486235",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486315",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486325",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486335",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486415",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486425",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486435",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486515",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486525",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486535",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486615",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486625",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486635",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486725",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486735",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486825",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486835",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489150",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489211",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489212",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489221",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489230",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "905000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "911000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "912000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "913000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "914000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "915000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "921000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "931000",
                "financial_entity_value": 10000
            },
            #R.III
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "903000",
                "financial_entity_value": 10000
            },
            #R.IV
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485190",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485205",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485211",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485311",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485412",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "901000",
                "financial_entity_value": 10000
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel matching rubrieken jaarrekeningsposten (Resultaat eigen vermogen: 811000 t/m 901000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "811000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "891000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "919000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "920000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "930000",
                "financial_entity_value": 10000
            },
            #R.I
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "904000",
                "financial_entity_value": 10000
            },
            #R.II
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "900000",
                "financial_entity_value": 10000
            },
            #S.II
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "902000",
                "financial_entity_value": 10000
            },
            #Q.II
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "417000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418200",
                "financial_entity_value": 10000
            },
            #Q.III
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413610",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413620",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414101",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414102",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414103",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414104",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414111",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414113",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414114",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414202",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414203",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414204",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414503",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414504",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414800",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "415000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "416000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419101",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419500",
                "financial_entity_value": 10000
            },
            #Q.IV
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "420000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "420100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422410",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422900",
                "financial_entity_value": 10000
            },
            #Q.V
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422600",
                "financial_entity_value": 10000
            },
            #Q.VI
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480012",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480022",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480032",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480532",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480632",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480932",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481212",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481232",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481242",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481252",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482142",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482152",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482212",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482232",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482242",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482252",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482312",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482322",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482332",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482342",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482352",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482412",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482422",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482432",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482442",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482452",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483142",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483152",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484112",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484122",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484132",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484232",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484322",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484332",
                "financial_entity_value": 10000
            },
            #Q.IX
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423120",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423140",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423190",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423220",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423410",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423420",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423510",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423520",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423910",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423920",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "431300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "432000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "433000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "441900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "442000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "442100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "442200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "443300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "444000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "444100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "445000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "445100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "445200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "449000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "451600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "452900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453910",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453920",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453930",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "453990",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454210",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454220",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "454290",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "455000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "455100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "459900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "461000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "461100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "461200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462700",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462800",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "462900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "463900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "464900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "465900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "466000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467120",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "467200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "468000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "471400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "472000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473200",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473400",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473500",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473600",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "473900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486115",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486125",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486135",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486215",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486225",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486235",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486315",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486325",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486335",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486415",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486425",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486435",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486515",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486525",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486535",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486615",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486625",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486635",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486725",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486735",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486825",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486835",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489150",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489211",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489212",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489221",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489222",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489230",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "905000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "911000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "912000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "913000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "914000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "915000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "921000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "931000",
                "financial_entity_value": 10000
            },
            #R.III
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "903000",
                "financial_entity_value": 10000
            },
            #R.IV
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485000",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485190",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485205",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485211",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485300",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485311",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485412",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "901000",
                "financial_entity_value": 10000
            },
            #D
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "051100",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "054130",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053110",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053420",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "054230",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053230",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053900",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "054930",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "059150",
                "financial_entity_value": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "091000",
                "financial_entity_value": 10000
            }
        ]
    }
]



# Static Tests
def test_if_headers_are_correct_for_query_24_2(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")
    test.set_reference_date_new_param_to("2023-06-30")

    # Assertions
    test.verify_header_present('kental')
    test.verify_header_present('waarde')

def test_if_number_of_rows_returned_is_correct_for_query_24_2(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio) 
    """
     # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")
    test.set_reference_date_new_param_to("2023-06-30")

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_24_2(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

    # test.set_start_period_to("2023-01-01")
    # test.set_end_period_to("2023-12-31")
    test.set_reference_date_new_param_to("2023-06-30")

    # Assertions
    test.verify_value("waarde", "ongedefineerd" ,where_conditions=[("kental","ICR")])

def test_if_dates_can_change_24_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """
    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

    # test.set_start_period_to("2024-01-01")
    # test.set_end_period_to("2024-12-31")
    test.set_reference_date_new_param_to("2024-06-30")

        # Assertions
    test.verify_value("waarde", "ongedefineerd", where_conditions=[("kental","ICR")])
    # test.verify_value("waarde", "0", where_condition=("kental","Totaal passiva"))

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_24_2_01(db_config):
    """ Testcase 01 (Geen matching rubrieken overige bedrijfsopbrengsten (ZZP kosten) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-06-30")

        # Verify actual result of the query
        test.verify_value("waarde", "ongedefineerd", where_conditions=[("kental","ICR")])
        # test.verify_value("waarde", "0", where_condition=("kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_24_2_02(db_config):
    """ Testcase 02 (Geen matching rubrieken overige bedrijfsopbrengsten (Sociale kosten) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-06-30")

        # Verify actual result of the query
        test.verify_value("waarde", "ongedefineerd", where_conditions=[("kental","ICR")])
        # test.verify_value("waarde", "0", where_condition=("kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_24_2_03(db_config):
    """ Testcase 03 (Wel matching rubrieken overige bedrijfsopbrengsten (812000) + Valt niet in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-06-30")

        # Verify actual result of the query
        test.verify_value("waarde", "ongedefineerd", where_conditions=[("kental","ICR")])
        # test.verify_value("waarde", "0", where_condition=("kental","Totaal passiva"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_24_2_04(db_config):
    """ Testcase 04 (Wel matching rubrieken jaarrekeningsposten (Bedrijfsopbrengsten: 811000 t/m 930000) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-06-30")

        # Verify actual result of the query
        # test.verify_value("waarde", "160000", where_condition=("kental","P Som der bedrijfsopbrengsten"))
        # test.verify_value("waarde", "10000", where_condition=("kental","P.I Netto omzet"))
        # test.verify_value("waarde", "0", where_condition=("kental","P.II Wijziging IN voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum"))
        # test.verify_value("waarde", "0", where_condition=("kental","P.III Geactiveerde productie voor het eigen bedrijf"))
        # test.verify_value("waarde", "150000", where_condition=("kental","P.IV Overige bedrijfsopbrengsten"))
        test.verify_value("waarde", "ongedefineerd", where_conditions=[("kental","ICR")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_24_2_04_a(db_config):
    """ Testcase 04a (Wel matching rubrieken jaarrekeningsposten (Bedrijfslasten: 417000 t/m 931000) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-06-30")

        # Verify actual result of the query
        # test.verify_value("waarde", "1230000", where_condition=("kental", "Q Som der bedrijfslasten"))
        # test.verify_value("waarde", "0", where_condition=("kental", "Q.I Kosten van grond- en hulpstoffen"))
        # test.verify_value("waarde", "40000", where_condition=("kental", "Q.II Kosten uitbesteed werk en andere externe kosten"))
        # test.verify_value("waarde", "520000", where_condition=("kental", "Q.III Lonen en salarissen"))
        # test.verify_value("waarde", "80000", where_condition=("kental", "Q.IV Sociale lasten"))
        # test.verify_value("waarde", "10000", where_condition=("kental", "Q.V Pensioenlasten"))
        # test.verify_value("waarde", "460000", where_condition=("kental", "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"))
        # test.verify_value("waarde", "0", where_condition=("kental", "Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa"))
        # test.verify_value("waarde", "0", where_condition=("kental", "Q.VIII Bijzondere waardevermindering van vlottende activa"))
        # test.verify_value("waarde", "120000", where_condition=("kental", "Q.IX Bijzondere waardevermindering van vlottende activa"))
        test.verify_value("waarde", "-27.173533", where_conditions=[("kental","ICR")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_24_2_04_b(db_config):
    """ Testcase 04b (Wel matching rubrieken jaarrekeningsposten (ICR: 811000 t/m 091000) + Valt wel in meetperiode)
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 24.2. Wat is het ICR? (Interest Coverage Ratio)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 24.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")
        test.set_reference_date_new_param_to("2024-06-30")

        # Verify actual result of the query
        # test.verify_value("waarde", "160000", where_condition=("kental","P Som der bedrijfsopbrengsten"))
        # test.verify_value("waarde", "10000", where_condition=("kental","P.I Netto omzet"))
        # test.verify_value("waarde", "150000", where_condition=("kental","P.IV Overige bedrijfsopbrengsten"))

        # test.verify_value("waarde", "10000", where_condition=("kental","R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten"))
        # test.verify_value("waarde", "10000", where_condition=("kental","R.II Andere rentebaten en soortgelijke opbrengsten"))
        # test.verify_value("waarde", "0", where_condition=("kental","R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten"))
        # test.verify_value("waarde", "0", where_condition = ("kental", "R.IV Rentelasten en soortgelijke kosten"))
        # test.verify_value("waarde", "180000", where_condition=("kental","R Resultaat voor belastingen"))
        test.verify_value("waarde", "-25.573533", where_conditions=[("kental","ICR")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
