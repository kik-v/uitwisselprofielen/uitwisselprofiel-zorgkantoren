from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:
# Oude testdata bevat al 1 VWOK

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen VWOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 0
        "Human": [
             {
                 "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel VWOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VWOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen VWOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel VWOK (12m, Vest. 1254))",
        "Amount": 10, #Indicator score: 10
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VWOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel VWOK (6m tijdens meetperiode, Vest. 1254))",
        "Amount": 10, #Indicator score: 10 
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VWOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-06-30"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel VWOK (6m niet tijdens meetperiode, Vest. 1254))",
        "Amount": 10, #Indicator score: 10 
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VWOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-07-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel VWOK (6m, Vest. 1254)) + Wel AOK (6m, Vest. 1254))",
        "Amount": 10, #Indicator score: 10 
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VWOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-06-30"
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "ArbeidsOvereenkomst" :[
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                                "location": "vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-07-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel VWOK (12m, Vest. 1287))",
        "Amount": 10, #Indicator score: 10
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VWOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


def test_if_headers_are_correct_for_query_9_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('aantal_vrijwilligers')

def test_if_number_of_rows_returned_is_correct_for_query_9_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_row_count(3)

def test_if_indicator_has_correct_value_for_query_9_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2023')

    # Assertions
    test.verify_value("aantal_vrijwilligers", "13", where_conditions=[("vestiging","000001254")])
    #.in_row(1)

# def test_if_indicator_has_correct_value_for_query_9_1_1(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0) Q1 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.1.rq')

#     #         test.set_quarter_to("'Q1'")
        # test.set_year_to('2024')

#     # Assertions
#     test.verify_value("aantal_vrijwilligers", "1", where_conditions=[("vestiging","000001254")])
#     #.in_row(1)

# def test_if_indicator_has_correct_value_for_query_9_1_2(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1.2. Aantal vrijwilligers per kwartaal (ONZ pers 3.0) Q2 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.2.rq')

#     #         test.set_quarter_to("'Q1'")
        # test.set_year_to('2024')

#     # Assertions
#     test.verify_value("aantal_vrijwilligers", "1", where_conditions=[("vestiging","000001254")])
#     #.in_row(1)

# def test_if_indicator_has_correct_value_for_query_9_1_3(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1.3. Aantal vrijwilligers per kwartaal (ONZ pers 3.0) Q3 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.3.rq')

#     #         test.set_quarter_to("'Q1'")
        # test.set_year_to('2024')

#     # Assertions
#     test.verify_value("aantal_vrijwilligers", "1", where_conditions=[("vestiging","000001254")])
#     #.in_row(1)

# def test_if_indicator_has_correct_value_for_query_9_1_4(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1.4. Aantal vrijwilligers per kwartaal (ONZ pers 3.0) Q4 2023
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.4.rq')

#     #         test.set_quarter_to("'Q1'")
        # test.set_year_to('2024')

#     # Assertions
#     test.verify_value("aantal_vrijwilligers", "1", where_conditions=[("vestiging","000001254")])
#     #.in_row(1)

# def test_if_indicator_has_correct_value_for_query_9_1_5(db_config):
#     """ Test of de indicator de juiste waarde heeft
#         Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1.5. Aantal vrijwilligers per kwartaal (ONZ pers 3.0) Q1 2024
#     """

#     # Setup of the test
#     test = QueryTest(db_config)

#     # Configuration and execution
#     test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.5.rq')

#     #         test.set_quarter_to("'Q1'")
        # test.set_year_to('2024')

#     # Assertions
#     test.verify_value("aantal_vrijwilligers", "1", where_conditions=[("vestiging","000001254")])
    #.in_row(1)

def test_if_dates_can_change_9_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

    test.set_quarter_to("'Q1'")
    test.set_year_to('2024')

    # Assertions
    test.verify_value("aantal_vrijwilligers", "8", where_conditions=[("vestiging","000001254")])

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_9_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen VWOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","8",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 02
def test_if_value_returned_is_correct_for_query_9_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel VWOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","8",where_conditions=[("vestiging","000001254")])
        # test.verify_value("Niet_zorg","10",where_conditions=[("vestiging","000001254")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 03
def test_if_value_returned_is_correct_for_query_9_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen VWOK (Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","8",where_conditions=[("vestiging","000001254")])
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 04
def test_if_value_returned_is_correct_for_query_9_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel VWOK (12m, Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","8",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


#Testcase 04a
def test_if_value_returned_is_correct_for_query_9_1_0_04a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel VWOK (6m tijdens meetperiode, Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","8",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


#Testcase 04b
def test_if_value_returned_is_correct_for_query_9_1_0_04b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel VWOK (6m niet tijdens meetperiode, Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","8",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


#Testcase 04c
def test_if_value_returned_is_correct_for_query_9_1_0_04c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel VWOK (6m, Vest. 1254)) + Wel AOK (6m, Vest. 1254))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","8",where_conditions=[("vestiging","000001254")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 04d
def test_if_value_returned_is_correct_for_query_9_1_0_04d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel VWOK (12m, Vest. 1287))
        Zorgkantoren Inkoopondersteuning & Beleidsontwikkeling 9.1. Aantal vrijwilligers per kwartaal (ONZ pers 3.0)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 9.1.rq')

        # Change measuring period parameters of query
        test.set_quarter_to("'Q1'")
        test.set_year_to('2024')

        # Verify actual result of the query
        test.verify_value("aantal_vrijwilligers","10",where_conditions=[("vestiging","000001287")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()