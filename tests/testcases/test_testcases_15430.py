from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Wel Wlz-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Wel Wlz-indicatie (<4VV))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_c = [
    {
        "Description": "Testcase 02c (Wel Zorgproces + Wel Wlz-indicatie (4VG-G))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VG-G"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_d = [
    {
        "Description": "Testcase 02d (Wel Zorgproces + Zvw-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_e = [
    {
        "Description": "Testcase 02d (Wel Zorgproces + Wmo-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Geen Zorgproces + Geen Wlz-indicatie + Wel Leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]
td_05 = [
    {
        "Description": "Testcase 05 (Geen Zorgproces + Wel Wlz-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel Zorgproces + Geen Wlz-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wel Zorgproces + Zvw-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["mpt"]
                    }
                ]
            }
        ]
    }
]

td_06_b = [
    {
        "Description": "Testcase 06b (Wel Zorgproces + Zvw-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Wel Zorgproces + Wel Wlz-indicatie (4VV) + Geen leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True, 
                        "ciz": ["4VV"], # "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [] #pgb, #vpt, mpt
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 04a (Wel Zorgproces + Wel Wlz-indicatie (5VV, zonder behandeling) + Geen leveringsvorm))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": False,
                        "ciz": ["5VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wel Zorgproces + Wel Wlz-indicatie (6VV) + VPT leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["6VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Wel Zorgproces + Wel Wlz-indicatie (7VV) + MPT leveringsvorm))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["7VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["mpt"]
                    }
                ]
            }
        ]
    }
]

td_08_b = [
    {
        "Description": "Testcase 08b (Wel Zorgproces + Wel Wlz-indicatie (8VV) + PGB leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["8VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_08_c = [
    {
        "Description": "Testcase 08c (Wel Zorgproces + Wel Wlz-indicatie (8VV) + PGB & MPT leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["8VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["mpt", "pgb"]
                    }
                ]
            }
        ]
    }
]

td_08_d = [
    {
        "Description": "Testcase 08d (Wel Zorgproces + Wel Wlz-indicatie (10VV + Geen behandeling) + VPT & PGB leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["10VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt","pgb"]
                    }
                ]
            }
        ]
    }
]



#Static Tests
def test_if_headers_are_correct_for_query_15_4_3_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')
    test.set_reference_date_to("2024-01-01")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('aantal_verblijf')
    test.verify_header_present('aantal_vpt')
    test.verify_header_present('aantal_mpt')
    test.verify_header_present('aantal_pgb')

def test_if_number_of_rows_returned_is_correct_for_query_15_4_3_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')
    test.set_reference_date_to("2024-01-01")
    # Assertions
    test.verify_row_count(3)

def test_if_indicator_has_correct_value_for_query_15_4_3_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

    test.set_reference_date_to("2024-01-01")

    # Assertions
    test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

def test_if_indicator_has_correct_value_for_query_15_4_3_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.5 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm op 2024-01-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.5.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

def test_if_indicator_has_correct_value_for_query_15_4_3_6(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.6 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm op 2024-04-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.6.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

def test_if_indicator_has_correct_value_for_query_15_4_3_7(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.7 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm op 2024-07-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.7.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

def test_if_indicator_has_correct_value_for_query_15_4_3_8(db_config):
    """ Test of de indicator de juiste waarde heeft
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.8 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm op 2024-10-01
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.8.rq')
    # test.set_reference_date_to("2023-01-01")
    
    # Assertions
    test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

def test_if_dates_can_change_15_4_3_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

    test.set_reference_date_to("2024-07-01")

    # Assertions
    test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_15_4_3_0_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Wlz-Indicatie + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_15_4_3_0_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Wlz-Indicatie + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_15_4_3_0_02(db_config):
    """ Testcase 02 (Wel Zorgproces + Geen Wlz-Indicatie + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_15_4_3_0_02_a(db_config):
    """ Testcase 02a (Geen Zorgproces + Wel Wlz-Indicatie (Niet op peildatum) + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "10",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_15_4_3_0_02_b(db_config):
    """ Testcase 02b (Geen Zorgproces + Wel Wlz-Indicatie (<4VV) + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02c
def test_if_value_returned_is_correct_for_query_15_4_3_0_02_c(db_config):
    """ Testcase 02c (Geen Zorgproces + Wel Wlz-Indicatie (4VG-G) + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02d
def test_if_value_returned_is_correct_for_query_15_4_3_0_02_d(db_config):
    """ Testcase 02d (Wel Zorgproces + Zvw-Indicatie + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02e
def test_if_value_returned_is_correct_for_query_15_4_3_0_02_e(db_config):
    """ Testcase 02e (Wel Zorgproces + Wmo-Indicatie + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_15_4_3_0_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel Wlz-Indicatie + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_15_4_3_0_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-Indicatie + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_15_4_3_0_04(db_config):
    """ Testcase 04 (Geen Zorgproces + Geen Wlz-Indicatie + Wel Leveringsvorm (vpt))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_15_4_3_0_05(db_config):
    """ Testcase 05 (Geen Zorgproces + Wel Wlz-Indicatie + Wel Leveringsvorm (mpt))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_15_4_3_0_06(db_config):
    """ Testcase 06 (Wel Zorgproces + Geen Wlz-Indicatie + Wel Leveringsvorm (vpt))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a
def test_if_value_returned_is_correct_for_query_15_4_3_0_06_a(db_config):
    """ Testcase 06a (Wel Zorgproces + Wel Zvw-Indicatie + Wel Leveringsvorm (mpt))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06b
def test_if_value_returned_is_correct_for_query_15_4_3_0_06_b(db_config):
    """ Testcase 06b (Wel Zorgproces + Wel Wmo-Indicatie + Wel Leveringsvorm (pgb))
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_15_4_3_0_07(db_config):
    """ Testcase 07 (Wel Zorgproces + Wel Wlz-Indicatie (4VV) + Geen Leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_pgb", "0",where_condition=("vestiging","000001254"))

        test.verify_value("aantal_verblijf", "10",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_mpt", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_pgb", "1",where_conditions=[("vestiging","000001287")])

        # test.verify_value("aantal_verblijf", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_pgb", "1",where_condition=("vestiging","Totaal Dummy Zorg B.V"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a
def test_if_value_returned_is_correct_for_query_15_4_3_0_07_a(db_config):
    """ Testcase 07a (Wel Zorgproces + Wel Wlz-Indicatie (5VV + Geen behandeling) + Geen leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_pgb", "0",where_condition=("vestiging","000001254"))

        test.verify_value("aantal_verblijf", "10",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_mpt", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_pgb", "1",where_conditions=[("vestiging","000001287")])

        # test.verify_value("aantal_verblijf", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_pgb", "1",where_condition=("vestiging","Totaal Dummy Zorg B.V")) 
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_15_4_3_0_08(db_config):
    """ Testcase 08 (Wel Zorgproces + Wel Wlz-Indicatie (6VV) + VPT leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_pgb", "0",where_condition=("vestiging","000001254"))

        # test.verify_value("aantal_verblijf", "11",where_conditions=[("vestiging","000001287")])
        test.verify_value("aantal_vpt", "10",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_mpt", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_pgb", "1",where_conditions=[("vestiging","000001287")])

        # test.verify_value("aantal_verblijf", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_vpt", "10",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_pgb", "1",where_condition=("vestiging","Totaal Dummy Zorg B.V"))  

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08a
def test_if_value_returned_is_correct_for_query_15_4_3_0_08_a(db_config):
    """ Testcase 08a (Wel Zorgproces + Wel Wlz-Indicatie (7VV) + MPT leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_pgb", "0",where_condition=("vestiging","000001254"))

        # test.verify_value("aantal_verblijf", "11",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_conditions=[("vestiging","000001287")])
        test.verify_value("aantal_mpt", "11",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_pgb", "1",where_conditions=[("vestiging","000001287")])

        # test.verify_value("aantal_verblijf", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_mpt", "10",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_pgb", "1",where_condition=("vestiging","Totaal Dummy Zorg B.V")) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b
def test_if_value_returned_is_correct_for_query_15_4_3_0_08_b(db_config):
    """ Testcase 08b (Wel Zorgproces + Wel Wlz-Indicatie (8VV) + PGB leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query        
        # test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_pgb", "0",where_condition=("vestiging","000001254"))

        # test.verify_value("aantal_verblijf", "11",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_mpt", "0",where_conditions=[("vestiging","000001287")])
        test.verify_value("aantal_pgb", "11",where_conditions=[("vestiging","000001287")])

        # test.verify_value("aantal_verblijf", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_pgb", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V")) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08c
def test_if_value_returned_is_correct_for_query_15_4_3_0_08_c(db_config):
    """ Testcase 08c (Wel Zorgproces + Wel Wlz-Indicatie (9BVV) + MPT & PGB leveringsvormen)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_pgb", "0",where_condition=("vestiging","000001254"))

        # test.verify_value("aantal_verblijf", "11",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_conditions=[("vestiging","000001287")])
        test.verify_value("aantal_mpt", "11",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_pgb", "11",where_conditions=[("vestiging","000001287")])

        # test.verify_value("aantal_verblijf", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_mpt", "10",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_pgb", "11",where_condition=("vestiging","Totaal Dummy Zorg B.V")) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08d
def test_if_value_returned_is_correct_for_query_15_4_3_0_08_d(db_config):
    """ Testcase 08d (Wel Zorgproces + Wel Wlz-Indicatie (10VV) + VPT & PBG leveringsvorm)
        Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.3.0 Aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.4.3.0.rq')

        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_verblijf", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_vpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_pgb", "0",where_condition=("vestiging","000001254"))

        # test.verify_value("aantal_verblijf", "11",where_conditions=[("vestiging","000001287")])
        test.verify_value("aantal_vpt", "10",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_mpt", "0",where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_pgb", "11",where_conditions=[("vestiging","000001287")])

        # test.verify_value("aantal_verblijf", "12",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_vpt", "10",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_mpt", "0",where_condition=("vestiging","Totaal Dummy Zorg B.V"))
        # test.verify_value("aantal_pgb", "11",where_condition=("vestiging","Totaal Dummy Zorg B.V")) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()