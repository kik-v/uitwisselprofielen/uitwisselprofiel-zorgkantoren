---
title: Testcases Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 19.3 - Liquiditeit, Werkkapitaalratio
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 19.3 - Liquiditeit, Werkkapitaalratio

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Heeft matching rubrieken jaarrekeningposten: N (ZZP Kosten) <br> Valt tijdens meetperiode: N (2023-01-01) | Peildatum: 2024-01-01 | Indicator score: -6520 |
| 02 | td_02 | Heeft matching rubrieken jaarrekeningposten: N (Sociale Kosten) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: -6520 |
| 03 | td_02 | Heeft matching rubrieken jaarrekeningposten: J (812000) <br> Valt tijdens meetperiode: N (2023-01-01) | Peildatum: 2024-01-01 | Indicator score: -6520 |
| 04 | td_04 | Heeft matching rubrieken jaarrekeningposten: J (Bedrijfsopbrengsten: 000011 t/m 032910) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Totale indicator score: 162800 |
| 04a | td_04_a | Heeft matching rubrieken jaarrekeningposten: J (Bedrijfslasten: 417000 t/m 931000) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: 1242020 |
| 04b | td_04_b | Heeft matching rubrieken jaarrekeningposten: J (Resultaat voor belastingen: 811000 t/m 930000) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: 171380 |
| 04c | td_04_c | Heeft matching rubrieken jaarrekeningposten: J (Resultaat na belastingen: 485000 t/m 904000) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Totale indicator score: 33480 |