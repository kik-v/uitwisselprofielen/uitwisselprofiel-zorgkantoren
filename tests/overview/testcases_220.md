---
title: Testcases Zorgkantoren 2.2.0. Aantal verloonde uren
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 2.2.0. Aantal verloonde uren

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Niet ZVL StageOvereenkomst |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Niet ZVL wel AOK - 36u Niet zorg, Vest. 1254 |
| 02a | td_02_a | Is Zorgverlener: N <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Niet ZVL wel AOK - 36u Niet zorg, Vest. 1287 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + geen AOK - INH OK |
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK - 36u, Vest. 1254 |
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft AOK: J  <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK - 18u zorg, Vest. 1254 & 1287 |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK - 18u zorg + Niet zorg, Vest. 1254 |
| 04c | td_04_c | Is Zorgverlener: J <br> Heeft AOK: J + N <br> (Startdatum A + INH OK: 2024-01-01, einddatum A + INH OK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK - 18u (9+9) Zorg, Vest. 1254 & 1287 |