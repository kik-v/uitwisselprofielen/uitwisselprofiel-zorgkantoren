---
title: Testcases Zorgkantoren 10.1.0. Aantal Leerlingen
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 10.1.0. Aantal Leerlingen

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK BBL: N | Peildatum: 2024-01-01 | INH OK, Vest. 1254 |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK BBL: J <br> (Startdatum AOK BBL: 2024-01-01, einddatum AOK BBL: 2024-12-31) | Peildatum: 2024-01-01 | AOK BBL, Vest. 1254 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK BBL: N | Peildatum: 2024-01-01 | INH OK met Zvl, Vest. 1254 |
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK BBL: J <br> (Startdatum AOK BBL: 2024-01-01, einddatum AOK BBL: 2024-12-31)  | Peildatum: 2024-01-01 | AOK BBL, Vest 1254 |
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft AOK BBL: J <br> (Startdatum AOK BBL: 2024-01-01, einddatum AOK BBL: 2024-12-31)  | Peildatum: 2024-01-01 | AOK BBL, Vest 1287 |