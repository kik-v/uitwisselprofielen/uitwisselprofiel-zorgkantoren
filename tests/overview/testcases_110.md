---
title: Testcases Zorgkantoren 1.1.0. Gemiddeld aantal personeelsleden
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 1.1.0. Gemiddeld aantal personeelsleden

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft WOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten bij Vest. 1254 |
| 01a | td_01_a | Is Zorgverlener: N <br> Heeft WOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten bij Vest. 1287 |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft WOK: J <br> (Startdatum WOK: 2024-01-01, einddatum WOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1254 |
| 02a | td_02_a | Is Zorgverlener: N <br> Heeft WOK: J <br> (Startdatum WOK: 2024-01-01, einddatum WOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1287 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft WOK: J <br> (Startdatum WOK: 2024-01-01, einddatum WOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1254 |
| 03a | td_03_a | Is Zorgverlener: J <br> Heeft WOK: J <br> (Startdatum WOK: 2024-01-01, einddatum WOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Vest. 1287 |
| 03b | td_03_b | Is Zorgverlener: J <br> Heeft WOK: J <br> (Startdatum WOK: 2024-02-01, einddatum WOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Vest. 1254 |
| 03c | td_03_c | Is Zorgverlener: J <br> Heeft WOK: J <br> (Startdatum WOK: 2024-03-01, einddatum WOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Vest. 1287 | 
| 04 | td_04 | Is Zorgverlener: J (WOK 1) + N (WOK 2) <br> Heeft WOK: J <br> (Startdatum WOK 1: 2024-01-01, einddatum WOK 1: 2024-02-01) <br> (Startdatum WOK 2: 2024-02-01, einddatum WOK 2: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest 1254 |
