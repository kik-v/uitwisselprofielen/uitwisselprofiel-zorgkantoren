---
title: Testcases Zorgkantoren 3.1.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 3.1.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum


## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK BPT: N | Peildatum: 2024-07-01 | INH OK, Vest. 1254 |
| 01a | td_01_a | Is Zorgverlener: N <br> Heeft AOK BPT: N | Peildatum: 2024-07-01 | AOK, Vest. 1254 |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK BPT: J (BPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Peildatum: 2024-07-01 | AOK BPT, Vest. 1254 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK BPT: N | Peildatum: 2024-07-01 | INH OK + ZVL, Vest. 1254 | 
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK BPT: J (BPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Peildatum: 2024-07-01 | AOK BPT + ZVL, Vest. 1254 |
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft AOK BPT: N (OBPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Peildatum: 2024-07-01 | AOK OBPT + ZVL, Vest. 1254 |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK BPT: J (BPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-06-30)| Peildatum: 2024-07-01 | AOK BPT + ZVL (Niet op PD), Vest. 1254 |
| 04c | td_04_c | Is Zorgverlener: J <br> Heeft AOK BPT: J (BPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Peildatum: 2024-07-01 | AOK BPT + ZVL, Vest. 1287 |
| 04d | td_04_d | Is Zorgverlener: J <br> Heeft AOK BPT: N (OBPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Peildatum: 2024-07-01 | AOK BPT + OBPT + ZVL, Vest. 1287 |
| 05 | td_05 | Is Zorgverlener: J <br> Heeft AOK BPT: J + J (BPT + BPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Peildatum: 2024-07-01 | 2 AOK BPT + BPT + ZVL, Vest. 1254 |
| 05a | td_05_a | Is Zorgverlener: J <br> Heeft AOK BPT: J + N (BPT + OBPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Peildatum: 2024-07-01 | AOK BPT + AOK OBPT, Vest. 1254 |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK BPT: N (2x OBPT) <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Peildatum: 2024-07-01 | 2 AOK OBPT, Vest. 1254 |