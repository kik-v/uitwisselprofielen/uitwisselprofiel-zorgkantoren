---
title: Testcases Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.1.0 Aantal wooneenheden
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.1.0 Aantal wooneenheden

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01  | td_01   | Heeft wooneenheid: N | Vestiging nr: nvt | Organisatie |
| 01a | td_01_a | Heeft wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging |
| 01b | td_01_b | Heeft wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging + Locatie 1 |
| 01c | td_01_c | Heeft wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging + Locatie 1 + Locatie 2 |
| 02  | td_02   | Heeft wooneenheid: J | Vestiging nr: 9988 | 1x Locatie 1 |
| 02a | td_02_a | Heeft wooneenheid: J | Vestiging nr: 9988 | 1x Locatie 2 |
| 02b | td_02_b | Heeft wooneenheid: J | Vestiging nr: 9988 | 1x Locatie 1 + 1x Locatie 2 |
| 02c | td_02_c | Heeft wooneenheid: J | Vestiging nr: 9988 | 2x Locatie 1 + 4x Locatie 2 |