---
title: Testcases Zorgkantoren 4.1.0. Gemiddelde contractomvang personeel
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 4.1.0. Gemiddelde contractomvang personeel

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK: N | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Vest. 1254 |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Niet zorg omvang 36u p/w, Vest. 1254 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31)| Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Vest. 1254 | 
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Zorg omvang 36u p/w, Vest. 1254 |
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Zorg omvang 18u p/w, Vest. 1254 |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK: J + N <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Zorg omvang 18u p/w, Vest. 1254 |
| 04c | td_04_c | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Zorg omvang 36u + 18u p/w, Vest. 1254 |
| 04d | td_04_d | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Zorg omvang 36u p/w, Vest 1287 |
| 05 | td_05 | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-06-30) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Zorg omvang 18u p/w, Vest 1254 |
| 06 | td_06 | Is Zorgverlener: J + N <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | 6m Zorg omvang 36u p/w + 6m Niet zorg omvang 36u p/w, Vest. 1254 |
| 06a | td_06_a | Is Zorgverlener: J + N <br> Heeft AOK: J <br> (Startdatum AOK: 2022-01-01, einddatum AOK: 2022-12-31) | Startdatum meetperiode: 2022-01-01. Einddatum meetperiode: 2022-04-01 | Zorg omvang 18u p/w + Niet zorg omvang 18u p/w, Vest. 1254 |