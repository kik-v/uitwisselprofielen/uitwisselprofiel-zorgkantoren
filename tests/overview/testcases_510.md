---
title: Testcases Zorgkantoren 5.1.0. Leeftijdsopbouw personeel in loondienst
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 5.1.0. Leeftijdsopbouw personeel in loondienst

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten bij Vest. 1254 |
| 01a | td_01_a | Is Zorgverlener: N <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten bij Vest. 1287 |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1254 |
| 02a | td_02_a | Is Zorgverlener: N <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1287 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1254 |
| 03a | td_03_a | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Vest. 1287 |
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-02-01)  | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest 1254 |
| 04a | td_04_a | Is Zorgverlener: J (AOK 1) + N (AOK 2) <br> Heeft AOK: J <br> (Startdatum AOK 1: 2024-01-01, einddatum AOK 1: 2024-02-01) <br> (Startdatum AOK 2: 2024-02-01, einddatum AOK 2: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest 1254 |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-02-01)  | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest 1254 |