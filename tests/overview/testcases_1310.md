---
title: Testcases Zorgkantoren 13.1.0. Percentage instroom personeel in loondienst (PIL) op een peildatum
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 13.1.0. Percentage instroom personeel in loondienst (PIL) op een peildatum

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK: N <br> Is Zorgverlener -1 : N <br> Heeft AOK -1 : N | Peildatum: 2024-12-31 | 2x StageOvereenkomst zonder zorgverlenerfunctie (1x SOK -2 t.o.v PD) |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK: N <br> Is Zorgverlener -1: N <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 1x StageOvereenkomst zonder zorgverlenerfunctie |
| 03 | td_03 | Is Zorgverlener: N <br> Heeft AOK: N <br> Is Zorgverlener -1: J <br> Heeft AOK -1: N | Peildatum: 2024-12-31 | 1x StageOvereenkomst zonder zorgverlenerfunctie, 1x StageOvereenkomst met zorgverlenerfunctie (-2 t.o.v PD) |
| 04 | td_04 | Is Zorgverlener: N <br> Heeft AOK: J <br> Is Zorgverlener -1: N <br> Heeft AOK -1: N | Peildatum: 2024-12-31 | 1x StageOvereenkomst zonder zorgverlenerfunctie (-2 t.o.v PD) |
| 05 | td_05 | Is Zorgverlener: J <br> Heeft AOK: N <br> Is Zorgverlener -1: N <br> Heeft AOK -1: N | Peildatum: 2024-12-31 | 2x StageOvereenkomst (1x met & 1 x zonder zorgverlenerfunctie) (1x SOK -2 t.o.v PD) |
| 06 | td_06 | Is Zorgverlener: N <br> Heeft AOK: N <br> Is Zorgverlener -1: J <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 1x StageOvereenkomst zonder zorgverlenerfunctie  |
| 07 | td_07 | Is Zorgverlener: N <br> Heeft AOK: J <br> Is Zorgverlener -1: N <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 2x AOK zonder zorgverlenerfunctie |
| 08 | td_08 | Is Zorgverlener: N <br> Heeft AOK: J <br> Is Zorgverlener -1: J <br> Heeft AOK -1: N | Peildatum: 2024-12-31 | 1x AOK zonder zorgverlenerfunctie, 1x SOK -2 t.o.v PD |
| 09 | td_09 | Is Zorgverlener: J <br> Heeft AOK: N <br> Is Zorgverlener -1: N <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 1x SOK met ZVL, 1x AOK zonder ZVL |
| 10 | td_10 | Is Zorgverlener: J <br> Heeft AOK: N <br> Is Zorgverlener -1: J <br> Heeft AOK -1: N | Peildatum: 2024-12-31 | 1x SOK met ZVL, 1x SOK met ZVL -2 t.o.v PD |
| 11 | td_11 | Is Zorgverlener: J <br> Heeft AOK: J <br> Is Zorgverlener -1: N <br> Heeft AOK -1: N | Peildatum: 2024-12-31 | 1x AOK met ZVL, 1x SOK zonder ZVL -2 t.o.v. PD |
| 12 | td_12 | Is Zorgverlener: N <br> Heeft AOK: J <br> Is Zorgverlener -1: J <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 1x AOK zonder ZVL, 1x AOK met ZVL |
| 13 | td_13 | Is Zorgverlener: J <br> Heeft AOK: N <br> Is Zorgverlener -1: J <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 1x SOK met ZVL, 1x AOK met ZVL |
| 14 | td_14 | Is Zorgverlener: J <br> Heeft AOK: J <br> Is Zorgverlener -1: N <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 1x AOK met ZVL, 1x AOK zonder ZVL |
| 15 | td_15 | Is Zorgverlener: J <br> Heeft AOK: J <br> Is Zorgverlener -1: J <br> Heeft AOK -1: N | Peildatum: 2024-12-31 | 1x AOK met ZVL, 1 SOK met ZVL |
| 16 | td_16 | Is Zorgverlener: J <br> Heeft AOK: J <br> Is Zorgverlener -1: J <br> Heeft AOK -1: J | Peildatum: 2024-12-31 | 2x AOK met ZVL (10x) |