---
title: Testcases Zorgkantoren 7.1.0. Aantal ingezette uren personeel per cliënt
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 7.1.0. Aantal ingezette uren personeel per cliënt

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK: N <br> Wlz indicatie: nvt | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Inhuurovereenkomst zonder zorgverlenerfunctie |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Wlz indicatie: nvt | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Arbeidsovereenkomst zonder zorgverlenerfunctie |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK: N <br> Wlz indicatie: nvt | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Inhuurovereenkomst zonder zorgverlenerfunctie |
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Wlz indicatie: nvt | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Arbeidsovereenkomst met zorgverlenerfunctie |
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Wlz indicatie: <4 | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Arbeidsovereenkomst met zorgverlenerfunctie + Client met Wlz 4vv |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Wlz indicatie: 4 t/m 10 | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Arbeidsovereenkomst met zorgverlenerfunctie + Client met Wlz 4vv |
| 04c | td_04_c | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Wlz indicatie: nvt | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Arbeidsovereenkomst met zorgverlenerfunctie + Client met geen Wlz |
| 04d | td_04_d | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Wlz indicatie: <4 <br> Zwangerschapsverlof + Verzuim | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Arbeidsovereenkomst met zorgverlenerfunctie + 10 Clienten met Wlz 4vv. 3 weken verzuim + 3 maanden zwangerschapsverlof |
| 05 | td_05 | Is Zorgverlener: nvt <br> Heeft AOK: nvt (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-06-30) <br> Wlz indicatie: N | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Geen zorgpersoneel + Client zonder Wlz Indicatie |
| 06 | td_06 | Is Zorgverlener: nvt <br> Heeft AOK: nvt <br> Wlz indicatie: 1 | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Geen zorgpersoneel + Client met Wlz indicatie 1VV |
| 07 | td_07 | Is Zorgverlener: nvt <br> Heeft AOK: nvt  <br> Wlz indicatie: 4 | Startdatum meetperiode: 2024-01-01 <br> Einddatum meetperiode: 2024-04-01 | Geen zorgpersoneel + Client met Wlz indicatie 4 |


