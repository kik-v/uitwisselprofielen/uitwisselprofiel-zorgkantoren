---
title: Testcases Zorgkantoren 8.2.0. Percentage kosten personeel niet in loondienst
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 8.2.0. Percentage kosten personeel niet in loondienst

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 100% (Vest. 1287) |
| 01a | td_01_a | Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode  | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 33.333% (Vest. 1287) |
| 01b | td_01_b | Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 66.667% (Vest. 1287) |
| 01c | td_01_c | Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 9.09% (Vest. 1287) |
| 01d | td_01_d | Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 9.09% (Vest. 1287) |
| 01e | td_01_e| Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 40% (Vest. 1287) |
| 01f | td_01_f| Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 42.857% (Vest. 1287) |
| 01g | td_01_g| Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 47.368% (Vest. 1287) |
| 01h | td_01_h| Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 50% (Vest. 1287) |
| 01i | td_01_i | Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 50% (Vest.  1254) |
| 02 | td_02 | Geen matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode  | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Indicator score: 0% | 
| 03 | td_03 | Wel matching rubrieken PNIL + Geen matching rubrieken PIL + Valt wel in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Geen indicator score |
| 04 | td_04 | Geen matching rubrieken PNIL + Geen matching rubrieken PIL + Valt niet in meetperiode | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Geen indicator score |

|
