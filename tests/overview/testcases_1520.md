---
title: Testcases Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.2.0 Aantal bezette wooneenheden
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.2.0 Aantal bezette wooneenheden

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01  | td_01   | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie |
| 01a | td_01_a | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging |
| 01b | td_01_b | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging + Locatie 1 |
| 01c | td_01_c | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging + Locatie 1 + Locatie 2 |
| 02  | td_02   | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 1x Locatie 1 |
| 02a | td_02_a | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 1x Locatie 2 |
| 02b | td_02_b | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 1x Locatie 1 + 1x Locatie 2 |
| 02c | td_02_c | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 2x Locatie 1 + 4x Locatie 2 |