---
title: Testcases Zorgkantoren 8.1.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 8.1.0 Percentage ingezette uren personeel niet in loondienst (PNIL)

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK: N <br> Heeft PNIL OK: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | StageOvereenkomst zonder zorgverlenerfunctie, Vest. 1254 |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Heeft PNIL OK: N  | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 8u PIL inzet, Vest. 1254 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK: N <br> Heeft PNIL OK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 8u inzet PNIL, Vest. 1254 | 
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Heeft PNIL OK: N  | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 8u inzet PIL, Vest. 1254 | 
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Heeft PNIL OK: J (Startdatum INH OK: 2023-01-01, einddatum INH OK: 2023-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 8u inzet PIL, Vest. 1287 |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK: J  (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Heeft PNIL OK: J (Startdatum INH OK: 2024-01-01, einddatum INH OK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 8u inzet PIL + PNIL, Vest. 1254 |
| 04c | td_04_c | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) <br> Heeft PNIL OK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 8u inzet PNIL, Vest. 1254 |
| 04d | td_04_d | Is Zorgverlener: J <br> Heeft AOK: N <br> Heeft PNIL OK: J (Startdatum INH OK: 2024-01-01, einddatum INH OK: 2024-12-31, Startdatum UITZ OK: 2024-01-01, einddatum UITZ OK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 8u + 8u inzet PNIL, Vest. 1254 |