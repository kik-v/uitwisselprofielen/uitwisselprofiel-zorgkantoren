---
title: Testcases Zorgkantoren 15.4.1.0. Aantal cliënten per financieringsstroom
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.4.1.0. Aantal cliënten per financieringsstroom

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Heeft zorgproces: N <br> Heeft financieringsstroom: N | Peildatum: 2024-07-01 | Arbeidsovereenkomst, Vest. 1254 |
| 01a | td_01_a | Heeft zorgproces: N <br> Heeft financieringsstroom: N | Peildatum: 2024-07-01 | Arbeidsovereenkomst, Vest. 1287 |
| 02 | td_02 | Heeft zorgproces: J <br> Heeft financieringsstroom: N | Peildatum: 2024-07-01 | Clienten, Vest. 1254 |
| 02a | td_02_a | Heeft zorgproces: J <br> Heeft financieringsstroom: J (Niet op peildatum) | Peildatum: 2024-07-01 | Clienten, Vest. 1254 |
| 02b | td_02_b | Heeft zorgproces: J <br> Heeft financieringsstroom: N (Wel CIZ indicatie) | Peildatum: 2024-07-01 | Clienten, Vest. 1254 |
| 03 | td_03 | Heeft zorgproces: N <br> Heeft financieringsstroom: J (Wlz) | Peildatum: 2024-07-01 | Clienten + Wlz indicatie, Vest. 1254 |
| 03a | td_03_a | Heeft zorgproces: N <br> Heeft financieringsstroom: J (Zvw) | Peildatum: 2024-07-01 | Clienten + Zvw indicatie, Vest. 1254 |
| 04 | td_04 | Heeft zorgproces: J <br> Heeft financieringsstroom: J (Wlz) | Peildatum: 2024-07-01 | Clienten + Wlz indicatie, Vest. 9988 |
| 04a | td_04_a | Heeft zorgproces: J <br> Heeft financieringsstroom: J (Zvw) | Peildatum: 2024-07-01 | Clienten + Zvw indicatie, Vest. 9988 |
| 04a | td_04_a | Heeft zorgproces: J <br> Heeft financieringsstroom: J (Zvw) | Peildatum: 2024-07-01 | Clienten + Wmo indicatie, Vest. 9988 |