---
title: Testcases Zorgkantoren 14.3.0 Aantal cliënten per zorgprofiel per leveringsvorm
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.3.0 Aantal cliënten per zorgprofiel per leveringsvorm - 2024-07-01.

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Medewerkers bij Vest. 1254 |
| 01a | td_01_a | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Medewerkers bij Vest. 1287 |
| 02 | td_02 | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Geen Wlz indicatie |
| 02a | td_02_a | Heeft Zorgproces: J <br> Heeft Wlz-indicatie: J (Startdatum: 2023-01-01 <br> Einddatum: 2024-06-30) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Geen Wlz indicatie op PD |
| 02b | td_02_b | Heeft Zorgproces: J <br> Heeft Wlz-indicatie: J (Startdatum: 2023-01-01 <br> Einddatum: 2024-06-30) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz Indicatie <4VV |
| 02c | td_02_c| Heeft Zorgproces: J <br> Heeft Wlz-indicatie: J (Startdatum: 2023-01-01 <br> Einddatum: 2024-06-30) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz Indicatie "4VG-G" |
| 02d | td_02_d | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Zvw indicatie |
| 02e | td_02_e | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wmo indicatie |
| 03 | td_03 | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 4VV + Geen Zorgproces |
| 03a | td_03_a | Heeft Zorgproces: J (Startdatum: 2023-01-01 <br> Einddatum: 2023-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 +  Wlz indicatie 4VV + Geen Zorgproces op PD |
| 04 | td_04 | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + leveringsvorm vpt |
| 05 | td_05 | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: J | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Geen Zorgproces + leveringsvorm vpt |
Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 4VV + Wel Zorgproces + Verblijf met behandeling |
| 06 | td_06 | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Zorgproces + leveringsvorm vpt |
| 06a | td_06_a | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Zorgproces + Zvw-Indicatie + leveringsvorm vpt |
| 06b | td_06_b | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Zorgproces + Wmo-Indicatie + leveringsvorm vpt |
| 07 | td_07 |Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 4VV + Wel Zorgproces + Verblijf met behandeling |
| 07a | td_047_a | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 5VV + Wel Zorgproces + Verblijf zonder behandeling |
| 08 | td_08 | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) (VPT) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 6VV + Wel Zorgproces + Verblijf met behandeling |
| 08a | td_08_a | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) (MPT)| Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 7VV + Wel Zorgproces + Verblijf met behandeling |
| 08b | td_08_b | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) (PGB)| Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 8VV + Wel Zorgproces + Verblijf met behandeling |
| 08c | td_08_c | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) (MPT & PGB) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 9BVV + Wel Zorgproces + Verblijf met behandeling |
| 08d | td_08_d | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) (VPT & PGB) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 10VV + Wel Zorgproces + Verblijf met behandeling |