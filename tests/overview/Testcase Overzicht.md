---
title: Testcases Overzicht Zorgkantoren Inkoopondersteuning & beleidsontwikkeling
---

# Overzicht Personeelssamenstelling

Hieronder staat een overzicht van de status van de testautomatisering voor Zorgkantoren Inkoopondersteuning & beleidsontwikkeling.

## Status ##

| Indicator           | Specificatie | Data         | Testautomatisering | Opmerking |
|---------------------|--------------|--------------|----------------|-----------|
| Indicator 1.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.1.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.1.2.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.1.3.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.1.4.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.1.5.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.2.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 2.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 2.2.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 3.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 4.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 5.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 6.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 7.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 8.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 8.2.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 9.1.0.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 10.1.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 11.1.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 11.2.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 11.3.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 11.4.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 12.1.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 12.2.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 13.1.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 13.2.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 13.3.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 13.4.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 14.1.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 14.2.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 14.3.0.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 15.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 15.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 15.3.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 15.4.1.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 15.4.2.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 15.4.3.md | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |  
| Indicator 16.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  | 
| Indicator 18.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  | 
| Indicator 18.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  | 
| Indicator 19.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 19.3.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 19.4.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 19.5.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 19.6.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 19.7.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 20.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 20.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 20.3.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 20.4.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 21.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 21.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 22.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 23.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 24.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 24.3.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 24.4.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 
| Indicator 24.5.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   .md bestand niet af  | 