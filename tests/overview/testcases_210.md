---
title: Testcases Zorgkantoren 2.1.0. Aantal ingezette uren personeel
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 2.1.0. Aantal ingezette uren personeel

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01 (Q1 2024). Einddatum meetperiode: 2024-03-31 | Niet ZVL StageOvereenkomst |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft AOK: J <br> (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Niet ZVL wel AOK |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + geen AOK (ST OK) |
| 03a | td_03_a | Is Zorgverlener: J <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + geen AOK (INH OK) |
| 03b | td_03_b | Is Zorgverlener: J <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + geen AOK (UITZ OK) |
| 03c | td_03_c | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK (Oproep OK) - 8u, Vest. 1254 |
| 03d | td_03_d | Is Zorgverlener: J <br> Heeft AOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + geen AOK (VW OK) | 
| 04 | td_04 | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK - 8u, Vest. 1254 & 1287 |
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK (BPT) - 8u, Vest. 1254 & 1287 |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK (OBPT) - 8u, Vest. 1254 & 1287 |
| 04c | td_04_c | Is Zorgverlener: J <br> Heeft AOK: J + N (Startdatum A & INH OK: 2024-01-01, einddatum A & INH OK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK - 8u, Vest. 1254 & 1287 + Geen AOK |
| 04d | td_04_d | Is Zorgverlener: J <br> Heeft AOK: J (Startdatum AOK: 2024-01-01, einddatum AOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-03-31 | Wel ZVL + Wel AOK - 24u, Vest. 1287 |