---
title: Testcases Zorgkantoren 9.1.0 Aantal vrijwilligers per kwartaal
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 9.1.0. Aantal vrijwilligers per kwartaal

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener: N <br> Heeft VWOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1254 |
| 02 | td_02 | Is Zorgverlener: N <br> Heeft VWOK: J <br> (Startdatum VWOK: 2024-01-01, einddatum VWOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Vest. 1254 |
| 03 | td_03 | Is Zorgverlener: J <br> Heeft VWOK: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | InhuurOvereenkomst met Zorgverlener functie, Vest. 1254 | 
| 04 | td_04 | Is Zorgverlener: J <br> Heeft VWOK: J <br> (Startdatum VWOK: 2024-01-01, einddatum VWOK: 2024-12-31) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 10 humans met VWOK, Vest. 1254 | 
| 04a | td_04_a | Is Zorgverlener: J <br> Heeft VWOK: J <br> (Startdatum VWOK: 2024-01-01, einddatum VWOK: 2024-06-30) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 10 humans met VWOK, Vest. 1254  |
| 04b | td_04_b | Is Zorgverlener: J <br> Heeft VWOK: J <br> (Startdatum VWOK: 2024-07-01, einddatum VWOK: 2024-04-01) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 10 humans met VWOK, Vest. 1254 |
| 04c | td_04_c | Is Zorgverlener: J <br> Heeft VWOK: J (Startdatum VWOK: 2024-07-01, einddatum VWOK: 2024-04-01) <br> Heeft AOK: J (Startdatum AOK: 2024-07-01, einddatum AOK: 2024-04-01)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 10 humans met VWOK & AOK, Vest. 1254 |
| 04d | td_04_d | Is Zorgverlener: J <br> Heeft VWOK: J (Startdatum VWOK: 2024-07-01, einddatum VWOK: 2024-04-01) <br> Heeft AOK: J (Startdatum AOK: 2024-07-01, einddatum AOK: 2024-04-01)| Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 10 human met VWOK, Vest. 1287 |

