---
title: Testcases Zorgkantoren 14.1.0. Aantal cliënten per zorgprofiel op peildatum
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 14.1.0. Aantal cliënten per zorgprofiel op peildatum - 2024-07-01.

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Medewerkers bij Vest. 1254 |
| 01a | td_01_a | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Medewerkers bij Vest. 1287 |
| 02 | td_02 | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Geen Wlz indicatie |
| 02a | td_02_a | Heeft Zorgproces: J <br> Heeft Wlz-indicatie: J (Startdatum: 2023-01-01 <br> Einddatum: 2024-06-30) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Geen Wlz indicatie op PD |
| 02b | td_02_b | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Zvw indicatie |
| 02c | td_02_c | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: N | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wmo indicatie |
| 03 | td_03 | Heeft Zorgproces: N <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 4VV + Geen Zorgproces |
| 03a | td_03_a | Heeft Zorgproces: J (Startdatum: 2023-01-01 <br> Einddatum: 2023-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 +  Wlz indicatie 4VV + Geen Zorgproces op PD |
| 04 | td_04 | Heeft Zorgproces: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) <br> Heeft Wlz-indicatie: J (Startdatum: 2024-01-01 <br> Einddatum: 2024-12-31) | Peildatum: 2024-07-01 | Clienten bij Vest. 1287 + Wlz indicatie 4VV + Wel Zorgproces |