---
title: Testcases Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3.0 Aantal personen dat per vestiging kan wonen
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 15.3.0 Aantal personen dat per vestiging kan wonen

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01  | td_01   | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie <br> (Alleen dummyzorg capaciteit) |
| 01a | td_01_a | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging <br> (Alleen dummyzorg capaciteit) |
| 01b | td_01_b | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging + Locatie 1 <br> (Alleen dummyzorg capaciteit) |
| 01c | td_01_c | Heeft bezette wooneenheid: N | Vestiging nr: nvt | Organisatie + Vestiging + Locatie 1 + Locatie 2 <br> (Alleen dummyzorg capaciteit) |
| 02  | td_02   | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 3x Locatie 1 |
| 02a | td_02_a | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 3x Locatie 2 |
| 02b | td_02_b | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 3x Locatie 1 + 3x Locatie 2 |
| 02c | td_02_c | Heeft bezette wooneenheid: J | Vestiging nr: 9988 | 6x Locatie 1 + 12x Locatie 2 |