---
title: Testcases Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 21.1 - Rentabiliteit, Winstratio
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 21.1 - Rentabiliteit, Winstratio

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Heeft matching rubrieken jaarrekeningposten: N (Sociale kosten) <br> Valt tijdens meetperiode: N (2023-01-01) | Peildatum: 2024-01-01 | Indicator score: 159760 |
| 02 | td_02 | Heeft matching rubrieken jaarrekeningposten: N (Sociale Kosten) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: 159760 |
| 03 | td_02 | Heeft matching rubrieken jaarrekeningposten: J (812000) <br> Valt tijdens meetperiode: N (2023-01-01) | Peildatum: 2024-01-01 | Indicator score: 159760 |
| 04 | td_04 | Heeft matching rubrieken jaarrekeningposten: J (Vaste Activa: 000011 t/m 032910) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Totale indicator score: 1361000 |
| 04a | td_04_a | Heeft matching rubrieken jaarrekeningposten: J (Vlottende Activa: 132400 t/m 311100) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: 348760 |
| 04b | td_04_b | Heeft matching rubrieken jaarrekeningposten: J (Totaal Activa: 000011 t/m 311100) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: 1709760 |
| 04c | td_04_c | Heeft matching rubrieken jaarrekeningposten: J (Eigen vermogen: 051100 t/m 091000) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Totale indicator score: 133400 |
| 04d | td_04_d | Heeft matching rubrieken jaarrekeningposten: J (Totaal Passiva: 061000 t/m 158330) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: 496640 |
| 04e | td_04_e | Heeft matching rubrieken jaarrekeningposten: J (Totaal Activa: 000011 t/m 311100) <br> Valt tijdens meetperiode: J (2024-01-01) | Peildatum: 2024-01-01 | Indicator score: 596640 |