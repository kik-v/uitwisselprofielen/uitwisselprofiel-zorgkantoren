---
title: Testcases Zorgkantoren 13.3.0. Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel Zorgkantoren Inkoopondersteuning & beleidsontwikkeling 13.3.0. Percentage doorstroom personeel in loondienst naar oplopend kwalificatieniveau

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: N <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | INH OK |
| 01a | td_01_a | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: N <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | ST OK |
| 01b | td_01_b | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: N <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | UTZ OK |
| 02 | td_02 | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: N <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | INH OK + ZVL |
| 03 | td_03 | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | AOK |
| 04 | td_04 | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | INH OK Kwal. Niv. 3 & 3 |
| 04a | td_04_a | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | INH OK Kwal. Niv. 3 & 4 |
| 04b | td_04_b | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | INH OK Kwal. Niv. 4 & 3 |
| 05 | td_05 | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | AOK ZVL |
| 05a | td_05_a | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. 3 & NA |
| 05b | td_05_b | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. NA & 3 |
| 05c | td_05_c | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. 3 (Niet op PD) & 4 |
| 05d | td_05_d | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: N | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. 3 & 4 (Niet op PD)|
| 06 | td_06 | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: N <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | INH OK ZVL Kwal. Niv. 3 & 3 |
| 07 | td_07 | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK Kwal. Niv. 3 & 3 |
| 07a | td_07a | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK Kwal. Niv. 3 & AOK ZVL Kwal. Niv. 4 |
| 07b | td_07b | Is Zorgverlener op PD & PD - 3M: N <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK AVL Kwal. Niv. 4 & AOK Kwal. Niv. 3 |
| 08 | td_08 | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. 3 & 3 |
| 08a | td_08_a | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. 3 & 4 |
| 08b | td_08_b | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. 4 & 3 |
| 08c | td_08_c | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. 3 & Behandelaar |
| 08d | td_08_d | Is Zorgverlener op PD & PD - 3M: J <br> Heeft AOK op PD & PD - 3M: J <br> Heeft Kwalificatieniveau op PD & PD - 3M: J | Peildatum: 2024-01-01 | AOK ZVL Kwal. Niv. Leerling & 3 |