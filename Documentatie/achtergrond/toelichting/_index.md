---
title: Toelichting
weight: 1
---
# Toelichting op het uitwisselprofiel Zorgkantoren Inkoopondersteuning en beleidsontwikkeling


# Basisinformatie personeel en cliënten

## Scope

De zorgkantoren starten in het Uitwisselprofiel met een afbakening voor Personele gegevens ten behoeve van eigen EWS-systeem en/of monitoring van gemaakte afspraken tussen zorgkantoor en zorgaanbieder. De basis voor de ontwikkeling van de set Personele gegevens in het Uitwisselprofiel Zorgkantoren V&V is het Uitwisselprofiel ODB/ indicatoren personeel, omdat deze openbare bron tot op heden jaarlijks wordt gebruikt door zorgkantoren om analyses op te doen. Deze wordt aangepast aan de specifieke informatiebehoeften van de zorgkantoren én aangevuld met relevante nieuwe indicatoren.

## Proces

De werkgroep Uitwisselprofiel Zorgkantoren heeft in 5 bijeenkomsten (9 februari, 23 februari, 9 maart, 23 maart en 6 april 2022) de basisset ODB Personeel geanalyseerd, gewijzigd en/of aangevuld zodat de set passender is bij de rol van het zorgkantoor in het Wlz zorgstelsel. Tijdens de bijeenkomsten zijn overwegingen en mogelijke variaties in opzet en berekening van de indicator besproken. In de periode tussen de bijeenkomsten (2 weken) hebben de delegaties vanuit de 3 zorgkantoren met collega’s zorginkoop de richting die voorligt besproken om te kijken of deze past bij hoe de zorginkopers deze gegevens gebruiken bij analyses en bij gesprekken met zorgaanbieders.

Daarnaast is voor het uitwisselprofiel zorgkantoren (indicatoren personeel en cliënten) een themabijeenkomst georganiseerd op 29 augustus 2022. Tijdens deze themabijeenkomst zijn met een deel van de klankbordgroep, namelijk een vertegenwoordiging van de zorgaanbieders en de zorgkantoren, de verschillende afspraken op alle lagen van het uitwisselprofiel besproken en getoetst. Ook de functionele beschrijvingen zijn inhoudelijk voorgelegd tijdens deze bijeenkomst ter toetsing. De uitkomsten vanuit deze themabijeenkomst zijn verwerkt in het uitwisselprofiel, zodat de vragen beter aansluiten bij de praktijk van zorgaanbieders en de informatiebehoefte van zorgkantoren.

## Periode

Gegevens op dagbasis beschikbaar voor zorgkantoren is niet nodig om te monitoren, gegevens op jaarbasis is (naar ervaring van de zorgkantoren op dit moment) te weinig frequent. Om deze reden is gekozen voor cijfers op kwartaalbasis. De wens van de zorgkantoren is om hiermee zoveel mogelijk aan te sluiten bij de interne planning- en controlcyclus van zorgaanbieders.

## Gemaakte keuzes t.a.v. indicatoren en concepten
| Informatievraag                                                  | Status              | Toelichting                                                                                               |
|-----------------------------------------------------------------|---------------------|------------------------------------------------------------------------------------------------------------|
| 1.1 Wat is het gemiddeld aantal personeelsleden?                | Gewijzigde vraag    | Het gemiddelde aantal personeelsleden geeft het zorgkantoor inzicht in de ontwikkeling in een jaar. <br> Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. |
| 1.2 Wat is het aantal personeelsleden op een peildatum?          | Gewijzigde vraag    |  Het aantal personeelsleden op een peildatum zorgt voor goede vergelijkbaarheid over de jaren heen. Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. |
| 2.1 Wat is het aantal ingezette uren?                            | Gewijzigde vraag    | Het gaat hier om ingezette fte. Deze indicator heeft enkel betrekking op zorgpersoneel, omdat voor niet-zorgpersoneel niet altijd tijd wordt geschreven en dit dus mogelijk veel werk oplevert voor de zorgaanbieder. |
| 2.2 Wat is het aantal verloonde uren?                            | Gewijzigde vraag    | Verloonde fte heeft een betere correlatie met de kosten, omdat de kosten meebewegen met de verloonde uren. |
| 3.1 Wat is het percentage personeel met een arbeidsovereenkomst voor bepaalde tijd op een peildatum? | Gewijzigde vraag    | Zorgkantoren willen graag zicht hebben op de hoeveelheid personeel dat voor bepaalde tijd in dienst is, in het licht van de huidige arbeidsmarkt is het belangrijk. |
| 4.1 Wat is de gemiddelde contractomvang van het personeel?       | Nieuwe vraag        | Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. Deze indicator wordt per kwartaal uitgevraagd. |
| 5.1 Wat is de leeftijdsopbouw van het personeel?                  | Nieuwe vraag        |                                                                                                            |
| 6.1 Wat is het percentage ingezette uren personeel per kwalificatieniveau? | Nieuwe vraag | Er wordt uitgegaan van de gewerkte uren vanuit het urenregistratiesysteem. |
| 7.1 Aantal ingezette uren personeel per cliënt                   | Nieuwe vraag        |                                                                                                            |
| 8.1 Wat is het percentage ingezette uren personeel niet in loondienst (PNIL)? | Nieuwe vraag |                                                                                                            |
| 8.2 Wat is het percentage kosten personeel niet in loondienst (PNIL)? | Nieuwe vraag     |                                                                                                            |
| 9.1 Wat is het aantal vrijwilligers?                              | Nieuwe vraag        | Alle vrijwilligers worden geïncludeerd.                                                                 |
| 10.1 Wat is het aantal leerlingen?                                | Nieuwe vraag        |                                                                                                            |
| 11.1 Wat is het kortdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)? | Nieuwe vraag | Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden, in procenten van het totaal aantal beschikbare kalenderdagen (dus geen werkdagen) van personeel in loondienst per kwartaal.  Het kortdurend ziekteverzuimpercentage betreft het verzuim van 28 dagen of korter en exclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor.  Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. <br> Voor de berekening van deze indicator is het noodzakelijk dat in de administratie van de zorgaanbieder wijzigingen in parttime factor, vestiging en functie gedurende de verslagperiode beschikbaar zijn. <br> De laatste dag van het kwartaal fungeert als peilmoment waarmee bepaald wordt of iemand kort- of langdurend ziek is. De resultaten worden niet met terugwerkende kracht bijgesteld. |
| 11.2 Wat is het kortdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)? | Nieuwe vraag | Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden, in procenten van het totaal aantal beschikbare kalenderdagen (dus geen werkdagen) van personeel in loondienst per kwartaal.  Het kortdurend ziekteverzuimpercentage betreft het verzuim van 28 dagen of korter en inclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor.  Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. <br> Voor de berekening van deze indicator is het noodzakelijk dat in de administratie van de zorgaanbieder wijzigingen in parttime factor, vestiging en functie gedurende de verslagperiode beschikbaar zijn. <br> De laatste dag van het kwartaal fungeert als peilmoment waarmee bepaald wordt of iemand kort- of langdurend ziek is. De resultaten worden niet met terugwerkende kracht bijgesteld. |
| 11.3 Wat is het langdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)? | Nieuwe vraag | Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden, in procenten van het totaal aantal beschikbare kalenderdagen (dus geen werkdagen) van personeel in loondienst per kwartaal. Het ziekteverzuimpercentage betreft het verzuim van langer dan 28 dagen en exclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor. Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. <br> Voor de berekening van deze indicator is het noodzakelijk dat in de administratie van de zorgaanbieder wijzigingen in parttime factor, vestiging en functie gedurende de verslagperiode beschikbaar zijn. <br> De laatste dag van het kwartaal fungeert als peilmoment waarmee bepaald wordt of iemand kort- of langdurend ziek is. De resultaten worden niet met terugwerkende kracht bijgesteld. |
| 11.4 Wat is het langdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)? | Nieuwe vraag | Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden, in procenten van het totaal aantal beschikbare kalenderdagen (dus geen werkdagen) van personeel in loondienst per kwartaal. Het ziekteverzuimpercentage betreft het verzuim van langer dan 28 dagen en inclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor. Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd. <br> Voor de berekening van deze indicator is het noodzakelijk dat in de administratie van de zorgaanbieder wijzigingen in parttime factor, vestiging en functie gedurende de verslagperiode beschikbaar zijn. <br> De laatste dag van het kwartaal fungeert als peilmoment waarmee bepaald wordt of iemand kort- of langdurend ziek is. De resultaten worden niet met terugwerkende kracht bijgesteld. |
| 12.1 Wat is de verzuimfrequentie (excl. zwangerschapsverlof)?       | Nieuwe vraag        |                                                                                                            |
| 12.2 Wat is de verzuimfrequentie (incl. zwangerschapsverlof)?       | Nieuwe vraag        |                                                                                                            |
| 13.1 Wat is het percentage instroom van personeel?                | Nieuwe vraag        |                                                                                                            |
| 13.2 Wat is het percentage uitstroom van personeel?               | Nieuwe vraag        |                                                                                                            |
| 13.3 Wat is het percentage doorstroom van personeel naar oplopend kwalificatieniveau? | Nieuwe vraag | Gekozen is om het kwalificatieniveau te laten bepalen door de zorgaanbieder, door de functies eenmalig in te delen in kwalificatieniveaus. De term “oplopend” is gekozen als neutrale term om geen oordeel uit te spreken over personeel dat naar een hoger kwalificatieniveau doorstroomt (bijvoorbeeld van niveau 2 naar niveau 3). |
| 13.4 Wat is het percentage doorstroom van personeel naar aflopend kwalificatieniveau? | Nieuwe vraag |  Gekozen is om het kwalificatieniveau te laten bepalen door de zorgaanbieder, door de functies eenmalig in te delen in kwalificatieniveaus.  De term “aflopend” is gekozen als neutrale term om geen oordeel uit te spreken over personeel dat naar een lager kwalificatieniveau doorstroomt (bijvoorbeeld van niveau 3 naar niveau 2). |
| 14.1 Wat is het aantal cliënten per zorgprofiel op een peildatum? | Nieuwe vraag |  Het zorgprofiel wordt afgeleid van de indicatie van de cliënt (en dus niet uit de geleverde of gedeclareerde zorg), omdat de meest betrouwbare informatie oplevert. |
| 14.2 Wat is het aantal cliënten per leveringsvorm op een peildatum? | Nieuwe vraag |                                                                                                            |
| 14.3 Wat is het aantal cliënten per zorgprofiel per leveringsvorm op een peildatum? | Nieuwe vraag | Het zorgprofiel wordt afgeleid van de indicatie van de cliënt (en dus niet uit de geleverde of gedeclareerde zorg), omdat de meest betrouwbare informatie oplevert. |

* Voor zorgkantoren is uitgegaan van de informatievragen vanuit de ODB (voor personeel). Op het moment dat een vraag vanuit de ODB is aangepast voor zorgkantoren, is er sprake van een gewijzigde informatievraag.

| Concept                            | Informatievraag                                   | Bestaande/ gewijzigde/ nieuw concept in de ontologie | Toelichting                                                                                                                  |
| ---------------------------------- | ------------------------------------------------- | ---------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| **Zorgkantoor indeling**                | Alle informatievragen                             | Nieuw concept                                       | Nederland is opgesplitst in 31 zorgkantoorregio’s. In elke regio is één zorgkantoor aangewezen om Wlz zorg in te kopen. Voor zorgkantoren is het daarom gewenst om resultaten te kunnen selecteren op:  • zorgkantoorregio;  • concessiehouder (alle zorgkantoorregio’s van één zorgkantoor samen); • alle zorgkantoorregio’s van Nederland |
| **Financieringsstromen langdurige zorg** | Alle informatievragen                             | Nieuw concept: Zvw zorg Nieuw concept: Wmo zorg  | Veel zorgaanbieders V&V bieden zorg vanuit verschillende financieringsstromen. Het is voor de zorgkantoren nu niet goed inzichtelijk welke zorg onder de Wlz valt en welke niet. Een uitsplitsing in de 3 grootste financieringsstromen voor ouderenzorg is een eerste stap naar meer inzicht. |
| **Wlz langdurige zorg sectoren**        | Alle informatievragen                             | Nieuw concept: GZ Nieuw concept: GGZ             | Zorgkantoren contracteren Wlz zorg bij zorgaanbieders. Deze valt uiteen in 3 zorgsoorten (met onderliggende indicaties):  • V&V (VV-codes);  • GZ (VG-, LVG-, LG-, ZGaud-, ZGvis-codes); • GGZ (GGZ B en GGz wonen).  Het is voor zorgkantoren nu niet goed inzichtelijk welke zorg onder welke zorgsoort in de Wlz valt. Een uitsplitsing in de 3 zorgsoorten is een eerste stap naar meer inzicht. |
| **Onderscheid zorg-/ niet zorg gerelateerd personeel** | Alle informatievragen                             | Nieuw concept: niet zorg gerelateerd personeel   | De zorgaanbieder bepaalt op basis van functie welke personen zorg gerelateerd zijn (zoals nu in ODB Personeel) en welke niet. |
| **Kwalificatieniveaus**                | 13.3 en 13.4                                      | Nieuw concept                                       | Om te voorkomen dat zorgaanbieders 2 verschillende lijstjes omtrent kwalificatieniveau per medewerker moeten bijhouden, zijn de kwalificatieniveaus ODB, IGJ en zorgkantoren aan elkaar gerelateerd. |
|**Leveringsvorm  Wlz** Leveringsvorm die door zorgkantoren worden gebruikt zijn: -Verblijf met behandeling; -Verblijf zonder behandeling; -VPT; -MPT;-PGB                   | 14.2 en 14.3                                      | Nieuw concept                                       | De categorieën voor leveringsvorm zijn overgenomen zoals door Zorginstituut Nederland wordt gehanteerd: Leveringsvormen (instelling, vpt, mpt en pgb) (Wlz). |
| **Type werkovereenkomst**  De volgende indeling wordt gehanteerd:  - Personeel in loondienst (exclusief leerlingen); - Personeel niet in loondienst; - Leerlingen; - Stagiaires; - Vrijwilligers       |                                                  | Bestaand concept                                   |                                                                                                                              |

## Praktijktoets: Toelichting op wijzigingen en aanvullingen

De belangrijkste wijzigingen/aanvullingen in het uitwisselprofiel en de bijbehorende indicatoren n.a.v. de praktijktoets (= inclusief SPARQL-query ontwikkeling) zijn:

**1. Zorg- en niet-zorg personeel op vestigings- en concernniveau verder verfijnd**

   In het functioneel opstellen van het uitwisselprofiel is met de zorgkantoren gekozen om alle indicatoren voor niet-zorgpersoneel op concernniveau te berekenen, aangezien zorgaanbieders de uren van niet-zorgpersoneel afwisselend op vestiging- of op concernniveau boeken. Bij het opstellen van de SPARQL-queries bleek dat het makkelijk is om ook voor niet-zorgpersoneel het vestigingsniveau te introduceren: alle mogelijke registraties bij zorgaanbieders van niet-zorgpersoneelslid worden zo ondervangen. Belangrijk is om te realiseren dat personeelsleden die uren maken op meerdere vestigingen (en ook op vestigingsniveau worden geregistreerd) dubbel geteld worden. Het totaal aantal niet-zorg personeelsleden op concernniveau hoeft dus niet een-op-een te kloppen met de vestigings-totalen.

**2. Indicator 3.2 Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd gemiddeld per dag over een kwartaal is vervallen**

   Bij de opbouw van de SPARQL-query bleek dat het gemiddelde per dag over 3 maanden uitrekenen meer dan 2 miljoen regels in de query opleverde. Dit was niet erg bevorderlijk voor de snelheid van het uitvoeren van deze SPARQL. Het nog haalbare invulling was om een gemiddelde per maand uit te rekenen voor 3 maanden. Daarmee zou de indicator dan weer heel erg op indicator 3.1 gaan lijken, om deze reden is besloten indicator 3.2 te laten vervallen.

**3. VV4 t/m VV10 is aangepast naar VV1 t/m VV10**

   Voor alle indicatoren waar met Wlz-VV-indicaties wordt gewerkt: VV1-VV10 worden geïncludeerd. Ondanks dat we weten dat VV1-VV3 nauwelijks worden gebruikt, maken we met deze inclusie de cijfers toch completer.

**4. Verdeelsleutels**

   Voor de beide van toepassing zijnde verdeelsleutels Financieringsstroom en Wlz Langdurige zorgsector is een SPARQL gemaakt op basis van het aantal dagen dat een cliënt in zorg is. Uit de praktijktoets komt dat er wellicht nog andere benaderingen voor de verdeelsleutel Financieringsstroom mogelijk zijn bijvoorbeeld op basis van declaraties of op basis van leveringsvormen. Deze benaderingen zullen in de praktijktoets verder worden onderzocht.

**5. Indicatoren 2.1 en 2.2: fte omgezet naar uren**

   Indicatoren 2.1 Ingezette fte en 2.2 Verloonde fte zijn veranderd naar “Ingezette uren” en “Verloonde uren”. We voorkomen hiermee verschillen in gebruik van fte-definitie (36, 38, 40 uur) of zelfs JUS-definitie. Daarbij: “Ingezette” uren is hetzelfde als “Gewerkte” uren, deze laatste term wordt gebruikt in het kennismodel.

**6. Indicator 4.1 Gemiddelde contractomvang**

   De definitie van contractomvang is als volgt uitgewerkt: Contractomvang wordt uitgevraagd als contractuele uren per kwartaal. We hebben eerder als uitgangspunt gedefinieerd dat om uren/week om te rekenen naar uren per jaar we uitgaan van 47 werkbare weken, dus 47 * aantal uur per week. Per kwartaal betekent dit: een kwart van de uren per jaar dus 47 * aantal uur per week gedeeld door het getal 4.

**7. Indicatoren 13.3-13.4 Aantallen doorstroom personeel**

   De indicatoren zijn aangepast in “Percentage doorstroom personeel” omdat het hiermee een indicator is die veel makkelijker te vergelijken is met andere zorgaanbieders. Verder worden alleen de kwalificatieniveaus verzorgenden/ verpleegkundigen 1-6 worden hier geïncludeerd en niet de (para)medische kwalificatieniveaus.

**8. Indicatoren 11.1-11.4 Verzuim**

   De zorgkantoren sluiten zich aan bij de CBS definitie met kalenderdagen, inclusief parttime factor. De keuze voor kalenderdagen is gebaseerd op het feit dat de werkdagen in de zorg niet standaard maandag t/m vrijdag zijn. Door voor kalenderdagen te kiezen, hoeft geen rekening gehouden te worden met wanneer iemand 'zou moeten werken'. Anders zou er noodzaak zijn om naar de registratie van beoogde te werken uren te gaan kijken (planning). Deze gegevens zijn lastiger uit bronsystemen te halen.
   
   In de berekening wordt gecorrigeerd voor de parttime factor (conform Vernet) omdat in de zorg veel medewerkers parttime werken. Een berekening met een parttime factor levert een zuiverder berekening van capaciteitsverlies in geval van parttime medewerkers.
   
   De laatste dag van de periode (kwartaal) wordt als uitgangspunt gebruikt in de berekening. Oftewel, wanneer een persoon op de laatste dag nog ziek is en korter dan 28 dagen, dan telt deze persoon mee in de indicator 'kort verzuim'.

**9. Uitwisselprofiel, alinea “Gewenste momenten van terugkoppeling”**

   Deze tekst was in de vorige versie nog niet ingevuld.

**10. Uitwisselprofiel, alinea “Twijfel over kwaliteit van data”**

   De tekst in het Uitwisselprofiel over hoe partijen elkaar bereiken bij twijfel over kwaliteit van data is aangepast. Hiervoor is bij het onderdeel Algemene gegevens de regel “Contactpersoon zorgaanbieder” toegevoegd.


# Capaciteit

## Scope

De zorgkantoren breiden het Uitwisselprofiel Zorgkantoren Inkoopondersteuning en beleidsontwikkeling uit met een set capaciteitsgegevens om zicht te houden op de voortgang op de gemaakte afspraken tussen zorgkantoor en zorgaanbieder. De basis voor de ontwikkeling van deze set is de Regiomonitor verpleegzorg die tot op heden jaarlijks door elk zorgkantoor met een Excel-bestand wordt uitgevraagd bij gecontracteerde zorgaanbieders. 

De capaciteitsgegevens worden door zorgkantoren gebruikt om analyses op te doen, het gesprek aan te gaan met gecontracteerde zorgaanbieders om zo te bewaken dat Wlz-zorg nu en in de toekomst gegarandeerd kan worden. Het is in dat kader relevant om te zien waar opschalingsruimte of de maximaal mogelijke capaciteit bij zorgaanbieders zit en of het personeelsbestand eventuele opschaling toelaat. Tevens is het wenselijk om de landelijk afgesproken ontwikkeling naar gelijkblijvend of minder intramurale wooneenheden versus uitbreiding van het aantal “plekken” scheiden wonen en zorg (geclusterd VPT) te kunnen volgen. Tenslotte is het monitoren van de cliënten met een diagnose in een Laag Volume/Hoog complexe (LV/HC) doelgroep relevant, aangezien de ontwikkeling naar Regionale Expertisecentra landelijk is ingezet. In de set is een informatievraag opgenomen over cliënten met het syndroom van Korsakov. Deze vraag is als eerste laag volume/ hoogcomplexe (LV/HC)-doelgroep opgenomen, aangezien de structuur van Regionale Expertisecentra/ Doelgroep Expertisecentra voor het syndroom van Korsakov als eerste is geïmplementeerd in Nederland. De werkgroep wil in de toekomst uitbreiden met informatievragen over de overige LV/HC-doelgroepen.

## Buiten scope

In de ZN landelijke monitor verpleegzorg versie 2022 worden wachtlijsten gepubliceerd. De werkgroep heeft besloten deze nu niet over te nemen in het uitwisselprofiel, aangezien enerzijds deze cijfers via iWlz op dagbasis beschikbaar zijn voor zorgkantoren en anderzijds er veel context en toelichting bij deze cijfers nodig is.

Daarnaast hebben zorgkantoren aangegeven de informatie uiteindelijk ook op gemeenteniveau (de gemeente wordt bepaald op basis van de postcode van de vestiging) en zorgkantoorregio (de zorgkantoorregio wordt bepaald op basis van de postcode van de vestiging) te willen ontvangen. In de toekomst worden de informatievragen uitgebreid met deze niveaus.

Soms zijn er verpleeghuizen waar veel capaciteit beschikbaar is, maar is de conditie van de panden of de kwaliteit niet goed. Hierdoor willen mensen er niet wonen. Dit kan ook een rol spelen in het bepalen van de capaciteit. Echter, dit is veelal kennis van zorginkopers. Daarnaast is het moeilijk om tot een goede definitie van de staat van de gebouwen te komen. Besloten wordt om in het uitwisselprofiel geen vragen m.b.t. renovatieplannen op te nemen.

De zorgkantoren vinden het onwenselijk om normstellend te zijn naar zorgaanbieders in het aantal medewerkers dat voor een aantal cliënten met leveringsvorm Modulair Pakket Thuis (MPT) wordt ingezet. Het aantal cliënten dat via een MPT leveringsvorm geholpen kan worden is niet alleen afhankelijk van het aantal beschikbare uren, maar ook van de zwaarte van de (wisselende) cliënten. Om deze reden is een vraag over het aantal uren zorgpersoneel dat nodig is om de maximale zorg (o.b.v. het maximaal aantal plekken) niet opgenomen.

## Periode

De wens van de zorgkantoren is qua capaciteitsgegevens om eveneens uit te gaan van cijfers op kwartaalbasis. Uitzondering zijn indicatoren 17.1 Wat is de geplande toe- of afname van het aantal wooneenheden? en 17.2 Wat is de geplande toe- en afname van het aantal cliënten per (clustering van) leveringsvorm(en)? Deze gegevens wijzigen te weinig frequent waardoor gekozen is om deze indicator uit te vragen op jaarbasis (dit is in lijn met de huidige uitvraagfrequentie van de Regiomonitor verpleegzorg). 

## Proces

De werkgroep Uitwisselprofiel Zorgkantoren capaciteit heeft in 5 bijeenkomsten (18 januari, 15 februari, 5 april, 26 april en 17 mei 2023) de informatiebehoefte van zorgkantoren op het gebied van capaciteit geanalyseerd, gewijzigd en/of aangevuld. Tijdens de bijeenkomsten zijn overwegingen en mogelijke variaties in opzet en berekening van de indicator besproken. In de periode tussen de bijeenkomsten (3-4 weken) hebben de afgevaardigden vanuit de 6 zorgkantoren met collega’s de richting die voorligt besproken om te kijken of deze past bij hoe de zorgkantoren deze gegevens gebruiken bij analyses en bij gesprekken met zorgaanbieders.

## Gemaakte keuzes t.a.v. indicatoren en concepten

| Informatievraag                                                  | Status              | Toelichting                                                                                               |
|-----------------------------------------------------------------|---------------------|------------------------------------------------------------------------------------------------------------|
| 2.1 Wat is het aantal ingezette uren?                | Gewijzigde vraag    | In de werkgroep is gesproken over het opnemen van een informatievraag over MPT in het uitwisselprofiel. MPT is niet te kwantificeren in capaciteit of bezetting, om deze reden is gekozen om geen aparte indicator MPT op te nemen maar te kiezen voor een uitbreiding voor indicator 2.1 met een verdeling voor leveringsvorm (= indicator 15.4.3.). Met deze verdeling kan een inschatting gemaakt worden het aantal beschikbare uren zorgpersoneel voor MPT-zorg en het aantal uren zorgpersoneel dat niet wordt ingevuld (onderbezetting). |
| 14.2 Wat is het aantal cliënten per leveringsvorm op een peildatum?          | Gewijzigde vraag    | Er is behoefte aan inzicht in de verhouding tussen het aantal intramurale cliënten en het aantal geclusterd Volledig Pakket Thuis (VPT) cliënten, zodat de landelijk ingezette beweging van minder intramuraal naar meer extramuraal gevolgd kan worden. In het kader van efficiëntie is gekozen om dit verhoudingsgetal toe te voegen aan de al bestaande indicator 14.2.  |
| 15.1 Wat is het aantal wooneenheden?                            | Nieuwe vraag    | Bij deze informatievraag wordt gevraagd naar het maximaal aantal wooneenheden, ongeacht wat er wel of niet bezet is. |
| 15.2 Wat is het aantal bezette wooneenheden?                           | Nieuwe vraag    | Bij deze informatievraag wordt gevraagd naar het aantal bezette wooneenheden. Hierbij wordt incidentele (bijv. personeelstekort) en structurele redenen (bijv. sluiting van een afdeling) meegewogen. |
| 15.3 Wat is het aantal personen dat per vestiging kan wonen? | Nieuwe vraag    | Bij deze informatievraag wordt gevraagd naar het maximaal aantal personen dat op een vestiging kan wonen, ongeacht wat er wel of niet bezet is en ook ongeacht of er sprake is van Wlz-zorg. |
| 15.4.1 Wat is het aantal bewoners per financieringsstroom?       | Nieuwe vraag        | Een indeling van het aantal bewoners per wet, inclusief een categorie “Overige bewoners” geeft de zorgkantoren een indicatie van de Wlz-capaciteit ten opzichte van de totale capaciteit die beschikbaar is op een vestiging. |
| 15.4.2 Wat is het aantal cliënten met een Wlz-indicatie per sector?                  | Nieuwe vraag        | Vanuit de Wlz-indicatie per sector bepalen de zorgkantoren zelf hoeveel cliënten psychogeriatrische zorg of somatische zorg krijgen. |                                                                                                           
|15.4.3 Wat is het aantal cliënten met een Wlz-indicatie met zorgprofiel VV per leveringsvorm? | Nieuwe vraag | De indeling in leveringsvorm geeft de zorgkantoren een indicatie van de verdeling van de soort zorg die op of vanuit de vestiging wordt geleverd. |
| 16.1 Wat is het aantal cliënten met het syndroom van Korsakov?| Nieuwe vraag |  Met deze informatievraag krijgen zorgkantoren inzicht in de verspreiding van cliënten met het syndroom van Korsakov over de verschillende vestigingen/organisaties ten opzichte van de landelijk ingerichte Regionale Expertisecentra/ Doelgroep Expertisecentra voor cliënten met het syndroom van Korsakov.Achtergrondinformatie: Expertisecentra (korsakovkenniscentrum.nl) |                                                                                                            
| 17.1 Wat is de geplande toe- of afname van het aantal wooneenheden? | Nieuwe vraag | In tegenstelling tot de overige vragen in de capaciteitsvragen vraagt deze vraag naar de toekomstig beschikbare capaciteit. Het is lastig om het antwoord op deze vraag uit een  bronsysteem bij de zorgaanbieder te halen, aangezien het veelal beleidsinformatie betreft. Deze vraag valt hiermee onder de categorie “Aanvullende gegevens” en zal aanvullend geregistreerd moeten gaan worden bij zorgaanbieders.  |                                                                                                    
| 17.2 Wat is de geplande toe- en afname van het aantal cliënten per (clustering van) leveringsvorm(en)? | Nieuwe vraag     |  In tegenstelling tot de overige vragen in de capaciteitsvragen vraagt deze vraag naar de toekomstig beschikbare capaciteit. Het is lastig om het antwoord op deze vraag uit een  bronsysteem bij de zorgaanbieder te halen, aangezien het veelal beleidsinformatie betreft. Deze vraag valt hiermee onder de categorie "Aanvullende gegevens" en zal aanvullend geregistreerd moeten gaan worden bij zorgaanbieders.   |                                                                                                  

| Concept                            | Informatievraag                                   | Bestaande/ gewijzigde/ nieuw concept in de ontologie | Toelichting                                                                                                                  |
| ---------------------------------- | ------------------------------------------------- | ---------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| **Capaciteit**                | Informatievragen 15 en 16                             | Bestaand concept                                       | Capaciteit: “de grootte van iets waar iets anders in kan”; De capaciteit van een vestiging en van de organisatie wordt uitgedrukt in het aantal wooneenheden of personen dat in een vestiging (en bij de organisatie als geheel) van een zorgaanbieder kan wonen;  De capaciteit uitgedrukt in personen wordt bepaald op basis van het aantal personen dat in elke wooneenheid kan wonen; Capaciteit en bezetting kunnen niet gecombineerd worden in een indicator.  |
| **Bezetting** | Informatievragen 15 en 16                             | Bestaand concept  | De bezetting betreft het aantal wooneenheden dat bewoond is door personen, zoals bijvoorbeeld cliënten of partners maar ook andere, niet Wlz-gefinancierde, personen; De bezetting van een vestiging kan worden uitgedrukt in “aantallen wooneenheden waarin cliënten op data moment verblijven” of “aantallen cliënten met specifieke kenmerken”, zoals indicatie, leveringsvorm of PG/ somatiek (o.b.v. bijvoorbeeld het zorgprofiel); • Capaciteit en bezetting kunnen niet gecombineerd worden in een indicator.  |
| **Wooneenheid**        | Alle informatievragen                             | Bestaand concept             | Het betreft wooneenheden die geschikt zijn voor bewoning door een of meerdere cliënten en/of een partner (of personen partners zijn, is onbekend); Een wooneenheid heeft geen specifieke eigenschappen: er kan niet worden aangegeven of een wooneenheid voor een bepaalde doelgroep geschikt is; Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een vestiging met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die vestiging wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’-eenheid’. |
| **Persoon** | Informatievraag 15.3                             | Bestaand concept   | Een persoon is een concept en iets dat je altijd bent. Je kunt niet niet een persoon zijn. |
| **Bewoner** | Informatievraag 15.3                             | Bestaand concept   | Een bewoner is een rol die je krijgt in een bepaalde context. De rol bewoner krijg je in de context van dat je in een verpleeghuis woont. |
|**Cliënt**   | Informatievragen 15.4.1, 15.4.2 en 16.1          | Bestaand concept   | Een cliënt is een rol die je krijgt in een bepaalde context, bijvoorbeeld op basis van een indicatie. |
| **VPT-cluster** | Informatievraag 17.1      | Bestaand concept | Volledig Pakket Thuis geclusterd omvat de zorgverlening in geclusterde woonvormen of kleinschalige wooninitiatieven van 3 of meer personen met een VPT op één verblijfplaats. Hierbij levert één Wlz-zorgaanbieder de ‘zorgcapaciteit’ maar niet het wonen. Wonen en zorg staan dus los van elkaar (bron: ZN Regiomonitor, 2022). NB. Het landelijk beleid en de afspraken over het vraagstuk overheveling zorg thuis is nog in ontwikkeling, De gehanteerde definitie voor geclusterd VPT in dit uitwisselprofiel betreft een werkdefinitie die wordt aangepast als nadere invulling van het beleid dit nodig maakt. |                                                                                                              
# Financiën

## Scope

De zorgkantoren breiden het Uitwisselprofiel Zorgkantoren Inkoopondersteuning en beleidsontwikkeling uit met een set financiële gegevens om zicht te houden op de voortgang op de gemaakte afspraken tussen zorgkantoor en zorgaanbieder. De financiële informatiebehoefte van de zorgkantoren bestaat uit de vragen die zorgkantoren stellen om te zicht krijgen op de financiële gezondheid c.q. het risico op discontinuïteit van zorg van een zorgaanbieder, zoals nu in de Jaarverantwoording Zorg beschikbaar zijn. De zorgkantoren stellen op basis van deze openbaar beschikbare data enkele zelf berekende financiële indicatoren op. Ook deze indicatoren zijn onderdeel van de financiële informatiebehoefte.
De financiële gegevens worden door zorgkantoren gebruikt om analyses op te doen, het gesprek aan te gaan met gecontracteerde zorgaanbieders om zo te bewaken dat Wlz-zorg nu en in de toekomst gegarandeerd kan worden. Het is in dat kader relevant om te zien hoe financieel gezond zorgaanbieders zijn en waar eventuele financiële risico’s ontstaan. De zorgkantoren hebben immers de zorgplicht voor hun cliënten.
De uitvraag betreft operationele financiële cijfers wat betekent dat deze cijfers niet van een accountantsverklaring zijn voorzien. De zorgkantoren zullen naast de operationele financiële cijfers uit dit uitwisselprofiel eveneens gebruik blijven maken van de Jaarverantwoording Zorg, aangezien deze cijfers wel van een accountantsverklaring zijn voorzien.

## Buiten scope

De normering die zorgkantoren gebruiken bij de beoordeling van de financiële gezondheid c.q. het risico op discontinuïteit van zorg van een zorgaanbieder is per zorgkantoor bepaald en is daarmee niet in scope. Andere financiële processen bij de zorgkantoren, zoals Bijzonder Beheer of Hardheidsclausule zijn eveneens niet in scope, aangezien de informatiebehoeften in deze processen te specifiek per zorgaanbieder zijn.

## Periode

De zorgkantoren vragen de financiële gegevens per half jaar uit: in februari de voorlopige jaarcijfers van het voorgaande jaar, in augustus de voorlopige halfjaarcijfers van het lopende jaar.

## Proces

De werkgroep “Financiën”, met vertegenwoordiging van de gezamenlijke zorgkantoren door deelname van vijf financieel specialisten van zorgkantoren Zilveren Kruis, CZ, VGZ en DSW, heeft in 8 bijeenkomsten (5 september 2023, 26 september 2023, 14 november 2023, 5 december 2023, 9 januari 2024, 6 februari 2024, 5 maart 2024 en 9 april 2024) de financiële informatiebehoeften van de zorgkantoren geanalyseerd.
De overige 3 zorgkantoren Zorg & Zekerheid, Menzis en Salland hebben met alle verslagen en aanvullende werkdocumenten van de werkgroep meegelezen en indien nodig contact gezocht met de werkgroep, zodat ook hun financiële informatiebehoeften goed opgenomen zijn in het uitwisselprofiel.
De analyse van de financiële informatiebehoefte is uitgevoerd aan de hand van alle EWS-systemen van de 7 zorgkantoren. Deze EWS-systemen zijn naast elkaar gezet en overeenkomsten en verschillen zijn besproken. Waar mogelijk zijn indicatoren samengevoegd, gewijzigd en/of aangevuld. Het doel was om te zorgen dat alle EWS-systemen volledig gedekt zijn, zodat de EWS-scoringsmethodiek niet onderbroken wordt.
Tijdens de bijeenkomsten zijn overwegingen en mogelijke variaties in opzet en berekening van de indicatoren besproken. In de periode tussen de bijeenkomsten (3-4 weken) hebben de afgevaardigden vanuit de 4 zorgkantoren met collega’s de richting die voorlag, besproken om te kijken of deze past bij hoe de zorgkantoren deze gegevens gebruiken bij analyses en bij gesprekken met zorgaanbieders. Enkele benodigde indicatoren bleken al in de eerdere versie van het uitwisselprofiel opgenomen en konden dus vervallen in de financiële indicatorenset. Enkele huidige indicatoren zijn (nu) niet opgenomen in het uitwisselprofiel, zoals:
•	Wat is de inhoud van de accountantsverklaring? Het betreft een pdf document dat lastig te ontsluiten is via KIK-V methodiek. Daarbij is de scope nu de operationele financiële cijfers, hiervoor wordt normaliter geen accountantsverklaring afgegeven.
•	Wat is de verhouding tussen PNIL-kosten en ziekteverzuim? De indicator zet twee totaal verschillende cijfers ten opzichte van elkaar, het betreffende zorgkantoor heroverweegt deze indicator alsnog voor een volgende versie van het uitwisselprofiel
De zorgkantoren willen na een jaar gebruik gezamenlijk evalueren of de financiële indicatoren voldoende of onvoldoende beeld geven van de financiële situatie bij een zorgaanbieder en welke aanpassingen aan het uitwisselprofiel dan nodig zijn. De indicatoren die in deze versie niet zijn opgenomen, kunnen in deze discussie alsnog in heroverweging genomen worden.

## Gemaakte keuzes t.a.v. indicatoren en concepten


| Informatievraag                                                  | Status              | Toelichting                                                                                               |
|-----------------------------------------------------------------|---------------------|------------------------------------------------------------------------------------------------------------|
| 18.1 Wat is de balans?                | Bestaande vraag    | De balans wordt opgebouwd uit grootboekrubrieken. |
| 18.2 Wat is winst- en verliesrekening?                | Bestaande vraag    | De winst- en verliesrekening wordt opgebouwd uit grootboekrubrieken. |
| 18.3 Wat is het kasstroomoverzicht?                | Bestaande vraag    |Het kasstroomoverzicht wordt opgebouwd uit grootboekrubrieken. |
| Vragen 19.1 t/m 24.9                | Nieuwe vragen    | Deze vragen worden allemaal afgeleid met de uitkomsten van de indicatoren 18.1, 18.2, 18.3 én de uitkomsten van indicatoren in versies 1.0.0. en 1.1.0. van dit uitwisselprofiel. Er is geen extra uitvraag aan zorgaanbieders voor nodig. |

| Concept     | Informatievraag | Bestaande/ gewijzigde/ nieuw concept in de ontologie | Toelichting |
|------------|----------------|------------------------------------------------------|-------------|
| **Financiën** | Informatievragen xxx | xxx concept | NTB |



# Toetsing

Privacy Company heeft een toets uitgevoerd om inzicht te krijgen in potentiële privacyrisico’s. Er is o.a. gekeken naar de grondslag van zorgkantoren om gegevens uit te vragen, er is een analyse gedaan op de gestelde vragen en er is gekeken naar de proportionaliteit van de gestelde vragen. Ook is hierbij rekening gehouden met eventuele herleidbaarheid naar individuele personen. 

Uit de privacytoets wordt geconcludeerd dat er geen nadere risico’s of aandachtspunten zijn die de uitrol van dit uitwisselprofiel in de weg staan. Zorgkantoren hebben een wettelijke grondslag waarop de uitvraag gebaseerd is en de gestelde vragen voldoen aan het proportionaliteitsvereiste. 

In het kader van herleidbaarheid blijft het toepassen van het algemene uitgangspunt om niet te rapporteren over groepen personen waarbij N<10 als randvoorwaarde gelden. De Privacy Company heeft geadviseerd om per vraag expliciet op te nemen waar dit uitgangspunt van toepassing is. Naar aanleiding van dit advies is een inventarisatie gedaan van de vragen waar dit betrekking op heeft en hier zal het uitgangspunt expliciet worden vermeld.