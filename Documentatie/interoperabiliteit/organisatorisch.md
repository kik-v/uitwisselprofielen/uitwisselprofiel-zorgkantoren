---
title: Organisatorische interoperabiliteit
weight: 2
---
## Aanlevering

De resultaten dienen per peildatum, per kwartaal, per half jaar, of per jaar beschikbaar te zijn. Afhankelijk van de interne processen bij de zorgaanbieders zijn deze minimaal 1 werkdag, maximaal 1 maand na de gevraagde meetperiode (peildatum, kwartaal, half jaar, jaar) beschikbaar. Per vraag is vastgelegd welke peildatum of meetperiode wordt gehanteerd: zie Semantische laag voor verdere details. Wat betreft de financiële gegevens wordt gevraagd naar de voorlopige financiële cijfers.

Het totale uitvraagschema voor de zorgkantoren voor de aandachtsgebieden personeel, cliënten, capaciteit en financiën per jaar ziet er als volgt uit:

- Januari: indicator 1 t/m 16 (personeel, cliënten, capaciteit over 4e kwartaal voorgaand jaar)
- Februari: indicator 18 t/m 24 (voorlopige financiële indicatoren over voorgaand jaar)
- April: indicator 1 t/m 16 (personeel, cliënten, capaciteit over 1e kwartaal lopend jaar)
- Juli: indicator 1 t/m 16 (personeel, cliënten, capaciteit over 2e kwartaal lopend jaar)
- Augustus: indicator 18 t/m 24 (voorlopige financiële indicatoren over 1e half jaar lopend jaar)
- Oktober: indicator 1 t/m 16 (personeel, cliënten, capaciteit over 3e kwartaal lopend jaar) en: indicator 17 (capaciteit over afgelopen 1,5 jaar t/m 1 juli lopend jaar)

De zorgkantoren hebben eerder een gezamenlijke set indicatoren op het gebied van cliënten, personeel en capaciteit ontwikkeld. Elk zorgkantoor hanteert daarnaast een eigen manier van vroegsignalering op basis van de door de zorgaanbieder aan te leveren financiële indicatoren. Deze vroegsignalering wordt opgevolgd door incidentele aanvullende informatie indien de financiële cijfers hiertoe aanleiding geven. De benodigde indicatoren voor alle zorgkantoren zijn samengevoegd in het uitwisselprofiel, met als belangrijke reden dat zorgaanbieders een uniforme financiële indicatoren set hebben die gelijk is voor alle zorgkantoren. De zorgkantoren stellen zich voor het komende jaar ten doel om gezamenlijk te evalueren welke indicatoren daadwerkelijk gebruikt zijn en welke eventuele opschoning mogelijk is.


## Doorlevering

Zorgkantoren ontvangen als concessiehouder de antwoorden van gecontracteerde zorgaanbieders op de informatievragen. Specifiek voor de capaciteitsgegevens geldt dat de geanonimiseerde antwoorden van deze uitvraag gecombineerd met wachtlijstgegevens ten behoeve van de ZN Landelijke monitor verpleegzorg worden doorgeleverd aan ZN.

## Momenten van terugkoppeling

De zorgkantoren zullen in gesprek met de zorgaanbieders invullen geven aan de terug te koppelen informatie voor de zorgaanbieders, bijvoorbeeld als spiegelinformatie.

## Looptijd

De looptijd van het uitwisselprofiel is continu doorlopend tot het moment van wijziging.

## Bewaartermijn

De zorgkantoren zullen zelf de antwoorden op vestigings-, organisatie-, gemeentelijk en regionaal niveau voor maximaal 3 jaar bewaren ten behoeve van trendanalyses. Zorgaanbieders hoeven, naast de reeds bij hen bestaande processen en aanpak van dataopslag van gegevens, in verband met de gegevensuitwisseling die volgt uit dit uitwisselprofiel geen aanvullende opslagmaatregelen te nemen.

## Afspraken bij twijfels over de kwaliteit van gegevens

Als een zorgkantoor twijfels heeft over de interpretatie van de gegevens en eventuele twijfel over de kwaliteit van gegevens, wordt tijdens de reguliere gesprekken tussen zorginkoper/ kwaliteitsadviseur van het zorgkantoor en de betreffende zorgaanbieder de juiste context en toelichting bij de cijfers besproken. 

## Afspraken over een eventuele mogelijkheid tot nalevering en correctie

De gegevens kunnen door de zorgaanbieder worden aangepast en het volgend kwartaal verbeterd worden klaargezet voor het zorgkantoor.

## In- en exclusiecriteria

De uitvraag is bedoeld voor organisaties die onder onderstaande inclusiecriteria vallen:

1. Doelgroep

- Tot de doelgroep behoren alle instellingen met een Wlz contract

- Tot de doelgroep behoren alle instellingen binnen een zorgkantoorregio afbakening (per concessiehouder)

- Tot de doelgroep behoren alle instellingen in de verpleeghuissector

2. Profiel (inclusiecriteria)

- De profielen conform [Wet langdurige zorg](https://wetten.overheid.nl/BWBR0036014/2021-05-12#BijlageA) worden geïncludeerd.
