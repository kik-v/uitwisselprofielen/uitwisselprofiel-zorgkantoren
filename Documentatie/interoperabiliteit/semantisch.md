---
title: Semantische interoperabiliteit
weight: 3
---
## Algemeen

| Gegeven | Doel en achtergrond |
| --- | --- |
| AGB code organisatie | Via de AGB code kunnen zorgkantoren declaraties koppelen aan het opgegeven NZA nummer en zo de productieaantallen controleren. |
| NZa-nummer, KvK nummer organisatie, KvK vestigingsnummers, Naam organisatie, Naam vestigingen | Ter controle juiste zorgaanbieder |
| Contactpersoon zorgaanbieder | Indien er bij het zorgkantoor vragen zijn over hetgeen is aangeleverd, kan het zorgkantoor contact opnemen met dit contactpersoon. |

## Personeel

| **Vraag**                                                                                            |
|------------------------------------------------------------------------------------------------------|
| 1.1 Wat is het gemiddeld aantal personeelsleden?                                                     |
| 1.2 Wat is het aantal personeelsleden op een peildatum?                                              |
| 2.1 Wat is het aantal ingezette uren?                                                                |
| 2.2 Wat is het aantal verloonde uren?                                                                |
| 3.1 Wat is het percentage personeel met een arbeidsovereenkomst voor bepaalde tijd op een peildatum? |
| 4.1 Wat is de gemiddelde contractomvang van het personeel?                                           |
| 5.1 Wat is de leeftijdsopbouw van het personeel?                                                     |
| 6.1 Wat is het percentage ingezette uren personeel per kwalificatieniveau?                           |
| 7.1 Wat is het aantal ingezette uren personeel ingezet per cliënt?                                   |
| 8.1 Wat is het percentage ingezette uren personeel niet in loondienst (PNIL)?                        |
| 8.2 Wat is het percentage kosten personeel niet in loondienst (PNIL)?                                |
| 9.1 Wat is het aantal vrijwilligers?                                                                 |
| 10.1 Wat is het aantal leerlingen?                                                                   |
| 11.1 Wat is het kortdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)?                      |
| 11.2 Wat is het kortdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)?                      |
| 11.3 Wat is het langdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)?                      |
| 11.4 Wat is het langdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)?                      |
| 12.1 Wat is de verzuimfrequentie (excl. zwangerschapsverlof)?                                        |
| 12.2 Wat is de verzuimfrequentie (incl. zwangerschapsverlof)?                                        |
| 13.1 Wat is het percentage instroom personeel?                                                       |
| 13.2 Wat is het percentage uitstroom personeel?                                                      |
| 13.3 Wat is het percentage doorstroom personeel naar oplopend kwalificatieniveau?                    |
| 13.4 Wat is het percentage doorstroom personeel naar aflopend kwalificatieniveau?                    |

**Doel en achtergrond**

1. Zorgkantoren willen inzicht in o.a. personeelsopbouw, verloop en ziekteverzuim, omdat de ontwikkelingen op de arbeidsmarkt van groot belang zijn.
2. Voor zorgkantoren is het van belang om te weten of ze het leveren van zorg kunnen blijven garanderen of dat er actie moet worden ondernomen.
3. Zorgkantoren willen zorgaanbieders met elkaar kunnen vergelijken in een benchmark. Op die manier kunnen zij minder presenterende zorgaanbieders koppelen aan beter presterende zorgaanbieders, zodat zij van elkaar kunnen leren.

## Cliënten

| Vraag |
| --- |
| 14.1 Wat is het aantal cliënten per zorgprofiel? |
| 14.2 Wat is het aantal cliënten per leveringsvorm? |
| 14.3 Wat is het aantal cliënten per zorgprofiel per leveringsvorm? |

**Doel en achtergrond**

1. Deze indicatoren schetsen iets over het profiel van de cliënten die er wonen, hoe deze zorg gefinancierd wordt en hoe zwaar de zorg is (want de indicatie geeft de zorgzwaarte aan).
2. Met en zonder behandeling laat zien hoe de zorg georganiseerd is en waar verantwoordelijkheden liggen (de huisarts of specialist ouderengeneeskunde). Hierin worden vaak grote kwaliteitsverschillen gemerkt en daarom is dit een belangrijk aandachtspunt.

## Capaciteit

| Vraag |
| --- |
| 15.1 Wat is het aantal wooneenheden? |
| 15.2 Wat is het aantal bezette wooneenheden? |
| 15.3 Wat is het aantal personen dat per vestiging kan wonen? |
| 16.1 Wat is het aantal cliënten met syndroom van Korsakov? |
| 17.1 Wat is de geplande toe- of afname van het aantal wooneenheden? |
| 17.2 Wat is de geplande toe- of afname van het aantal cliënten per (clustering) van leveringsvorm(en)? |

**Doel en achtergrond**

1. De informatievragen over capaciteit geven een overall beeld van de Wlz-capaciteit bij zorgaanbieders en geven een specifieker beeld van de intramurale capaciteit (in wooneenheden of in bewoners) ten opzichte van de ontwikkeling van geclusterd VPT per vestiging/ organisatie. Voor zorgkantoren is het van belang om zicht te hebben op de beschikbare capaciteit voor cliënten met een Wlz-indicatie, maar ook zicht te hebben op hoe de overige capaciteit bij een zorgaanbieder wordt ingezet. Het is de taak van een zorginkoper om in de gaten te houden dat er voor nu en naar de toekomst toe voldoende Wlz-zorg beschikbaar is.
2. De indicatoren onder hoofdstuk 15 en 16 worden per kwartaal uitgevraagd, de indicatoren onder hoofdstuk 17 wordt per jaar uitgevraagd.

## Basisindelingen

| Vraag |
| --- |
| 15.4.1 Wat is het aantal bewoners per wet? |
| 15.4.2 Wat is het aantal cliënten per Wlz-indicatie per sector? |
| 15.4.3 Wat is het aantal cliënten per Wlz-VV leveringsvorm? |
| 15.4.4 Wat is het goedgekeurd gedeclareerd bedrag per wet? |
| 15.4.5 Wat is het goedgekeurd gedeclareerd bedrag per langdurige zorg sector? |
| 15.4.6 Wat is het goedgekeurd gedeclareerd bedrag per leveringsvorm in de sector VV? |

**Doel en achtergrond**

1. De indicatoren in het uitwisselprofiel worden door de zorgaanbieder per organisatie en per vestiging aangeleverd. De eenheden “organisatie” en “vestiging” geven nog te weinig beeld van welke zorg- en dienstverlening geleverd wordt. Hiertoe zijn een zestal basisindelingen opgenomen in het uitwisselprofiel. Zorgkantoren passen deze zelf naar behoefte achteraf toe op de overige indicatoren.
2. Zorgkantoren kunnen met deze indicatoren op basis van cliënten en/of op basis van gedeclareerde bedragen een aanvullend inzicht krijgen in onder welke wetten, welke Wlz zorgsector en/of welke Wlz VV-leveringsvormen de zorg geleverd wordt. 

## Financiën

## Algemeen

De financiële indicatoren geven de zorgkantoren een eerste signaal of indicatie van de financiële gezondheid van een zorgaanbieder. Zorgaanbieders worden, conform de vroegsignalering systematiek van een individueel zorgkantoor, rood, oranje of groen gescoord op de door hen gekozen indicatoren. Zorgkantoren beschouwen de uitkomsten van de indicatoren binnen het hoofdstuk ten opzichte van elkaar, maar ook de uitkomsten over alle hoofdstukken heen worden bekeken. Alle uitkomsten van de indicatoren samen geven een beeld van de financiële situatie van een zorgaanbieder. Dit signaal staat niet op zich, het is onderdeel van een bredere monitorings- en gesprekscyclus tussen zorgkantoor en zorgaanbieder. 

Zorgkantoren hebben een zorgplicht naar de Wlz-cliënten in de regio. Wanneer een zorgaanbieder zowel op korte als op lange termijn niet aan de financiële verplichtingen kan voldoen, is het mogelijk dat de continuïteit van zorg in gevaar komt. 
De indicatoren onder de hoofdstukken 18 tot en met 22 zijn voorzien van één doel en achtergrond-omschrijving. De indicatoren in de hoofdstukken 23 en 24 zijn te verschillend aard en zijn voorzien van verschillende doel- en achtergrondomschrijvingen. De definities van de financiële indicatoren zijn terug te vinden in de functionele beschrijvingen van de indicatorberekening. 

NB. Op elke plekken ontbreken tussenliggende nummers, dit is geen fout. Deze nummers zijn gerelateerd aan concept-vragen die uiteindelijk niet zijn opgenomen in het uitwisselprofiel.

| Vraag |
| --- |
| 18.1 Wat is de balans o.b.v. grootboek (Prismant)? |
| 18.2 Wat is de winst- en verliesrekening o.b.v. grootboek (Prismant)? |
| 18.3 Wat is het kasstroomoverzicht? |

**Doel en achtergrond**

Indicatoren 18.1, 18.2 en 18.3 betreffen de balans, winst- en verliesrekening en het kasstroomoverzicht in de structuur zoals ook gebruikt in de Jaarverantwoording Zorg. De cijfers voor de balans (18.1) en resultatenrekening (18.2) worden berekend op basis van het grootboek in het financieel-administratief systeem. Dit betreft de lopende financiële administratie, zonder het eventuele jaarwerk van een accountant. In bovenstaand uitvraagschema is zichtbaar welke periode(s) gebruikt worden om te komen tot afsluiting van de uitvraagde perioden. 

## Liquiditeit
| Vraag |
| --- |
| 19.1 Wat is de DSCR (Debt Service Coverage Ratio)? |
| 19.2 Wat is het werkkapitaal op korte termijn? |
| 19.3 Wat is de werkkapitaalratio? |
| 19.4 Wat is de quick ratio? |
| 19.5 Wat is de current ratio? |
| 19.6 Wat is de EBITDA? |
| 19.7 Wat is de EBITDA-ratio? |
| 19.8 Wat is de cashflow? |

**Doel en achtergrond**

Een indicator om vast te stellen of een zorgaanbieder op korte termijn aan de financiële verplichtingen kan voldoende is de liquiditeitspositie. Als de liquiditeitspositie onvoldoende is, kan dat een signaal zijn voor de zorgkantoren dat de continuïteit van de zorg in gevaar is. Het is dan van belang dat zorgkantoren zo nodig tijdig maatregelen nemen.

De liquiditeitsindicatoren worden berekend op basis van de financiële basisinformatie die beschikbaar komt met de antwoorden op indicatoren 18.1, 18.2 en 18.3.

## Solvabiliteit

| Vraag |
| --- |
| 20.1 Wat is de solvabiliteitsratio? |
| 20.2 Wat is het weerstandsvermogen? |
| 20.3 Wat is de debt ratio? |
| 20.4 Wat zijn de Reserves? |

**Doel en achtergrond**

Een indicator om de financiële gezondheid van een zorgaanbieder te monitoren is de solvabiliteit. Als de solvabiliteitspositie onvoldoende is, kan dat een signaal zijn voor de zorgkantoren dat de continuïteit van de zorg in gevaar is. Het is van belang dat zorgkantoren zo nodig tijdig maatregelen nemen.

De solvabiliteitsindicatoren worden berekend op basis van de financiële basisinformatie die beschikbaar komt met de antwoorden op indicatoren 18.1, 18.2 en 18.3.

## Rentabiliteit
| Vraag |
| --- |
| 21.1 Wat is de winstratio? |
| 21.2 Wat het Resultaat Eigen Vermogen (REV)? |

**Doel en achtergrond**

De rentabiliteitsindicatoren van een zorgaanbieder zijn is essentieel voor het beoordelen van haar financiële gezondheid. Hogere ratio’s duiden op een efficiënter gebruik van het eigen vermogen, wat kan wijzen op gezonde financiële positie.

De rentabiliteitsindicatoren worden berekend op basis van de financiële basisinformatie die beschikbaar komt met de antwoorden op indicatoren 18.1, 18.2 en 18.3.

## Groei
| Vraag |
| --- |
| 22.1 Wat is de omzetgroei? |

**Doel en achtergrond**

De omzet groei geeft inzicht in de groei en financiële stabiliteit van een zorgaanbieder. Consistente groeicijfers wijzen op financiële stabiliteit. In combinatie met andere financiële indicatoren bieden groeicijfers een goed beeld van de prestaties en de vooruitzichten van een onderneming.

De groei-indicator wordt berekend op basis van de financiële basisinformatie die beschikbaar komt met de antwoorden op indicatoren 18.1, 18.2 en 18.3.

## Kapitaal

De indicatoren op het gebied van kapitaal worden berekend op basis van de financiële basisinformatie die beschikbaar komt met de antwoorden op indicatoren 18.1, 18.2 en 18.3.
| Vraag |
| --- |
| 23.1 Wat is de Loan to Value (LTV)? |

**Doel en achtergrond**

De loan to value (LTV) geeft de verhouding weer tussen de leensom en de (markt)waarde van de activa, en geeft daarmee aan in hoeverre een zorgaanbieder actief is gefinancierd met langlopende schulden. De LTV is belangrijk omdat het inzicht geeft in de financiële gezondheid van een zorgaanbieder en zijn vermogen om schulden te beheren. Zorgkantoren gebruiken LTV-ratio’s om de hefboomwerking van een zorgaanbieder te beoordelen, wat van invloed is op de solvabiliteit en het risicoprofiel. Een hoge LTV ratio geeft aan dat een zorgaanbieder mogelijk zwaar gefinancierd is, wat het risico op financiële problemen kan verhogen. De LTV wordt gebruikt bij financiële rapporten omdat het zorgkantoren helpt structuur en blootstelling van een zorgaanbieder te begrijpen. Daarnaast kunnen veranderingen in LTV ratio in de loop van de tijd wijzen op potentiële financiële uitdagingen of kansen voor een zorgaanbieder.

| Vraag |
| --- |
| 23.2 Wat is de net-debt/ EBITDA verhouding? |

**Doel en achtergrond**

Net-debt en EBITDA zijn belangrijke maatstaven. Net-debt (of netto schuld) verwijst naar het verschil tussen de totale schulden van een zorgaanbieder en de liquide middelen en andere kortlopende activa die de zorgaanbieder bezit. EBITDA is de operationele winst van een zorgaanbieder vóór rente, belastingen en afschrijvingen. De EBITDA wordt gebruikt om de financiële gezondheid van een zorgaanbieder te beoordelen en de capaciteit van een zorgaanbieder om zijn schulden af te lossen te analyseren. Een lage net-debt/ EBITDA verhouding duidt doorgaans op een gezonde financiële situatie, terwijl een hoge verhouding op een grotere financiële last en mogelijk risico’s voor een zorgaanbieder duiden.

| Vraag |
| --- |
| 23.3 Wat is de CAPEX? |

**Doel en achtergrond**

CAPEX (ook wel Kapitaaluitgaven) verwijst naar de investeringen die een zorgaanbieder doet in vaste activa. De CAPEX is belangrijk omdat het de toekomstige groei en efficiëntie van een zorgaanbieder kan beïnvloeden. Door de CAPEX te analyseren kunnen zorgkantoren inzicht krijgen in hoe een zorgaanbieder zijn middelen gebruikt om waarde te creëren en te behouden. De CAPEX kan ook invloed hebben op de financiële prestaties van een zorgaanbieder, omdat het de kasstroom op korte termijn kan verminderen maar op lange termijn de operationele efficiëntie en winstgevendheid kan verbeteren.

## Overige financiële indicatoren

De overige financiële indicatoren worden berekend op basis van de financiële basisinformatie die beschikbaar komt met de antwoorden op indicatoren 18.1, 18.2 en 18.3 én met informatie die beschikbaar komt met de antwoorden uit personeelsindicatoren.

| Vraag |
| --- |
| 24.1 Wat is de verhouding dekking vaste activa en lang vermogen? |

**Doel en achtergrond**

De verhouding van de dekking van vaste activa en langlopend vermogen is een financiële ratio die aangeeft in hoeverre de vaste activa van een zorgaanbieder zijn gefinancierd door langlopend vermogen. Deze ratio is belangrijk omdat het inzicht geeft in de vermogensstructuur van een zorgaanbieder en de mate van financiële stabiliteit. Een hogere ratio duidt op een grotere mate van financiering van vaste activa door langlopend vermogen, wat kan wijzen op een hogere mate van financiële hefboomwerking en risico. Aan de andere kant kan een lagere ratio wijzen op een meer conservatieve financieringsstructuur met minder schuld. Deze indicator wordt gebruikt om de financiële gezondheid van een zorgaanbieder te beoordelen en om trends in de tijd te volgen.

| Vraag |
| --- |
| 24.2 Wat is de ICR (interest coverage ratio)? |

**Doel en achtergrond**

De ICR geeft aan hoeveel keer de operationele winst (EBIT) de rentelasten van een zorgaanbieder kan dekken. Deze indicator is belangrijk omdat het de capaciteit van een zorgaanbieder om zijn rentelasten te dragen en schulden af te lossen beoordeelt. Een hogere ICR geeft aan dat een zorgaanbieder meer operationele winst genereert ten opzichte van zijn rentelasten, wat een positief signaal over de financiële stabiliteit van een zorgaanbieder. Een lagere ICR kan duiden op een hoger risico op financiële moeilijkheden, omdat een zorgaanbieder mogelijk niet in staat is om zijn rentelasten te dekken met zijn winst.

| Vraag |
| --- |
| 24.3 Wat is de verhouding kort vreemd vermogen en lang vreemd vermogen? |

**Doel en achtergrond**

De verhouding tussen kort vreemd vermogen en lang vreemd vermogen is een financiële ratio die aangeeft hoeveel van het totale vreemd vermogen van een zorgaanbieder bestaat uit kortlopende verplichtingen in vergelijking met langlopende verplichtingen. Deze indicator is belangrijk omdat het de liquiditeit en solvabiliteit van een zorgaanbieder beïnvloedt. Een hogere verhouding van kort vreemd vermogen ten opzichte van lang vreemd vermogen kan duiden op een grotere behoefte aan kortlopende financiering, wat kan wijzen op een verhoogd  liquiditeiten risico. Aan de andere kant kan een lagere verhouding een stabielere financieringsstructuur aangeven met minder druk op korte termijn liquide middelen.

| Vraag |
| --- |
| 24.4 Wat is de verhouding vorderingen en totale opbrengsten? |

**Doel en achtergrond**

De verhouding tussen vorderingen en totale opbrengsten is een financiële ratio die aangeeft hoeveel van de totale opbrengst van een zorgaanbieder wordt gegenereerd uit vorderingen. Deze indicator is belangrijk omdat het kan wijzen op potentiële problemen met debiteuren beheer. Een hogere ratio duidt op een grotere afhankelijkheid van vorderingen voor de totale opbrengsten, wat kan wijzen op een hoger risico op oninbare vorderingen. Een lagere ratio kan daarentegen duiden op lager risico.

| Vraag |
| --- |
| 24.5 Wat is de arbeidsintensiteit PIL & PNIL? |

**Doel en achtergrond**

Arbeidsintensiteit PIL & PNIL is een ratio die de verhouding tussen de loonkosten van medewerker in loondienst en niet in loondienst en de totale omzet van een zorgaanbieder aangeeft. Deze ratio is belangrijk omdat het de arbeidskosten ten opzichte van de totale omzet van een zorgaanbieder weergeeft. Een hogere arbeidsintensiteit  kan wijzen op hogere kosten. Aan de andere kant kan een lagere arbeidsintensiteit wijzen op hogere winst. Deze indicator wordt gebruikt om trends in de arbeidskosten in de loop van tijd te identificeren.

| Vraag |
| --- |
| 24.7 Wat is arbeidsintensiteit PNIL? |

**Doel en achtergrond**

Arbeidsintensiteit PNIL is een ratio die de verhouding tussen de loonkosten van medewerkers niet in loondienst en de totale omzet van een zorgaanbieder aangeeft. De inhuur van extra personeel verwijst naar het aannemen van tijdelijke of extern personeel om te voldoen aan tijdelijke pieken in de werklast of om specifieke vaardigheden aan te vullen. Het bijhouden van de kosten van inhuur van extra personeel is belangrijk omdat het de flexibiliteit van personeel in een zorgaanbieder weerspiegelt. Hoge kosten voor het inhuren van extra personeel kan wijzen op inefficiënties in de personeel planning of tekortkomingen in de interne capaciteit.

| Vraag |
| --- |
| 24.9 Wat is de PNIL-ratio? |

**Doel en achtergrond**

De PNIL-ratio, zijnde de verhouding van de kosten voor personeel niet in loondienst ten opzichte van de totale loonkosten, is een financiële maatstaf die aangeeft hoeveel van het totale personeelsbestand bestaat uit externe of tijdelijk personeel in vergelijking met personeel in vaste dient. Deze ratio is belangrijk omdat het inzicht geeft in de mate van afhankelijkheid van extern personeel. Een hogere ratio kan duiden op  een grotere afhankelijkheid van extern personeel, wat kan leiden tot hogere personeelskosten. Aan de andere kant kan een lager ratio wijzen op een meer stabiele personeelsbestand met lagere risico's op kosten op de lange termijn.


## Definities informatievragen

## Algemene uitgangspunten

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel. 

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten:

[Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)


### Indicatoren zorgkantoren

De functionele beschrijving van de berekening per indicator staat hier: [Klik hier](/Gevalideerde_vragen_technisch/)

Voor alle indicatoren Personeel geldt dat deze door de zorgkantoren aanvullend ingedeeld worden per wet (Wlz, Zvw, Wmo, overig) én binnen de Wlz uitgesplitst in langdurige zorgsector (Wlz VV, GGZ-B, GGZ-W, LG, LVG, VG, ZGAUD en ZGVIS). Voor indicator 2.1 Ingezette uren personeel geldt dat deze aanvullend ingedeeld wordt per Wlz VV leveringsvorm (Verblijf, VPT, MPT, DTV, PGB). De zorgkantoren gebruiken de antwoorden op de indicatoren 15.4.1 tot en met 15.4.6 om deze indelingen te maken.

## Benodigde gegevenselementen

De concepten, eigenschappen en relaties die nodig zijn om de indicatoren zorgkantoren te beantwoorden staan hier: [klik hier](/Gevalideerde_vragen_technisch/Modelgegevensset).

## Aggregatieniveau

Het aggregatieniveau is voor alle indicatoren organisatie- en vestigingsniveau.

## Contextinformatie

In de reguliere gesprekscyclus tussen zorgkantoor en zorgaanbieder kan een toelichting worden gegeven op de uitkomsten in het geval van niet verwachte of afwijkende cijfers. Daarnaast kunnen ook verbeterplannen en kwaliteitsplannen en -verslagen voor contextinformatie worden gebruikt.
