---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag

De grondslag voor deze gegevensuitwisseling is te vinden in de Wet langdurige zorg (Wlz).

## Doel van gegevensverwerking

Zie [Juridisch kader](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/juridisch-kader) rij 4 van de tabel, Wlz artikel 4.2.2 (met name b, f en g)

Het doel van deze gegevensuitwisseling is om als zorgkantoor zicht te kunnen houden op de verschillende gemaakte afspraken met gecontracteerde zorgaanbieders. Met het volgen van het verloop, de beschikbaarheid van (juiste) personeel, het verzuim en of voldaan wordt aan afspraken die op het personele vlak met zorgaanbieders overeen gekomen zijn. Met het volgen van de toegang tot zorg, de potentiële leegstand bij zorgaanbieders en de geplande capaciteitsuitbreidingen houden de zorgkantoren zicht op de afspraken die op het gebied van capaciteit met zorgaanbieders zijn gemaakt. Met de financiële indicatoren op basis van de kerncijfers uit de jaarrekeningstructuur, de balans, de winst- en verliesrekening en het kasstroomoverzicht monitoren de zorgkantoren de financiële gezondheid van de zorgaanbieders. 

Bij eventuele verdere uitbreiding van het uitwisselprofiel kan verruiming van dit doel aan de orde zijn. De manier waarop de zorgkantoren de gemaakte afspraken volgen verschilt per zorgkantoor want deze is afhankelijk van het inkoopbeleid en strategisch belangrijke thema’s van het betreffende zorgkantoor.