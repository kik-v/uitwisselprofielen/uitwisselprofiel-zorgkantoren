---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.3.0-RC1 |
| Versie | 1.3.0-RC1 |
| Doel | Release 1.3.0-RC1 betreft een uitbreiding van de informatiebehoefte van zorgkantoren naar informatie over financiën in ondersteuning van het inkoopproces en ten behoeve van beleidsontwikkeling. |
| Doelgroep | Zorgkantoren (CZ, DSW, Salland, Menzis, VGZ, Zilveren Kruis, Zorg & Zekerheid); Zorgaanbieders verpleeghuiszorg; Zorgverzekeraars Nederland|
| Totstandkoming | De ontwikkeling van release 1.3.0-RC1 is uitgevoerd door het programma KIK-V in samenwerking met zorgkantoren DSW, VGZ, Zilveren Kruis en CZ, evenals Zorgverzekeraars Nederland. De uitwerking is tussentijds en aan het eind van de ontwikkeling getoetst bij en akkoord bevonden door de overige zorgkantoren Menzis, Salland en Zorg & Zekerheid. Release 1.3.0-RC1 wordt vastgesteld door de Ketenraad KIK-V. |
| Inwerkingtreding | 06-03-2025 |
| Operationeel toepassingsgebied | Het volgen van contractafspraken tussen zorgkantoor en zorgaanbieders het gebied van financiën |
| Status | Ter vaststelling door Ketenraad KIK-V |
| Functionele scope | Release 1.3.0-RC1 omvat de aanvulling financiële informatie ten behoeve van de implementatie |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |